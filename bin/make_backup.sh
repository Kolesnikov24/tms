#!bin/sh
cp /usr/local/tms/db.sqlite3 ~/db_backup/db.sqlite3
cd ~/db_backup/
zip db_backup_`date +%d_%m_%Y`.zip ~/db_backup/db.sqlite3
rm ~/db_backup/db.sqlite3
