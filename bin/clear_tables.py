import sys
import os
import django
from django.db.models import Q

# For use django app
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'src'))


os.environ['DJANGO_SETTINGS_MODULE'] = 'tms.settings'
django.setup()

import TrackManager.models as models


min_id = models.Client.objects.all().order_by('pk').first().pk
print('Min client_id is {}. Delete others'.format(min_id))

models.Client.objects.all().exclude(pk=min_id).delete()

print('Update Min client_id with fake data')

models.Client.objects.filter(pk=min_id).update(
    Name='Test client',
    ContractNumber='112',
    ContractDate='2019-01-01',
    INN='324234234',
    KPP='324234234',
    LegalAdress='Lenina 1',
    BankAccount='00000001',
    shortname='test client short name',
    email='test_client@example.com',
    agent='Test agent')


min_d_id = models.Driver.objects.all().order_by('pk').first().pk
print('Min driver_id is {}. Delete others'.format(min_d_id))

print('Update Min driver_id with fake data')
models.Driver.objects.filter(pk=min_d_id).update(FIO='Test Driver',PassportData='444 3333',
                                            Phone='+7 111 222 99 00',
                                            DrivenLesson='1234 5688'
                                            )


models.Driver.objects.filter(~Q(pk=min_d_id)).delete()
