## Deploy нового экземпляра приложения
 - Создаем на диске новую папку
```
mkdir -p /usr/local/apps/psib
```
 - Создаем в postgres новую БД и владельца
```
create database psib_db;
create user psib_app with encrypted password 'V_26z![9';
grant all privileges on database psib_db to psib_app;
```
 -  Вытягиваем из git последнюю версию  в текущую папку
```
git clone https://gitlab.com/razzor58/tms.git ./
```
- Проставляем переменные окружения в conf/env.cfg
```
vi ./conf/env.cfg
source <(sed -E -n 's/[^#]+/export &/ p' ./conf/env.cfg)
```

-  Накатываем все миграции
```
cd src
python manage.py migrate
```

- Формируем дампа приложения
```
# Формирование дампа из мастер-системы
# copy_app_dicts.sh
# перенос в папку приложения
# scp -P 4522 ./app_dump.json.gz admin@81.1.218.146:/usr/local/apps/psib/

```
- Заполняем справочники из дампа
```
python manage.py loaddata ../app_dump.json
```

- Тестовый запуск приложения
- [ ] Правим шаблон конфигурации и запускаем службу 
- [ ] Прописываем на nginx новый location