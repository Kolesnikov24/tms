#!/usr/bin/env bash

cd ../src && gulp
cd ../devops/tms && bash build.sh
cd ../nginx && bash build.sh
cd .. && docker-compose up -d