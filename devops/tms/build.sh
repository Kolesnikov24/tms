#!/usr/bin/env bash

cp -r ../../src/tms src
cp -r ../../src/TrackManager src
cp -r ../../src/*.py src
cp -r ../../src/requirements.txt .

docker build -t drsklif/tms-django:0.1 .
