import os
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from TrackManager.views import landing, landing_search

# handler404 = 'TrackManager.views.not_found'
# handler500 = 'TrackManager.views.internal_error'

APP_NAME = os.environ.get('APP_NAME', 'tms')

urlpatterns = [
                  url(r'^ctrack/', include('TrackManager.urls2')),
                  url(r'^' + APP_NAME + '/auth/', include('django.contrib.auth.urls')),
                  url(r'^' + APP_NAME + '/admin/', admin.site.urls),
                  url(r'^' + APP_NAME + '/', include('TrackManager.urls')),
                  url(r'^$', landing, name='landing'),
                  url(r'^search/$', landing_search, name='landing_search')
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
