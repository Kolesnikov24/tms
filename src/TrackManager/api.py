# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from copy import deepcopy
from datetime import datetime, timedelta, date

from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage
from django.core.serializers import serialize
from django.conf import settings
from django.forms.models import model_to_dict
from django.http import JsonResponse, HttpResponse, Http404
from django.db.models import Sum, Max
from django.utils import timezone

from .constants import LOGIN_URL, STATUS_CHOICES, get_operation, API_LOGIN_URL, VALIDATE_ERROR
from .tools import check_client, add_year, logger, get_filter_dict
from .tariffs import calc_order_rates
from .forms import OrderFormForCalc, OrderForm, OrderWorkFormDisp, OrderWorkForm
from .models import (Order, UserProfile, OrderAdditionalService, Salary, ClientBill, Driver,
                     PaymentsReport, OrderDocuments, Customers, OrderTemplate)


@login_required(login_url=API_LOGIN_URL)
def check_tariff(request):
    # Находим заявку по номеру
    number = request.POST.get('number')
    days_ago = datetime.today() - timedelta(days=120)

    orders_dataset = Order.objects.filter(Number=number, CreateDate__gt=days_ago).order_by('-CreateDate')
    if orders_dataset.count() > 0:
        origin_order = orders_dataset[0]
        # Считаем тарифы для полной копии объекта
        order, error = calc_order_rates(deepcopy(origin_order), request.user)
        if error is not None:
            JsonResponse(error, safe=False)

        # Для вывода в JSON
        x = model_to_dict(order)
        x['Operation'] = order.get_Operation_display()
        x['Client'] = str(order.Client)
        x['TerminalLoad'] = str(order.TerminalLoad)
        x['TerminalDeliver'] = str(order.TerminalDeliver)
    else:
        x = {"Number": 0}

    return JsonResponse(x, safe=False)


@login_required(login_url=API_LOGIN_URL)
def calc_tariff_form(request):
    form = OrderFormForCalc(request.POST)

    if not form.is_valid():
        return JsonResponse({'err': VALIDATE_ERROR, 'messages': form.errors})

    order = form.save(commit=False)
    x = calc_order_rates(order, request.user, check_tariff=True)
    ignore_fields = ('save_to_db', 'add_salary', 'add_price')
    x_filter = dict(filter(lambda i: i[0] not in ignore_fields, x.items()))
    return JsonResponse(x_filter, safe=False)


@login_required(login_url=API_LOGIN_URL)
def update_payments_report(request):
    # Параметры запроса
    request_date = request.POST.get('date_start')
    month = datetime.strptime(request_date, '%Y-%m-%d').strftime("%m")
    year = datetime.strptime(request_date, '%Y-%m-%d').year

    # Список формируется для всех не уволенных водителей--
    for driver in Driver.objects.all().exclude(Status='D'):

        # Находим все выполненные водителем заявки
        orders_filter = dict(Status=9, ExportDate__month=month, ExportDate__year=year, driver=driver)
        driver_orders = Order.objects.filter(**orders_filter)

        # Считаем  по заявка сумму ЗП
        salary_sum = driver_orders.aggregate(Sum('salary_total'))['salary_total__sum']

        # Для каждого водителеля только одна запись ведомости - вне зависимости от машин
        obj, created = PaymentsReport.objects.update_or_create(driver=driver, month=request_date)

        #  для новой записи просто ставим сумму
        if created:
            obj.salary = salary_sum or 0
            obj.summary = salary_sum or 0

        # Если запись уже есть и заполнена другие - нужно только пересчитать с общую сумму
        else:
            obj.summary -= obj.salary
            obj.salary = salary_sum or 0
            obj.summary += obj.salary

        # проставляем в ведомость машины, на которых ездил водитель (справочная информация в интерфейсе)
        driver_trucks = driver_orders.order_by('truck').distinct('truck')
        for order in driver_trucks:
            auto = order.truck
            obj.truck.add(auto)

        # Сохраняем запись
        obj.save()

    return JsonResponse({"resp": "ok"}, safe=False)


def save_order_attach(request, upload_name='file', order_id=None, just_doc=True):
    if upload_name in request.FILES:
        file_obj = request.FILES[upload_name]

        key = request.POST.get('pk') or order_id
        order = Order.objects.get(pk=key)

        # limit size
        if file_obj.size > 15 * 1024 * 1024:
            return JsonResponse({'err': 'Max size exceeded'}, safe=False)
        else:
            file_name = request.POST.get('filename') or file_obj._name

            # for app
            if 'upload-image' in file_name:
                template = 'foto' if just_doc else 'app_foto'
                file_name = '{0}_{1}.jpg'.format(template, order.pk)
                file_obj._name = file_name

            req_user = None if request.user.is_anonymous else request.user

            if req_user:
                is_client = True if check_client(request) == 1 else False
            else:
                is_client = False

            obj = OrderDocuments(order=order,
                                 name=file_name,
                                 file=file_obj,
                                 upload_by=req_user,
                                 is_client_doc=is_client)

            obj.save()

    return True


@login_required(login_url=API_LOGIN_URL)
def save_order_doc(request):
    order = save_order_attach(request)
    return JsonResponse({'id': order}, safe=False)


@login_required(login_url=API_LOGIN_URL)
def delete_order_doc(request):
    key = request.POST.get('pk', 0)
    order = Order.objects.get(pk=key)
    qs = OrderDocuments.objects.filter(order=order, file=request.POST['filename_to_delete'])
    qs.delete()
    return JsonResponse({'id': order.id}, safe=False)


def download_doc(request, path):
    file_path = os.path.join(settings.MEDIA_ROOT, path)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/octet-stream")
            response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_path)
            return response
    raise Http404


@login_required(login_url=LOGIN_URL)
def get_all_orders(request):
    return JsonResponse(serialize('json', Order.objects.all()), safe=False)


def get_client(request):
    #  TODO: #2 Переписать get_client()  request.user.userprofile.cliens.all()
    client = None
    profile = UserProfile.objects.filter(user=request.user)
    if profile.count() > 0:
        client = profile[0].client
    return client


def json_err(msg):
    return JsonResponse({'err': msg}, status=500)


@login_required(login_url=LOGIN_URL)
def change_status(request):
    if request.method == 'GET':
        return JsonResponse({'err': 'POST method required'})

    user = request.user
    action = request.POST['action']
    key = request.POST['pk']
    order = Order.objects.get(pk=key)
    if action == 'send':
        order.send(user)
    elif action == 'approve':
        order.approve(user)
    elif action == 'unapprove':
        order.unapprove(user, request.POST['UnapproveReason'])
    elif action == 'cancel':
        # 12.05.20189 нельзя отменить заявку на которую уже назначили водителя
        if order.Status == 4:
            return json_err("Заявка уже назначена водителю. Связитесь с менеджером")
        order.cancel(user, request.POST['CancelReason'])
    elif action == 'decline':
        order.decline(user, request.POST['DenyReason'])
    elif action == 'assignCar':
        # 03.12.2018 запрет назначения для несогласованных заявок
        if order.Status < 3:
            return json_err("Заявка несогласована")

        truck_id = request.POST['truck_id']
        trailer_id = request.POST['trailer_id']
        driver_id = request.POST['driver_id']
        auto_visit_take_plan = request.POST.get('auto_visit_take_plan')
        auto_visit_return_plan = request.POST.get('auto_visit_return_plan')
        order.assign_car(user, truck_id, trailer_id, driver_id, auto_visit_take_plan, auto_visit_return_plan)
    elif action == 'unassignCar':
        order.un_assign_car(user, request.POST['UnassignReason'])
    elif action == 'close':
        order.close(user)
        # New tariff function
        calc_order_rates(order, request.user, save_to_db=True)
    status_text = dict(STATUS_CHOICES)[order.Status]
    return JsonResponse({'id': order.id, 'status_id': order.Status, 'status': status_text}, safe=False)


@login_required(login_url=LOGIN_URL)
def save_order_data(request):
    try:
        order = Order.objects.get(pk=request.POST['pk'])
        order.save_data(request.user, **request.POST)
    except Exception as e:
        return JsonResponse({'err': str(e)}, safe=False)

    return JsonResponse({'id': order.id}, safe=False)


def get_or_create_customer(data):
    customer = data['customer_new']

    if str(customer).isdigit():
        return int(customer)
    else:
        obj, _ = Customers.objects.get_or_create(name=customer)
        return obj.id


@login_required(login_url=LOGIN_URL)
def save_order(request):
    if request.method == 'GET':
        return JsonResponse({'err': 'POST method required'})

    order_id = request.GET.get('id', 0)
    # Issue 17. Don't replace  Manager whose create Order while update them
    if order_id == 0:
        updated_form_data = request.POST.copy()
        updated_form_data['customer_new'] = get_or_create_customer(updated_form_data)
        form = OrderForm(updated_form_data)
    else:
        # Place First Manager on Form data
        instance = Order.objects.get(pk=order_id)
        updated_form_data = request.POST.copy()
        updated_form_data['Manager'] = instance.Manager.id
        updated_form_data['customer_new'] = get_or_create_customer(updated_form_data)
        form = OrderForm(updated_form_data, instance=instance)

    if not form.is_valid():
        return JsonResponse({'err': VALIDATE_ERROR, 'messages': form.errors, 'id': order_id},
                            content_type='application/json')
    else:
        order = form.save(commit=False)
        if order_id == 0:
            number = Order.objects.filter(CreateDate__gte=date.today().replace(day=1)).aggregate(Max('Number'))
            if number['Number__max'] == '' or number['Number__max'] is None:
                number = '00000'
            else:
                number = number['Number__max']
            order.Number = ('0000' + str(int(number) + 1))[-5:]
            order.Manager = request.user
            # order.CreateDate = datetime.datetime.now().replace(tzinfo=pytz.utc)

        # Save order
        order.save()

    return JsonResponse({
        'err': '',
        'id': order.id,
        'number': order.Number
    })


@login_required(login_url=LOGIN_URL)
def save_order_template(request):
    order_id = request.GET.get('id', 0)
    order = Order.objects.get(pk=order_id)
    name = order.Customer + ' - ' + get_operation(order.Operation) + ' - ' + order.Address

    s = OrderTemplate(
        order=order,
        name=name,
        create_by=request.user,
        client=order.Client
    )
    s.save()

    return JsonResponse({
        'err': '',
        'id': s.pk,
        'number': ''
    })


@login_required(login_url=LOGIN_URL)
def load_order_from_template(request):
    template_id = request.GET.get('template_id', 0)
    data = get_filter_dict([OrderTemplate
                           .objects
                           .select_related('order')
                           .get(pk=template_id).order],
                           fields=[
                               'Responsible',
                               'Client',
                               'customer_new',
                               'Proxy',
                               'CargoName',
                               'ContainerNum',
                               'Address',
                               'District',
                               'RaywayCr',
                               'RaywayCr2',
                               'RaywayCrDistrict',
                               'RaywayCr2District'
                               'ExportDate',
                               'ExportTime',
                               'Operation',
                               'TerminalLoad',
                               'TerminalDeliver',
                               'Weight',
                               'Foot',
                               'Comment',
                               'dz',
                               'destination_station',
                               'cargo_manager'])
    res = {'err': 'Нет данных'} if len(data) < 1 else data[0]

    return JsonResponse(res)


@login_required(login_url=LOGIN_URL)
def save_work(request):
    if request.method == 'GET':
        return JsonResponse({'err': 'POST method required'})

    order_id = request.GET.get('id', 0)
    if order_id == 0:
        return JsonResponse({'err': 'wrong request identifier'})

    # Trouble with leap year 29/02/2020 - add Year for format '%d.%m %H:%M'
    form_data = add_year(request.POST)

    # для диспетчера восток - урезанная форма
    if 18 in [g.id for g in request.user.groups.all()]:
        form = OrderWorkFormDisp(form_data, instance=Order.objects.get(pk=order_id))
    else:
        form = OrderWorkForm(form_data, instance=Order.objects.get(pk=order_id))

    status = int(form.data['Status'])
    if not form.is_valid():
        return JsonResponse({'err': VALIDATE_ERROR, 'messages': form.errors, 'id': order_id})

    order = form.save()

    # Костыль для диспетчера восток - пересчитываем простой который не посчитали на фронте
    if 18 in [g.id for g in request.user.groups.all()]:
        try:
            order.client_idle_cost = order.client_idle_price * order.client_idle_minutes
            order.client_total += order.client_idle_cost
            order.salary_idle_cost = order.salary_idle_price * order.salary_idle_minutes
            order.salary_total += order.salary_idle_cost
            order.save()
        except Exception as e:
            logger.error(e)

    # save additional services
    add_services = list(filter(lambda x: 'add_service' in x[0], request.POST.items()))
    add_services_for_save = []
    if len(add_services) == 0:
        OrderAdditionalService.objects.filter(order=order).delete()
    else:
        for s in filter(lambda x: 'service_code' in x[0], add_services):
            if s[1] != '':
                d = dict(filter(lambda x: s[1] in x[0], add_services))
                clear_keys = dict([(k.split('.')[-1], v) for k, v in d.items()])
                add_services_for_save.append(clear_keys)
                _, _ = OrderAdditionalService.objects.update_or_create(order=order, service_code=s[1],
                                                                       defaults=clear_keys)
    # save order
    order.fix(user=request.user, Status=status, add_services=add_services_for_save)

    return JsonResponse({
        'err': '',
        'id': order.id,
        'number': order.Number
    })


@login_required(login_url=LOGIN_URL)
def save_job(request):
    operation = request.POST.get('Operation')
    fio = request.POST.get('fio')
    comment = request.POST.get('comment')
    date_start = request.POST.get('date_start')

    d = datetime.strptime(date_start, "%Y-%m-%d").day
    m = datetime.strptime(date_start, "%Y-%m-%d").month
    y = datetime.strptime(date_start, "%Y-%m-%d").year
    full_date = datetime.strptime(date_start, "%Y-%m-%d")

    rec = Salary.objects.filter(employee=Driver.objects.filter(FIO__contains=fio)[0], day=d, month=m, year=y)

    if rec.count() > 0:
        rec.update(value=operation, comment=comment)
    else:
        s = Salary(employee=Driver.objects.filter(FIO__contains=fio)[0], day=d, month=m, year=y, value=operation,
                   comment=comment, fulldate=full_date)
        s.save()

    return JsonResponse({
        'result': 'ok'
    })


def calc_summary(data, last_column_id):
    if data:
        month_result = {'jan': 0, 'feb': 0, 'mar': 0, 'apr': 0,
                        'may': 0, 'jun': 0, 'jul': 0, 'aug': 0,
                        'sep': 0, 'oct': 0, 'nov': 0, 'dec': 0, 'all': 0}

        summary_structure = {
            20: {'row_num': last_column_id + 1, 'name': 'Итого', 'Foot': 20},
            40: {'row_num': last_column_id + 2, 'name': 'Итого', 'Foot': 40},
            'total': {'row_num': last_column_id + 3, 'name': 'Итого', 'Foot': 'Всего'}
        }
        res = {
            20: deepcopy(month_result),
            40: deepcopy(month_result),
            'total': deepcopy(month_result)
        }

        for row in data:
            if row['Foot'] == 'Всего':
                continue
            for name, value in row.items():
                if name in res[row['Foot']]:
                    res[row['Foot']][name] += value
                    res['total'][name] += value

        for key, value in res.items():
            value.update(summary_structure[key])
            data.append(value)

    return data


@login_required(login_url=LOGIN_URL)
def del_act(request):
    """Удаляем акт"""
    act_id = request.POST.get('pk', request.GET.get('pk', 1))
    # сначала сбросим идентификатор акта в заявках, чтобы не удалить их
    Order.objects.filter(bill_number=act_id).update(bill_number=None)
    # удалим сам акт
    ClientBill.objects.get(pk=act_id).delete()
    # TODO: изменить схему БД, чтобы удаление акта не приводило к удалению заявки

    return JsonResponse({'status': 'OK'})


@login_required(login_url=LOGIN_URL)
def send_acts(request):
    """Отправляем акты по email"""
    act_id = request.POST.get('pk', request.GET.get('pk', 1))
    new_status = request.POST.get('status')
    act = ClientBill.objects.get(pk=act_id)

    if new_status == '3':
        try:
            mail_to = act.client.email
            email = EmailMessage('АВТО ВЛ: Акты на оплату',
                                 'Добырый день. Документ во вложении.\nС уважением, АВТО ВЛ',
                                 'autovl.tms@gmail.com',
                                 [mail_to]
                                 )
            email.attach_file('acts/act_{}.xlsx'.format(act_id))
            email.send(fail_silently=False)
            logger.info("Send act to {} ".format(mail_to))
        except Exception as e:
            logger.error(e)

    act.status = new_status
    act.save()
    resp = 'ok'

    return JsonResponse({'resp': resp})


@login_required(login_url=LOGIN_URL)
def hack(request):
    """Пересчет ставки по закрытым заявкам"""
    d = []
    df = request.GET.get('df')
    o = request.GET.get('o')
    oid = request.GET.get('oid')
    if df is not None:
        for i in Order.objects.filter(Status=5, client_transfer__gt=0, CreateDate__gte=df):
            i.close(request.user)
            order_with_rates, error = calc_order_rates(i, request.user, save_to_db=True)
            d.append({'num': i.Number, 'error': error})
            print(i.Number)
    elif o is not None:
        for i in (o.split(',')):
            time_ago = timezone.now() - timedelta(days=31)
            item = Order.objects.filter(Number=i, CreateDate__gte=time_ago).order_by('-pk')[0]
            order_with_rates, error = calc_order_rates(item, request.user, save_to_db=True)
            d.append({'num': item.Number, 'error': error})
    # find order by id
    elif oid is not None:
        item = Order.objects.get(pk=oid)
        order_with_rates, error = calc_order_rates(item, request.user, save_to_db=True)
        d.append({'num': item.Number, 'error': error})

    else:
        d = {"orders not found"}
    return JsonResponse(d, safe=False)


@login_required(login_url=LOGIN_URL)
def check_salary(request):
    """
    Проверка табеля на корректность за определенный месяц
    """

    y = request.GET.get('y')
    m = request.GET.get('m')

    orders = Order.objects.filter(Status__in=[5, 8, 9], ExportDate__year=y, ExportDate__month=m)
    salary_obj = Salary.objects.filter(year=y, month=m).exclude(value__in=['Б', 'О', 'Р', 'М', 'Н'])

    no_salary = []

    if orders.count() == 0 or salary_obj.count() == 0:
        return JsonResponse({'status': 'orders not found'}, safe=False)

    for order in orders:
        s = salary_obj.filter(comment__contains=',{},'.format(order.pk))
        if s.count() == 0:
            no_salary.append(order.Number)

    # В идеале все ОК, если массив no_salary пуст
    return JsonResponse({
        'status': 'orders found' if len(no_salary) > 0 else 'salary OK',
        'orders': no_salary
    }, safe=False)
