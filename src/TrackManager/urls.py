from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from . import views
from . import dt
from . import api
from . import file_handlers

urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + \
              [
                  #  Pages
                  url(r'^info/$', views.info, name='info'),
                  url(r'^maps/$', views.maps, name='maps'),
                  url(r'^ymaps/$', views.ymaps, name='ymaps'),
                  url(r'^manager/$', views.tasks, name='manager'),
                  url(r'^dispatcher/$', views.dispatcher, name='dispatcher'),
                  url(r'^manager-calc/$', views.manager_calc, name='manager_calc'),
                  url(r'^salary/$', views.salary, name='salary'),
                  url(r'^schedule/$', views.schedule, name='schedule'),
                  url(r'^app_report/$', views.app_report, name='app_report'),
                  url(r'^acts/$', views.acts, name='acts'),
                  url(r'^reports/$', views.reports_page, name='reports_page'),
                  url(r'^payments/$', views.payments, name='payments'),
                  url(r'^deny/$', views.deny, name='deny'),
                  url(r'^tariff_check/$', views.tariff_check, name='tariff_check'),
                  url(r'^client-price/$', views.client_price, name='client-price'),
                  url(r'^driver-price/$', views.driver_price, name='driver-price'),
                  url(r'get_history/$', views.hist, name='hist'),
                  url(r'get_driver_actions/$', views.driver_actions_modal, name='get_driver_actions'),

                  # Order API
                  url(r'change_status/$', api.change_status, name='change_status'),
                  url(r'save_order/$', api.save_order, name='save_order'),
                  url(r'save_order_template/$', api.save_order_template, name='save_order_template'),
                  url(r'load_order_from_template/$', api.load_order_from_template, name='load_order_from_template'),
                  url(r'save_work/$', api.save_work, name='save_work'),
                  url(r'save_job/$', api.save_job, name='save_job'),
                  url(r'save_order_data/$', api.save_order_data, name='save_order_data'),
                  url(r'save_order_doc/$', api.save_order_doc, name='save_order_doc'),
                  url(r'delete_order_doc/$', api.delete_order_doc, name='delete_order_doc'),
                  url(r'check_tariff/$', api.check_tariff, name='check_tariff'),
                  url(r'calc_tariff_form/$', api.calc_tariff_form, name='calc_tariff_form'),
                  url(r'update_payments_report/$', api.update_payments_report, name='update_payments_report'),
                  url(r'get_all_orders/$', api.get_all_orders, name='get_all_orders'),
                  url(r'send_act/$', api.send_acts, name='send_acts'),
                  url(r'del_act/$', api.del_act, name='del_act'),
                  url(r'check_salary/$', api.check_salary, name='check_salary'),

                  # Download attachments
                  url(r'download/(?P<path>.*)$', api.download_doc),

                  # Report files
                  url(r'get_report/$', file_handlers.excel, name='excel'),
                  url(r'report_disp/$', file_handlers.excel, name='disp_excel'),
                  url(r'report_sal/$', file_handlers.report_sal, name='report_sal'),
                  url(r'report_total_drivers/$', file_handlers.report_total_drivers, name='report_total_drivers'),
                  url(r'containers_all/$', file_handlers.containers_all, name='containers_all'),
                  url(r'tn$', file_handlers.transport_invoice, name='tn'),
                  url(r'pl', file_handlers.road_list, name='pl'),
                  url(r'get_act_report$', file_handlers.get_act_report, name='get_act_report'),
                  url(r'act_to_excel$', file_handlers.act_to_excel, name='act_to_excel'),
                  url(r'payments_to_excel$', file_handlers.payments_to_excel, name='payments_to_excel'),
                  url(r'all_payment_to_excel$', file_handlers.all_payments_to_excel, name='all_payments_to_excel$'),
                  url(r'download-driver-price/$', file_handlers.download_driver_price, name='download-driver-price'),
                  url(r'download-client-price/$', file_handlers.download_client_price, name='download-client-price'),

                  # Data tables
                  url(r'get_manager_table/$', dt.ManagerOrderListJson.as_view(), name='manager_list_json'),
                  url(r'get_manager_calc_table/$', dt.OrdersCalc.as_view(), name='orders_calc'),
                  url(r'get_dispatcher_table/$', dt.DispatcherOrderListJson.as_view(), name='dispatcher_list_json'),
                  url(r'get_experiment/$', dt.OrdersExperimental.as_view(), name='dispatcher_experimental'),
                  url(r'get_salary_table/$', dt.SalaryReport.as_view(), name='salaryreport'),
                  url(r'get_acts_table/$', dt.ActReport.as_view(), name='actreport'),
                  url(r'get_payments_table/$', dt.PaymentsReportTable.as_view(), name='get_payments_table'),

                  url(r'get_reports_table/$', dt.get_reports_table, name='get_reports_table'),
                  url(r'get_clients_table/$', dt.get_clients_table, name='get_clients_table'),
                  url(r'get_schedule_table/$', dt.schedule_table, name='s_table'),
                  url(r'get_app_report_table/$', dt.app_report_table, name='app_report_table'),
              ]
