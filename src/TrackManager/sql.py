from datetime import datetime

from django.db import connection

TOTAL_DRIVERS = """SELECT
                    d."name" as "name",
                    substr(t."number",2,3) as "car_num",
                    count(o."id") as "cnt",
                    round(sum(o."client_total"::NUMERIC),2) as "client_total",
                    round(sum(o."salary_total"::NUMERIC),2) as "salary_total",
                    round(sum(o."profit"::NUMERIC),2) as "profit"
                    FROM "TrackManager_truck" t
                    JOIN "TrackManager_dispzone" d on d."id" = t."dz_id"
                    LEFT JOIN "TrackManager_order" o on t."id" = o."truck_id"
                    {where}
                    group by d.name,substr(t."number",2,3)
                    union
                    SELECT '_Итого:',
                            ' ',
                            count(o.id) as "cnt",
                            round(sum(o."client_total"::NUMERIC),2) as "client_total",
                            round(sum(o."salary_total"::NUMERIC),2) as "salary_total",
                            round(sum(o."profit"::NUMERIC),2) as "profit"
                            FROM "TrackManager_truck" t
                            JOIN "TrackManager_dispzone" d on d."id" = t."dz_id"
                            LEFT JOIN "TrackManager_order" o on t."id" = o."truck_id"
                            {where}
                    order by 2 asc
                """

CLIENTS_TOTAL = """
                    select
                    b."name" "name"
                    ,b."Foot" "Foot"
                    ,sum(b.jan) "jan"
                    ,sum(b.feb) "feb"
                    ,sum(b.mar) "mar"
                    ,sum(b.apr) "apr"
                    ,sum(b.may) "may"
                    ,sum(b.jun) "jun"
                    ,sum(b.jul) "jul"
                    ,sum(b.aug) "aug"
                    ,sum(b.sep) "sep"
                    ,sum(b.oct) "oct"
                    ,sum(b.nov) "nov"
                    ,sum(b.decem) "dec"
                    ,sum(b.all_m) "all"
                    from
                    (Select
                    a."name"
                    ,a."Foot"
                    ,case when a.m = '01'  then a.cnt else 0 end "jan"
                    ,case when a.m = '02'  then a.cnt else 0 end "feb"
                    ,case when a.m = '03'  then a.cnt else 0 end "mar"
                    ,case when a.m = '04'  then a.cnt else 0 end "apr"
                    ,case when a.m = '05'  then a.cnt else 0 end "may"
                    ,case when a.m = '06'  then a.cnt else 0 end "jun"
                    ,case when a.m = '07'  then a.cnt else 0 end "jul"
                    ,case when a.m = '08'  then a.cnt else 0 end "aug"
                    ,case when a.m = '09'  then a.cnt else 0 end "sep"
                    ,case when a.m = '10'  then a.cnt else 0 end "oct"
                    ,case when a.m = '11'  then a.cnt else 0 end "nov"
                    ,case when a.m = '12'  then a.cnt else 0 end "decem"
                    ,a.cnt "all_m"
                    from
                    (SELECT
                            c."Name" as "name",
                            o."Foot",
                            EXTRACT(MONTH from o."ExportDate") as "m",
                            count(o."id")  cnt
                            FROM "TrackManager_order" o
                            JOIN "TrackManager_client" c on c."id" = o."Client_id"
                             {where}
                            group by c."Name",
                                    o."Foot",
                                    EXTRACT(MONTH from o."ExportDate")
                            order by 1,3,2) a )b
                            group by
                            b."name"
                            ,b."Foot"
                            order by  b."name" ,b."Foot"
        """

PRICES_FOR_NON_ACTIVE_CLIENTS = """select
                x."client_id", x."client_name", x."dz_id_min", x."dz_id_max", x."dateFrom"
                from
                (select p."client_id", client."Name" as client_Name,
                min(p."dz_id") as dz_id_min, max(p."dz_id") as dz_id_max, p."dateFrom"
                ,row_number() over (partition by p."client_id" order by p."dateFrom" desc) rn
            from "TrackManager_clientadditionalprice" as p
              join "TrackManager_client" client on p.client_id = client.id
            where client_id is not null
            and client.status = 'I'
            group by p."client_id", client."Name", p."dateFrom"
            order by client."Name" asc, p."dateFrom" desc) x where rn=1"""


def clint_prices(sql_filter):
    return """select
                x."client_id", x."client_name", x."dz_id_min", x."dz_id_max", x."dateFrom"
                from
                (select p."client_id", client."Name" as client_Name,
                min(p."dz_id") as dz_id_min, max(p."dz_id") as dz_id_max, p."dateFrom"
                ,row_number() over (partition by p."client_id" order by p."dateFrom" desc) rn
            from "TrackManager_clientadditionalprice" as p
              join "TrackManager_client" client on p.client_id = client.id
            where client_id is not null
            and client.status = 'A'
            group by p."client_id", client."Name", p."dateFrom"
            order by client."Name" asc, p."dateFrom" desc) x where rn {}""".format(sql_filter)


def driver_prices(sql_filter):
    return """select x."dt",x."driver_type",x."dz_id_min",x."dz_id_max", x."dateFrom"
                from
                (select "dt", replace(replace(replace(replace(dt, 'O', 'Наш'), 'F', 'Получастник'),
                 'P', 'Частник'), 'S', 'Партнер') as driver_type,
                  min(p."dz_id") as dz_id_min, max(p."dz_id") as dz_id_max, p."dateFrom",
                  row_number() over (partition by "dt" order by p."dateFrom" desc) as rn
                from "TrackManager_driveradditionalprice" p
                group by "dt", "dateFrom"
                order by p."dateFrom" desc, dt) x
            where x.rn {}""".format(sql_filter)


def app_report(where_str, date_start, date_end):
    return f"""SELECT
        "FIO"
        ,case when d.dz_id=1 then 'Восток' else 'Клещиха' end as "dz"
        ,case when d.app_token is not null then 1 else 0 end as "app_install"
        ,nullif(d.app_version, '') as "app_version"
        ,count(distinct o.id) as "order total"
        ,count(distinct os.order_id) as "order_status_count"
        ,count(distinct od.id) as "foto_count"
        ,sum(case when o.recognized_container_num is not null then 1 else 0 end) as "regognized_total"
        ,sum(case when o.recognized_container_num is not null
                and o.manual_input = true then 1 else 0 end)  as "manual_input"
        FROM "TrackManager_driver" d
            LEFT JOIN "TrackManager_order" o on d."id" = o."driver_id"
            LEFT JOIN "TrackManager_orderdocuments" od on o.id = od.order_id
                    and (od."name" like 'app_foto%' or od."name" like 'upload%')
            LEFT JOIN "TrackManager_orderdriveraction" os on o.id = os.order_id and os.action = 8
        where d."Status" = 'A'
         and o."ExportDate" between '{date_start}' and '{date_end}'
         {where_str}
        group by "FIO", case when d.app_token is not null then 1 else 0 end, nullif(d.app_version, '')
         ,case when d.dz_id=1 then 'Восток' else 'Клещиха' end
        order by 2,3 desc
    """


def schedule_request(date_start, date_end, where_str, year_filter, month_filter, order_field, order_dir):
    return """SELECT
        tr."number", t."number", d."FIO", t."foot", tr."dz_id", o."ContainerNum",ter1."Name",ter2."Name",
        o."Address",o."District",o."RaywayCr",o."RaywayCr2",o."Operation",o."ExportTime", c."Name",o."Customer",
        (select count(o."id") from "TrackManager_order" o where d."id" = o."driver_id" and o."ExportDate" between '{0}'
        and '{1}' and o."Status" not in (6,7))  trip_num,
        (select s."value" from "TrackManager_salary" s where s."employee_id" = d."id"
        and s."fulldate" =  '{0}' limit 1)  add_work,
        (select sum(cast (s."value" as int)) from "TrackManager_salary" s where s."employee_id" = d."id"
        and s."year" = '{3}' and s."month" = '{4}' and s."value" not in ('Б','О','Р','М','Н'))  month_trips,
        s."comment" as "Comment"
        FROM "TrackManager_driver" d
            LEFT JOIN "TrackManager_salary" s on s."employee_id" = d."id" and  s."fulldate" between '{0}' and '{1}'
            LEFT JOIN (select * from "TrackManager_order" where "ExportDate" between '{0}' and '{1}'
            and ("Status" in (4,5,8,9) or "Status" is null) ) o on d."id" = o."driver_id"
            LEFT JOIN "TrackManager_truck" tr on d."id" = tr."driver_id" and tr."status"='A'
            LEFT JOIN "TrackManager_client" c on c."id" = o."Client_id"
            LEFT JOIN (select max("foot") foot, "car_id" from "TrackManager_trailer" group by "car_id") mf
                        on mf."car_id" = tr."id"
            LEFT JOIN "TrackManager_trailer" t on t."car_id" = tr."id" and t."foot" = mf."foot" and t."status" = 'A'
            LEFT JOIN "TrackManager_terminal" ter2 on ter2."id" = o."TerminalDeliver_id"
            LEFT JOIN "TrackManager_terminal" ter1 on ter1."id" = o."TerminalLoad_id"
        where d."Status" = 'A'
            {2}
        order by {5} {6}
        """.format(date_start.strftime('%Y-%m-%d %H:%M'),
                   date_end.strftime('%Y-%m-%d %H:%M'),
                   where_str,
                   year_filter,
                   month_filter,
                   order_field,
                   order_dir)


def dict_fetch_all(cursor):
    """  Returns all rows from a cursor as a dict  """
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


def get_orders_for_payment(period, driver_id):
    month = datetime.strptime(period, '%Y-%m-%d').strftime("%m")
    year = datetime.strptime(period, '%Y-%m-%d').year
    sal_sum = '''select
                    t."Address",
                    t."AddressDeliver",
                    t."AddressLoad",
                    t."CargoName",
                    t."Client_id",
                    t."Comment",
                    t."ContainerNum",
                    t."CreateDate",
                    t."Customer",
                    t."District",
                    t."ExportDate",
                    t."ExportTime",
                    t."Foot",
                    t."Manager_id",
                    t."Number",
                    case
                        when t."Operation" = 1 then 'погр.'
                        when t."Operation" = 2 then 'выгр.'
                        when t."Operation" = 3 then 'пор.'
                    end as "Operation",
                    t."Proxy",
                    t."RaywayCr",
                    t."RaywayCr2",
                    t."Responsible",
                    t."Status",
                    td."Name" as "TerminalDeliver",
                    tl."Name" as "TerminalLoad",
                    t."Weight",
                    t.additional_wastes,
                    t.approve_date,
                    t.assign_date,
                    t.auto_visit_return_fact,
                    t.auto_visit_return_num,
                    t.auto_visit_return_plan,
                    t.auto_visit_take_fact,
                    t.auto_visit_take_num,
                    t.auto_visit_take_plan,
                    t.bill_number_id,
                    t.cancel_date,
                    t.cancel_reason,
                    t.cargo_manager,
                    t.client_cleaning,
                    t.client_forwarding,
                    t.client_idle_cost,
                    t.client_idle_minutes,
                    t.client_idle_price,
                    t.client_rate,
                    t.client_total,
                    t.client_transfer,
                    t.correction_date,
                    t.decline_date,
                    t.decline_reason,
                    t.destination_station,
                    t.dispatch_date,
                    t.dispatcher_approve_id,
                    t.dispatcher_assign_id,
                    t.dispatcher_decline_id,
                    t.dispatcher_unapprove_id,
                    t.dispatcher_unassign_id,
                    t.driver_id,
                    t.dz_id,
                    t.id,
                    t.operation_date_begin,
                    t.operation_date_end,
                    t.payment_date,
                    t.platon,
                    t.profit,
                    t.salary_cleaning,
                    t.salary_forwarding,
                    t.salary_idle_cost,
                    t.salary_idle_minutes,
                    t.salary_idle_price,
                    t.salary_rate,
                    t.salary_total,
                    t.salary_transfer,
                    t.trailer_id,
                    t.truck_id,
                    t.unapprove_date,
                    t.unapprove_reason,
                    t.unassign_date,
                    t.unassign_reason,
                    coalesce(s.add_services, 0) AS add_services
                from
                    "TrackManager_order" t
                    join "TrackManager_terminal" tl on tl.id = t."TerminalLoad_id"
                    join "TrackManager_terminal" td on td.id = t."TerminalDeliver_id"
                    left join (select
                                    sum("salary_total") as add_services, order_id
                                from "TrackManager_orderadditionalservice"
                                 group by order_id
                                 ) s on s."order_id" = t."id"
                where t."driver_id" = {0}
                and t."Status" = 9
                and to_char("ExportDate", 'YYYYMM')  = '{2}{1}'
        '''
    cursor = connection.cursor()
    sql = sal_sum.format(driver_id, month, year)
    cursor.execute(sql)

    return dict_fetch_all(cursor)


def get_orders_for_payment_all(period):
    month = datetime.strptime(period, '%Y-%m-%d').strftime("%m")
    year = datetime.strptime(period, '%Y-%m-%d').year
    sal_sum = '''select
                    t."Address",
                    t."AddressDeliver",
                    t."AddressLoad",
                    t."CargoName",
                    t."Client_id",
                    t."Comment",
                    t."ContainerNum",
                    t."CreateDate",
                    t."Customer",
                    t."District",
                    t."ExportDate",
                    t."ExportTime",
                    t."Foot",
                    t."Manager_id",
                    t."Number",
                    case
                        when t."Operation" = 1 then 'погр.'
                        when t."Operation" = 2 then 'выгр.'
                        when t."Operation" = 3 then 'пор.'
                    end as "Operation",
                    t."Proxy",
                    t."RaywayCr",
                    t."RaywayCr2",
                    t."Responsible",
                    t."Status",
                    td."Name" as "TerminalDeliver",
                    tl."Name" as "TerminalLoad",
                    t."Weight",
                    t.additional_wastes,
                    t.approve_date,
                    t.assign_date,
                    t.auto_visit_return_fact,
                    t.auto_visit_return_num,
                    t.auto_visit_return_plan,
                    t.auto_visit_take_fact,
                    t.auto_visit_take_num,
                    t.auto_visit_take_plan,
                    t.bill_number_id,
                    t.cancel_date,
                    t.cancel_reason,
                    t.cargo_manager,
                    t.client_cleaning,
                    t.client_forwarding,
                    t.client_idle_cost,
                    t.client_idle_minutes,
                    t.client_idle_price,
                    t.client_rate,
                    t.client_total,
                    t.client_transfer,
                    t.correction_date,
                    t.decline_date,
                    t.decline_reason,
                    t.destination_station,
                    t.dispatch_date,
                    t.dispatcher_approve_id,
                    t.dispatcher_assign_id,
                    t.dispatcher_decline_id,
                    t.dispatcher_unapprove_id,
                    t.dispatcher_unassign_id,
                    t.driver_id,
                    t.dz_id,
                    t.id,
                    t.operation_date_begin,
                    t.operation_date_end,
                    t.payment_date,
                    t.platon,
                    t.profit,
                    t.salary_cleaning,
                    t.salary_forwarding,
                    t.salary_idle_cost,
                    t.salary_idle_minutes,
                    t.salary_idle_price,
                    t.salary_rate,
                    t.salary_total,
                    t.salary_transfer,
                    t.trailer_id,
                    t.truck_id,
                    t.unapprove_date,
                    t.unapprove_reason,
                    t.unassign_date,
                    t.unassign_reason,
                    coalesce(s.add_services, 0) AS add_services
                from
                    "TrackManager_order" t
                    join "TrackManager_terminal" tl on tl.id = t."TerminalLoad_id"
                    join "TrackManager_terminal" td on td.id = t."TerminalDeliver_id"
                    left join (select
                                    sum("salary_total") as add_services, order_id
                                from "TrackManager_orderadditionalservice"
                                 group by order_id
                                 ) s on s."order_id" = t."id"
                where t."Status" = 9
                and to_char("ExportDate", 'YYYYMM')  = '{1}{0}'
                order by t."driver_id"
        '''
    cursor = connection.cursor()
    sql = sal_sum.format(month, year)
    cursor.execute(sql)

    return dict_fetch_all(cursor)


class SQLProvider:

    def __init__(self, source_sql, params, mode=None):
        self.params = params
        self.mode = mode
        self.dataset = []
        sql = source_sql.format(where=self.get_filter())
        with connection.cursor() as cursor:
            cursor.execute(sql)
            columns = [col[0] for col in cursor.description]
            columns.insert(0, 'row_num')
            summary = None
            row_set = cursor.fetchall()
            for i, row in enumerate(row_set):
                if '_' not in row[0]:
                    item = [i + 1]
                    item.extend(row)
                    self.dataset.append(dict(zip(columns, item)))
                else:
                    summary = row

            if summary:
                summary_row = [len(row_set) + 1]
                summary_row.extend(summary)
                self.dataset.append(dict(zip(columns, summary_row)))

    def get_filter(self):
        conditions = ["""o."Status" in (5, 8, 9)""", ]

        dz = self.params.get('columns[1][search][value]')
        auto = self.params.get('columns[2][search][value]')
        date_start = self.params.get('date_start')
        date_end = self.params.get('date_end')

        if date_start and date_end:
            if '23:59:59' not in date_end:
                date_end += ' 23:59:59'
            conditions.append("""o."ExportDate">='{0}' AND o."ExportDate"<='{1}'""".format(date_start, date_end))

        if auto:
            conditions.append("""t."id"={0}""".format(auto))
        if dz:
            column = "d" if self.mode is None else "c"
            conditions.append("""{1}."id"={0}""".format(dz, column))

        where_str = "WHERE " + " AND ".join(conditions) if len(conditions) > 0 else ""

        return where_str

    def get_table(self):
        response = {
            "draw": self.params['draw'],
            "result": 'ok',
            "data": self.dataset,
            "recordsFiltered": 1,
            "recordsTotal": 1
        }
        return response
