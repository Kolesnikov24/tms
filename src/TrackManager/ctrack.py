# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import random
import json
import time
import urllib.request as urllib
from urllib.parse import parse_qs, urlparse

from functools import wraps
from datetime import datetime
from datetime import timedelta
from copy import deepcopy

from django.utils import timezone
from django.contrib.sessions.backends.db import SessionStore
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.response import Response
from rest_framework import serializers, generics, status
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.views import APIView

from . import models
from . import models as m
from .api import save_order_attach
from .tools import logger, get_filter_dict
from .constants import get_operation, MONTH_NAMES


SENDER = 'INFORM'
SERVICE_URL = 'http://smspilot.ru/api.php'
API_KEY = 'J401Q2V5RLN81HFQ293M8L66M0CH7ERIE53IZ0DL7U8YE64671272JZN1T4KO382'
OK_RESP = {"resp": "OK"}
APP_TOKEN = '1ee87410-730b-4a6e-ab05-19cf01fdcdf9'
DISPLAY_FIELDS = ('id', 'Number', 'Address', 'ExportDate', 'Operation',
                  'Foot', 'Weight', 'TerminalLoad', 'TerminalDeliver', 'Responsible',
                  'AddressLoad', 'AddressDeliver', 'CargoName', 'Status',
                  'RaywayCr', 'RaywayCr2', 'ExportTime', 'ContainerNum', 'Comment')


def get_route_str(obj):
    address = obj.TerminalLoad.Name + ' - ' + obj.Address + ' - ' + obj.TerminalDeliver.Name
    route = get_operation(obj.Operation) + ' ' + address
    return route


def get_orders_for_history(obj_list):
    res_list = []
    for obj in obj_list:
        try:
            res_list.append({
                'Number': obj.Number,
                'ExportDate': obj.ExportDate.strftime('%d.%m.%Y'),
                'Weight': obj.Weight,
                'route': get_route_str(obj),
                'salary_total': obj.salary_total
            })
        except Exception as e:
            logger.error(e, exc_info=True)

    return res_list


def get_salary_total(obj_list):
    total = 0
    for obj in obj_list:
        try:
            total += obj.salary_total
        except Exception:
            total += 0
    return round(total, 0)


def get_param_from_url(request, name):
    full_url = request.build_absolute_uri()
    query_params = parse_qs(urlparse(full_url).query)
    p_val = query_params.get(name, [None])[0]
    return p_val


def get_driver_by_session(func):

    def wrapper(*args, **kwargs):
        try:
            request = args[1]
            sk = request.GET.get('session_key')
            s = SessionStore(session_key=sk)
            driver_id = s.get('driver_id')
            driver = models.Driver.objects.get(pk=driver_id)
            return func(*args, driver=driver, **kwargs)

        except ObjectDoesNotExist as e:
            logger.error(e, exc_info=True)
            return Response({'message': 'Данные водителя не найдены'}, status=403)

        except Exception as common_err:
            logger.error(f'Request failed {common_err}')
            logger.error(common_err, exc_info=True)
            return Response({'message': 'Ошибка на сервере, обратитесь к администратору'}, status=200)
    return wrapper


def get_driver_orders(driver):
    """
    ok
    :param driver:
    :return:

    """
    orders = None
    try:

        if driver:
            td = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)

            orders = models.Order.objects.filter(driver=driver, last_driver_action__gt=0, last_driver_action__lt=8). \
                prefetch_related('orderdriveraction_set'). \
                order_by('ExportDate', 'ExportTime')

            if len(orders) == 0:
                orders = models.Order.objects.filter(driver=driver, Status=4, ExportDate__gte=td). \
                    prefetch_related('orderdriveraction_set'). \
                    order_by('ExportDate', 'ExportTime')
    except Exception as e:
        logger.error("Can not find driver orders")
        logger.error(e, exc_info=True)

    return list(orders)


def check_token(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        if request.GET.get('token', '') == APP_TOKEN:
            return function(request, *args, **kwargs)
        else:
            return JsonResponse({'message': 'auth required', 'code': 407}, safe=False)

    return wrap


def auth(driver, token, phone, app_version=None):

    # Save session
    s = SessionStore()
    s['driver_id'] = driver.pk
    s.create()
    logger.info(f'create session {s.session_key} for driver_id {driver.pk} phone={phone}')
    # save app_token and app_token in Driver object
    driver.app_token = token
    if app_version:
        driver.app_version = app_version
    driver.save()

    return {'message': 'ok',
            'code': 200,
            'session_key':  s.session_key,
            'driver_name': str(driver)}


def send_code(phone, text):
    url = SERVICE_URL + '?send=%s&to=%s&from=%s&apikey=%s&format=json' % (text, phone, SENDER, API_KEY)
    resp = urllib.urlopen(url)
    j = json.loads(resp.read())

    if 'error' in j:
        logger.error(f'sms send faled : {j}')
    # else:
        # logger.info(j)
    return True


def get_phone(request):
    phone_number_raw = request.GET.get('phone_number')
    phone_number = ''.join(filter(lambda x: x.isdigit(), phone_number_raw))

    return phone_number


def get_driver_by_phone(phone_number):
    phone = str(phone_number)
    number_groups = ['8', phone[1:4], phone[4:7], phone[7:9], phone[9:11]]
    phone_number_app_format = "-".join(number_groups)
    driver = m.Driver.objects.filter(Phone__contains=phone_number_app_format).first()
    return driver


class RegConfirmApiSerializer(serializers.ModelSerializer):
    """ Cериализатор запроса подтверждения номера телефона """

    class Meta:
        model = m.RegRequests
        fields = ('confirm_code', 'phone_number')


class RegApiSerializer(serializers.ModelSerializer):
    """ Cериализатор модели регистрации телефона """

    class Meta:
        model = m.RegRequests
        fields = ('phone_number',)
        read_only_fields = ('id',)

    def create(self, validated_data):
        phone = validated_data['phone_number']
        confirm_code = random.randint(1000, 9999)
        req_data = {
            'confirm_code': confirm_code,
            'valid_to': timezone.now() + timedelta(seconds=300),
            'confirm_status': 0,
            'attempts_count': 0,
            'phone_number': phone
        }

        # send sms
        send_code(phone, confirm_code)

        req_obj = m.RegRequests(**req_data)
        req_obj.save()

        return req_obj

    def to_representation(self, obj):
        return {
            'id': obj.id
        }


class OrderApiSerializer(serializers.ModelSerializer):
    """ Cериализатор модели статуса водителя"""

    class Meta:
        model = m.OrderDriverAction
        exclude = ('id', )


class RegApi(generics.CreateAPIView):
    """
    Регистрация. Шаг 1 - ввод номера телефона
    """
    serializer_class = RegApiSerializer
    allowed_methods = ['GET']

    def get(self, request, *args, **kwargs):
        try:
            phone = get_phone(request)
            logger.info(f"auth request: driver phone = {phone}")
            driver = get_driver_by_phone(phone)

            if driver is None:
                logger.error(f'auth request failed: driver not found by phone={phone}')
                return Response({"message": "Не найден водитель с указанным номером телефона"}, status=403)
            else:
                serializer = RegApiSerializer(data=request.GET)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data)
        except Exception as e:
            logger.error(f'auth request failed: {e}')
            logger.error(e, exc_info=True)
            return Response({"message": "Ошибка при авторизации"}, status=403)

        return Response(serializer.errors, status=400)


class RegConfirmApi(generics.GenericAPIView):
    """
    Регистрация. Шаг 2 - подтверждение номера телефона кодом из СМС
    """

    serializer_class = RegConfirmApiSerializer
    allowed_methods = ['GET']

    def get_object(self, *args, **kwargs):
        return m.RegRequests.objects.filter(phone_number=args[0]).order_by('-pk').first()

    def get(self, request):
        try:
            # Get request params
            received_code = int(request.GET.get('confirm_code'))
            token = request.GET.get('token')
            app_version = request.GET.get('app_version')

            # Get phone with digits
            phone_number = get_phone(request)

            # Find auth request
            reg_request = self.get_object(phone_number)
            if reg_request is None:
                return Response({'error': 'Неизвестсный номер'}, status=status.HTTP_403_FORBIDDEN)

            phone = reg_request.phone_number
            confirm_code = reg_request.confirm_code

            saved_data = {
                'attempts_count': reg_request.attempts_count + 1
            }
            serializer = RegApiSerializer(reg_request, data=saved_data, partial=True)

            # Find driver
            driver = get_driver_by_phone(phone)

            # Condition satisfied
            if received_code == confirm_code and \
                    serializer.is_valid() and \
                    driver is not None:

                saved_data.update({'confirm_status': 3})

                # Complete auth
                result = auth(driver, token, phone_number, app_version)

                # update request status
                serializer.save()

            else:
                logger.error(f'auth confirm failed: driver= {driver}, phone= {phone}, code= {confirm_code}')
                result = {'error': 'Неверный код подтверждения', 'code': 403}

        except Exception as es:
            logger.error(es, exc_info=True)
            result = {'error': 'Ошибка подтверждения', 'code': 500}

        return Response(status=result['code'], data=result)


class OrderView(APIView):
    """
    Возвращает заявки водителя в статусе "выполняется" по токену сессии
    """

    @get_driver_by_session
    def get(self, request, **kwargs):
        orders = get_driver_orders(kwargs['driver'])
        if orders:
            resp = {'message': 'OK', 'data': get_filter_dict(orders, DISPLAY_FIELDS)}
        else:
            resp = {"message": "Нет назначенных заявок", "data": []}
        return Response(resp)


class UploadDocView(APIView):
    """
    Загрузка фото
    """
    parser_classes = [JSONParser, MultiPartParser]

    @get_driver_by_session
    def post(self, request, *args, **kwargs):
        try:
            just_doc = True
            orders = get_driver_orders(kwargs['driver'])
            recognize_container_num = get_param_from_url(request, 'recognize_container_num')
            order_id = get_param_from_url(request, 'order_id')
            if get_param_from_url(request, 'manual_import') == "true":
                manual_input = True
            else:
                manual_input = False

            if order_id is not None:
                order = m.Order.objects.get(pk=order_id)

            else:
                order = orders[0]

            if order:
                if recognize_container_num:
                    order.recognized_container_num = recognize_container_num
                    order.save()
                    just_doc = False

                order.manual_input = manual_input
                order.save()

                _ = save_order_attach(request, upload_name='upload', order_id=order.pk, just_doc=just_doc)
                txt = "Ok"

            else:
                txt = "Order not found"
        except Exception as e:
            logger.error(request.build_absolute_uri())
            logger.error(e)
            txt = str(e)
        return Response({'message': txt})


class OrderHistoryView(APIView):
    """
    Возвращает историю начислений по заявкам за последний месяц
    """

    @get_driver_by_session
    def get(self, request, **kwargs):

        request_date = request.GET.get('request_date')
        if request_date:
            month = datetime.strptime(request_date, '%Y-%m-%d').strftime("%m")
            year = datetime.strptime(request_date, '%Y-%m-%d').year
        else:
            month = datetime.now().strftime("%m")
            year = datetime.now().strftime("%Y")

        orders_filter = dict(Status=9,
                             ExportDate__month=int(month),
                             ExportDate__year=int(year),
                             driver__id=kwargs['driver'].pk)
        driver_orders = models.Order.objects.filter(**orders_filter)
        display_fields = ('Number', 'Address', 'ExportDate', 'operation_date_begin',
                          'operation_date_end', 'salary_rate', 'salary_total', 'Operation',
                          'salary_idle_cost', 'salary_transfer', 'Foot',
                          'Weight', 'ExportTime', 'salary_total', 'ContainerNum')

        resp = {'message': 'OK', 'data': get_filter_dict(driver_orders, display_fields)}

        return Response(resp)


class OrderAggregateHistoryView(APIView):
    """
    Возвращает историю начислений по месяцам
    """

    @get_driver_by_session
    def get(self, request, **kwargs):
        x = 3
        now = time.localtime()
        month_lst = [time.localtime(time.mktime((now.tm_year, now.tm_mon - n, 1, 0, 0, 0, 0, 0, 0)))[:2]
                     for n in range(x)]
        driver_id = kwargs['driver'].pk
        res = []

        for mon_tuple in month_lst:
            orders_filter = dict(Status=9, ExportDate__month=mon_tuple[1], ExportDate__year=mon_tuple[0],
                                 driver__id=driver_id)
            driver_orders = models.Order.objects.filter(**orders_filter)\
                .order_by('-ExportDate', '-ExportTime')

            # Prepare resp
            period = "".join([str(x) for x in mon_tuple])
            name = MONTH_NAMES[mon_tuple[1]] + ' ' + str(mon_tuple[0])
            total = get_salary_total(driver_orders)
            order_list = get_orders_for_history(driver_orders)

            # add month result to response object
            res.append({'period': period,
                        'name': name,
                        'total': total,
                        'total_count': len(driver_orders),
                        'order_list': order_list
                        })

        resp = {'message': 'OK', 'data': res}

        return Response(resp)


class OrderDriverActionView(APIView):
    """
    Меняет статус водителя из приложения
    """
    @get_driver_by_session
    def post(self, request, **kwargs):

        app_data = json.loads(request.data).get('data')
        if app_data:
            updated_req_data = deepcopy(app_data)
            updated_req_data.update({'driver': kwargs['driver'],
                                    'order': app_data['order_id']})
            serializer = OrderApiSerializer(data=updated_req_data)
            if serializer.is_valid():
                serializer.save()
                act = app_data['action']
                try:
                    m.Order.objects.filter(pk=app_data['order_id']).update(last_driver_action=act)
                except Exception as e:
                    logger.error(e, exc_info=True)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                errors = serializer.errors
        else:
            errors = {'error': 'не передан параметр data'}

        return Response(errors, status=status.HTTP_400_BAD_REQUEST)


class FireBaseTokenView(APIView):
    """
    Сохраняем токен приложения
    """
    @get_driver_by_session
    def post(self, request, **kwargs):

        if isinstance(request.data, str):
            token = json.loads(request.data).get('token', '')
        else:
            token = request.data.get('token', '')

        if len(token) > 0:
            driver = kwargs['driver']
            driver.app_token = token
            driver.save()
            resp = {'message': 'OK'}
            resp_status = 200
        else:
            resp = {'error': 'token not found'}
            resp_status = 400

        return Response(resp, status=resp_status)
