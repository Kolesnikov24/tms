from os import getenv
from django.utils import timezone
from pytz import timezone as py_timezone

MONTH_LIST_RUS = ['Январь', 'Февраль', 'Март', 'Апрель',
                  'Май', 'Июнь', 'Июль', 'Август',
                  'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']

MONTH_LIST_ENG = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']

MONTH_LIST_LOWER = ['января', 'февраля', 'марта', 'апреля',
                    'мая', 'июня', 'июля', 'августа',
                    'сентября', 'октября', 'ноября', 'декабря']

MONTH_NAMES = ['_', 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
               'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']

APP_NAME = getenv('APP_NAME', 'tms')

LOGIN_URL = '/' + APP_NAME + '/auth/login'

API_LOGIN_URL = '/login'

DENY_URL = '/' + APP_NAME + '/deny'

DATE_FORMAT = '%d.%m.%Y'

DATETIME_FORMAT = '%d.%m %H:%M'

DATE_INPUT_FORMATS = ('%d.%m.%Y',)

TIME_FORMATS = ('%d.%m %H:%M', '%d.%m.%Y %H:%M')

TIME_FORMAT = '%H:%M:%S'

DT_FORMAT = '%Y.%m.%d %H:%m'

NSK_PREFIX = 'Новосибирская обл, Новосибирский р-н, '

VALIDATE_ERROR = 'Ошибка валидации'

TIME_ZONE = timezone.get_current_timezone()

MAX_DISPLAY_LENGTH = 9999

LOCAL_TIMEZONE = py_timezone('Asia/Novosibirsk')


def get_value(choices, key):
    return dict(choices)[key]


def get_add_service(key):
    return get_value(ADD_SERVICE_CODES, key)


def get_operation(key):
    return get_value(OPERATION_CHOICES, key)


def get_status(key):
    return get_value(STATUS_CHOICES, key)


def get_bill_status(key):
    return get_value(BILL_STATUS_CHOICES, key)


def get_driver_type(key):
    return get_value(DRIVER_TYPE_CHOICES, key)


def get_zone(key):
    return get_value(ZONE_CHOICES, key)


def get_coast(key):
    return get_value(COAST_CHOICES, key)


def get_app_action(key):
    return get_value(ORDER_DRIVER_STATUS, key)


OPERATION_CHOICES = (
    (1, 'погр.'),
    (2, 'выгр.'),
    (3, 'пор.'),
)

BILL_STATUS_CHOICES = (
    (1, 'Заготовка'),
    (2, 'Проверен'),
    (3, 'Отправлен'),
    (4, 'Согласован'),
    (5, 'Оплачен'),
    (6, 'Отклонен'),
    (7, 'Выставлен'),
)

FOOT_CHOICES = (
    (20, '20'),
    (40, '40'),
)

BOOL_CHOICES = (
    (0, 'Нет'),
    (1, 'Да'),
)

ACTS_CHOICES = (
    (0, 'Только указанного клиента'),
    (1, 'Всех клинетов'),
)

STATUS_CHOICES = (
    (1, 'Заготовка'),
    (2, 'Согласование'),
    (3, 'Согласовано'),
    (4, 'Выполняется'),
    (5, 'Завершено'),
    (6, 'Отказ'),
    (7, 'Отмена'),
    (8, 'Сдан'),
    (9, 'Проверено'),
)

ORDER_DRIVER_STATUS = (
    (0, 'не назначен'),
    (1, 'назначен'),
    (2, 'принято'),
    (3, 'на терминале'),
    (4, 'поставлен'),
    (5, 'у клиента'),
    (6, 'погружен'),
    (7, 'сдается'),
    (8, 'сдан'),
)

DRIVER_TYPE_CHOICES = (
    ('O', 'Наш'),
    ('F', 'Получастник'),
    ('P', 'Частник'),
    ('S', 'Партнер'),

)

ADD_SERVICE_TYPES = (
    ('R', 'Выручка'),
    ('C', 'Затраты'),
)

ADD_SERVICE_CODES = (
    ('forwarding', 'Экспедирование'),
    ('cleaning', 'Зачистка'),
    ('stamp', 'Пломба'),
)

PAYMENT_DIRECTION = (
    ('salary', 'Водитель'),
    ('client', 'Клиент'),
    ('company', 'Компания'),
)

DirectionTaxes = (
    ('P', 'Платон'),
)
DRIVER_STATUS = (
    ('A', 'Работает'),
    ('D', 'Уволен'),
    ('I', 'Не активен'),
)

CLIENT_STATUS = (
    ('A', 'Действующий'),
    ('I', 'Не активен'),
)

ZONE_CHOICES = (
    (1, 'Восток'),
    (2, 'Клещиха'),
)

RAYWAY_TARIFF = (
    (1, 'переезд по одному берегу'),
    (2, 'переезд по одному берегу+переезд на другой берег'),
    (3, 'два переезда по одному берегу'),
)

COAST_CHOICES = (
    (1, 'Левый'),
    (2, 'Правый'),
)

DIRECTION_CHOICES = (
    ('city', 'Город'),
    ('region', 'Пригород'),
    ('country', 'Межгород'),
    ('terminal', 'Терминал'),
)

SCHEDULE_CHOICES = (
    ('Б', 'Больничный'),
    ('Р', 'Ремонт'),
    ('О', 'Отпуск'),
    ('Н', 'Не планировать'),
)

CONFIRM_CODE_STATUSES = (
    (1, 'Новый'),
    (2, 'Отправлен'),
    (3, 'Подвержден'),
    (4, 'Истек срок действия')
)
