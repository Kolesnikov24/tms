import copy
import json
from collections import defaultdict, namedtuple
from datetime import datetime
from os import path, remove

from django.contrib.auth.decorators import permission_required, login_required
from django.core.serializers.json import DjangoJSONEncoder
from django.db import connection
from django.utils import timezone
from django.http import JsonResponse
from django.shortcuts import render, redirect

from .sql import clint_prices, PRICES_FOR_NON_ACTIVE_CLIENTS, driver_prices
from .prices import PriceLoader
from .tools import get_user_menu_tabs, check_client, get_available_client, has_privs
from .models import (Driver, Truck, Direction, Trailer, Client, Order, PaymentsReport, OrderTemplate,
                     OrderHistory, DispZone, OrderDriverAction, UserProfile, Option)
from .forms import (FeedbackForm, OrderForm, OrderWorkForm,
                    PaymentsReportForm, ClientPriceForm, DriverPriceForm)
from .constants import (SCHEDULE_CHOICES, BILL_STATUS_CHOICES, APP_NAME, DENY_URL, LOGIN_URL,
                        MONTH_LIST_RUS, MONTH_LIST_ENG, TIME_ZONE, DATE_FORMAT, TIME_FORMAT, get_app_action)


APP_TITLE = Option.objects.filter(code='APP_TITLE').first()


def app_render(request, template, user_context):
    context = {
        'APP_NAME': APP_NAME,
        'APP_TITLE': APP_TITLE.value,
        'menu_items': get_user_menu_tabs(request)
    }
    context.update(user_context)
    return render(request, template, context)


def landing_search(request):
    context = {}
    if request.method == 'POST':
        order = Order.objects \
            .filter(ContainerNum=request.POST.get('cn', '')) \
            .order_by('-ExportDate') \
            .first()
        if order:
            context = {
                "order": order,
                "driver_action": order.get_driver_action(),
            }

    return render(request, 'landing/search.html', context)


def landing(request):
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if not form.is_valid():
            message = form.errors
        else:
            form.save()
            message = "Сообщение отправлено. Мы свяжемся с Вами в ближайшее время!"
    else:
        message = ""

    return render(request, 'landing/index.html', {
        "feedback_message": message
    })


def deny(request):
    return app_render(request, 'TrackManager/deny.html', {})


def possible_actions():
    # Define possible object condition an simplest workflow
    actions = {
        'Отклонена': [('ShowHistory', 'Просмотр истории'), ],
        'Новая': [('accept', 'Согласовать'), ('RequestDeny', 'Отказать')],
        'Согласована': [('RequestAssignAuto', 'Назначить авто'), ('RequestUnapprove', 'Отмена согласования'),
                        ('RequestDeny', 'Отказать')],
        'Машина назначена': [('RequestDeny', 'Отказать'), ('RequestUnassignAuto', 'Снять авто')]
    }
    return actions


@permission_required('TrackManager.manager', login_url=DENY_URL)
@login_required(login_url=LOGIN_URL)
def tasks(request):
    clients = get_available_client(request)
    initial = {'Manager': request.user}
    if clients.count() == 1:
        initial['Client'] = clients[0]

    f = OrderForm(initial=initial)
    f.fields["Client"].queryset = clients
    order_templates = OrderTemplate.objects.filter(create_by=request.user)

    orders = []
    context = {'orders': orders,
               'clients': clients,
               'order_templates': order_templates,
               'form': f,
               'edit_form': '',
               'order_actions': possible_actions()
               }
    return app_render(request, 'TrackManager/manager.html', context)


@permission_required('TrackManager.manager_calc', login_url=DENY_URL)
@login_required(login_url=LOGIN_URL)
def manager_calc(request):
    f = OrderWorkForm()
    is_client = check_client(request)

    drivers = Driver.objects.filter(Status='A').order_by('FIO')
    trucks = Truck.objects.filter(status='A')
    directions = Direction.objects.filter(Type='city').exclude(id__in=[59, 16]).order_by("Name")
    profile = UserProfile.objects.get(user=request.user)

    # TODO: перенести в access_control
    # Передаем в контект роль и доступные поля
    table_fields = [i[0] for i in profile.info_profile.table_fields.all().values_list('field_name', )]
    table_fields_json = json.dumps(table_fields, cls=DjangoJSONEncoder)
    user_roles = [i[0] for i in request.user.groups.values_list('name', )]
    user_roles_json = json.dumps(user_roles, cls=DjangoJSONEncoder, ensure_ascii=False)

    hide_act_buttons = 1
    hide_colvis_buttons = 1
    is_dispatcher_v = False

    if 'Диспетчер Восток' in user_roles or 'Менеджер' in user_roles:
        is_dispatcher_v = True

    if 'Администратор' in user_roles or 'Руководитель' in user_roles:
        hide_act_buttons = 0
        hide_colvis_buttons = 0

    if 'Бухгалтер' in user_roles or 'Менеджер Офис':
        hide_act_buttons = 0

    if 'Менеджер Клещиха' in user_roles:
        is_client = 0

    context = {'form': f,
               'clients': get_available_client(request, calc_module=True),
               'drivers': drivers,
               'trucks': trucks,
               'directions': directions,
               'country_directions': Direction.objects.filter(Type='country').exclude(id__in=[16]).order_by("Name"),
               'table_fields': table_fields_json,
               'is_dispatcher_v': is_dispatcher_v,
               'hide_act_buttons': hide_act_buttons,
               'hide_colvis_buttons': hide_colvis_buttons,
               'user_roles': user_roles_json,
               'is_client': is_client,
               'hidden_prop': 'hidden' if is_dispatcher_v else '',
               }
    return app_render(request, 'TrackManager/manager-calc.html', context)


@permission_required('TrackManager.payments', login_url=DENY_URL)
@login_required(login_url=LOGIN_URL)
def payments(request):
    if request.method == 'POST':
        # Переопределим FK переменные
        form_data = copy.copy(request.POST)
        form_data['driver'] = Driver.objects.get(FIO__contains=form_data['driver']).pk
        form = PaymentsReportForm(form_data)
        instance = PaymentsReport.objects.get(driver=form_data['driver'], month=form_data['month'])
        if form.is_valid():
            form = PaymentsReportForm(form_data, instance=instance)
            form.save()
        return JsonResponse({'id': "ok"}, safe=False)

    # Неправильный порядок после миграции
    table_headers = ['ID', 'Расчетный период', 'Водитель', 'Машины', 'Начислено',
                     'Доплаты', 'Ремонт', 'ДТ', 'Газпром', 'Симбиоз', 'Аванс', 'Карта', 'Налог',
                     'Кредит', 'Гараж', 'Долг', 'ИТОГО']
    context = {
        'table_headers': table_headers,
        'form': PaymentsReportForm()
    }

    return app_render(request, 'TrackManager/payments.html', context)


@permission_required('TrackManager.dispatcher', login_url=DENY_URL)
@login_required(login_url=LOGIN_URL)
def dispatcher(request):
    trailers = Trailer.objects.select_related('car').filter(status='A')
    trailers_sort = sorted(trailers, key=lambda t: t.car.number[1:4])

    drivers = Driver.objects.filter(Status='A').order_by('FIO')
    clients = Client.objects.filter(status='A').order_by('Name')
    context = {'trailers': trailers_sort,
               'drivers': drivers,
               'clients': clients,
               'order_actions': possible_actions()
               }
    return app_render(request, 'TrackManager/dispatcher.html', context)


def not_found(request):
    return render(request, template_name='TrackManager/404.html', status=404)


def internal_error(request):
    return render(request, template_name='TrackManager/500.html', status=500)


@login_required(login_url=LOGIN_URL)
def hist(request):
    h = []
    orders = Order.objects.filter(Number=request.POST['number'])
    for order in orders:
        data = OrderHistory.objects.filter(Order=Order.objects.get(pk=order.pk))
        if request.method == 'POST':
            for i in data:
                h.append(
                    {
                        'date': i.Actiontime.strftime('%Y-%d-%m %H:%m'),
                        'manager': str(i.Manager),
                        'status': i.NewValue
                    }
                )

    context = {
        'menu_items': get_user_menu_tabs(request),
        'hist': h
    }
    return render(request, 'TrackManager/info.html', context)


@login_required(login_url=LOGIN_URL)
def info(request):
    h = []
    context = {
        'menu_items': get_user_menu_tabs(request),
        'hist': h
    }
    return render(request, 'TrackManager/info.html', context)


@permission_required('TrackManager.maps', login_url=DENY_URL)
@login_required(login_url=LOGIN_URL)
def maps(request):
    context = {
        'APP_NAME': APP_NAME,
        'APP_TITLE': APP_TITLE.value,
        'menu_items': get_user_menu_tabs(request),
    }
    return render(request, 'TrackManager/maps.html', context)


@permission_required('TrackManager.maps', login_url=DENY_URL)
@login_required(login_url=LOGIN_URL)
def ymaps(request):
    context = {
        'APP_NAME': APP_NAME,
        'APP_TITLE': APP_TITLE.value,
        'menu_items': get_user_menu_tabs(request),
    }
    return render(request, 'TrackManager/ya_maps.html', context)


@login_required(login_url=LOGIN_URL)
def schedule(request):
    table_header = ['#', 'Машина', 'Прицеп', 'Ф. И. О.', 'Фут', 'Зона', 'Контейнер', 'Забор', 'Сдача', 'Адрес', 'Район',
                    'Переезд', 'Опер.', 'Время', 'Клиент', 'Грузополучатель', 'Статус', 'Рейсов за месяц', 'Примечание']
    trucks = Truck.objects.filter(status='A')
    drivers = Driver.objects.filter(Status='A').order_by('FIO')

    return app_render(request, 'TrackManager/schedule.html', {
        'range': table_header,
        'trucks': trucks,
        'drivers': drivers,
        'schedule_choices': SCHEDULE_CHOICES,
    })


@login_required(login_url=LOGIN_URL)
def salary(request):
    driver_list = defaultdict(list)
    for driver in Driver.objects.all():
        for i in range(1, 33):
            driver_list[driver.FIO].append(i)

    return app_render(request, 'TrackManager/salary.html',
                      {'range': [i - 3 for i in range(0, 40)],
                       'driver_list': driver_list,
                       'drivers': Driver.objects.filter(Status='A')})


@login_required(login_url=LOGIN_URL)
def acts(request):
    return app_render(request, 'TrackManager/acts.html', {
        'clients': list(get_available_client(request)),
        'bill_statuses': BILL_STATUS_CHOICES,
    })


@login_required(login_url=LOGIN_URL)
def reports_page(request):
    if not has_privs(request, 'reports_page'):
        return redirect('deny')

    return app_render(request, 'TrackManager/reports.html', {
        'clients': Client.objects.all().order_by('Name'),
        'mon': MONTH_LIST_RUS,
        'mon2': MONTH_LIST_ENG,
        'trucks': Truck.objects.all()
    })


@login_required(login_url=LOGIN_URL)
def app_report(request):
    context = {
        'drivers': Driver.objects.filter(Status='A')
    }
    return app_render(request, 'TrackManager/app_report.html', context)


@login_required(login_url=LOGIN_URL)
def tariff_check(request):
    clients = get_available_client(request)
    initial = {'Manager': request.user,
               'Client': clients[0],
               'dz': 1
               }
    f = OrderForm(initial=initial)
    f.fields["Client"].queryset = clients

    context = {
        'clients': clients,
        'init_client_id': clients[0].pk,
        'is_client': check_client(request),
        'city_directions': Direction.objects.filter(Type='city').exclude(id__in=[16]).order_by("Name"),
        'region_directions': Direction.objects.filter(Type='region').exclude(id__in=[16]).order_by("Name"),
        'country_directions': Direction.objects.filter(Type='country').exclude(id__in=[16]).order_by("Name"),
        'form': f
    }
    template = 'TrackManager/check_tariff.html'
    return app_render(request, template, context)


@login_required(login_url=LOGIN_URL)
def client_price(request):
    if request.method == 'POST':
        form = ClientPriceForm(request.POST, request.FILES)
        if form.is_valid():
            price_file = request.FILES['price']
            with open('client_price.xlsx', 'wb+') as destination:
                for chunk in price_file.chunks():
                    destination.write(chunk)

            pl = PriceLoader('client_price.xlsx',
                             DispZone.objects.get(pk=int(form.data['dz'])),
                             datetime.strptime(form.data['date_from'], '%d.%m.%Y').date()
                             )
            pl.load_client_price(Client.objects.get(pk=int(form.data['client'])))

            if path.isfile('client_price.xlsx'):
                remove('client_price.xlsx')

    query_filter = {
        '=1': None,
        '>1': None
    }
    for sql_filter in query_filter:
        with connection.cursor() as cursor:
            cursor.execute(clint_prices(sql_filter))
            db_rows = cursor.fetchall()

        names = namedtuple('Record', 'client_id,client_Name,dz_id_min,dz_id_max,dateFrom')
        query_filter[sql_filter] = list(map(names._make, db_rows))

    with connection.cursor() as cursor:
        cursor.execute(PRICES_FOR_NON_ACTIVE_CLIENTS)
        db_rows = cursor.fetchall()

    names = namedtuple('Record', 'client_id,client_Name,dz_id_min,dz_id_max,dateFrom')
    prices_for_non_active = list(map(names._make, db_rows))

    context = {
        'form': ClientPriceForm(),
        'prices': query_filter['=1'],
        'prices_na': prices_for_non_active,
        'prices_arc': query_filter['>1']
    }

    return app_render(request, 'TrackManager/client_price.html', context)


@login_required(login_url=LOGIN_URL)
def driver_price(request):
    if request.method == 'POST':
        form = DriverPriceForm(request.POST, request.FILES)
        if form.is_valid():
            price_file = request.FILES['price']
            tmp_name = 'driver_price.xlsx'
            with open(tmp_name, 'wb+') as destination:
                for chunk in price_file.chunks():
                    destination.write(chunk)

            pl = PriceLoader(tmp_name,
                             DispZone.objects.get(pk=int(form.data['dz'])),
                             datetime.strptime(form.data['date_from'], '%d.%m.%Y').date()
                             )
            pl.load_driver_price(form.data['driver_type'], form.data.get('driver'))

            if path.isfile(tmp_name):
                remove(tmp_name)

    query_filter = {
        '=1': None,
        '>1': None
    }
    for sql_filter in query_filter:
        with connection.cursor() as cursor:
            cursor.execute(driver_prices(sql_filter))
            db_rows = cursor.fetchall()

        names = namedtuple('Record', 'dt,driver_type,dz_id_min,dz_id_max,dateFrom')
        query_filter[sql_filter] = list(map(names._make, db_rows))

    return app_render(request, 'TrackManager/driver_price.html', {
        'form': DriverPriceForm(),
        'prices': query_filter['=1'],
        'drivers': Driver.objects.filter(Status='A'),
        'prices_arc': query_filter['>1']
    })


def driver_actions_modal(request):
    order_id = request.GET.get('order_id', 1)
    data = []
    prev_action_time = datetime.today()
    for ind, obj in enumerate(OrderDriverAction.objects.filter(order__id=order_id).order_by('action_time')):
        if ind > 0 and obj.action not in (1, 3, 5, 7):
            td = (obj.action_time - prev_action_time)
            hours, remainder = divmod(td.seconds, 3600)
            minutes, _ = divmod(remainder, 60)
            duration = str(hours) + ' ч. ' + str(minutes) + ' мин.'
        else:
            duration = ""

        local_time = timezone.localtime(obj.action_time, TIME_ZONE)
        item = {
            "status": get_app_action(obj.action),
            "action_date": local_time.strftime(DATE_FORMAT),
            "action_time": local_time.strftime(TIME_FORMAT),
            "duration": duration
        }
        prev_action_time = obj.action_time
        data.append(item)

    return render(request, 'TrackManager/driver_report_modal.html', {
        'actions': data
    })
