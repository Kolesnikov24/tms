from collections import defaultdict, namedtuple
from datetime import datetime, date, timedelta
from itertools import groupby
from pytz import timezone

from django.db import connection
from django.core import serializers
from django.http import JsonResponse
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.contrib.auth.decorators import login_required

from .sql import SQLProvider, TOTAL_DRIVERS, CLIENTS_TOTAL, schedule_request, app_report
from .api import calc_summary
from .constants import (LOGIN_URL, NSK_PREFIX, MAX_DISPLAY_LENGTH, LOCAL_TIMEZONE,
                        get_operation, get_status, get_zone, get_bill_status)
from .tools import local_date, check_client, get_available_client, xstr
from .models import (Order, Salary, Driver, UserLocation, Truck, ClientBill,
                     AddDirectionTaxes, Direction, PaymentsReport)


class ManagerOrderListJson(BaseDatatableView):
    model = Order
    columns = ['Number', 'Status', 'ExportDate', 'Client.Name', 'Customer', 'Foot', 'Weight', 'Operation',
               'TerminalLoad', 'Address', 'District', 'TerminalDeliver', 'truck']
    order_columns = ['row_num', 'Number', '', 'Status', '',
                     'ExportDate', 'Client.Name', '', '', '',
                     '', 'Foot', 'Weight', 'Operation',
                     'TerminalLoad', '', 'District']
    max_display_length = MAX_DISPLAY_LENGTH
    tz = LOCAL_TIMEZONE

    def get_initial_queryset(self):
        date_start = self.request.POST.get('date_start', date.today())
        date_end = self.request.POST.get('date_end', date.today())
        if not isinstance(date_start, date):
            date_start = datetime.strptime(date_start, '%Y-%m-%d').date()
        if not isinstance(date_end, date):
            date_end = datetime.strptime(date_end, '%Y-%m-%d').date()
        if date_start == date_end:
            date_end = date_start + timedelta(days=1)
        else:
            date_end = date_end + timedelta(days=1)

        date_start = self.tz.localize(datetime.combine(date_start, datetime.min.time()))
        date_end = self.tz.localize(datetime.combine(date_end, datetime.min.time()))

        # определяем фильтр по статусам
        status_ids = self.request.POST.get('status_ids')
        if status_ids is None:
            status_ids = [1, 2, 3, 4, 5, 6, 7, 8]
        else:
            status_ids = status_ids.split(',')

        params = dict(ExportDate__gte=date_start,
                      ExportDate__lt=date_end,
                      Client__in=get_available_client(self.request),
                      Status__in=status_ids)

        qs = Order.objects.filter(**params).prefetch_related('orderdriveraction_set')

        return qs

    def filter_queryset(self, qs):
        # use parameters passed in GET request to filter queryset

        # simple example:
        search = self.request.POST.get('search[value]', None)
        if search:
            qs = qs.filter(Foot__istartswith=search)

        # more advanced example using extra parameters
        filter_client = self.request.POST.get('columns[4][search][value]', None)
        if filter_client:
            qs = qs.filter(Client_id=filter_client)

        filter_foot = self.request.POST.get('columns[11][search][value]', None)
        if filter_foot:
            qs = qs.filter(Foot=filter_foot)

        return qs

    def prepare_results(self, qs):
        json_data = []
        row_num = 1
        for item in qs:
            status = get_status(item.Status)
            json_data.append({
                'row_num': row_num,
                'DT_RowId': 'request_' + str(item.id),
                'Number': item.Number,
                'action': '',
                'Status': status + "<br>" + item.decline_reason if status == 'Отказ' else status,
                'ExportDate': item.ExportDate.astimezone(self.tz).strftime('%d.%m.%Y %H:%M'),
                'Client': item.Client.Name,
                'Customer_old': '' if item.Customer is None or item.Customer == 'None' else item.Customer,
                'Customer': '' if item.customer_new is None else item.customer_new.name,
                'CustomerId': item.customer_new.pk,
                'Foot': item.Foot,
                'Weight': item.Weight,
                'Operation': get_operation(item.Operation),
                'TerminalLoad': item.AddressLoad.replace(NSK_PREFIX, '')
                if item.TerminalLoad.Name == "Прочее" else item.TerminalLoad.Name,
                'TerminalDeliver': item.AddressDeliver.replace(NSK_PREFIX, '')
                if item.TerminalDeliver.Name == "Прочее" else item.TerminalDeliver.Name,
                'Address': '' if item.Address is None or item.Address == 'None' else item.Address.replace(
                    NSK_PREFIX, ''),
                'District': '' if item.District is None or item.District == 'None' else item.District,
                'RaywayCr': item.RaywayCr,
                'RaywayCr2': item.RaywayCr2,
                'RaywayCrDistrict': xstr(item.RaywayCrDistrict),
                'RaywayCr2District': xstr(item.RaywayCr2District),
                'truck': xstr(item.truck),
                'Responsible': item.Responsible,
                'ContainerNum': item.ContainerNum,
                'recognized_container_num': item.recognized_container_num or '',
                'Comment': item.Comment,
                'CargoName': item.CargoName,
                'Proxy': item.Proxy,
                'ClientId': item.Client_id,
                'dzId': item.dz_id,
                'OperationId': item.Operation,
                'driver': xstr(item.driver),
                'driver_action': item.get_driver_action(),
                'driver_phone': '' if item.driver is None else item.driver.Phone,
                'auto_visit_take_plan': item.auto_visit_take_plan.strftime(
                    '%d.%m %H:%M') if item.auto_visit_take_plan else '',
                'auto_visit_take_fact': item.auto_visit_take_fact.strftime(
                    '%d.%m %H:%M') if item.auto_visit_take_fact else '',
                'auto_visit_take_num': item.auto_visit_take_num or '',
                'auto_visit_return_plan': item.auto_visit_return_plan.strftime(
                    '%d.%m %H:%M') if item.auto_visit_return_plan else '',
                'auto_visit_return_fact': item.auto_visit_return_fact.strftime(
                    '%d.%m %H:%M') if item.auto_visit_return_fact else '',
                'auto_visit_return_num': item.auto_visit_return_num or '',
                'TerminalLoadId': item.TerminalLoad_id,
                'TerminalDeliverId': item.TerminalDeliver_id,
                'approve_date': '' if item.approve_date is None else item.approve_date.astimezone(self.tz).strftime(
                    '%d.%m %H:%M'),
                'dispatcher_approve': '' if item.dispatcher_approve is None else '{0} {1}'.format(
                    item.dispatcher_approve.first_name, item.dispatcher_approve.last_name),
                'destination_station': item.destination_station,
                'cargo_manager': item.cargo_manager,
                'attachments': serializers.serialize('json', item.orderdocuments_set.filter(is_client_doc=False)) or [],
                'attachments_client': serializers.serialize('json',
                                                            item.orderdocuments_set.filter(is_client_doc=True)) or []
            })
            row_num = row_num + 1
        return json_data


class DispatcherOrderListJson(BaseDatatableView):
    model = Order
    columns = ['ExportDate', 'ExportTime', 'Client', 'Customer', 'Proxy', 'ContainerNum', 'Foot', 'Weight',
               'CargoName', 'Operation', 'Address',
               'District', 'TerminalLoad', 'TerminalDeliver', 'AddressLoad', 'AddressDeliver', 'RaywayCr', 'RaywayCr2',
               'Responsible', 'Comment', 'truck', 'trailer', 'driver']
    order_columns = ['ExportDate', 'pk']


class OrdersExperimental(BaseDatatableView):
    model = Order
    columns = ['Number', 'ExportDate', 'ExportTime', 'Client', 'Customer', 'Foot', 'ContainerNum',
               'Weight', 'Operation', 'Proxy', 'TerminalLoad', 'TerminalDeliver', 'Address', 'District',
               'RaywayCr', 'RaywayCr2', 'Responsible', 'Status', 'truck', 'trailer', 'driver',
               'dispatch_date', 'approve_date', 'decline_date', 'correction_date', 'Comment',
               'dispatcher_approve', 'dz']
    order_columns = ['', 'Number', '', '', 'ExportTime', 'Client__shortname', 'Customer', 'Foot', '', '',
                     'Weight', 'Operation', '', 'TerminalLoad', '', 'District',
                     '', 'TerminalDeliver', '', 'Status', '', '', 'driver__FIO',
                     'dispatch_date', 'approve_date', '', '', '', '', '', 'dz']
    max_display_length = MAX_DISPLAY_LENGTH

    tz = LOCAL_TIMEZONE

    def get_initial_queryset(self):
        # корректируем введенную дату
        # date_min = (date.today().replace(day=1) - timedelta(days=2)).replace(day=1)
        # date_max = date.today() + timedelta(days=100)
        date_start = self.request.POST.get('date_start', date.today())
        date_end = self.request.POST.get('date_end', date.today())
        if not isinstance(date_start, date):
            date_start = datetime.strptime(date_start, '%Y-%m-%d').date()
        if not isinstance(date_end, date):
            date_end = datetime.strptime(date_end, '%Y-%m-%d').date()
        if date_start == date_end:
            date_end = date_start + timedelta(days=1)
        else:
            date_end = date_end + timedelta(days=1)

        date_start = self.tz.localize(datetime.combine(date_start, datetime.min.time()))
        date_end = self.tz.localize(datetime.combine(date_end, datetime.min.time()))

        # определяем фильтр по статусам
        status_ids = self.request.POST.get('status_ids')
        if status_ids is None:
            status_ids = [1, 2, 3, 4, 5, 6, 7, 8]
        else:
            status_ids = status_ids.split(',')

        # Условия запрооса
        query_filters = dict(ExportDate__gte=date_start,
                             ExportDate__lt=date_end,
                             Status__in=status_ids)

        # Определяем зону диспетчеризации
        user_dz = UserLocation.objects.filter(user=self.request.user)
        if user_dz.count() > 0:
            query_filters['dz'] = user_dz[0].zone

        return Order.objects.filter(**query_filters).prefetch_related('orderdriveraction_set')

    def filter_queryset(self, qs):
        # use parameters passed in GET request to filte queryset

        # simple example:
        search = self.request.POST.get('search[value]', None)
        if search:
            qs = qs.filter(Foot__istartswith=search)

        # more advanced example using extra parameters
        filter_client = self.request.POST.get('columns[3][search][value]', None)
        if filter_client:
            qs = qs.filter(Client_id=filter_client)

        filter_foot = self.request.POST.get('columns[5][search][value]', None)
        if filter_foot:
            qs = qs.filter(Foot=filter_foot)

        filter_dz = self.request.POST.get('columns[25][search][value]', None)
        if filter_dz:
            qs = qs.filter(dz_id=filter_dz)

        return qs

    def prepare_results(self, qs):
        json_data = []
        row_num = 1
        for item in qs:
            json_data.append({
                'row_num': row_num,
                'DT_RowId': 'request_' + str(item.id),
                'Number': item.Number,
                'action': '',
                # 'ExportDate': '{0} {1}'.format(item.ExportDate.strftime('%d.%m'), item.ExportTime.strftime('%H:%M')),
                'ExportDate': item.ExportDate.astimezone(self.tz).strftime('%d.%m %H:%M'),
                'Client': item.Client.Name,
                'Customer_old': '' if item.Customer is None or item.Customer == 'None' else item.Customer,
                'Customer': '' if item.customer_new is None else item.customer_new.name,
                'Foot': item.Foot,
                'ContainerNum': item.ContainerNum,
                'Weight': item.Weight,
                'Operation': get_operation(item.Operation),
                'Proxy': item.Proxy,
                'TerminalLoad': item.AddressLoad.replace(NSK_PREFIX, '')
                if item.TerminalLoad.Name == "Прочее" else item.TerminalLoad.Name,
                'TerminalDeliver': item.AddressDeliver.replace(NSK_PREFIX, '')
                if item.TerminalDeliver.Name == "Прочее" else item.TerminalDeliver.Name,
                'Address': '' if item.Address is None or item.Address == 'None'
                else item.Address.replace(NSK_PREFIX, ''),
                'District': '' if item.District is None or item.District == 'None' else item.District,
                'RaywayCr': item.RaywayCr,
                'RaywayCr2': item.RaywayCr2,
                'Responsible': item.Responsible,
                'Manager': item.Manager.first_name + " " + item.Manager.last_name,
                'Status': get_status(item.Status),
                'truck': '' if item.truck is None else item.truck.number,
                'trailer': '' if item.trailer is None else item.trailer.number,
                'driver': '' if item.driver is None else str(item.driver),
                'driver_phone': '' if item.driver is None else item.driver.Phone,
                'driver_action': item.get_driver_action(),
                'dispatch_date': '' if item.dispatch_date is None else '{0} {1} {2}'.format(
                    item.dispatch_date.astimezone(self.tz).strftime('%d.%m %H:%M'), item.Manager.first_name,
                    item.Manager.last_name),
                'approve_date': '' if item.approve_date is None else item.approve_date.astimezone(self.tz).strftime(
                    '%d.%m %H:%M'),
                'decline_date': '' if item.decline_date is None else item.decline_date.astimezone(self.tz).strftime(
                    '%d.%m %H:%M'),
                'correction_date': '' if item.correction_date is None else item.correction_date.astimezone(
                    self.tz).strftime('%d.%m %H:%M'),
                'Comment': item.Comment,
                'dispatcher_approve': '' if item.dispatcher_approve is None else '{0} {1}'.format(
                    item.dispatcher_approve.first_name, item.dispatcher_approve.last_name),
                'dispatcher_decline': '' if item.dispatcher_decline is None else '{0} {1}'.format(
                    item.dispatcher_decline.first_name, item.dispatcher_decline.last_name),
                'decline_reason': item.decline_reason,
                'cancel_date': '' if item.cancel_date is None else item.cancel_date.astimezone(self.tz).strftime(
                    '%d.%m %H:%M'),
                'cancel_reason': item.cancel_reason,
                'dz': item.dz_id,
                'auto_visit_take_plan': item.auto_visit_take_plan.strftime(
                    '%d.%m %H:%M') if item.auto_visit_take_plan else '',
                'auto_visit_take_fact': item.auto_visit_take_fact.strftime(
                    '%d.%m %H:%M') if item.auto_visit_take_fact else '',
                'auto_visit_take_num': item.auto_visit_take_num or '',
                'auto_visit_return_plan': item.auto_visit_return_plan.strftime(
                    '%d.%m %H:%M') if item.auto_visit_return_plan else '',
                'auto_visit_return_fact': item.auto_visit_return_fact.strftime(
                    '%d.%m %H:%M') if item.auto_visit_return_fact else '',
                'auto_visit_return_num': item.auto_visit_return_num or '',
                'destination_station': item.destination_station,
                'cargo_manager': item.cargo_manager,
                'dispatcher_comment': item.dispatcher_comment or '',
                'recognized_container_num': item.recognized_container_num or '',
                'attachments': serializers.serialize('json', item.orderdocuments_set.filter(is_client_doc=False)) or [],
                'attachments_client': serializers.serialize('json',
                                                            item.orderdocuments_set.filter(is_client_doc=True)) or []
            })
            row_num = row_num + 1
        return json_data


class SalaryReport(BaseDatatableView):
    order_columns = ['employee']

    def get_initial_queryset(self):

        date_start = self.request.POST.get('date_start', date.today().strftime('%Y-%m-%d'))
        month = datetime.strptime(date_start, '%Y-%m-%d').month
        y = datetime.strptime(date_start, '%Y-%m-%d').year
        # Условия запрооса
        query_filters = dict(month=month, year=y)
        qs = Salary.objects.filter(**query_filters)

        return qs

    def filter_queryset(self, qs):
        # use parameters passed in GET request to filte queryset
        filter_dz = self.request.POST.get('dz_id', None)

        filter_driver = self.request.POST.get('driver_id') or None

        if filter_dz:
            # qs = qs.filter(Q(truck__dz__in=filter_dz) | Q(truck__isnull=True)).distinct()
            qs = qs.filter(employee__dz=filter_dz)

        if filter_driver:
            # qs = qs.filter(Q(truck__dz__in=filter_dz) | Q(truck__isnull=True)).distinct()
            qs = qs.filter(employee=filter_driver)

        return qs

    def prepare_results(self, qs):
        filter_dz = self.request.POST.get('dz_id', None)

        # Определяем зону диспетчеризации принудительно
        user_dz = UserLocation.objects.filter(user=self.request.user)

        if user_dz.count() > 0:
            filter_dz = user_dz[0].zone.id

        json_data = []
        truck_list = []
        zone_list = []
        dd = defaultdict(list)
        # fill 31 items for all drivers
        drivers = Driver.objects.filter(truck__dz_id=filter_dz) if filter_dz else Driver.objects.all()
        for item in drivers:
            if qs.filter(employee=item).count() > 0:
                dd[str(item)] = [''] * 31
                item_truck_list = Truck.objects.filter(driver=item, status='A')

                if item_truck_list.count() > 0:
                    dd[str(item)].append(str(item_truck_list[0]))
                    dd[str(item)].append(item.dz.name)
                else:
                    dd[str(item)].append('нет авто')
                    dd[str(item)].append('нет авто')
        for q in qs:
            if q.employee in drivers:
                dd[str(q.employee)][q.day - 1] = q.value

        # prepare vars for columns sum
        common_row_sum = 0
        #  Init sum for columns
        col_sum = ['Итого']
        col_sum.extend([0] * 35)
        col_sum_b = ['Б']
        col_sum_b.extend([0] * 31)
        col_sum_v = ['О']
        col_sum_v.extend([0] * 31)
        col_sum_r = ['Р']
        col_sum_r.extend([0] * 31)
        for key, value in sorted(dd.items(), key=lambda kv: kv[0]):
            truck_list.append(value[-2])
            zone_list.append(value[-1])
            lst = [key]
            lst.extend([v for v in value[:-2]])
            add_row_summ = dict(b=0, v=0, r=0, m=0)
            row_summ = 0
            col_ind = 1
            for item in lst[1:]:
                try:
                    row_summ += int(item)
                    common_row_sum += int(item)
                    col_sum[col_ind] += int(item)
                except Exception:
                    if item == 'Б':
                        col_sum[32] += 1
                        add_row_summ['b'] += 1
                        col_sum_b[col_ind] += 1
                    elif item == 'О':
                        col_sum[33] += 1
                        add_row_summ['v'] += 1
                        col_sum_v[col_ind] += 1
                    elif item == 'Р':
                        col_sum[34] += 1
                        add_row_summ['r'] += 1
                        col_sum_r[col_ind] += 1
                    else:
                        pass
                finally:
                    col_ind += 1
            lst.extend([value for key, value in add_row_summ.items()])
            lst.append(row_summ)
            json_data.append(lst)
        col_sum.append(common_row_sum)
        col_sum.insert(0, '')
        col_sum.insert(1, '')
        col_sum.insert(2, '')
        json_data.append(col_sum)

        # insert truck into result
        for ind, row in enumerate(json_data[:-1]):
            row.insert(0, ind + 1)
            row.insert(1, truck_list[ind])
            row.insert(3, zone_list[ind])

        # add summary
        col_sum_b.insert(0, '')
        col_sum_b.insert(1, '')
        col_sum_b.insert(2, '')
        col_sum_b.extend([''] * 5)
        json_data.append(col_sum_b)

        col_sum_v.insert(0, '')
        col_sum_v.insert(1, '')
        col_sum_v.insert(2, '')
        col_sum_v.extend([''] * 5)
        json_data.append(col_sum_v)

        col_sum_r.insert(0, '')
        col_sum_r.insert(1, '')
        col_sum_r.insert(2, '')
        col_sum_r.extend([''] * 5)
        json_data.append(col_sum_r)

        return json_data


class OrdersCalc(BaseDatatableView):
    model = Order
    order_columns = ['', 'Number', 'Status', '', 'ExportDate', 'driver.FIO', '', 'Client.Name',
                     '', '', 'Foot', 'Weight', 'Operation', 'dz', 'TerminalLoad', '', 'District',
                     '', '', 'TerminalDeliver', '', '', 'additional_wastes',
                     'platon', 'date_begin', 'date_end', 'salary_rate',
                     'salary_idle_minutes', 'salary_idle_price', 'salary_idle_cost', 'salary_transfer',
                     'salary_total', 'client_rate', 'client_idle_minutes', 'client_idle_price',
                     'client_idle_cost', 'client_transfer', 'client_total', 'bill_number',
                     'payment_date', 'profit', 'manager', 'cargo_manager']

    max_display_length = MAX_DISPLAY_LENGTH

    tz = LOCAL_TIMEZONE
    tz_filter = timezone('UTC')

    def get_initial_queryset(self, req=None, params=None):
        data = params or self.request.POST
        # корректируем введенную дату
        date_max = date.today() + timedelta(days=100)
        date_start = data.get('date_start', date.today())
        date_end = data.get('date_end', date_max)
        if not isinstance(date_start, date):
            date_start = datetime.strptime(date_start, '%Y-%m-%d').date()
        if not isinstance(date_end, date):
            date_end = datetime.strptime(date_end, '%Y-%m-%d').date()
        if date_start == date_end:
            date_end = date_start + timedelta(days=1)
        else:
            date_end = date_end + timedelta(days=1)

        date_start = self.tz.localize(datetime.combine(date_start, datetime.min.time()))
        date_end = self.tz.localize(datetime.combine(date_end, datetime.min.time()))

        # определяем фильтр по статусам
        status_ids = data.get('status_ids')
        if status_ids is None:
            status_ids = [4, 5, 8]
        else:
            status_ids = status_ids.split(',')

        # for excel
        if req:
            date_start = req.GET.get('date_start')
            date_end = req.GET.get('date_end')

        # Условия запрооса
        query_filters = dict(ExportDate__gte=date_start,
                             ExportDate__lt=date_end,
                             Status__in=status_ids)

        # Менеджер по отработке видит все заявки, клиент видит все свои заявки
        # обычный менеджер видит свои заявки и заявки своих клиентов
        # manager_groups = ['Менеджер по отработке', 'Менеджер', 'Менеджер Клиента']
        if self.request.user.groups.filter(id=18).exists():
            user_dz = UserLocation.objects.filter(user=self.request.user)[0].zone
            query_filters['dz_id'] = user_dz

        query_filters['Client__in'] = get_available_client(self.request, calc_module=True)
        return Order.objects.filter(**query_filters)

    def filter_queryset(self, qs, params=None):
        data = params or self.request.POST
        filter_client = data.get('columns[5][search][value]', None)
        if filter_client:
            qs = qs.filter(Client_id=filter_client)

        filter_foot = data.get('columns[7][search][value]', None)
        if filter_foot:
            qs = qs.filter(Foot=filter_foot)

        filter_driver = data.get('columns[3][search][value]', None)
        if filter_driver:
            qs = qs.filter(driver_id=filter_driver)

        filter_truck = data.get('columns[4][search][value]', None)
        if filter_truck:
            qs = qs.filter(truck_id=filter_truck)

        filter_dz = data.get('columns[10][search][value]', None)
        if filter_dz:
            qs = qs.filter(dz=filter_dz)

        filter_rec_c = data.get('columns[9][search][value]', None)
        if filter_rec_c:
            qs = qs.filter(manual_input=True)

        filter_district = data.get('columns[13][search][value]', None)

        if filter_district:
            if filter_district == "Межгород":
                d = Direction.objects.exclude(Type='city').values_list("Name")
                qs = qs.filter(District__in=d)
            elif filter_district == "Пригород":
                d = Direction.objects.filter(Type='region').values_list("Name")
                qs = qs.filter(District__in=d)
            else:
                qs = qs.filter(District=filter_district)

        return qs

    def prepare_results(self, qs):
        json_data = []
        sum_platon = 0
        sum_salary_rate = 0
        sum_salary_idle_minutes = 0
        sum_salary_idle_price = 0
        sum_salary_idle_cost = 0
        sum_salary_transfer = 0
        sum_salary_total = 0
        sum_client_rate = 0
        sum_client_idle_minutes = 0
        sum_client_idle_price = 0
        sum_client_idle_cost = 0
        sum_client_transfer = 0
        sum_client_total = 0
        sum_profit = 0

        row_num = 1
        for item in qs:
            platon = AddDirectionTaxes.objects.filter(direction=item.District).first()
            status = get_status(item.Status)
            json_data.append({
                'row_num': row_num,
                'DT_RowId': 'request_' + str(item.id),
                'Number': item.Number,
                'Status': status,
                'ExportDate': item.ExportDate.astimezone(self.tz).strftime('%d.%m %H:%M'),
                'driver': '' if item.driver is None else str(item.driver),
                'driver_action': item.get_driver_action(),
                'truck': '' if item.truck is None else item.truck.number,
                'Client': item.Client.Name,
                'ClientFullName': item.Client.shortname or item.Client.Name,
                'ContainerNum': item.ContainerNum,
                'Foot': item.Foot,
                'Weight': item.Weight,
                'Operation': get_operation(item.Operation),
                'TerminalLoad': item.AddressLoad.replace(NSK_PREFIX, '')
                if item.TerminalLoad.Name == "Прочее" else item.TerminalLoad.Name,
                'Address': '' if item.Address is None or item.Address == 'None' else item.Address.replace(
                    NSK_PREFIX, ''),
                'District': '' if item.District is None or item.District == 'None' else item.District,
                'RaywayCr': item.RaywayCr,
                'RaywayCr2': item.RaywayCr2,
                'TerminalDeliver': item.AddressDeliver.replace(NSK_PREFIX, '')
                if item.TerminalDeliver.Name == "Прочее" else item.TerminalDeliver.Name,
                'Comment': item.Comment,
                'additional_wastes': item.additional_wastes,
                'platon': platon.value if platon else 0,
                'salary_rate': item.salary_rate,
                'salary_idle_minutes': item.salary_idle_minutes,
                'salary_idle_price': item.salary_idle_price,
                'salary_idle_cost': item.salary_idle_cost,
                'salary_transfer': item.salary_transfer,
                'salary_total': item.salary_total,
                'client_rate': "%.2f" % item.client_rate if item.client_rate else 0,
                'client_idle_minutes': item.client_idle_minutes,
                'client_idle_price': item.client_idle_price,
                'client_idle_cost': item.client_idle_cost,
                'client_transfer': item.client_transfer,
                'client_total': "%.2f" % item.client_total if item.client_total else 0,
                'bill_number': "{0};{1}".format(item.bill_number.pk,
                                                str(item.bill_number)) if item.bill_number is not None else ';',
                'payment_date': '' if item.payment_date is None else item.payment_date.astimezone(self.tz).strftime(
                    '%d.%m.%Y'),
                'profit': "%.2f" % item.profit if item.profit else 0,
                'manager': item.Manager.first_name + " " + item.Manager.last_name,
                'date_begin': ''
                if item.operation_date_begin is None
                else item.operation_date_begin.astimezone(self.tz).strftime('%d.%m %H:%M'),
                'date_end': '' if item.operation_date_end is None
                else item.operation_date_end.astimezone(self.tz).strftime('%d.%m %H:%M'),
                'dz': get_zone(item.dz_id),
                'destination_station': item.destination_station,
                'cargo_manager': item.cargo_manager,
                'add_services': serializers.serialize('json', item.orderadditionalservice_set.all(),
                                                      fields=('service_code', 'client_total', 'salary_total')),
                'recognized_container_num': item.recognized_container_num or '',
                'attachments': serializers.serialize('json', item.orderdocuments_set.filter(is_client_doc=False)) or [],
                'attachments_client': serializers.serialize('json',
                                                            item.orderdocuments_set.filter(is_client_doc=True)) or []

            })
            row_num = row_num + 1
            # summary
            sum_platon += platon.value if platon else 0
            sum_salary_rate += item.salary_rate or 0
            sum_salary_idle_minutes += item.salary_idle_minutes or 0
            sum_salary_idle_price += item.salary_idle_price or 0
            sum_salary_idle_cost += item.salary_idle_cost or 0
            sum_salary_transfer += item.salary_transfer or 0
            sum_salary_total += item.salary_total or 0
            sum_client_rate += item.client_rate or 0
            sum_client_idle_minutes += item.client_idle_minutes or 0
            sum_client_idle_price += item.client_idle_price or 0
            sum_client_idle_cost += item.client_idle_cost or 0
            sum_client_transfer += item.client_transfer or 0
            sum_client_total += item.client_total or 0
            sum_profit += item.profit or 0
        json_data.append({
            'row_num': "", 'DT_RowId': "0", 'Number': "", 'Status': "", 'driver_action': "",
            'ExportDate': "", 'driver': '', 'truck': '', 'Client': '',
            'ClientFullName': '', 'ContainerNum': '', 'recognized_container_num': "", 'Foot': '',
            'Weight': '', 'Operation': '', 'TerminalLoad': '', 'Address': '', 'District': '',
            'RaywayCr': '', 'RaywayCr2': '', 'TerminalDeliver': '', 'Comment': '', 'additional_wastes': '_ИТОГО: ',
            'platon': round(sum_platon, 1),
            'salary_rate': round(sum_salary_rate, 1),
            'salary_idle_minutes': round(sum_salary_idle_minutes, 1),
            'salary_idle_price': '',
            'salary_idle_cost': round(sum_salary_idle_cost, 1),
            'salary_transfer': round(sum_salary_transfer, 1),
            'salary_total': round(sum_salary_total, 1),
            'client_rate': round(sum_client_rate, 1),
            'client_idle_minutes': round(sum_client_idle_minutes, 1),
            'client_idle_price': '',
            'client_idle_cost': round(sum_client_idle_cost, 1),
            'client_transfer': round(sum_client_transfer, 1),
            'client_total': round(sum_client_total, 1),
            'bill_number': ';',
            'payment_date': '',
            'profit': round(sum_profit, 1),
            'manager': '', 'date_begin': '',
            'date_end': '', 'dz': '', 'destination_station': '',
            'cargo_manager': '', 'add_services': '[]',
            'attachments': '[]', 'attachments_client': '[]'
        })

        return json_data


class ActReport(BaseDatatableView):
    model = ClientBill
    order_columns = ['number', 'client', 'number', 'date_from', 'date_to', 'order_count', 'money']
    max_display_length = MAX_DISPLAY_LENGTH

    tz = LOCAL_TIMEZONE

    def get_initial_queryset(self):
        query_filters = dict()
        status_ids = self.request.POST.get('status_ids', '1,2,3').split(',')
        date_start = self.request.POST.get('date_start')
        ds = datetime.strptime(date_start, '%Y-%m-%d').date()
        date_end = self.request.POST.get('date_end')
        de = datetime.strptime(date_end + " 23:59", '%Y-%m-%d %H:%M')

        # new: Пользователь видит только доступных клиентов
        query_filters['client__in'] = get_available_client(self.request)

        # Для клиенитов показывать только проверенные акты
        if check_client(request=self.request) == 1:
            query_filters['status__gt'] = 1

        if len(status_ids) > 0:
            query_filters['status__in'] = status_ids

        if date_start is not None:
            query_filters['date_to__lte'] = de
            query_filters['date_from__gte'] = ds

        if len(query_filters) > 0:
            res = ClientBill.objects.filter(**query_filters).order_by('client__Name', '-date_from')
        else:
            res = ClientBill.objects.all().order_by('client__Name', '-date_from')
        return res

    def filter_queryset(self, qs, params=None):
        data = params or self.request.POST
        filter_client = data.get('columns[1][search][value]', None)

        if filter_client:
            qs = qs.filter(client__id=filter_client)

        return qs

    def prepare_results(self, qs):
        json_data = []
        is_admin = 1 if self.request.user.pk in (1, 2, 7, 8) else 0
        # для разных кнопок для клиентов и сотрудников
        is_client_bill = check_client(request=self.request)
        row_num = 1

        # summary counters
        total_orders = 0
        total_money = 0

        # rows
        for item in qs:
            money_cost = item.money or 0
            total_orders += item.order_count
            total_money += money_cost

            if item.created_by:
                manager = item.created_by.first_name
            else:
                manager = ''
            status = get_bill_status(item.status)
            json_data.append({
                'row_num': row_num,
                'DT_RowId': 'request_' + str(item.id),
                'number': item.number,
                'status': status,
                'create_date': item.create_date.astimezone(self.tz).strftime('%d.%m %H:%M') + "<br>" + manager,
                'money': item.money or '',
                'send_date': '' if item.send_date is None else item.send_date.astimezone(self.tz).strftime(
                    '%d.%m %H:%M'),
                'order_count': item.order_count,
                'date_from': '' if item.date_from is None else item.date_from.astimezone(self.tz).strftime(
                    '%d.%m %H:%M'),
                'date_to': '' if item.date_to is None else item.date_to.astimezone(self.tz).strftime('%d.%m %H:%M'),
                'client': item.client.Name,
                'is_client_bill': is_client_bill,
                'is_admin': is_admin

            })
            row_num = row_num + 1
        # summary row:

        json_data.append({
            'row_num': "",
            'DT_RowId': 'request_0',
            'number': "",
            'status': "",
            'create_date': "_ ИТОГО",
            'money': total_money,
            'send_date': '',
            'order_count': total_orders,
            'date_from': '',
            'date_to': '',
            'client': '',
            'is_client_bill': '',
            'is_admin': is_admin
        })
        return json_data


class PaymentsReportTable(BaseDatatableView):
    model = PaymentsReport
    order_columns = ['id', ]

    def get_initial_queryset(self):
        date_start = self.request.POST.get('date_start')
        qs = PaymentsReport.objects.filter(month=date_start)
        return qs

    def filter_queryset(self, qs):
        filter_dz = self.request.POST.get('dz_id', None)

        if filter_dz:
            qs = qs.filter(driver__dz=filter_dz)

        return qs

    def prepare_results(self, qs):
        qs = qs.order_by('driver__FIO')
        json_data = []
        row_num = 1
        summary = 0
        salary = 0
        bonus = 0
        repair = 0
        fuel = 0
        gazprom = 0
        simbios = 0
        prepayment = 0
        card = 0
        tax = 0
        credit = 0
        garage = 0
        debt = 0
        for item in qs:
            auto_list = [str(i) for i in item.truck.all()]
            auto_list_row = "<br>".join(auto_list) if len(auto_list) > 0 else 'Нет машины'
            json_data.append({
                'row_num': row_num,
                'DT_RowId': str(item.id),
                'month': item.month,
                'driver': str(item.driver),
                'auto': auto_list_row,
                'salary': item.salary,
                'bonus': item.bonus,
                'repair': item.repair,
                'fuel': item.fuel,
                'gazprom': item.gazprom,
                'simbios': item.simbios,
                'prepayment': item.prepayment,
                'card': item.card,
                'tax': item.tax,
                'credit': item.credit,
                'garage': item.garage,
                'debt': item.debt,
                'summary': round(item.summary, 1)
            })
            row_num = row_num + 1
            summary += item.summary or 0
            salary += salary or 0
            bonus += item.bonus or 0
            repair += item.repair or 0
            fuel += item.fuel or 0
            gazprom += item.gazprom or 0
            simbios += item.simbios or 0
            prepayment += item.prepayment or 0
            card += item.card or 0
            tax += item.tax or 0
            credit += item.credit or 0
            garage += item.garage or 0
            debt += item.debt or 0

        # summary_row
        json_data.append({
            'row_num': "",
            'DT_RowId': 0,
            'month': "",
            'driver': "",
            'auto': "_ИТОГО:",
            'salary': round(salary, 1),
            'bonus': round(bonus, 1),
            'repair': round(repair, 1),
            'fuel': round(fuel, 1),
            'gazprom': round(gazprom, 1),
            'simbios': round(simbios, 1),
            'prepayment': round(prepayment, 1),
            'card': round(card, 1),
            'tax': round(tax, 1),
            'credit': round(credit, 1),
            'garage': round(garage, 1),
            'debt': round(debt, 1),
            'summary': round(summary, 1)
        })
        return json_data


@login_required(login_url=LOGIN_URL)
def schedule_table(request):
    start = int(request.POST.get('start'))
    length = int(request.POST.get('length'))
    date_start = local_date(request.POST.get('date_start'))
    date_end = local_date(request.POST.get('date_end'))

    # table definition
    col_names = 'TruckNumber,TrailerNumber,FIO,foot,dz,ContainerNum,LoadName,' \
                'DeliverName,Address,' \
                'District,RaywayCr,RaywayCr2,Operation,ExportTime,ClientName,' \
                'Customer,trip_num,add_work,month_trips,Comment'

    # order parameters
    order_index = int(request.POST.get('order[0][column]', "1"))
    order_dir = request.POST.get('order[0][dir]', 'asc') or 'asc'

    order_columns = ['', 'substr(tr."number",2,3)', 'TrailerNumber', 'FIO', 't."foot"',
                     'tr."dz_id"', 'ContainerNum', 'LoadName', 'DeliverName', 'Address',
                     'District', 'RaywayCr', 'RaywayCr2', 'Operation', 'ExportTime',
                     'ClientName', 'Customer', 'trip_num', 'add_work', 'month_trips', 'Comment']
    order_field = order_columns[order_index]

    # date parameters
    if date_start == date_end:
        date_end = date_end + timedelta(days=1)

    # filters
    dz = request.POST.get('columns[4][search][value]')
    driver = request.POST.get('columns[2][search][value]')
    truck = request.POST.get('columns[0][search][value]')
    status = request.POST.get('columns[15][search][value]')
    foot = request.POST.get('columns[3][search][value]')
    where = []
    if dz:
        where.append("tr.dz_id={0}".format(dz))
    if driver:
        where.append("d.id={0}".format(driver))
    if truck:
        where.append("tr.id={0}".format(truck))
    if foot:
        where.append("mf.foot={0}".format(foot))

    # Диспетчера видят только свою зону диспетчеризации
    user_dz = UserLocation.objects.filter(user=request.user)
    if user_dz.count() > 0:
        where.append("tr.dz_id={0}".format(user_dz[0].zone.id))

    where_str = "AND ( " + " AND ".join(where) + " )" if where else ""
    year_filter = str(date_start).split('-')[0]
    month_filter = str(date_start).split('-')[1]

    sql = schedule_request(date_start, date_end, where_str, year_filter, month_filter, order_field, order_dir)

    with connection.cursor() as cursor:
        cursor.execute("""set timezone TO 'Asia/Novosibirsk';""")
        cursor.execute(sql)
        db_rows = cursor.fetchall()

    data_names = namedtuple('Record', col_names)
    data = []
    records_total = Driver.objects.count()
    # for collect unique data
    trailer_list = []
    trailer_foot_list = []

    if status:
        try:
            status = int(status)
            db_rows = [elem for elem in db_rows if elem[16] == status and elem[17] not in ('Р', 'О', 'Б', 'М', 'Н')]
        except Exception:
            db_rows = [elem for elem in db_rows if elem[17] is not None and elem[17] == status]

    if length == -1:
        length = len(db_rows)

    row_num = 1
    for row in map(data_names._make, db_rows[start:start + length]):
        # Insert row nums
        trailer_list.append(row.TrailerNumber)
        if row.TruckNumber not in trailer_foot_list:
            trailer_foot_list.append(row.TruckNumber)
            data_item = row._asdict()
            data_item['row_num'] = row_num
            row_num = row_num + 1
            data.append(data_item)

    draw = request.GET.get('draw')
    response = {
        "draw": draw,
        "result": 'ok',
        "data": data,
        "recordsFiltered": records_total,
        "recordsTotal": records_total
    }
    return JsonResponse(response)


@login_required(login_url=LOGIN_URL)
def get_reports_table(request):
    data_table = SQLProvider(TOTAL_DRIVERS, request.POST)
    return JsonResponse(data_table.get_table(), safe=False)


@login_required(login_url=LOGIN_URL)
def get_clients_table(request):
    data_table = SQLProvider(CLIENTS_TOTAL, request.POST, mode='client')
    response = data_table.get_table()

    def sort_field(x):
        return x['name']

    resp = []
    col = 0
    orders_grouped = groupby(response['data'], sort_field)
    for key, group in orders_grouped:
        orders = list(group)
        orders_count = len(orders)
        col += 1
        orders[0]['row_num'] = col
        if orders_count == 1:
            resp.append(orders[0])
        else:
            col += 1
            orders[1]['row_num'] = col
            resp.extend(orders)
            col += 1
            resp.append({
                'row_num': col,
                'name': orders[0]['name'],
                'Foot': 'Всего',
                'jan': orders[0]['jan'] + orders[1]['jan'],
                'feb': orders[0]['feb'] + orders[1]['feb'],
                'mar': orders[0]['mar'] + orders[1]['mar'],
                'apr': orders[0]['apr'] + orders[1]['apr'],
                'may': orders[0]['may'] + orders[1]['may'],
                'jun': orders[0]['jun'] + orders[1]['jun'],
                'jul': orders[0]['jul'] + orders[1]['jul'],
                'aug': orders[0]['aug'] + orders[1]['aug'],
                'sep': orders[0]['sep'] + orders[1]['sep'],
                'oct': orders[0]['oct'] + orders[1]['oct'],
                'nov': orders[0]['nov'] + orders[1]['nov'],
                'dec': orders[0]['dec'] + orders[1]['dec'],
                'all': orders[0]['all'] + orders[1]['all'],
            })
    resp_with_summary = calc_summary(resp, col)
    response['data'] = resp_with_summary

    return JsonResponse(response, safe=False)


@login_required(login_url=LOGIN_URL)
def app_report_table(request):
    dz = request.POST.get('columns[2][search][value]')
    driver = request.POST.get('columns[1][search][value]')
    version = request.POST.get('columns[4][search][value]')

    date_start = local_date(request.POST.get('date_start'))
    date_end = local_date(request.POST.get('date_end'))

    where = []
    if dz:
        where.append(f"d.dz_id={dz}")
    if driver:
        where.append(f"d.id={driver}")
    if version:
        where.append(f"d.app_version='{version}'")

    where_str = "AND ( " + " AND ".join(where) + " )" if where else ""
    sql = app_report(where_str, date_start, date_end)

    with connection.cursor() as cursor:
        cursor.execute("""set timezone TO 'Asia/Novosibirsk';""")
        cursor.execute(sql)
        db_rows = cursor.fetchall()

    n = 'FIO,dz,app_install,app_version,orders_total,order_status_count,foto_count,recognized_total,manual_input'
    data_names = namedtuple('data_names', n)

    row_num = 1
    data = []
    for ind, row in enumerate(map(data_names._make, db_rows)):
        item = dict(row._asdict())
        item['row_num'] = row_num + ind
        data.append(item)

    response = {
        "draw": request.GET.get('draw'),
        "result": 'ok',
        "data": data,
        "recordsFiltered": len(db_rows),
        "recordsTotal": len(db_rows)
    }
    return JsonResponse(response)
