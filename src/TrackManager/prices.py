from datetime import datetime
from openpyxl import load_workbook
from .models import ClientAdditionalPrice, Price, DriverAdditionalPrice, DriverDirectionPrice, DistrictCoastMapping


class PriceDownloader(object):
    """
    Загрузчик прайсов в формате XLSX
    """

    dz_id = None
    dateFrom = None

    def __init__(self, dz_id, dateFrom):
        """
        Конструктор загрузчика прайсов
        :param dz_id: идентификатор зоны диспетчеризации
        :param dateFrom: дата начала действия прайса
        """

        self.dz_id = dz_id
        self.dateFrom = dateFrom

    def generate_client_price(self, client_id):
        """
        генерация клиентского прайса
        :param client_id: идентификатор клиента
        :return:
        """

        query = {
            'client_id': client_id,
            'dz_id': self.dz_id,
            'dateFrom': self.dateFrom
        }

        fname = "client_{client_id}-dz_{dz_id}-dateFrom_{dateFrom}.xlsx".format(**query)

        coeff = list(ClientAdditionalPrice.objects.filter(**query))
        # добавлена сортировка по футам, которая требуется для перехода строки
        prices = list(Price.objects.filter(**query).order_by('to_point', 'foot'))

        if coeff is None or prices is None:
            return None

        price_workbook = load_workbook("resources/price-template-clients.xlsx")
        s1 = price_workbook['Коэффициенты']
        s2 = price_workbook['Направления']

        for c in coeff:
            col = "B" if c.foot == 20 else "C"
            s1[col + "2"].value = c.price_one_coast
            s1[col + "3"].value = c.price_other_coast
            s1[col + "4"].value = c.time_to_load
            s1[col + "5"].value = c.price_idle
            # s1[col + "6"].value = c.weight_22_coef
            s1[col + "6"].value = c.weight_24_coef
            s1[col + "7"].value = c.weight_26_coef
            s1[col + "8"].value = c.weight_28_coef or 0
            s1[col + "9"].value = c.weight_30_coef
            s1[col + "10"].value = c.empty_coef

        row = 2
        for c in prices:
            r = str(row)
            if c.foot == 40:
                row += 1
            s2["A" + r].value = c.to_point
            s2[("B" if c.foot == 20 else "C") + r].value = c.price

        price_workbook.save(fname)

        return fname

    def generate_driver_price(self, driver_type):
        """
        генерация водительского прайса
        :param driver_type: категория водителя
        :return:
        """

        query = {
            'dt': driver_type,
            'dz_id': self.dz_id,
            'dateFrom': self.dateFrom
        }

        fname = "dt_{dt}-dz_{dz_id}-dateFrom_{dateFrom}.xlsx".format(**query)

        coeff = list(DriverAdditionalPrice.objects.filter(**query))
        # добавлена сортировка по футам, которая требуется для перехода строки
        prices = list(DriverDirectionPrice.objects.filter(**query).order_by('to_point', 'foot'))

        if coeff is None or prices is None:
            return None

        price_workbook = load_workbook("resources/price-template-drivers.xlsx")
        s1 = price_workbook['Коэффициенты']
        s2 = price_workbook['Направления']

        for c in coeff:
            col = "B" if c.foot == 20 else "C"
            s1[col + "2"].value = c.price_one_coast
            s1[col + "3"].value = c.price_other_coast
            s1[col + "4"].value = c.time_to_load
            s1[col + "5"].value = c.price_idle
            # s1[col + "6"].value = c.weight_22_coef
            s1[col + "6"].value = c.weight_24_coef
            s1[col + "7"].value = c.weight_26_coef
            s1[col + "8"].value = c.weight_28_coef or 0
            s1[col + "9"].value = c.weight_30_coef
            s1[col + "10"].value = c.empty_coef

        row = 2
        for c in prices:
            r = str(row)
            if c.foot == 40:
                row += 1
            s2["A" + r].value = c.to_point
            s2[("B" if c.foot == 20 else "C") + r].value = c.price

        price_workbook.save(fname)

        return fname


class PriceLoader(object):
    """
    Загрузчик прайсов в формате XLSX
    """

    file = None
    dz = None
    date_from = None
    now = datetime.now()

    def __init__(self, file, dz, date_from):
        """
        Конструктор загрузчика прайсов
        :param file: путь к файлу с прайсом
        :param dz: зона диспетчеризации
        :param date_from: дата начала действия прайса
        """
        self.file = file
        self.dz = dz
        self.date_from = date_from
        self.dcl = DirectionCoastLoader()

    def load_client_price(self, client):
        """
        Загрузка клиентского прайса (в одном файле сразу и 20 фут и 40 фут)
        :rtype: bool
        :return: статус загрузки (True/False)
        """

        price_workbook = load_workbook(self.file, read_only=True)
        s1 = price_workbook['Коэффициенты']
        s2 = price_workbook['Город']
        s3 = price_workbook['Пригород']
        s4 = price_workbook['Межгород']

        ClientAdditionalPrice.objects.filter(client=client, dateFrom=self.date_from, dz=self.dz).delete()
        Price.objects.filter(client=client, dateFrom=self.date_from, dz=self.dz).delete()

        # Если есть действующие тарифы, считаем, что они заканичваются тогда, когда начинаются новые
        ClientAdditionalPrice.objects.filter(client=client, dateTo=None, dz=self.dz).update(dateTo=self.date_from)
        Price.objects.filter(client=client, dateTo=None, dz=self.dz).update(dateTo=self.date_from)

        # загружаем коэффициенты
        for foot in (20, 40):
            add_price = ClientAdditionalPrice()
            add_price.createDate = self.now
            add_price.client = client
            add_price.dz = self.dz
            add_price.dateFrom = self.date_from
            add_price.foot = foot
            column = 'C' if foot == 20 else 'D'
            add_price.price_one_coast = s1[column + '2'].value
            add_price.price_other_coast = s1[column + '3'].value
            add_price.time_to_load = s1[column + '4'].value
            add_price.price_idle = s1[column + '5'].value
            add_price.weight_22_coef = 1  # unused coefficient
            add_price.weight_24_coef = s1[column + '6'].value
            add_price.weight_26_coef = s1[column + '7'].value
            add_price.weight_28_coef = s1[column + '8'].value
            add_price.weight_30_coef = s1[column + '9'].value
            add_price.weight_35_coef = s1[column + '9'].value  # unused coefficient
            add_price.empty_coef = s1[column + '10'].value
            add_price.save()

        # загружаем тарифы по городу
        self.load_client_direction_price(s2, client)

        # загружаем тарифы по пригороду
        self.load_client_direction_price(s3, client)

        # загружаем тарифы по межгороду
        self.load_client_direction_price(s4, client)

        return True

    def load_client_direction_price(self, sheet, client):
        max_row = sheet.max_row + 1
        for foot in (20, 40):
            column = 'B' if foot == 20 else 'C'
            for row in range(2, max_row):
                price = Price()
                price.createDate = self.date_from
                price.client = client
                price.dz = self.dz
                price.dateFrom = self.date_from
                price.foot = foot
                price.to_point = str(sheet['A' + str(row)].value).strip()
                price.price = sheet[column + str(row)].value
                coast = sheet['D' + str(row)].value
                if coast:
                    self.dcl.check_district(str(price.to_point).strip(), 1 if coast == 'Левый' else 2)
                if price.to_point != 'None':
                    price.save()

    def load_driver_price(self, driver_type, driver=None):
        """
        Загрузка водительского прайса (в одном файле сразу и 20 фут и 40 фут)
        :rtype: bool
        :return: статус загрузки (True/False)
        """

        price_workbook = load_workbook(self.file)
        s1 = price_workbook['Коэффициенты']
        s2 = price_workbook['Город']
        s3 = price_workbook['Пригород']
        s4 = price_workbook['Межгород']

        DriverAdditionalPrice.objects.filter(dt=driver_type, dateFrom=self.date_from, dz=self.dz).delete()
        DriverDirectionPrice.objects.filter(dt=driver_type, dateFrom=self.date_from, dz=self.dz).delete()

        # Действующим прайсам необходимо поставить окончание срока действия
        DriverAdditionalPrice.objects.filter(dt=driver_type, dateTo__isnull=True, dz=self.dz).update(
            dateTo=self.date_from)
        DriverDirectionPrice.objects.filter(dt=driver_type, dateTo__isnull=True, dz=self.dz).update(
            dateTo=self.date_from)

        # загружаем коэффициенты
        for foot in (20, 40):
            add_price = DriverAdditionalPrice()
            if driver is not None:
                add_price.driver_id = driver
            add_price.createDate = self.now
            add_price.dt = driver_type
            add_price.dz = self.dz
            add_price.dateFrom = self.date_from
            add_price.foot = foot
            # add_price.time_to_load = 180 if foot == 20 else 240
            column = 'C' if foot == 20 else 'D'
            add_price.price_one_coast = s1[column + '2'].value
            add_price.price_other_coast = s1[column + '3'].value
            add_price.time_to_load = s1[column + '4'].value
            add_price.price_idle = s1[column + '5'].value
            add_price.weight_22_coef = 1  # unused coefficient
            add_price.weight_24_coef = s1[column + '6'].value
            add_price.weight_26_coef = s1[column + '7'].value
            add_price.weight_28_coef = s1[column + '8'].value
            add_price.weight_30_coef = s1[column + '9'].value
            add_price.weight_35_coef = s1[column + '9'].value  # unused coefficient
            add_price.empty_coef = s1[column + '10'].value
            add_price.save()

        # загружаем тарифы по городу
        self.load_driver_direction_price(s2, driver_type, driver)

        # загружаем тарифы по пригороду
        self.load_driver_direction_price(s3, driver_type, driver)

        # загружаем тарифы по межгороду
        self.load_driver_direction_price(s4, driver_type, driver)

        return True

    def load_driver_direction_price(self, sheet, driver_type, driver=None):
        max_row = sheet.max_row + 1
        for foot in (20, 40):
            column = 'B' if foot == 20 else 'C'
            for row in range(2, max_row):
                price = DriverDirectionPrice()
                price.createDate = self.date_from
                price.dt = driver_type
                if driver is not None:
                    price.driver_id = driver
                price.dz = self.dz
                price.dateFrom = self.date_from
                price.foot = foot
                # price.to_point = sheet['A' + str(row)].value
                price.to_point = str(sheet['A' + str(row)].value).strip()
                price.price = sheet[column + str(row)].value
                coast = sheet['D' + str(row)].value
                if coast:
                    # self.dcl.check_district(price.to_point, 1 if coast == 'Левый' else 2)
                    self.dcl.check_district(str(price.to_point).strip(), 1 if coast == 'Левый' else 2)
                if price.to_point != 'None':
                    price.save()


class DirectionCoastLoader(object):
    districts = {}

    def __init__(self):
        d = DistrictCoastMapping.objects.values_list("district", "coast")
        if d is not None and len(d) > 0:
            self.districts = dict(d)

    def check_district(self, district, coast):
        if district not in self.districts.keys():
            self.districts[district] = coast
            mapping = DistrictCoastMapping()
            mapping.district = district
            mapping.coast = coast
            mapping.save()
