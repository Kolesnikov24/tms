from copy import copy
from datetime import datetime
from logging import getLogger
from collections import OrderedDict

from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from .models import UserProfile, Client
from .constants import LOCAL_TIMEZONE, get_operation

logger = getLogger('tms')


def str_date(obj, fmt='%H:%M', default=''):
    res = default
    if obj is not None:
        res = obj.strftime(fmt)
    return res


def add_year(post_data):
    def add_(s):
        dt = s.split(' ')
        return dt[0] + '.' + current_year + ' ' + dt[1]

    field_list = ['operation_date_begin', 'operation_date_end']
    current_year = str(datetime.now().year)

    try:
        form_data = copy(post_data)
        for k, v in form_data.items():
            if k in field_list:
                form_data[k] = add_(v)
    except Exception:
        form_data = post_data

    return form_data


def local_date(input_date):
    return LOCAL_TIMEZONE.localize(datetime.combine(datetime.strptime(input_date, '%Y-%m-%d').date(),
                                                    datetime.min.time()))


def get_user_groups_ids(request):
    return [g.id for g in request.user.groups.all()]


def get_user_menu_tabs(request):
    """
    Возвращает список разделов, доступные пользователю
    :param request:
    :return: [str]
    """
    menu_labels = {
        'manager': 'Ввод заявок',
        'dispatcher': 'Обработка заявок',
        'manager_calc': 'Справка',
        'salary': 'Табель',
        'schedule': 'Работа машин',
        'acts': 'Акты',
        'reports_page': 'Отчеты',
        'client-price': 'Прайсы',
        'tariff_check': 'Калькулятор',
        'payments': 'Ведомость',
        'maps': 'Карта',

    }
    menu_order_list = ['manager', 'dispatcher', 'manager_calc', 'schedule', 'salary', 'acts', 'reports_page',
                       'client-price', 'tariff_check', 'payments', 'maps']
    menu_user_tabs = OrderedDict()
    priveleges = [i.replace('TrackManager.', '') for i in request.user.get_all_permissions()]
    for tab in menu_order_list:
        if tab in priveleges:
            menu_user_tabs[tab] = menu_labels[tab]
    return menu_user_tabs


def check_client(request):
    group_ids = [g.id for g in request.user.groups.all()]
    if 22 in group_ids:
        is_client = 1
    else:
        is_client = 0
    return is_client


def get_manager_list(request):
    """
    Возвращает список пользователей, заявки которых может видеть текущий пользователь
    для роли boss, admin возвращает User.all
    :param request:
    :return: [User.objects()]
    """
    manager_list = [request.user]
    supervisors = User.objects.filter(groups__name='Supervisor')
    user_groups_names = request.user.groups.values_list('name', flat=True)
    client_id = None

    group_ids = [g.id for g in request.user.groups.all()]

    # Руководитель, Администратор, Менеджер Офис
    if 2 in group_ids or 5 in group_ids or 21 in group_ids:
        manager_list = User.objects.all()

    # Клиент
    if 22 in group_ids:
        client_id = UserProfile.objects.filter(user=request.user)[0].client

    # Супервайзер работает с заявками, созданными другими пользователями (клиентами)
    if 'Supervisor' in user_groups_names:
        for group in user_groups_names:
            for user in User.objects.filter(groups__name=group):
                # Add clients only
                if user not in supervisors:
                    manager_list.append(user)

    return manager_list, client_id


def get_available_client(request, calc_module=False):
    user_groups_ids = get_user_groups_ids(request)

    # Именно в справке  Менеджер Клещихи(20) видит всех клинетов
    if calc_module and 20 in user_groups_ids:
        clients = Client.objects.filter(status='A').order_by('Name')
    else:
        clients = request.user.userprofile.clients.filter(status='A').order_by('Name')
    return clients


def xstr(s):
    """ convert None to """
    return '' if s is None else str(s)


def has_privs(request, item):
    priveleges = [i.replace('TrackManager.', '') for i in request.user.get_all_permissions()]
    return item in priveleges


def get_filter_dict(obj_list, fields):
    res_list = []
    for obj in obj_list:
        res = {}
        for k, v in model_to_dict(obj).items():
            if k in fields:
                if k == 'ExportDate':
                    res[k] = v.strftime('%d.%m.%Y')
                elif k in ('ExportTime', 'operation_date_end', 'operation_date_begin'):
                    if v is not None:
                        res[k] = v.strftime('%H:%M')
                    else:
                        res[k] = ''
                elif k == 'Status':
                    res[k] = obj.get_driver_action()
                elif k == 'TerminalLoad':
                    res[k] = obj.TerminalLoad.Name
                elif k == 'TerminalDeliver':
                    res[k] = obj.TerminalDeliver.Name
                elif k == 'Operation':
                    res[k] = get_operation(v)
                elif k in ('AddressLoad', 'AddressDeliver') and \
                        (obj.RaywayCr != '' or obj.RaywayCr != ''):
                    res[k] = ''
                else:
                    res[k] = v

        # join driver status
        res['driver_action'] = obj.get_driver_action(raw=True)
        res_list.append(res)

    return res_list
