from django import template
from .. import constants as choices
from .. import models

register = template.Library()


@register.filter(is_safe=True)
def def_dict(value):
    return value.items()


@register.filter(is_safe=True)
def label_with_classes(value, arg):
    return value.label_tag(attrs={'class': arg})


@register.filter(is_safe=True)
def input_with_classes(value, arg):
    result = value.as_widget(attrs={'class': arg})
    if 'terminal-selector' in arg:
        other_id = '#id_AddressLoad' if 'id_TerminalLoad' in str(value.as_widget()) else '#id_AddressDeliver'
        result = value.as_widget(attrs={'class': arg, 'data-other': other_id})
    return result


@register.filter(is_safe=True)
def get_first(value):
    return value[0][1]


@register.filter(is_safe=True)
def get_action(value):
    return value[0][0]


@register.filter(is_safe=True)
def add_classes(value):
    cl = 'form-control'
    attrs = {'class': cl}
    return value.label_tag(attrs={'class': 'control-label'}) + value.as_widget(attrs=attrs)


@register.filter(is_safe=True)
def add_idle(value):
    db_data = models.AdditonalPrice.objects.filter(service_code='free_idle')
    trf = "/".join([str(i.default_value).split('.')[0] for i in db_data])
    cl = 'form-control'
    attrs = {'class': cl, 'data-idleFree': trf}
    return value.label_tag(attrs={'class': 'control-label'}) + value.as_widget(attrs=attrs)


@register.filter(is_safe=True)
def get_driver_type(value):
    types = dict(choices.DRIVER_TYPE_CHOICES)
    return types[value]


@register.filter(is_safe=True)
def get_driver_name(value):
    return str(value)
