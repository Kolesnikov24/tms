# -*- coding: utf-8 -*-

from datetime import datetime
from django import forms

from . import models
from .constants import (OPERATION_CHOICES, FOOT_CHOICES, DRIVER_TYPE_CHOICES,
                        LOCAL_TIMEZONE, DATE_INPUT_FORMATS, TIME_FORMATS)


def correct_date(ds):
    new_date = datetime(year=datetime.now().year,
                        day=ds.day,
                        month=ds.month,
                        hour=ds.hour,
                        minute=ds.minute)

    tz = LOCAL_TIMEZONE

    return tz.localize(new_date)


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = models.Feedback
        exclude = ('id',)


class OrderForm(forms.ModelForm):
    class Meta:
        model = models.Order
        exclude = ('id', 'Number', 'Status', 'CreateDate', 'truck', 'trailer', 'driver',
                   'dispatch_date', 'accept_date', 'correction_date', 'cancel_date', 'decline_date',
                   'decline_reason', 'assign_date', 'unassign_date', 'unassign_reason')

        labels = {
            'CreateDate': 'Дата создания',
            'TaskDate': 'Дата исполнения',
            'Client': 'Клиент',
            'Customer': 'Грузополучатель',
            'Proxy': 'Доверенность',
            'ContainerNum': '№ Контейнера',
            'Foot': 'Фут',
            'Weight': 'Вес брутто, кг',
            'Operation': 'Операция',
            'TransportProvide': 'Подача автотранспорта',
            'ContainerLoad': 'Загр. контейнера',
            'ContainerDeliver': 'Забор. контейнера',
            'Address': 'Адрес погр/выгр',
            'District': 'Район',
            'RaywayCr': 'Переезд',
            'RaywayCr2': 'Переезд2',
            'Responsible': 'Ответственное лицо, телефон',
            'Payer': 'Плательщик',
            'DestinationPoint': 'Пункт назначения',
            'Receiver': 'Получатель',
            'ReceiverRailWayCode': 'ЖД код получателя',
            'ReceiverOKPO': 'ОКПО получателя',
            'SpecialComment': 'Особые отметки',
            'Notification': 'Уведомить строну о',
            'CargoName': 'Наименование груза',
            'PositionQuantity': 'Количество мест',
            'PackingType': 'Род упаковки',
            'Seal': 'Пломба',
            'Forwarding': 'Экспедирование',
            'Comment': 'Примечание',
            'Status': 'Статус',
            'BoardNumber': 'Номер машины',
            'ArrivalDate': 'Дата/время подачи',
            'AcceptDate': 'Дата/время приема',
            'DeclineDate': 'Дата/время отказа',
            'СorrectionDate': 'Дата/время корретировки',
            'ExportDate': 'Дата',
            'ExportTime': 'Время',
            'TerminalLoad': 'Терминал забора контейнера',
            'TerminalDeliver': 'Терминал сдачи контейнера',
            'AddressLoad': 'Адрес забора контейнера',
            'AddressDeliver': 'Адрес сдачи контейнера',
            'dz': 'Зона диспетчеризации',
            'cargo_manager': 'Менеджер'
        }

    Client = forms.ModelChoiceField(label='Клиент', queryset=models.Client.objects.all())
    Operation = forms.ChoiceField(label='Операция', choices=OPERATION_CHOICES)
    Foot = forms.ChoiceField(label='Фут', choices=FOOT_CHOICES)
    TerminalLoad = forms.ModelChoiceField(label='Терминал забора контейнера', queryset=models.Terminal.objects.all())
    TerminalDeliver = forms.ModelChoiceField(label='Терминал сдачи контейнера', queryset=models.Terminal.objects.all())
    ExportDate = forms.DateTimeField(label='Дата', input_formats=DATE_INPUT_FORMATS)
    Comment = forms.CharField(label='Примечание', widget=forms.Textarea(attrs={'cols': 80, 'rows': 2}), required=False)
    dz = forms.ModelChoiceField(label='Зона диспетчеризации', queryset=models.DispZone.objects.all())

    def clean_ExportDate(self):
        d = self.cleaned_data['ExportDate']
        return d

    def clean(self):
        super(OrderForm, self).clean()
        cleaned_data = self.cleaned_data
        d = self.cleaned_data['ExportDate']
        t = self.cleaned_data['ExportTime']
        d = datetime.combine(d, t)
        d = LOCAL_TIMEZONE.localize(d)
        cleaned_data['ExportDate'] = d
        return cleaned_data


class OrderWorkForm(forms.ModelForm):
    class Meta:
        model = models.Order
        fields = (
            'additional_wastes', 'platon', 'salary_rate', 'salary_idle_minutes', 'salary_idle_price',
            'salary_idle_cost', 'salary_transfer', 'salary_total', 'client_rate', 'client_idle_minutes',
            'client_idle_price', 'client_idle_cost', 'client_transfer', 'client_total', 'bill_number', 'payment_date',
            'profit', 'operation_date_begin', 'operation_date_end', 'ContainerNum', 'Weight', 'Status')
        labels = {
            'additional_wastes': 'Дополнительные затраты', 'platon': 'Платон', 'salary_rate': 'Ставка (руб)',
            'salary_idle_minutes': 'Простой (мин)', 'salary_idle_price': 'Цена (мин)',
            'salary_idle_cost': 'Простой (руб)', 'salary_transfer': 'Переезд', 'salary_total': 'Итог з/п',
            'client_rate': 'Ставка (руб)', 'client_idle_minutes': 'Простой > нормы', 'client_idle_price': 'Цена (мин)',
            'client_idle_cost': 'Простой (руб)', 'client_transfer': 'Переезд', 'client_total': 'Сумма',
            'bill_number': '№ Счета', 'payment_date': 'Дата оплаты', 'profit': 'Прибыль',
            'operation_date_begin': 'Начало операции', 'operation_date_end': 'Завершение операции',
            'ContainerNum': 'Контейнер', 'Weight': 'Вес'
        }

    payment_date = forms.DateTimeField(label='Дата оплаты', input_formats=('%d.%m.%Y',), required=False)
    operation_date_begin = forms.DateTimeField(label='Начало операции', input_formats=TIME_FORMATS, required=False)
    operation_date_end = forms.DateTimeField(label='Завершение операции', input_formats=TIME_FORMATS, required=False)
    client_idle_minutes = forms.CharField(label='Простой > нормы', required=False)
    Status = forms.IntegerField()

    def clean(self):
        super(OrderWorkForm, self).clean()
        cleaned_data = self.cleaned_data
        return cleaned_data


class OrderWorkFormDisp(forms.ModelForm):
    class Meta:
        model = models.Order
        fields = (
            'salary_idle_minutes',
            'client_idle_minutes',
            'operation_date_begin', 'operation_date_end', 'ContainerNum', 'Status')
        labels = {
            'additional_wastes': 'Дополнительные затраты', 'platon': 'Платон', 'salary_rate': 'Ставка (руб)',
            'salary_idle_minutes': 'Простой (мин)', 'salary_idle_price': 'Цена (мин)',
            'salary_idle_cost': 'Простой (руб)', 'salary_transfer': 'Переезд', 'salary_total': 'Итог з/п',
            'client_rate': 'Ставка (руб)', 'client_idle_minutes': 'Простой > нормы', 'client_idle_price': 'Цена (мин)',
            'client_idle_cost': 'Простой (руб)', 'client_transfer': 'Переезд', 'client_total': 'Сумма',
            'bill_number': '№ Счета', 'payment_date': 'Дата оплаты', 'profit': 'Прибыль',
            'operation_date_begin': 'Начало операции', 'operation_date_end': 'Завершение операции',
            'ContainerNum': 'Контейнер', 'Weight': 'Вес'
        }

    operation_date_begin = forms.DateTimeField(label='Начало операции', input_formats=TIME_FORMATS, required=False)
    operation_date_end = forms.DateTimeField(label='Завершение операции', input_formats=TIME_FORMATS, required=False)
    client_idle_minutes = forms.CharField(label='Простой > нормы', required=False)
    salary_idle_minutes = forms.CharField(label='Простой > нормы ЗП', required=False)

    def clean(self):
        super(OrderWorkFormDisp, self).clean()
        cleaned_data = self.cleaned_data

        cleaned_data['operation_date_begin'] = correct_date(self.cleaned_data['operation_date_begin'])
        cleaned_data['operation_date_end'] = correct_date(self.cleaned_data['operation_date_end'])

        return cleaned_data


class ClientPriceForm(forms.Form):
    """
    Форма для загрузки клиентских прайсов
    """
    client = forms.ModelChoiceField(label='Клиент', queryset=models.Client.objects.all())
    dz = forms.ModelChoiceField(label='Зона диспетчеризации', queryset=models.DispZone.objects.all())
    date_from = forms.DateTimeField(label='Дата начала', input_formats=DATE_INPUT_FORMATS)
    price = forms.FileField(label='Прайс')


class DriverPriceForm(forms.Form):
    """
    Форма для загрузки водительских прайсов
    """
    driver_type = forms.ChoiceField(label='Категория водителя', choices=DRIVER_TYPE_CHOICES)
    dz = forms.ModelChoiceField(label='Зона диспетчеризации', queryset=models.DispZone.objects.all())
    date_from = forms.DateTimeField(label='Дата начала', input_formats=DATE_INPUT_FORMATS)
    price = forms.FileField(label='Прайс')


class OrderFormForCalc(forms.ModelForm):
    class Meta:
        model = models.Order
        exclude = ('id', 'Number', 'Status', 'CreateDate', 'truck', 'trailer',
                   'dispatch_date', 'accept_date', 'correction_date', 'cancel_date', 'decline_date',
                   'decline_reason', 'assign_date', 'unassign_date', 'unassign_reason')

        labels = {
            'CreateDate': 'Дата создания',
            'TaskDate': 'Дата исполнения',
            'Client': 'Клиент',
            'Customer': 'Грузополучатель',
            'Proxy': 'Доверенность',
            'ContainerNum': '№ Контейнера',
            'Foot': 'Фут',
            'Weight': 'Вес брутто, кг',
            'Operation': 'Операция',
            'TransportProvide': 'Подача автотранспорта',
            'ContainerLoad': 'Загр. контейнера',
            'ContainerDeliver': 'Забор. контейнера',
            'Address': 'Адрес погр/выгр',
            'District': 'Район',
            'RaywayCr': 'Переезд',
            'RaywayCr2': 'Переезд2',
            'Responsible': 'Ответственное лицо, телефон',
            'Payer': 'Плательщик',
            'DestinationPoint': 'Пункт назначения',
            'Receiver': 'Получатель',
            'ReceiverRailWayCode': 'ЖД код получателя',
            'ReceiverOKPO': 'ОКПО получателя',
            'SpecialComment': 'Особые отметки',
            'Notification': 'Уведомить строну о',
            'CargoName': 'Наименование груза',
            'PositionQuantity': 'Количество мест',
            'PackingType': 'Род упаковки',
            'Seal': 'Пломба',
            'Forwarding': 'Экспедирование',
            'Comment': 'Примечание',
            'Status': 'Статус',
            'BoardNumber': 'Номер машины',
            'ArrivalDate': 'Дата/время подачи',
            'AcceptDate': 'Дата/время приема',
            'DeclineDate': 'Дата/время отказа',
            'СorrectionDate': 'Дата/время корретировки',
            'ExportDate': 'Дата',
            'ExportTime': 'Время',
            'TerminalLoad': 'Терминал забора контейнера',
            'TerminalDeliver': 'Терминал сдачи контейнера',
            'AddressLoad': 'Адрес забора контейнера',
            'AddressDeliver': 'Адрес сдачи контейнера',
            'dz': 'Зона диспетчеризации'
        }

    Client = forms.ModelChoiceField(label='Клиент', queryset=models.Client.objects.all())
    Operation = forms.ChoiceField(label='Операция', choices=OPERATION_CHOICES)
    Foot = forms.ChoiceField(label='Фут', choices=FOOT_CHOICES)
    TerminalLoad = forms.ModelChoiceField(label='Терминал забора контейнера', queryset=models.Terminal.objects.all())
    TerminalDeliver = forms.ModelChoiceField(label='Терминал сдачи контейнера', queryset=models.Terminal.objects.all())
    ExportDate = forms.DateTimeField(label='Дата', input_formats=DATE_INPUT_FORMATS)
    Comment = forms.CharField(label='Примечание', widget=forms.Textarea(attrs={'cols': 80, 'rows': 2}), required=False)
    dz = forms.ModelChoiceField(label='Зона диспетчеризации', queryset=models.DispZone.objects.all())

    def clean_ExportDate(self):
        d = self.cleaned_data['ExportDate']
        return d

    def clean(self):
        super(OrderFormForCalc, self).clean()
        cleaned_data = self.cleaned_data
        d = self.cleaned_data['ExportDate']
        t = self.cleaned_data['ExportTime']
        d = datetime.combine(d, t)
        d = LOCAL_TIMEZONE.localize(d)
        cleaned_data['ExportDate'] = d
        return cleaned_data


class InfoProfileForm(forms.ModelForm):
    class Meta:
        model = models.InfoProfile
        exclude = ('id',)

    table_fileds = forms.ModelMultipleChoiceField(queryset=models.UserTablePermission.objects.all())


class PaymentsReportForm(forms.ModelForm):
    class Meta:
        model = models.PaymentsReport
        exclude = ('id', 'truck')

        widgets = {
            'driver': forms.TextInput(attrs={'readonly': True}),
            'salary': forms.TextInput(attrs={'readonly': True}),
            'month': forms.HiddenInput(),
            'summary': forms.HiddenInput()
        }

    def save(self, commit=True):
        ins = super(PaymentsReportForm, self).save(commit=False)
        c_fields = [ins.fuel, ins.gazprom, ins.simbios, ins.prepayment, ins.card, ins.tax, ins.credit, ins.garage,
                    ins.debt]
        i_fields = [ins.salary, ins.repair, ins.bonus]
        costs = sum(map(lambda x: x or 0, c_fields))
        income = sum(map(lambda x: x or 0, i_fields))
        ins.summary = income - costs
        if commit:
            ins.save()
        return ins
