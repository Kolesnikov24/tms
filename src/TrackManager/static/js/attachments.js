


    var update_data_table = function () {

        var table = $('.tms_dt').DataTable();
        table.rows()
            .every(function () {
                var d = this.data();
                d.counter++;                // update data source for the row
                this.invalidate();          // invalidate the data DataTables has cached for this row
            });
        table.draw();
    };




    // Модальное окно - документы
    $('#AttachModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        current_row = button.closest('tr');
     });

    // Сохраняем документ
    $('#attachmentsBtnSave').on('click', function() {

        order_pk = current_row.attr('id').replace('request_', '');
        var file_input  = $('#attachments_input')[0]

        var form_data = new FormData();
        form_data.append('pk', order_pk);
        form_data.append('filename', $('#filename_input').val());
        form_data.append('file', file_input.files[0]);

        $.ajax({
                   url: 'save_order_doc/',
                   type: 'post',
                   data: form_data,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function(data){
                         $('#AttachModal').modal('hide');
                         update_data_table();
                   }
               });
     });


    $(document).on("click", "new_window_link" , function() {
//        window.open(,'title', 'width=800, height=700');
        var link = $(this).attr('href');
        window.open(link, '_blank');
//        var win =
//        if (win) {
//            //Browser has allowed it to be opened
//            win.focus();
//        } else {
//            //Browser has blocked it
//            alert('Please allow popups for this website');
//        }

        return false;
     });




    // отрисовка вложений для таблицы

    var get_document_link = function (data) {
    var res = ''
    var modal_link = '<a href="#" data-toggle="modal" data-target="#AttachModal">Добавить</a>';
    if (data != "[]") {

        var ad = JSON.parse(data),res = '', row = '', delete_icon='', download_icon='';
            var current_user = $('#current_user').val();


            $.each(ad, function(ind,item) {
               delete_icon = ' <a href="#" data-toggle="modal" data-filename="' + item.fields.file + '" data-order_id="' + item.fields.order
                              + '" data-target="#ConfirmAction">'
                              + '<i class="glyphicon glyphicon-trash"></i></a>';

               download_icon=' <a href="/' + app_name + '/download/' + item.fields.file + '">'
                              + '<i class="glyphicon glyphicon-download-alt"></i></a>';


//               var name = item.fields.file;

               //row = '<br><a href="#" data-toggle="modal" data-target="#imagemodal" data-src="/documents/' + item.fields.file + '">' + item.fields.file + '</a>';

               row = '<br><a target="_blank" href="/' + app_name + '/documents/' + item.fields.file + '">' + item.fields.name + '</a>';

               if (item.fields.upload_by == current_user) {
                 res = res + row + delete_icon + download_icon
               }
               else {
                    res = res + row + download_icon
               }

            })

        }

    return res + '<br>' + modal_link
    }


 // всплывашка документов. часть кода в common.js
    $('#imagemodal').on('show.bs.modal', function(e) {
        var img_resolutions = ['jpg','JPG','JPEG','jpeg','gif','GIF','PNG','png','BMP','bmp'],
            document_path = $(e.relatedTarget).attr('data-src'),
            doc_res = document_path.split('.').pop();

        if (img_resolutions.indexOf(doc_res) > -1) {
            $('.imagepreview').attr('src', document_path);
            $('.imagepreview').show();
        }
        else {
            $('.alert').show();
        }

	});

	$('#imagemodal').on('hidden.bs.modal', function (e) {
	    $('.attached_preview').hide();
	})

    var filename_to_delete,order_id_to_delete;
    //  Удалить документ
    $('#ConfirmAction').on('show.bs.modal', function(e) {
        var link = $(e.relatedTarget);

        order_id_to_delete = link.attr('data-order_id');
        filename_to_delete = link.attr('data-filename');

	});

    // Подтвердить удаление документа
    $('#ConfirmBtn').on('click', function(e) {
        var data = {
            "pk": order_id_to_delete,
		    "filename_to_delete":filename_to_delete
        };

        $.post('/' + app_name + '/delete_order_doc/', data,'json')
            .done(function (data) {
                $('#ConfirmAction').modal('hide');
                update_data_table();

            })
            .fail(function (xhr, errmsg, err) {
                console.log(xhr + " | " + errmsg + " | " + err);
            });
	});

