$(function () {
    var date_format = 'DD.MM.YYYY';

    $('#datepicker').datetimepicker({
        locale: 'ru',
        format: date_format
    }); 

    $('#btnSave').on('click', function () {
        $('#driverPriceForm').submit();
    });

    $('#id_driver_type').on('change', function () {

       if ( $(this).val() == 'S') {
             $("#id_driver_select").attr('disabled', false)
       }
        else {
            $("#id_driver_select").attr('disabled', true)
        }

    });

});
