$(document).ready(function() {

    date_start = moment().startOf('month').format('YYYY-MM-DD'),
    date_end = date_start;

    // Переопределяем настройки DataTableDefault.js
    default_dt_settings.columns=  [
            {'data': 'row_num', 'className': 'cell-row_num'},
            {'data': 'month', 'className': 'cell-month' },
            {'data': 'driver', 'className': 'cell-driver'},
            {'data': 'auto', 'className': 'cell-auto'},
            {'data': 'salary', 'className': 'cell-salary'},
            {'data': 'bonus', 'className': 'cell-bonus'},
            {'data': 'repair', 'className': 'cell-repair'},
            {'data': 'fuel', 'className': 'cell-fuel'},
            {'data': 'gazprom', 'className': 'cell-gazprom'},
            {'data': 'simbios', 'className': 'cell-simbios'},
            {'data': 'prepayment', 'className': 'cell-prepayment'},
            {'data': 'card', 'className': 'cell-card'},
            {'data': 'tax', 'className': 'cell-tax'},
            {'data': 'credit', 'className': 'cell-credit'},
            {'data': 'garage', 'className': 'cell-garage'},
            {'data': 'debt', 'className': 'cell-debt'},
            {'data': 'summary', 'className': 'cell-summary'},
     ];


    var current_row = null;
    var dz_id = ''
    var excel_url = '/' + app_name + '/payments_to_excel?d=1&m=201901';
    var excel_url_all = '/' + app_name + '/all_payment_to_excel?m=' + date_start;


    default_dt_settings.buttons = [
      {
        text: 'Обновить данные по ЗП',
        action: function (e, dt, node, config ) {
            $.post("update_payments_report/", {'date_start': date_start })
                .done( function (data) {
                    update_data_table('#PaymentsTable')
             });
        }
      },
//      {
//        text: 'Выгрузить все',
//        action: function (e, dt, node, config ) {
//           alert('not implemented yet');
//        }
//      },
      {
        text: 'Сохранить расчетку',
        action: function (e, dt, node, config ) {
            window.location = excel_url;

        }
      },
      {
            extend: 'excel',
            text: 'Таблица в excel',
            filename: function () {
                return 'Ведомость ' + date_start + ' - ' + date_end;
            }
       },
      {
        text: 'Сохранить все расчетки',
        action: function (e, dt, node, config ) {
            window.location = excel_url_all;

        }
      }
    ]
    // Метод API
    default_dt_settings.ajax = {
            'url': '/' + app_name + '/get_payments_table/', 'type': 'POST', 'data': function (d) {
                d.date_start = date_start;
                d.date_end = date_end;
                d.dz_id = dz_id;
            }
     };
//     // Графа Итого
//    default_dt_settings.footerCallback = function ( row, data, start, end, display ) {
//        var api = this.api();
//
//            // Total over all pages
//        var total = api
//                .column( 16 )
//                .data()
//                .reduce( function (a, b) {
//                    return a + b ;
//                }, 0 );
//
//        var t = $('#PaymentsTable').DataTable();
//        var ln = data.length + 1;
//        var summary_row =  {
//                  "row_num": ln,
//                  "month": "2019-02-01",
//                  "driver":  "",
//                  "auto": "",
//                  "salary":"",
//                  "bonus": "",
//                  "repair": "",
//                  "fuel": "",
//                  "gazprom": "",
//                  "simbios": "",
//                  "prepayment": "",
//                  "card": "",
//                  "tax": "",
//                  "credit": "",
//                  "garage": "",
//                  "debt": "",
//                  "summary": total
//                 };
////                 $( api.row ).add
//            t.row.add(summary_row1)
////           update_data_table('#PaymentsTable');
//
//    }


    // Создаем таблицу
    init_data_table('#PaymentsTable', default_dt_settings)


    // Показывать окно редактирования при двойном клике на строку
    $('#PaymentsTable tbody').on( 'dblclick', 'tr', function (e) {
        current_row = $(e.currentTarget);
        $('#EditRow').modal('show');
    });

    // Вставка значений строки в форму
    $('#EditRow').on('show.bs.modal', function(e) {

        $.each(current_row.find('td'), function(ind,elem) {
            var input_id = '#id_' + elem.className.split('-')[1];
            $(input_id).val(elem.textContent)
        })

    });

    // Кнопка сохранить вне формы, поэтому отправляем по событию
    $('#btnSave').on('click', function (e) {
        modal_form_submit('/' + app_name + '/payments/','#PaymentReportForm','#EditRow','#PaymentsTable')
    });


    // Плагин выборка дат со стандартными настройками по месяцам (см dateRange.js)
     $('#date_range').daterangepicker(date_range_month,
        function (start, end) {
             date_start = start.format('YYYY-MM-DD');
             date_end = end.format('YYYY-MM-DD');
             update_data_table('#PaymentsTable');
            // if date updated
             excel_url_all = '/' + app_name + '/all_payment_to_excel?m=' + date_start;

        });



    $('#filter_dz').on('change', function () {
        dz_id = $(this).val();
        update_data_table('#PaymentsTable');
    });


    // Выделение строки цветом по клику
    $('#PaymentsTable tbody').on('click', 'tr',  function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            var table = $('#PaymentsTable').DataTable();
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            var rd = table.row( this ).data();
            excel_url = '/' + app_name + '/payments_to_excel?id=' + rd.DT_RowId
        }
    });

});
