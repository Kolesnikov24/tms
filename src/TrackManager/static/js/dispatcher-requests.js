$(function () {
    $.fn.select2.defaults.set("theme", "bootstrap");
    var current_row;
    var status_ids = '2,3,4,5,6,7,8,9';
    var date_format = 'DD.MM.YYYY';
    var date_start = moment().format('YYYY-MM-DD'),
        date_end = date_start;

    var excel_date = function (start, end) {
        var new_hr = $('#exl_save').attr('data-url') + "?date_start=" + start + "&date_end=" + end
        $('#exl_save').attr('href', new_hr)
    };

    excel_date(date_start, date_end)

    $('.timepicker').datetimepicker({
        locale: 'ru',
        format: 'DD.MM HH:mm'
    });

    var approve = function (row) {

        var id = row.attr('id').replace('request_', '');
        $.post('change_status/', {'action': 'approve', 'pk': id})
            .done(function (data) {
                if (!data || data.status_id != 3) return;

                var status_cell = $('td', row).eq(18);
                btn = $('.btn-request-action', row),
                    btn2 = $('.btn-group .btn', row).eq(1),
                    menu_items = $('.dropdown-menu li', row);
                var assign_auto_menu_item = $("[data-target='#RequestAssignAuto']", menu_items).parent(),
                    unapprove_menu_item = $("[data-target='#RequestUnapprove']", menu_items).parent();

                menu_items.removeClass('disabled').addClass('disabled');
                assign_auto_menu_item.removeClass('disabled');
                unapprove_menu_item.removeClass('disabled');
                btn.removeClass('btn-success btn-primary btn-warning').addClass('btn-primary').text($('a', assign_auto_menu_item).text());
                btn2.removeClass('btn-success btn-primary btn-warning').addClass('btn-primary');
                status_cell.text(data.status);
                update_data_table();
            })
            .fail(function (xhr, errmsg, err) {
                console.log(xhr + " | " + errmsg + " | " + err);
            });
    };

    var unapprove = function (row) {
        var id = row.attr('id').replace('request_', '');

        $.post('change_status/', {'action': 'unapprove', 'pk': id, 'UnapproveReason': $('#unapprove_reason').val()})
            .done(function (data) {
                if (!data || data.status_id != 2) return;
                var btn = $('.btn-request-action', row),
                    btn2 = $('.btn-group .btn', row).eq(1),
                    menu_items = $('.dropdown-menu li', row),
                    approve_menu_item = menu_items.first(),
                    deny_menu_item = $("[data-target='#RequestDeny']", menu_items).parent(),
                    status_cell = $('td', row).eq(18);

                menu_items.removeClass('disabled').addClass('disabled');
                approve_menu_item.removeClass('disabled');
                deny_menu_item.removeClass('disabled');
                btn.removeClass('btn-success btn-primary btn-warning').addClass('btn-success').text($('a', approve_menu_item).text());
                btn2.removeClass('btn-success btn-primary btn-warning').addClass('btn-success');
                status_cell.text(data.status);
                $('#RequestUnapprove').modal('hide');
                update_data_table();
            })
            .fail(function (xhr, errmsg, err) {
                console.log(xhr + " | " + errmsg + " | " + err);
            });
    };

    var deny = function (row) {
        var id = row.attr('id').replace('request_', '');

        $.post('change_status/', {'action': 'decline', 'pk': id, 'DenyReason': $('#deny_reason').val()})
            .done(function (data) {
                if (!data || data.status_id != 6) return;
                var cell1 = $('td', row).eq(1),
                    status_cell = $('td', row).eq(18);
                cell1.text('Отказано');
                status_cell.text(data.status);
                $('#RequestDeny').modal('hide');
                update_data_table();
            })
            .fail(function (xhr, errmsg, err) {
                console.log(xhr + " | " + errmsg + " | " + err);
            });
    };

    var close = function (row) {
        if(row == null) {alert('Не удалось завершить заявку!\n\nНекорректная строка'); return;}
        var id = row.attr('id');
        if(id == null || id === '') {alert('Не удалось завершить заявку!\n\nНекорректный идентификатор'); return;}
        id = id.replace('request_', '');
        $.post('change_status/', {'action': 'close', 'pk': id})
            .done(function (data) {
                if (!data || data.status_id !== 5) return;
                var status_cell = $('td', row).eq(18);
                $('td', row).eq(1).text(data.status);
                status_cell.text(data.status);
                update_data_table();
            })
    };

    var assign = function (row) {
        var id = row.attr('id').replace('request_', ''),
            truck = $('#track option:selected'),
            truck_val = truck.val().split('_'),
            truck_id = truck_val[0],
            truck_number = truck.data('truck'),
            trailer_id = truck_val[1],
            trailer_number = truck.data('trailer'),
            driver = $('#driver option:selected'),
            driver_id = driver.val(),
            driver_name = driver.text(),
            auto_visit_take_plan = $('#id_auto_visit_take_plan').val(),
            auto_visit_return_plan = $('#id_auto_visit_return_plan').val();

        $.post('change_status/', {
            'action': 'assignCar',
            'pk': id,
            'truck_id': truck_id,
            'trailer_id': trailer_id,
            'driver_id': driver_id,
            'auto_visit_take_plan':auto_visit_take_plan,
            'auto_visit_return_plan':auto_visit_return_plan
        })
            .done(function (data) {
                if (!data || data.status_id != 4) return;
                var btn = $('.btn-request-action', row),
                    btn2 = $('.btn-group .btn', row).eq(1),
                    menu_items = $('.dropdown-menu li', row),
                    unassign_auto_menu_item = $("[data-target='#RequestUnassignAuto']", menu_items).parent(),
                    status_cell = $('td', row).eq(18),
                    track_cell = $('td', row).eq(19),
                    trailer_cell = $('td', row).eq(20),
                    driver_cell = $('td', row).eq(21);

                menu_items.removeClass('disabled').addClass('disabled');
                unassign_auto_menu_item.removeClass('disabled');
                btn.removeClass('btn-success btn-primary btn-warning').addClass('btn-warning').text($('a', unassign_auto_menu_item).text());
                btn2.removeClass('btn-success btn-primary btn-warning').addClass('btn-warning');
                status_cell.text(data.status);
                track_cell.text(truck_number);
                trailer_cell.text(trailer_number);
                driver_cell.text(driver_name);
                $('#RequestAssignAuto').modal('hide');
                update_data_table();
            })
            .fail(function (xhr, errmsg, err) {
                $('#assign_error').text(xhr.responseJSON.err)
                console.log(xhr + " | " + errmsg + " | " + err);
            });
    };

    var unassign = function (row) {
        var id = row.attr('id').replace('request_', '');

        $.post('change_status/', {'action': 'unassignCar', 'pk': id, 'UnassignReason': $('#unassign_reason').val()})
            .done(function (data) {
                if (!data || data.status_id != 3) return;
                var btn = $('.btn-request-action', row),
                    btn2 = $('.btn-group .btn', row).eq(1),
                    menu_items = $('.dropdown-menu li', row),
                    unapprove_menu_item = $("[data-target='#RequestUnapprove']", menu_items).parent(),
                    assign_auto_menu_item = $("[data-target='#RequestAssignAuto']", menu_items).parent(),
                    status_cell = $('td', row).eq(18),
                    track_cell = $('td', row).eq(19),
                    trailer_cell = $('td', row).eq(20),
                    driver_cell = $('td', row).eq(21);

                menu_items.removeClass('disabled').addClass('disabled');
                unapprove_menu_item.removeClass('disabled');
                assign_auto_menu_item.removeClass('disabled');
                btn.removeClass('btn-success btn-primary btn-warning').addClass('btn-primary').text($('a', assign_auto_menu_item).text());
                btn2.removeClass('btn-success btn-primary btn-warning').addClass('btn-primary');
                status_cell.text(data.status);
                track_cell.text('');
                trailer_cell.text('');
                driver_cell.text('');
                $('#RequestUnassignAuto').modal('hide');
                update_data_table();
            })
            .fail(function (xhr, errmsg, err) {
                console.log(xhr + " | " + errmsg + " | " + err);
            });
    };

    $('#track').on('change', function () {
        var driver = $('#track option:selected').data('driver');
        $('#driver').val(driver);
    });

    $(document).on('click', '.btn-request-action', function (e) {
        var btn = $(this), action = btn.text();
        if (action == 'Согласовать') {
            approve(btn.closest('tr'));
        }
        else {
            var menu_item = $(".dropdown-menu a:contains('" + action + "')", btn.closest('div'));
            menu_item.trigger('click');
        }
        e.preventDefault();
    });

    $('#RequestUnapprove').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        current_row = button.closest('tr');
    });

    $('#RequestUnapproveBtnSave').on('click', function () {
        unapprove(current_row);
    });

    $('#RequestDeny').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        current_row = button.closest('tr');
    });

    $('#RequestDenyBtnSave').on('click', function () {
        deny(current_row);
    });

    $('#RequestAssignAuto').on('show.bs.modal', function (event) {
        $('#assign_error').empty();
        var button = $(event.relatedTarget); // Button that triggered the modal
        current_row = button.closest('tr');       
    });
    

    $('#RequestAutoVisit').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        current_row = button.closest('tr');
        var visit_take_value = current_row.children('td.cell-auto_visit_take_plan').children('a').text(),
            visit_return_value = current_row.children('td.cell-auto_visit_return_plan').children('a').text();
        $('#id_auto_visit_take_plan1').val(visit_take_value)  
        $('#id_auto_visit_return_plan1').val(visit_return_value)        
    });


    $('#RequestAutoVisitBtnSave').on('click', function () {
        save_order_data({
                'pk':current_row.attr('id').replace('request_', ''),
                'auto_visit_take_plan':$('#id_auto_visit_take_plan1').val() ,
                'auto_visit_return_plan':$('#id_auto_visit_return_plan1').val()
            },
            function() {
                $('#RequestAutoVisit').modal('hide');
            }
        );
    });


    // Модальное окно - коммент диспетчера
    $('#CommentEditModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        current_row = button.closest('tr');
        var comment = current_row.children('td.cell-dispatcher_comment').children('span').text();
        $('#disp_comment_input').val(comment);
     });

    // Сохраняем коммент
    $('#DispCommentBtnSave').on('click', function() {
        save_order_data({
                'pk':current_row.attr('id').replace('request_', ''),
                'dispatcher_comment': $('#disp_comment_input').val()
            },
            function() {
                $('#CommentEditModal').modal('hide');
            }
        );
    });


    $('#RequestAssignAutoBtnSave').on('click', function () {
        assign(current_row);
    });

    $('#RequestUnassignAuto').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        current_row = button.closest('tr');
    });

    $('#RequestUnassignAutoBtnSave').on('click', function () {
        unassign(current_row);
    });

    var removeNSK = function (s) {
        return s === null || typeof s === "undefined" ? "" : s.replace(' ул ', ' ')
            .replace('Кемеровская обл, ', '')
            .replace('Новосибирская обл, ', '')
            .replace('Томская обл, ', '')
            .replace('Омская обл, ', '')
            .replace('г Новосибирск, ', '');
    };

    var settings = dataTableSettings;
    settings.autoWidth = false;
    settings.scrollX = false;
    settings.fixedColumns = false;
    settings.processing = true;
    settings.searching = true;
    settings.ajax = {
        'url': '/' + app_name + '/get_experiment/', 'type': 'POST', 'data': function (d) {
            d.status_ids = status_ids;
            d.date_start = date_start;
            d.date_end = date_end;
        }
    };
    settings.dom = 'Bfrtip';
    settings.buttons = [
        {
            extend: 'print',
            text: 'Печать',
            exportOptions: {
                columns: [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
            }
        },
        {
            extend: 'excel',
            text: 'В excel',
            exportOptions: {
                columns: [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,29,31]
            },
            filename: function () {
                return 'Заявки ' + date_start + ' - ' + date_end;
            }
        },
        {
            extend: 'colvis',
            collectionLayout: 'fixed four-column'
        }
    ];
    settings.language = {
        buttons: {
            colvis: 'Выбрать колонки'
        }
    };
    settings.columns = [
        {'data': 'row_num', 'className': 'cell-row_num', 'width': '15px', 'orderable': false },
        {'data': 'Number', 'className': 'cell-number', 'width': '30px'},
        {'data': 'action', 'className': 'cell-action', 'width': '140px', 'orderable': false },
        {'data': 'driver_action', 'className': 'cell-status', 'width': '60px', 'orderable': false },
        {'data': 'ExportDate', 'className': 'cell-date', 'width': '60px'},
        {'data': 'Client', 'className': 'cell-client', 'width': '70px'},
        {'data': 'Customer', 'className': 'cell-customer', 'width': '100px'},
        {'data': 'Foot', 'className': 'cell-foot', 'width': '30px'},
        {'data': 'ContainerNum', 'className': 'cell-container', 'width': '100px', 'orderable': false },
        {'data': 'recognized_container_num', 'className': 'cell-recognized_container_num', 'width': '100px', 'orderable': false },
        {'data': 'Weight', 'className': 'cell-weight', 'width': '30px'},
        {'data': 'Operation', 'className': 'cell-operation', 'width': '35px'},
        {'data': 'Proxy', 'className': 'cell-proxy', 'width': '100px', 'orderable': false },
        {'data': 'TerminalLoad', 'className': 'cell-terminalload', 'width': '60px'},
        {'data': 'Address', 'className': 'cell-address', 'width': '140px', 'orderable': false },
        {'data': 'District', 'className': 'cell-district', 'width': '90px'},
        {'data': 'RaywayCr', 'className': 'cell-RaywayCr1', 'width': '60px', 'orderable': false },
        {'data': 'TerminalDeliver', 'className': 'cell-terminaldeliver', 'width': '60px'},
        {'data': 'Responsible', 'className': 'cell-responsible', 'width': '100px', 'orderable': false },
        {'data': 'Status', 'className': 'cell-status', 'width': '80px', 'orderable': false },
        {'data': 'truck', 'className': 'cell-truck', 'width': '80px', 'orderable': false },
        {'data': 'trailer', 'className': 'cell-trailer', 'width': '80px', 'orderable': false },
        {'data': 'driver', 'className': 'cell-driver', 'width': '80px'},
        {'data': 'dispatch_date', 'className': 'cell-dispatch_date', 'width': '80px'},
        {'data': 'approve_date', 'className': 'cell-approve', 'width': '80px'},
        {'data': 'decline_date', 'className': 'cell-decline_date', 'width': '60px', 'orderable': false },
        {'data': 'correction_date', 'className': 'cell-correction_date', 'width': '60px', 'orderable': false },
        {'data': 'auto_visit_take_plan', 'className': 'cell-auto_visit_take_plan', 'width': '60px', 'orderable': false },
        {'data': 'auto_visit_return_plan', 'className': 'cell-auto_visit_return_plan', 'width': '60px', 'orderable': false },
        {'data': 'Comment', 'className': 'cell-comment', 'width': '150px', 'orderable': false },
        {'data': 'dz', 'className': 'cell-dz', 'width': '0'},
        {'data': 'destination_station', 'className': 'cell-destination_station', 'width': '150px', 'orderable': false },
        {'data': 'cargo_manager', 'className': 'cell-cargo_manager', 'width': '70px', 'orderable': false },
        {'data': 'dispatcher_comment', 'className': 'cell-dispatcher_comment', 'width': '150px', 'orderable': false },
        {'data': 'attachments', 'className': 'cell-attachments', 'width': '150px', 'orderable': false },
        {'data': 'attachments_client', 'className': 'cell-attachments_client', 'width': '150px', 'orderable': false },
    ];
    settings.columnDefs = [{
        "targets": 2,
        "data": "Status",
        "orderable": false,
        "render": function (data, type, full) {
            var s = full.Status;
            if (s == 'Заготовка' || s == 'Завершено' || s == 'Отказ' || s == 'Отмена' || s == 'Сдан' || s == 'Проверено') {
                return s == 'Отказ' ? s + '<br>' + full.decline_reason : s;
            }
            var template = '<div class="btn-group btn-group-xs">\
              <button type="button" class="btn {{btn}} btn-xs btn-request-action">{{action}}</button>\
              <button type="button" class="btn {{btn}} dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                <span class="caret"></span>\
                <span class="sr-only">Toggle Dropdown</span>\
              </button>\
              <ul class="dropdown-menu">\
                <li class="{{approve_disabled}}"><a href="#">Согласовать</a></li>\
                <li class="{{unapprove_disabled}}"><a href="#" data-toggle="modal" data-target="#RequestUnapprove">Отмена согласования</a></li>\
                <li class="{{deny_disabled}}"><a href="#" data-toggle="modal" data-target="#RequestDeny">Отказать</a></li>\
                <li class="{{assign_auto_disabled}}"><a href="#" data-toggle="modal" data-target="#RequestAssignAuto">Назначить авто</a></li>\
                <li class="{{unassign_auto_disabled}}"><a href="#" data-toggle="modal" data-target="#RequestUnassignAuto">Снять авто</a></li>\
                <li class=""><a href="#" id="RequestClose">Завершить</a></li>\
              </ul>\
            </div>';
            var action = 'Согласовать',
                btn = 'btn-success',
                approve_disabled = 'disabled',
                deny_disabled = 'disabled',
                assign_auto_disabled = 'disabled',
                unassign_auto_disabled = 'disabled',
                unapprove_disabled = 'disabled';

            if (s == 'Согласование') {
                approve_disabled = '';
                deny_disabled = '';
            }
            if (s == 'Согласовано') {
                action = 'Назначить авто';
                btn = 'btn-primary';
                unapprove_disabled = '';
                assign_auto_disabled = '';
            }
            if (s == 'Выполняется') {
                action = 'Снять авто';
                btn = 'btn-warning';
                unassign_auto_disabled = '';
            }
            var re = new RegExp('{{btn}}', 'g'),
                html = template
                    .replace('{{action}}', action)
                    .replace(re, btn)
                    .replace('{{approve_disabled}}', approve_disabled)
                    .replace('{{unapprove_disabled}}', unapprove_disabled)
                    .replace('{{deny_disabled}}', deny_disabled)
                    .replace('{{assign_auto_disabled}}', assign_auto_disabled)
                    .replace('{{unassign_auto_disabled}}', unassign_auto_disabled);
            return html;
        }
    },

    {
            "targets": 3,
            "data": "driver_action",
            "orderable": true,
            "render": function (data, type, full) {
            var order_id  = parseInt(full.DT_RowId.replace('request_', ''))
            var temp = data + '<br>'
                  + '<a href="/' + app_name + '/get_driver_actions?order_id=' + order_id + '"'
                  + 'data-toggle="modal" data-remote="false" data-target="#AppStatusHistory">Отчет</a>';

            var t = data == '' ? data : temp
            return t
            }
        },

        {
            "targets": 13,
            "data": "TerminalLoad",
            "orderable": true,
            "render": function (data, type, full) {
                return "<span title='" + full.TerminalLoad + "'>" + removeNSK(full.TerminalLoad) + "</span>"
            }
        },
        {
            "targets": 14,
            "data": "Address",
            "orderable": true,
            "render": function (data, type, full) {
                return "<span title='" + full.Address + "'>" + removeNSK(full.Address) + "</span>";
            }
        },
        {
            "targets": 16,
            "data": "RaywayCr",
            "orderable": true,
            "render": function (data, type, full) {
                var template = "<span title='" + full.RaywayCr + "'>" + removeNSK(full.RaywayCr) + "</span>";
                if (full.RaywayCr2) template += "<hr class='short'><span title='" + full.RaywayCr2 + "'>" + removeNSK(full.RaywayCr2) + "</span>";
                return template;
            }
        },
        {
            "targets": 17,
            "data": "TerminalDeliver",
            "orderable": true,
            "render": function (data, type, full) {
                return "<span title='" + full.TerminalDeliver + "'>" + removeNSK(full.TerminalDeliver) + "</span>"
            }
        },
        {
            "targets": 22,
            "data": "driver",
            "orderable": true,
            "render": function (data, type, full) {
                return data + "<br> " + full.driver_phone;
            }
        },
        {
            "targets": 24,
            "data": "approve_date",
            "orderable": true,
            "render": function (data, type, full) {
                return data + " " + full.dispatcher_approve;
            }
        },
        {
            "targets": 25,
            "data": "decline_date",
            "orderable": true,
            "render": function (data, type, full) {
                if (data) return data + " " + full.dispatcher_decline;
                if (full.cancel_date) return "<span title='" + full.cancel_date + "\n" + full.Manager + "\n" + full.cancel_reason + "'>" + full.cancel_date + " " + full.Manager + "</span>";
                return "";
            }
        },
           {
            "targets": 27,
            "data": "auto_visit_take_plan",
            "orderable": true,
            "render": function (data, type, full) {
                return AutoVisitFormat(full.auto_visit_take_plan,
                                full.auto_visit_take_fact,
                                full.auto_visit_take_num,
                                'take'
                            )
            }
        },
            {
            "targets": 28,
            "data": "auto_visit_return_plan",
            "orderable": true,
            "render": function (data, type, full) {
                return AutoVisitFormat(full.auto_visit_return_plan,
                                full.auto_visit_return_fact,
                                full.auto_visit_return_num,
                                'return'
                            )
            }
        },
        {
            "targets": 33,
            "data": "dispatcher_comment",
            "orderable": true,
            "render": function (data, type, full) {
                return '<span class="disp_comment_val">' + data + '</span><br>' +
                        '<a href="#" data-toggle="modal" data-target="#CommentEditModal">Редактировать</a>'
            }
        },
        {
            "targets": 34,
            "data": "attachments",
            "orderable": true,
            "render": function (data, type, full) {
                return get_document_link(data)
            }
       },
       {
            "targets": 35,
            "data": "attachments_client",
            "orderable": true,
            "render": function (data, type, full) {
                return get_document_link(data)
            }
       }
    ];
    settings.order = [[4, "asc"]];

    var table = $('#RequestList').DataTable(settings);
    table.columns().every(function () {
        var that = this, header = this.header();

        $('input', header).on('keyup change', function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });

    $('#RequestList tbody').on('click', 'tr', function () {
        select_table_row($(this), '#RequestList')
    });

    $(document).on('click', '#RequestClose', function () {
        var button = $(event.target); // Button that triggered the modal
        current_row = button.closest('tr');
        close(current_row);
    });

    $('#dropdownMenu1').on('click', function (event) {
        $(this).parent().toggleClass("open");
    });

    $('body').on('click', function (e) {
        var filter_status_menu = $('#dropdownMenu1'), open = $('.open');
        if (!filter_status_menu.is(e.target) && filter_status_menu.has(e.target).length === 0 && open.has(e.target).length === 0) {
            filter_status_menu.parent().removeClass('open');
        }
    });

    $('#filter_status_apply').on('click', function () {
        $('#dropdownMenu1').parent().removeClass('open');
        var ids = [];
        $('[name=filter_status]:checked').each(function () {
            ids.push(this.value);
        });
        status_ids = ids.join();
        update_data_table();
    });

    $('#btn_refresh').on('click', function () {
        update_data_table();
    });


    $('#date_range').daterangepicker(
        {
            locale: {
                "format": date_format,
                "applyLabel": "Применить",
                "cancelLabel": "Отмена",
                "fromLabel": "С",
                "toLabel": "По",
                "customRangeLabel": "Другой",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            startDate: moment().format(date_format),
            endDate: moment().format(date_format)
        },
        function (start, end, label) {
            date_start = start.format('YYYY-MM-DD');
            date_end = end.format('YYYY-MM-DD');
            update_data_table();
            excel_date(date_start, date_end);
        });

    $('#filter_client').select2({width: '100%', theme: "bootstrap"});
    $('#filter_client').on('change', function () {
        var table = $('#RequestList').DataTable(), val = $(this).val();
        table.column(3).search(val).draw();
    });

    $('#filter_foot').on('change', function () {
        var table = $('#RequestList').DataTable(), val = $(this).val();
        table.column(5).search(val).draw();
    });

    $('#filter_dz').on('change', function () {
        var table = $('#RequestList').DataTable(), val = $(this).val();
        table.column(25).search(val).draw();
    });

    $('#track').select2({width: '100%', theme: "bootstrap"});


});
function newFunction() {
    var context = {
        id: 0
    };
    return context;
}

