$(function () {
    var id = 0;
    var status_ids = '4,5,8,9';
    var date_format = 'DD.MM.YYYY';
    var date_start = moment().format('YYYY-MM-DD'),
        date_end = date_start;
    var row_colors = {'Выполняется': 'warning', 'Завершено': 'info', 'Сдан': 'success', 'Проверено': 'success'};

    var table_id = '#RequestList';

    var excel_date = function (start, end) {
        var new_hr = $('#exl_save').attr('data-url') + "?date_start=" + start + "&date_end=" + end;
        $('#exl_save').attr('href', new_hr)
    };

    excel_date(date_start, date_end)


    $('#timepicker1').datetimepicker({
        locale: 'ru',
        format: 'DD.MM HH:mm'
    });


    $('#timepicker2').datetimepicker({
        locale: 'ru',
        format: 'DD.MM HH:mm'
    });


    var time_diff = 0;

    var time_compare = function (current) {
        var dayDiff = 0,
            begin_d = new Date($('#timepicker1').data('DateTimePicker').date()),
            end_d = new Date($('#timepicker2').data('DateTimePicker').date());

        var d_a = begin_d.getDate(),
            d_b = end_d.getDate();

        if (begin_d > end_d || d_a > d_b) {
            $('#id_operation_date_end').closest('div').parent()
                .append("<h6 class='form-errors' style='color:red'>Дата начала завершения должна быть больше даты начала!</h6>")
        }
        else {
            if (d_b - d_a > 0) {
                var mid = 24 * 60;
                var before_mid = mid - begin_d.getHours() * 60 - begin_d.getMinutes();
                dayDiff = before_mid + end_d.getHours() * 60 + end_d.getMinutes();
            }
            else {
                var h_a = begin_d.getHours() * 60,
                    m_a = begin_d.getMinutes(),
                    h_b = end_d.getHours() * 60,
                    m_b = end_d.getMinutes();

                dayDiff = h_b + m_b - h_a - m_a;
            }
        }

        return dayDiff
    };

    // default free minute = 0; Redefine on db data on modal show
    var idle_free_minute = 0;


    var define_idle = function (idle) {
        var res_idle = 0;
        if (idle > idle_free_minute) {
            res_idle = idle - idle_free_minute
        }
        return res_idle
    };

    var input_totals = function(input_name) {

        var totals = 0;

        $('.add_service_row').find('[name*="' + input_name + '"]').each(function(){
            totals = parseFloat($(this).val()) + totals;
        });

        return totals
    };

    var recalc_form = function() {

      var client_rate = $('#id_client_rate').val();
      var salary_rate = $('#id_salary_rate').val();

      var client_idle_cost = $('#id_client_idle_cost').val();
      var salary_idle_cost = $('#id_salary_idle_cost').val();

      var client_transfer = $('#id_client_transfer').val();
      var salary_transfer = $('#id_salary_transfer').val();

      var add_service_client_total = input_totals('client_total');
      var add_service_salary_total = input_totals('salary_total');


      // Calc total
      var client_total = +client_rate + +client_idle_cost + +client_transfer + +add_service_client_total;
      var salary_total = +salary_rate + +salary_idle_cost + +salary_transfer + +add_service_salary_total;

      $('#id_client_total').val(client_total);
      $('#id_salary_total').val(salary_total);

    };


    $('.picker').on('dp.change',function () {
        $(".form-errors").remove();
        if ($('#timepicker1') !== 'undefined' && $('#timepicker2') !== 'undefined') {
          //Set idle form
            var idle_minutes = define_idle(time_compare());
            var idle_client_price = $('#id_client_idle_price').val();
            var idle_salary_price = $('#id_salary_idle_price').val();

            var client_idle_cost = idle_client_price * idle_minutes;
            var salary_idle_cost = idle_salary_price * idle_minutes;

            $('#id_client_idle_minutes').val(idle_minutes);
            $('#id_salary_idle_minutes').val(idle_minutes);

            $('#id_client_idle_cost').val(client_idle_cost);
            $('#id_salary_idle_cost').val(salary_idle_cost);

        //Set total forms
        recalc_form()

        }
    });

    $('#btnRecalc').on('click', function() {
        recalc_form();
        save(false,false)
    });


    $('#datepicker').datetimepicker({
        locale: 'ru',
        format: 'DD.MM.YYYY'
    });

    $('#Request').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget);

        $('#RequestForm')[0].reset();
        var ctx = button.closest('tr');
        id = parseInt(ctx.attr('id').replace('request_', ''), 10);
        var
            status = $('.cell-status', ctx).text(),
            salary_rate = $('.cell-salary_rate', ctx).text(),
            salary_idle_minutes = $('.cell-salary_idle_minutes', ctx).text(),
            salary_idle_price = $('.cell-salary_idle_price', ctx).text(),
            salary_idle_cost = $('.cell-salary_idle_cost', ctx).text(),
            salary_transfer = $('.cell-salary_transfer', ctx).text(),
            salary_total = $('.cell-salary_total', ctx).text(),
            client_rate = $('.cell-client_rate', ctx).text(),
            client_idle_minutes = $('.cell-client_idle_minutes', ctx).text(),
            client_idle_price = $('.cell-client_idle_price', ctx).text(),
            client_idle_cost = $('.cell-client_idle_cost', ctx).text(),
            client_transfer = $('.cell-client_transfer', ctx).text(),
            client_total = $('.cell-client_total', ctx).text(),
            additional_wastes = $('.cell-additional_wastes', ctx).text(),
            platon = $('.cell-platon', ctx).text(),
            bill_number = $('.cell-bill_number', ctx).find('span').data('bill_id'),
            payment_date = $('.cell-payment_date', ctx).text(),
            profit = $('.cell-profit', ctx).text(),
            operation_date_begin = $('.cell-date-begin', ctx).text(),
            operation_date_end = $('.cell-date-end', ctx).text(),
            container_number = $('.cell-ContainerNum', ctx).text(),
            weight = $('.cell-Weight', ctx).text(),
            foot = parseInt($('.cell-Foot', ctx).text(), 10);

        request_num  = $('.cell-number', ctx).text();
        $('#req_num_title').text(request_num);

        if(status === 'Сдан' || status === 'Завершено' || status === 'Проверено') $('#btnValidate').show(); else $('#btnValidate').hide();
        if(status === 'Проверено') $('#btnSave').hide(); else $('#btnSave').show();

        var $id_client_idle_minutes = $('#id_client_idle_minutes');

        if ($id_client_idle_minutes.length ) {

             if (foot === 20) {
                idle_free_minute = $id_client_idle_minutes.data('idlefree').split('/')[0];
             }
            else {
                idle_free_minute = $id_client_idle_minutes.data('idlefree').split('/')[1]
            }
        }



        $('#id_salary_rate').val(salary_rate);
        $('#id_salary_idle_minutes').val(salary_idle_minutes);
        $('#id_salary_idle_price').val(salary_idle_price);
        $('#id_salary_idle_cost').val(salary_idle_cost);
        $('#id_salary_transfer').val(salary_transfer);
        $('#id_salary_total').val(salary_total);
        $('#id_client_rate').val(client_rate);
        $id_client_idle_minutes.val(client_idle_minutes);
        $('#id_client_idle_price').val(client_idle_price);
        $('#id_client_idle_cost').val(client_idle_cost);
        $('#id_client_transfer').val(client_transfer);
        $('#id_client_total').val(client_total);
        $('#id_additional_wastes').val(additional_wastes);
        $('#id_platon').val(platon);
        $('#id_bill_number').val(bill_number);

        $('#id_payment_date').val(payment_date);
        $('#id_profit').val(profit);
        $('#id_operation_date_begin').val(operation_date_begin);
        $('#id_operation_date_end').val(operation_date_end);
        $('#id_ContainerNum').val(container_number).prop('readonly', !!container_number);
        $('#id_Weight').val(weight);

        //add services
        $(".cell_add_services", ctx).each(function(index) {
            var t = $(this);
            var a  = parseInt($(this).find('span').text());

//            var x = sub_names[index]
            if ( a > 0 ) {
                var service_code = t.find('span').attr('data-service_code'), service_name = t.find('span').data('service_name');


                var selector = '[name="add_service.' + service_code +'.service_code"]'
                elem = $(selector)
                if (elem.length == 0) {
                       var row = insert_add_service()
                       $('.service_code_select',row).val(service_code)
                       $('.service_code_select',row).change()
                }

                $('[name="add_service.' + service_code + '.' + service_name +'"]',row).val(a);


            }
        });

    }).on('hide.bs.modal', function () {
        $(".form-errors").remove();
        $(".add_service_row").remove();

    });


    var row_init = function (row) {
        var statusId = $('#id_Status').val();
        $('.cell-status', row).text(statusId === '9' ? 'Проверено' : 'Сдан');
        $('.cell-salary_rate', row).text($('#id_salary_rate').val());
        $('.cell-salary_idle_minutes', row).text($('#id_salary_idle_minutes').val());
        $('.cell-salary_idle_price', row).text($('#id_salary_idle_price').val());
        $('.cell-salary_idle_cost', row).text($('#id_salary_idle_cost').val());
        $('.cell-salary_transfer', row).text($('#id_salary_transfer').val());
        $('.cell-salary_total', row).text($('#id_salary_total').val());
        $('.cell-client_rate', row).text($('#id_client_rate').val());
        $('.cell-client_idle_minutes', row).text($('#id_client_idle_minutes').val());
        $('.cell-client_idle_price', row).text($('#id_client_idle_price').val());
        $('.cell-client_idle_cost', row).text($('#id_client_idle_cost').val());
        $('.cell-client_transfer', row).text($('#id_client_transfer').val());
        $('.cell-client_total', row).text($('#id_client_total').val());
        $('.cell-additional_wastes', row).text($('#id_additional_wastes').val());
        $('.cell-platon', row).text($('#id_platon').val());
        $('.cell-bill_number', row).text($('#id_bill_number').val());
        $('.cell-payment_date', row).text($('#id_payment_date').val());
        $('.cell-profit', row).text($('#id_profit').val());

    };

    var save = function (validate, change_status) {

        //    default value of parameters
        if (change_status === undefined) {
          change_status = true;
        }

        var row = $('#request_' + id);

        //Если заявка не завершена диспетчером - кидаем алерт и ничего не делаем
        var allowed_statuses = ['Завершено', 'Сдан', 'Проверено']
        var request_status = $('.cell-status', row).text()

        if (allowed_statuses.indexOf(request_status) < 0) {
            alert("Заявка не завершена диспетчером! ")
            return
        }


        if (change_status) {
            $('#id_Status').val(validate ? '9' : '8');
        }
        else {
            $('#id_Status').val(5)
        }

        var salary_idle_minutes = $('#RequestForm').find('input[name="salary_idle_minutes"]').val();
        if (salary_idle_minutes == "") {
                alert("Заполните формы с датами!")
                return
        }

        $.ajax({
            url: "save_work/?id=" + id, // the endpoint
            type: "POST", // http method
            data: $('#RequestForm').serialize(), // data sent with the post request
            // handle a successful response
            success: function (json) {
                var err = json.err.length;
                console.log(json); // log the returned json to the console
                if (err === 0) {
                    $(".form-errors").remove();
                    $('#Request').modal('hide');
                    update_data_table();
                }
                else {
                    //Remove old errors if errors occures more then once
                    row.attr('id', 'request_' + json.id);
                    $.each(json.messages, function (field) {
                        // var cl = $('.form-group');
                        if ($('[name="' + field + '"]').is("select")) {
                            $('[name="' + field + '"]').closest('div').append("<h6 class='form-errors' style='color:red'>Поле не может быть пустым</h6>")
                        }
                        else {
                            $('[name="' + field + '"]').closest('div').parent().append("<h6 class='form-errors' style='color:red'>Поле не может быть пустым</h6>")
                        }
                    })
                }
            },

            // handle a non-successful response
            error: function (xhr/*, errmsg, err*/) {
                console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    };

    $('#btnSave').on('click', function () {
        save(false);
    });

    $('#btnValidate').on('click', function () {
        save(true);
    });



    var insert_add_service = function() {

      if ( $('.AddServiceHeader').length  == 0 )  {
                var title = $('<div class="form-header AddServiceHeader bill_row">Расчет доп.услуг</div>');
                title.insertAfter($('.bill_row').last());
            }


            var copy_row = $('.AddServiceRow').clone(true, true);
            copy_row.removeClass('AddServiceRow');
            copy_row.find(':input').removeAttr('disabled');
            copy_row.addClass('add_service_row');

//            $('.service_code_select', copy_row).addClass('sample')

            copy_row.insertAfter($('.bill_row').last());
            return copy_row

    }
    // добавляем строку доп услуг
    $('#AddServiceAppend').on('click', function () {
      var a = insert_add_service()
    });

//    пересчитываем форму с учетом данных всех строк с доп услугами
    $('#add_btn').on('click', function () {
       recalc_form();

    });

// Удаляем форму с доп услугами
    $('#trash_btn').on('click', function () {

       $(this).closest('.add_service_row').remove();

        if ( $('.add_service_row').length  == 0 )  {
            $('.AddServiceHeader').remove();
        }

       recalc_form();

    });


// проставляем имена для элементов ввода для сериализации
    $('.service_code_select').on('change', function () {

       var t = $(this)
       inputs = t.closest('.add_service_row').find(':input'),

      $.each(inputs,function( index, item ) {
        var orig_name = $(item).attr('name')
        $(this).attr('name','add_service.' + t.val() + '.' + $(this).attr('name').split('.').pop());

        });

        $(this).removeClass('sample');
    });

    var removeNSK = function (s) {
        return s === null || typeof s === "undefined" ? "" : s.replace(' ул ', ' ')
            .replace('Кемеровская обл, ', '')
            .replace('Новосибирская обл, ', '')
            .replace('Томская обл, ', '')
            .replace('Омская обл, ', '')
            .replace('г Новосибирск, ', '');
    };


    var get_services = function (full, code, name) {
        var ad = JSON.parse(full.add_services);
        var res = { 'client_total': 0, 'salary_total': 0 };
        $.each(ad, function(ind,item) {
            if (item.fields.service_code == code) {
                res = item.fields
            }

        })
        return "<span data-service_code='" + code + "' data-service_name='"+ name + "'>" + res[name] + "</span>"
    }


    var settings = dataTableSettings;
    settings.scrollX = false;
    settings.columns = [
        {'data': 'row_num', 'className': 'cell-row_num', 'width': '20px', 'orderable': false},
        {'data': 'Number', 'className': 'cell-number', 'width': '30px'},
        {'data': 'Status', 'className': 'cell-status', 'width': '60px'},
        {'data': 'driver_action', 'className': 'cell-driver_action', 'width': '60px', 'orderable': false},
        {'data': 'ExportDate', 'className': 'cell-ExportDate', 'width': '60px'},
        {'data': 'driver', 'className': 'cell-driver', 'width': '80px'},
        {'data': 'truck', 'className': 'cell-truck', 'width': '70px', 'orderable': false},
        {'data': 'Client', 'className': 'cell-Client', 'width': '70px'},
        {'data': 'ContainerNum', 'className': 'cell-ContainerNum', 'width': '80px', 'orderable': false},
        {'data': 'recognized_container_num', 'className': 'cell-recognized_container_num', 'width': '80px', 'orderable': false},
        {'data': 'Foot', 'className': 'cell-Foot', 'width': '30px'},
        {'data': 'Weight', 'className': 'cell-Weight', 'width': '30px'},
        {'data': 'Operation', 'className': 'cell-Operation', 'width': '35px'},
        {'data': 'dz', 'className': 'cell-dz', 'width': '60px'},
        {'data': 'TerminalLoad', 'className': 'cell-TerminalLoad', 'width': '60px'},
        {'data': 'Address', 'className': 'cell-Address', 'width': '160px', 'orderable': false},
        {'data': 'District', 'className': 'cell-District', 'width': '70px'},
        {'data': 'RaywayCr', 'className': 'cell-RaywayCr', 'width': '90px', 'orderable': false},
        {'data': 'RaywayCr2', 'className': 'cell-RaywayCr2', 'width': '90px', 'orderable': false},
        {'data': 'TerminalDeliver', 'className': 'cell-TerminalDeliver', 'width': '60px'},
        {'data': 'destination_station', 'className': 'cell-destination_station', 'width': '150px', 'orderable': false},
        {'data': 'Comment', 'className': 'cell-Comment', 'width': '180px', 'orderable': false},
        {'data': 'additional_wastes', 'className': 'cell-additional_wastes', 'width': '50px'},
        {'data': 'platon', 'className': 'cell-platon', 'width': '40px'},
        {'data': 'date_begin', 'className': 'cell-date-begin', 'width': '60px'},
        {'data': 'date_end', 'className': 'cell-date-end', 'width': '60px'},
        {'data': 'salary_rate', 'className': 'cell-salary_rate', 'width': '50px'},
        {'data': null, 'className': 'cell_add_services additional_service_driver', 'width': '100px', 'orderable': false},
        {'data': null, 'className': 'cell_add_services additional_service_driver', 'width': '50px', 'orderable': false},
        {'data': 'salary_idle_minutes', 'className': 'cell-salary_idle_minutes', 'width': '50px'},
        {'data': 'salary_idle_price', 'className': 'cell-salary_idle_price', 'width': '50px'},
        {'data': 'salary_idle_cost', 'className': 'cell-salary_idle_cost', 'width': '50px'},
        {'data': 'salary_transfer', 'className': 'cell-salary_transfer', 'width': '50px'},
        {'data': 'salary_total', 'className': 'cell-salary_total', 'width': '40px'},
        {'data': 'client_rate', 'className': 'cell-client_rate', 'width': '50px'},
        {'data': null, 'className': 'cell_add_services additional_service_client', 'width': '100px', 'orderable': false},
        {'data': null, 'className': 'cell_add_services additional_service_client', 'width': '50px', 'orderable': false},
        //пломба не нужна пока
//        {'data': null, 'className': 'cell_add_services additional_service_client', 'width': '50px'},
        {'data': 'client_idle_minutes', 'className': 'cell-client_idle_minutes', 'width': '50px'},
        {'data': 'client_idle_price', 'className': 'cell-client_idle_price', 'width': '50px'},
        {'data': 'client_idle_cost', 'className': 'cell-client_idle_cost', 'width': '50px'},
        {'data': 'client_transfer', 'className': 'cell-client_transfer', 'width': '50px'},
        {'data': 'client_total', 'className': 'cell-client_total', 'width': '50px'},
        {'data': 'bill_number', 'className': 'cell-bill_number', 'width': '80px'},
        {'data': 'payment_date', 'className': 'cell-payment_date', 'width': '50px'},
        {'data': 'profit', 'className': 'cell-profit', 'width': '50px'},
        {'data': 'manager', 'className': 'cell-manager', 'width': '70px'},
        {'data': 'cargo_manager', 'className': 'cell-cargo_manager', 'width': '70px'},
        {'data': 'attachments', 'className': 'cell-attachments', 'width': '150px', 'orderable': false},
        {'data': 'attachments_client', 'className': 'cell-attachments_client', 'width': '150px', 'orderable': false}

    ];

    // Показываем только те колонки, которые разрешены пользователю
    // user_visible_fields - задается в контексте страницы
    settings.columns = settings.columns.filter(function(item) {
        var service_name = (item.data == null) ? item.className.split(' ')[1] : item.data
        item.visible = user_visible_fields.indexOf(service_name) >= 0;
        return item
    })

    settings.select =  {
       "style": "multi"
    };
    settings.columnDefs = [

    {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        },
        {
        "targets": 1,
        "data": "Number",
        "orderable": true,
        "render": function (data) {
        var template = "<a href='#' role='button' data-toggle='modal' data-target='#Request' data-mode='edit'>" + data + "</a>";
            if (is_client == 1){
                    template =  data
                }
        return template
        }

        },
        {
            "targets": 3,
            "data": "driver_action",
            "orderable": true,
            "render": function (data, type, full) {
            var order_id  = parseInt(full.DT_RowId.replace('request_', ''))
            var temp = data + '<br>'
                  + '<a href="/' + app_name + '/get_driver_actions?order_id=' + order_id + '"'
                  + 'data-toggle="modal" data-remote="false" data-target="#AppStatusHistory">Отчет</a>';

            var t = data == '' ? data : temp
            return t
            }
        },
        {
            "targets": 14,
            "data": "TerminalLoad",
            "orderable": true,
            "render": function (data) {
                return "<span title='" + data + "'>" + removeNSK(data) + "</span>"
            }
        },
        {
            "targets": 15,
            "data": "Address",
            "orderable": true,
            "render": function (data,type,full) {
                return "<span title='" + data + "'>" + removeNSK(data) + "</span>"
            }
        },
        {
            "targets": 17,
            "data": "RaywayCr",
            "orderable": true,
            "render": function (data) {
                return "<span title='" + data + "'>" + removeNSK(data) + "</span>"
            }
        },
        {
            "targets": 18,
            "data": "RaywayCr2",
            "orderable": true,
            "render": function (data) {
                return "<span title='" + data + "'>" + removeNSK(data) + "</span>"
            }
        },
        {
            "targets": 19,
            "data": "TerminalDeliver",
            "orderable": true,
            "render": function (data) {
                return "<span title='" + data + "'>" + removeNSK(data) + "</span>"
            }
        },
        {
            "targets": [27,28, 35, 36],
            "data": "add_services",
            "orderable": true,
            "render": function (data,type,full,meta) {
                var res = '';
                if (full.add_services != "[]") {
                        switch (meta.col) {
                          case 35:
                            res = get_services(full,"forwarding", "client_total")
                            break;
                          case 27:
                            res = get_services(full,"forwarding", "salary_total")
                            break;
                          case 37:
                            res = get_services(full,"stamp","client_total")
                            break;
                          case 36:
                            res = get_services(full,"cleaning","client_total")
                            break;
                          case 28:
                            res = get_services(full,"cleaning","salary_total")
                            break;
                        }
                }
                return res
            }
        },
        {
            "targets": 42,
            "data": "bill_number",
            "orderable": true,
            "render": function (data,type,full,meta) {
                res = data.split(';')
                return "<span data-bill_id='" + res[0] + "'>" + res[1] + "</span>"
            }
        },
        {
            "targets": 47,
            "data": "attachments",
            "orderable": true,
            "render": function (data, type, full) {
               return get_document_link(data)
            }
       },
       {
            "targets": 48,
            "data": "attachments_client",
            "orderable": true,
            "render": function (data, type, full) {
               return get_document_link(data)
            }
       }
    ];



    settings.createdRow = function (row, data) {
        var r = $(row), cls = row_colors[data.Status];
        if(typeof cls !== "undefined" && cls) r.addClass(cls);
    };

    settings.fixedColumns = false;
    settings.searching = true;
    settings.dom = 'Bfrtip';
    settings.autoWidth = false;
    settings.buttons = [
        {
            extend: 'print',
            text: 'Печать'
        },
        {
            extend: 'excel',
            text: 'В excel',
            filename: function () {
                return 'Справка ' + date_start + ' - ' + date_end;
            }
        },
        {
            text: 'Сформировать акт',
            className: 'act_button',
            action: function ( e, dt, node, config ) { 
                try {
                    e.preventDefault();
                    var lbl,
                        f = $('#act_form'),
                        client_id = $('#filter_client').val();
                    if(!client_id) {
                        alert('Необходимо выбрать клиента!!!');
                        return;
                    }
                    var old_url = f.attr('action');
                    var new_url = f.attr('action') + '?' + $.param(dt.ajax.params())

                    $('#act_form').on('submit', function () {
                        $('#act_form').attr('action', new_url);
                    });

                    $('#act_form').submit();
                    $('#act_form').attr('action', old_url);
                }
                catch (err) {
                    alert('Ошибка ' + err.name + ":" + err.message + "\n" + err.stack);
                }

                update_data_table();
            }
        },        
        {
            extend: 'colvis',
            className: 'colvis_button',
            collectionLayout: 'fixed four-column'
        }
    ];

    settings.language = {
        buttons: {
            colvis: 'Выбрать колонки'
        }
    };
    settings.order = [[4, "asc"]];

    settings.ajax = {
        'url': '/' + app_name + '/get_manager_calc_table/', 'type': 'POST', 'data': function (d) {
            d.status_ids = status_ids;
            d.date_start = date_start;
            d.date_end = date_end;
        }
    };
    $('#RequestList').DataTable(settings);

    var update_data_table = function () {
        var table = $('#RequestList').DataTable();
        table.rows()
            .every(function () {
                var d = this.data();
                d.counter++; // update data source for the row
                this.invalidate(); // invalidate the data DataTables has cached for this row
            });
        table.draw();
    };

    $('#RequestList tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            var table = $('#RequestList').DataTable();
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });


    /*$('body').on('click', function (e) {
     var filter_status_menu = $('#dropdownMenu1'), open = $('.open');
     if (!filter_status_menu.is(e.target) && filter_status_menu.has(e.target).length === 0 && open.has(e.target).length === 0) {
     filter_status_menu.parent().removeClass('open');
     }
     });*/

    $('#btn_refresh').on('click', function () {
        update_data_table();
    });

    $('#date_range').daterangepicker({
            locale: {
                "format": date_format,
                "applyLabel": "Применить",
                "cancelLabel": "Отмена",
                "fromLabel": "С",
                "toLabel": "По",
                "customRangeLabel": "Другой",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            startDate: moment().format(date_format),
            endDate: moment().format(date_format)
        },
        function (start, end) {
            date_start = start.format('YYYY-MM-DD');
            date_end = end.format('YYYY-MM-DD');
            update_data_table();
            excel_date(date_start, date_end);
        });



    $('#filter_client').select2({width: '100%', theme: "bootstrap"})
    $('#filter_driver').select2({width: '100%', theme: "bootstrap"})

    $('.table_filter').on('change', function () {
        var col_num = $(this).data('col_num')
        var table = $(table_id).DataTable(), val = $(this).val();
        table.column(col_num).search(val).draw();
    });

// 2020-01-03 deleted duplicate code
//    $('#filter_driver')
//        .select2({width: '100%', theme: "bootstrap"})
//        .on('change', function () {
//            var table = $('#RequestList').DataTable(), val = $(this).val();
//            table.column(3).search(val).draw();
//        });
//
//    $('#filter_truck')
//        .select2({width: '100%', theme: "bootstrap"})
//        .on('change', function () {
//            var table = $('#RequestList').DataTable(), val = $(this).val();
//            table.column(4).search(val).draw();
//        });
//
//    $('#filter_client')
//        .select2({width: '100%', theme: "bootstrap"})
//        .on('change', function () {
//            var table = $('#RequestList').DataTable(), val = $(this).val();
//            table.column(5).search(val).draw();
//        });
//
//    $('#filter_foot').on('change', function () {
//        var table = $('#RequestList').DataTable(), val = $(this).val();
//        table.column(7).search(val).draw();
//    });
//
//    $('#filter_dz').on('change', function () {
//        var table = $('#RequestList').DataTable(), val = $(this).val();
//        table.column(10).search(val).draw();
//    });
//
//    $('#filter_district').on('change', function () {
//        var table = $('#RequestList').DataTable(), val = $(this).val();
//        table.column(13).search(val).draw();
//    });
//
//    $('#filter_district1').on('change', function () {
//        var table = $('#RequestList').DataTable(), val = $(this).val();
//        table.column(13).search(val).draw();
//    });
//
//    $('#filter_rec_c').on('change', function () {
//        var table = $('#RequestList').DataTable(), val = $(this).val();
//        table.column(9).search(val).draw();
//    });


    $('#dropdownMenu1').on('click', function () {
        $(this).parent().toggleClass("open");
    });

    $('body').on('click', function (e) {
        var filter_status_menu = $('#dropdownMenu1'), open = $('.open');
        if (!filter_status_menu.is(e.target) && filter_status_menu.has(e.target).length === 0 && open.has(e.target).length === 0) {
            filter_status_menu.parent().removeClass('open');
        }
    });

    $('#filter_status_apply').on('click', function () {
        $('#dropdownMenu1').parent().removeClass('open');
        var ids = [];
        $('[name=filter_status]:checked').each(function () {
            ids.push(this.value);
        });
        status_ids = ids.join();
        update_data_table();
    });

    // Скрываем возможность выбора колонок для обычных пользователя
    // var is_admin = user_roles.indexOf('Администратор');

    if (is_client == 1 || hide_act_buttons == 1) {
        var table = $('#RequestList').DataTable();
        table.buttons('.act_button').nodes().css("display", "none");
    }

    if (is_client == 1 || hide_colvis_buttons == 1) {
        var table = $('#RequestList').DataTable();
        table.buttons('.colvis_button').nodes().css("display", "none");
    }

    $("#AppStatusHistory").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });

});
