// Wialon site dns
var dns = "https://hosting.wialon.com";

// Main function
function getToken() {
    // construct login page URL
	var url = dns + "/login.html"; // your site DNS + "/login.html"
	url += "?client_id=" + "App";	// your application name
    url += "&access_type=" + 0x100;	// access level, 0x100 = "Online tracking only"
    url += "&activation_time=" + 0;	// activation time, 0 = immediately; you can pass any UNIX time value
    url += "&duration=" + 604800;	// duration, 604800 = one week in seconds
    url += "&flags=" + 0x1;			// options, 0x1 = add username in response

    url += "&redirect_uri=" + dns + "/post_token.html"; // if login succeed - redirect to this page

    // listen message with token from login page window
    window.addEventListener("message", tokenRecieved);

    // finally, open login page in new window
    window.open(url, "_blank", "width=760, height=500, top=300, left=500");
}

// Help function


function logout() {
    var sess = wialon.core.Session.getInstance();
	if (sess && sess.getId()) {
    	sess.logout(function() {
            document.getElementById("logout").setAttribute("disabled", "");
            document.getElementById("login").removeAttribute("disabled");
        });
    }
}


var map, marker; // global variables

// Print message to log
function msg(text) { $("#log").prepend(text + "<br/>"); }

function init() { // Execute after login succeed
	var sess = wialon.core.Session.getInstance(); // get instance of current Session
	// specify what kind of data should be returned
	var flags = wialon.item.Item.dataFlag.base | wialon.item.Unit.dataFlag.lastMessage;

	sess.loadLibrary("itemIcon"); // load Icon Library
	sess.updateDataFlags( // load items to current session
	    [{type: "type", data: "avl_unit", flags: flags,mode: 0}], // Items specification
	    function (code) { // updateDataFlags callback
		    if (code) { msg(wialon.core.Errors.getErrorText(code)); return; } // exit if error code

		    var units = sess.getItems("avl_unit"); // get loaded 'avl_resource's items
		    if (!units || !units.length){ msg("No units found"); return; } // check if units found
		    for (var i = 0; i< units.length; i++) // construct Select list using found resources
			    $("#units").append("<option value='"+ units[i].getId() +"'>"+ units[i].getName()+ "</option>");
		    // bind action to select change event
		    $("#units").change( showUnit );
	});
}

function showUnit(){ // show selected unit on map
	var val = $("#units").val(); // get selected unit id
	if(!val) return; // exit if no unit selected
	var unit = wialon.core.Session.getInstance().getItem(val); // get unit by id
	if(!unit) return; // exit if no unit
	var pos = unit.getPosition(); // get unit position
	if(!pos) return; // exit if no position
    // print message with information about selected unit and its position
	msg("<img src='"+ unit.getIconUrl(32) +"'/> " + unit.getName()+" selected. Position "+ pos.x+", "+pos.y);

	if (map) { // check if map created
        var icon = L.icon({
            iconUrl: unit.getIconUrl(32)
        });
        if (!marker) {
            marker = L.marker({lat: pos.y, lng: pos.x}, {icon: icon}).addTo(map);
        } else {
           marker.setLatLng({lat: pos.y, lng: pos.x});
           marker.setIcon(icon);
        }
        map.setView({lat: pos.y, lng: pos.x});
	}
}

function initMap() {
    // create a map in the "map" div, set the view to a given place and zoom
    map = L.map('map').setView([53.9, 27.55], 10);

    // add an OpenStreetMap tile layer
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://gurtam.com">Gurtam</a>'
    }).addTo(map);
}

function showUnit(){ // show selected unit on map
	var val = $("#units").val(); // get selected unit id
	if(!val) return; // exit if no unit selected
	var unit = wialon.core.Session.getInstance().getItem(val); // get unit by id
	if(!unit) return; // exit if no unit
	var pos = unit.getPosition(); // get unit position
	if(!pos) return; // exit if no position
    // print message with information about selected unit and its position
	msg("<img src='"+ unit.getIconUrl(32) +"'/> " + unit.getName()+" selected. Position "+ pos.x+", "+pos.y);

	if (map) { // check if map created
        var icon = L.icon({
            iconUrl: unit.getIconUrl(32)
        });
        if (!marker) {
            marker = L.marker({lat: pos.y, lng: pos.x}, {icon: icon}).addTo(map);
        } else {
           marker.setLatLng({lat: pos.y, lng: pos.x});
           marker.setIcon(icon);
        }
        map.setView({lat: pos.y, lng: pos.x});
	}
}

function tokenRecieved(e) {
    // get message from login window
    var msg = e.data;
    if (typeof msg == "string" && msg.indexOf("access_token=") >= 0) {
        // get token
       	var token = msg.replace("access_token=", "");
        // now we can use token, e.g show it on page
//        document.getElementById("token").innerHTML = token;
//        document.getElementById("login").setAttribute("disabled", "");
//        document.getElementById("logout").removeAttribute("disabled");

        // or login to wialon using our token
        wialon.core.Session.getInstance().initSession("https://hst-api.wialon.com");

        wialon.core.Session.getInstance().loginToken(token, "", function(code) {
            if (code) {
                alert("Ошибка авторизации, код:" + code)
                return;
            }

            initMap();
            init(); // when login suceed then run init() function
//            var user = wialon.core.Session.getInstance().getCurrUser().getName();
//            alert("Authorized as " + user);
        });

        // remove "message" event listener
        window.removeEventListener("message", tokenRecieved);
    }
}
// execute when DOM ready
$(document).ready(function () {

    var w = $("#map_box").width()

    var h = $(window).height() - $('#filter_box').height() - $('#main_box').height();


    $("#map").css( "width",w)
    $("#map").css( "height",h)
//some actions
     getToken()
//
});
