
  $('#datepicker').datetimepicker({
    locale: 'ru',
    format: 'DD.MM.YYYY',
    defaultDate: moment(),
    
}); 



// client_id  - в контексте страницы

var make_price_url = function () {

     var curr_value = $(this).val();

     if ( curr_value.length === 0 ) {
         curr_value = client_id
     }


     $('.price_link').each(function(){
            var oldUrl = $(this).attr("href"),
                dz_id = oldUrl.split('?')[1].split('&')[0].replace('dz_id=',''),
                base_url = oldUrl.split('?')[0];

            var newUrl = base_url + '?dz_id=' + dz_id + '&client_id=' + curr_value;
            $(this).attr("href", newUrl);
        });
};


$('#id_Client').select2({width: '100%'}).ready(make_price_url);



$('#id_Client').on('change', make_price_url);


$("#Request").on('shown.bs.modal', function( ) {
    $("#error_message").text('')
    $("#id_client_rate").val('')
    $("#id_client_transfer").val('')
    $("#id_client_total").val('')
})

$("#RequestForm").submit(function( ) {
  
  event.preventDefault();

  $("#error_message").text('')

  var e = $(event.currentTarget)
  url = e.attr('action')
  
  params = e.serialize()

  $.post(url, params).done(function (o) {

    if ( typeof(o.error) != undefined) {
      $("#error_message").text(o.error)
    }

    $("#id_client_rate").val(o.client_price)
    $("#id_client_transfer").val(o.client_transfer)
    $("#id_client_total").val(o.client_total)
    $("#id_client_weight_rate").val(o.client_weight_rate)

    $("#id_salary_rate").val(o.salary_price)
    $("#id_salary_transfer").val(o.salary_transfer)
    $("#id_salary_total").val(o.salary_total)
    $("#id_salary_weight_rate").val(o.salary_weight_rate)
    // alert(o.client_rate)
  })

})

$( "#check_tariff" ).submit(function( event ) {
  event.preventDefault();
  var e = $(event.currentTarget)
  var t = $("tbody")
  t.empty()
  number = e.find('input').val()
  // console.log(e)
  url = e.attr('action')
  
  params = {
      'number': number
  }
    
    
  $.post(url, params).done(function (o) {
    
    if (o.Number == 0) {
      var rows  = '<tr><td>Заявка не найдена</td><td>&nbsp</td></tr>';
    }
    else {
      var rows =  '<tr><td>Номер </td><td>' + o.Number + '</td></tr>'
                + '<tr><td>Клиент</td><td>' + o.Client + '</td></tr>'
                + '<tr><td>Вес</td><td>' + o.Weight + '</td></tr>'
                + '<tr><td>Фут</td><td>' + o.Foot + '</td></tr>'
                + '<tr><td>Забор</td><td>' + o.TerminalLoad + '</td></tr>'
                + '<tr><td>Операция</td><td>' + o.Operation + '</td></tr>'
                + '<tr><td>Адрес назначения</td><td>' + o.Address + '</td></tr>'
                + '<tr><td>Район назначения</td><td>' + o.District + '</td></tr>'
                + '<tr><td>Сдача</td><td>' + o.TerminalDeliver + '</td></tr>'
                + '<tr><td>Переезд_1</td><td>' + o.RaywayCr + '</td></tr>'
                + '<tr><td>Переезд_2</td><td>' + o.RaywayCr2 + '</td></tr>'
                + '<tr><td> === Расчет тарифа ====</td><td>&nbsp</td></tr>'
                + '<tr><td><b>Ставка</b></td><td>' + o.client_rate + '</td></tr>'
                + '<tr><td><b>Переезд</b></td><td>' + o.client_transfer + '</td></tr>'
    }

      t.append(rows)

})

});

 $('#Request').modal('show');