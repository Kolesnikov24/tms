    // Format autovisit for Datatable 18, 19 colums



    var update_data_table = function () {

        var table = $('.tms_dt').DataTable();
        table.rows()
            .every(function () {
                var d = this.data();
                d.counter++;                // update data source for the row
                this.invalidate();          // invalidate the data DataTables has cached for this row
            });
        table.draw();
    };


    var AutoVisitFormat = function (plan, fact,num,mode) {
        var template = '';
        var hr = function(text,mode,field) {
            return '<a href="#" id="href_auto_visit_' + mode + '_' + field + '" data-toggle="modal" data-target="#AutoVisitEdit">'+ text +'</a>'
        };
        if (num == "") {
            template = "<span'>П:" + plan + "<br>"
            + "№:" + hr("Добавить", mode) + "</span>";
        }
        else {
            template = "<span title='" + plan + "'>"
            + "П:" + plan + "<br>"
            + "№:" + hr(num,mode,'num') + "<br>"
            + "Ф:" + hr(fact,mode,'fact') + "</span>";
        }
        return template;
    };


        // Universal method for save Order data
    var save_order_data = function (data, callback) {
        /*
            Data parameters should contains
            - Order pk
            - Order model's fields
        */
        $.post('save_order_data/', data,'json')
            .done(function (data) {
                callback();

                update_data_table();

            })
            .fail(function (xhr, errmsg, err) {
                console.log(xhr + " | " + errmsg + " | " + err);
            });
    };

        // Save vistit data from modal window
    $('#btnAutoVisitSave').on('click', function () {

        data  = $('#AutoVisitEditForm').serialize();
        data = data + "&pk=" + context.order_id
        var hide_modal = function() {
            $('#AutoVisitEdit').modal('hide');
        }
        save_order_data(data,hide_modal);
    });


        // Gloabal scope
    var context = {
        order_id:null
    };

    // Define varibles when modal open
    $('.auto_visit_modal').on('show.bs.modal', function (event) {
        var ctx = $(event.relatedTarget).closest('tr');
        context.order_id = parseInt(ctx.attr('id').replace('request_', ''), 10);

        var auto_visit_take_num = $('#href_auto_visit_take_num', ctx).text(),
            auto_visit_take_fact = $('#href_auto_visit_take_fact', ctx).text(),
            auto_visit_return_num = $('#href_auto_visit_return_num', ctx).text(),
            auto_visit_return_fact = $('#href_auto_visit_return_fact', ctx).text();

        $('#auto_visit_take_num').val(auto_visit_take_num);
        $('#auto_visit_take_fact').val(auto_visit_take_fact);
        $('#auto_visit_return_num').val(auto_visit_return_num);
        $('#auto_visit_return_fact').val(auto_visit_return_fact);

    });


        $("#AppStatusHistory").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget).attr("href");
            var t = $(this).find(".modal-body");
            t.load(link);
        });


    var add_order_id_to_report_button = function(elem) {

        var id = elem.attr('id').replace('request_', ''),
            report_button = $('.report_button');

        report_button.removeAttr('disabled');

        $('.report_button').each(function(ind, button){

            var current_url = $(button).attr('href'),
                base_url = current_url.split('?')[0] + '?',
                params = current_url.split('?')[1];

            params.split("&").forEach(function(item) {
                if (item.split("=")[0] != 'order_id') {
                    base_url = base_url + item;
                }
            });

           $(button).attr('href', base_url + '&order_id=' + id );

        });
    };


    var select_table_row = function(row, table_id) {
        var table = $(table_id).DataTable()
        if (row.hasClass('selected')) {
            row.removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            row.addClass('selected');
            add_order_id_to_report_button(row)
        }
    };