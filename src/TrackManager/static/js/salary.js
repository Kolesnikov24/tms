$(document).ready(function() {

  var date_start = moment().format('YYYY-MM-DD'),
      date_end = date_start,
      dz_id = '';
      driver_id=null;
      var start = moment().startOf('month');
      var end = moment().startOf('month').add(1, 'month');

      $('#date_range').daterangepicker({
        locale: {
          "format":"DD.MM.YYYY",
          "applyLabel": "Применить",
          "cancelLabel": "Отмена",
          "customRangeLabel": "Выбрать дату",
        },
          startDate: start,
          endDate: end,
          showCustomRangeLabel:true,
          ranges: {
            'Январь': [moment().startOf('year'), moment().startOf('year').add(1, 'month')],
            'Февраль': [moment().startOf('year').add(1, 'month'), moment().startOf('year').add(2, 'month')],
            'Март': [moment().startOf('year').add(2, 'month'), moment().startOf('year').add(3, 'month')],
            'Апрель': [moment().startOf('year').add(3, 'month'), moment().startOf('year').add(4, 'month')],
            'Май': [moment().startOf('year').add(4, 'month'), moment().startOf('year').add(5, 'month')],
            'Июнь': [moment().startOf('year').add(5, 'month'), moment().startOf('year').add(6, 'month')],
            'Июль': [moment().startOf('year').add(6, 'month'), moment().startOf('year').add(7, 'month')],
            'Август': [moment().startOf('year').add(7, 'month'), moment().startOf('year').add(8, 'month')],
            'Сентябрь': [moment().startOf('year').add(8, 'month'), moment().startOf('year').add(9, 'month')],
            'Октябрь': [moment().startOf('year').add(9, 'month'), moment().startOf('year').add(10, 'month')],
            'Ноябрь': [moment().startOf('year').add(10, 'month'), moment().startOf('year').add(11, 'month')],
            'Декабрь': [moment().startOf('year').add(11, 'month'), moment().startOf('year').add(12, 'month')]
             /*'Текущий месяц': [moment().startOf('month'), moment().startOf('month').add(1, 'month')],
             'Следующий месяц': [moment().startOf('month').add(1, 'month'), moment().startOf('month').add(2, 'month')],
             'Предыдущий месяц': [moment().startOf('month').subtract(1, 'month'),moment().startOf('month')],*/
          }
      },
      function (start, end) {
             date_start = start.format('YYYY-MM-DD');
             date_end = end.format('YYYY-MM-DD');
             update_data_table();
         }
    );


    var weekend_days = function () {
      var arr = [];
      var now = new Date();
      var m = now.getMonth();
      var y = now.getYear();

      for (var d = new Date(y, m, 1); d <= new Date(y, m + 1, 0); d.setDate(d.getDate() + 1)) {

        var day = d.getDay();
        if (day == 6 || day == 0) {
            arr.push(d);
        }
      }

      return arr
    };


    $('#DriverTable').DataTable( {
        ajax: {
            'url': '/' + app_name + '/get_salary_table/', 'type': 'POST', 'data': function (d) {
                d.date_start = date_start;
                d.date_end = date_end;
                d.dz_id = dz_id;
                d.driver_id = driver_id;
            }
        },
        serverSide: true,
        bLengthChange: false,
        bSort : false,
        searching: false,
        paging: false,
        info: false,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'print',
                text: 'Печать',
                // exportOptions: {
                //     columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16]
                // }
            },
            {
                extend: 'excel',
                text: 'В excel',
                // exportOptions: {
                //     columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16]
                // },
                filename: function () {
                    return 'Табель ' + date_start + ' - ' + date_end;
                }
            },
            {
                extend: 'colvis',
                collectionLayout: 'fixed four-column'
            }
        ],
        language: {
            buttons: {
                colvis: 'Выбрать колонки'
            }
        },
//        columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20, 21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,39],
        columnDefs: [
            { "title": "№", "targets": 0 },
            { "title": "Машина", "targets": 1 },
            { "title": "ФИО", "targets":2 },
            { "title": "Зона", "targets":3 },
            { "title": "Б", "targets": 35 },
            { "title": "О", "targets": 36 },
            { "title": "Р", "targets": 37 },
            { "title": "М", "targets": 38 , "visible": false },
            { "title": "Заявки", "targets": 39 },
            { "targets": "_all",
              "createdCell": function (td, cellData, rowData, row, col) {
                  if (col > 3 && col < 35) {
                    // var dr = $("#date_range").startDate
                    var now = new Date();
                    var m = parseInt(date_start.split('-')[1]) -1 //now.getMonth() ;
                    var y = date_start.split('-')[0] //now.getFullYear();
                    var day = new Date(y,m,col-3).getDay()

                      if (day == 6 || day == 0) {
                          $(td).css('background-color', '#9FF781')
                      }
                }
              }


            }
          ]
    } );

    var update_data_table = function () {
        var table = $('#DriverTable').DataTable();
        table.rows()
            .every(function () {
                var d = this.data();
                d.counter++; // update data source for the row
                this.invalidate(); // invalidate the data DataTables has cached for this row
            });
        table.draw();
    };

    $('#DriverTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            var table = $('#DriverTable').DataTable();
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('#filter_dz').on('change', function () {
        dz_id = $(this).val();
        update_data_table();
    });

    // copy past from schedule
     $('#filter_driver')
        .select2({width: '100%', theme: "bootstrap"})
        .on('change', function () {
            driver_id = $(this).val();
            update_data_table();
        });

} );
