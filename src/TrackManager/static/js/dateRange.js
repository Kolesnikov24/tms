
 var date_range_month = {
        locale: {
          "format":"DD.MM.YYYY",
          "applyLabel": "Применить",
          "cancelLabel": "Отмена",
          "customRangeLabel": "Выбрать дату",
        },
          startDate: moment().startOf('month'),
          endDate: moment().startOf('month').add(1, 'month'),
          showCustomRangeLabel:true,
          ranges: {
            'Январь': [moment().startOf('year'), moment().startOf('year').add(1, 'month')],
            'Февраль': [moment().startOf('year').add(1, 'month'), moment().startOf('year').add(2, 'month')],
            'Март': [moment().startOf('year').add(2, 'month'), moment().startOf('year').add(3, 'month')],
            'Апрель': [moment().startOf('year').add(3, 'month'), moment().startOf('year').add(4, 'month')],
            'Май': [moment().startOf('year').add(4, 'month'), moment().startOf('year').add(5, 'month')],
            'Июнь': [moment().startOf('year').add(5, 'month'), moment().startOf('year').add(6, 'month')],
            'Июль': [moment().startOf('year').add(6, 'month'), moment().startOf('year').add(7, 'month')],
            'Август': [moment().startOf('year').add(7, 'month'), moment().startOf('year').add(8, 'month')],
            'Сентябрь': [moment().startOf('year').add(8, 'month'), moment().startOf('year').add(9, 'month')],
            'Октябрь': [moment().startOf('year').add(9, 'month'), moment().startOf('year').add(10, 'month')],
            'Ноябрь': [moment().startOf('year').add(10, 'month'), moment().startOf('year').add(11, 'month')],
            'Декабрь': [moment().startOf('year').add(11, 'month'), moment().startOf('year').add(12, 'month')]
             /*'Текущий месяц': [moment().startOf('month'), moment().startOf('month').add(1, 'month')],
             'Следующий месяц': [moment().startOf('month').add(1, 'month'), moment().startOf('month').add(2, 'month')],
             'Предыдущий месяц': [moment().startOf('month').subtract(1, 'month'),moment().startOf('month')],*/
          }
      };