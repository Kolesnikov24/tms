$(document).ready(function () {

    var date_format = 'DD.MM.YYYY';

    var driver_cols = [
            {'data': 'row_num', 'className': 'cell-row_num', 'width': '5px'},
            {'data': 'name', 'className': 'cell-TruckNumber', 'width': '20px'},
            {'data': 'car_num', 'className': 'cell-TruckNumber', 'width': '50px'},
            {'data': 'cnt', 'className': 'cell-TrailerNumber', 'width': '50px'},
            {'data': 'client_total', 'className': 'cell-FIO', 'width': '80px'},
            {'data': 'salary_total', 'className': 'cell-foot', 'width': '10px'},
            {'data': 'profit', 'className': 'cell-dz', 'width': '60px'},
        ];

    var clients_cols = [
            {'data': 'row_num', 'className': 'cell-row_num', 'width': '5px'},
            {'data': 'name', 'className': 'cell-client', 'width': '20px'},
            {'data': 'Foot', 'className': 'cell-foot', 'width': '20px'},
            {'data': 'jan', 'className': 'cell-month cell-month-jan', 'width': '50px'},
            {'data': 'feb', 'className': 'cell-month cell-month-feb', 'width': '50px'},
            {'data': 'mar', 'className': 'cell-month cell-month-mar', 'width': '80px'},
            {'data': 'apr', 'className': 'cell-month cell-month-apr', 'width': '10px'},
            {'data': 'may', 'className': 'cell-month cell-month-may', 'width': '40px'},
            {'data': 'jun', 'className': 'cell-month cell-month-jun', 'width': '40px'},
            {'data': 'jul', 'className': 'cell-month cell-month-jul', 'width': '40px'},
            {'data': 'aug', 'className': 'cell-month cell-month-aug', 'width': '40px'},
            {'data': 'sep', 'className': 'cell-month cell-month-sep', 'width': '40px'},
            {'data': 'oct', 'className': 'cell-month cell-month-oct', 'width': '40px'},
            {'data': 'nov', 'className': 'cell-month cell-month-nov', 'width': '40px'},
            {'data': 'dec', 'className': 'cell-month cell-month-dec', 'width': '40px'},
            {'data': 'all', 'className': 'cell-month cell-month-all', 'width': '40px'}
        ];

    var init_clients_table = function( table_id, url, columns_array ) {
        $(table_id).DataTable({
            ajax: {
                'url': url, 'type': 'POST',
                'data': function (d) {
                    d.date_start = date_start;
                    d.date_end = date_end;
                }
            },
            autoWidth: true,
            serverSide: true,
            bLengthChange: false,
            paging: false,
            iDisplayLength: 50,
            searching: true,
            info: false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    text: 'Печать'
                },
                {
                    extend: 'excel',
                    text: 'В excel',
                    filename: function () {
                        return 'Сводный отчет ' + date_start + ' - ' + date_end;
                    }
                },
                {
                    extend: 'colvis',
                    collectionLayout: 'fixed four-column'
                }
            ],
            language: {
                buttons: {
                    colvis: 'Выбрать колонки'
                }
            },
            columns: columns_array,
            createdRow: function (row, data) {
                if(data.Foot === 'Всего')
                    $(row).addClass('row-total');
            }
        }).on('draw', function () {
            var data_rows = $('#ClientsTable').find('tbody tr'),
                    cnt = data_rows.length > 0 ? data_rows.length - 1 : 0,
                    client_current = '',
                    client_prev = '',
                    client_next = '';
                for(i = 0; i <= cnt; i++) {
                    if(i === 0) {
                        client_current = $(data_rows[i]).find('.cell-client').text();
                    }
                    if(i > 0) {
                        client_prev = client_current;
                        client_current = client_next;
                    }
                    if(i === cnt) {
                        client_next = '';
                    } else {
                        client_next = $(data_rows[i + 1]).find('.cell-client').text();
                    }

                    if(client_current !== client_prev && client_current !== client_next) {
                        $(data_rows[i]).addClass('row-total');
                    }
                }
        });
    };

    var ds = moment().startOf('month'), de = moment().startOf('month').add(1, 'month').add(-1, 'day');
    date_start = ds.format('YYYY-MM-DD');
    date_end = de.format('YYYY-MM-DD');

    $('#date_range').daterangepicker(
    {
        locale: {
            "format": date_format,
            "applyLabel": "Применить",
            "cancelLabel": "Отмена",
            "fromLabel": "С",
            "toLabel": "По",
            "customRangeLabel": "Другой",
            "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
            "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            "firstDay": 1
        },
        startDate: ds.format(date_format),
        endDate: de.format(date_format)
    },
    function (start, end, label) {
        date_start = start.format('YYYY-MM-DD');
        date_end = end.format('YYYY-MM-DD');
        update_data_table('#ReportsTable');
    });

    var t1 = '#ReportsTable';

    // Метод API
    default_dt_settings.ajax = {
            'url': '/' + app_name + '/get_reports_table/', 'type': 'POST', 'data': function (d) {
                d.date_start = date_start;
                d.date_end = date_end;

            }
     };
    default_dt_settings.columns = driver_cols

    // Создаем таблицу
    init_data_table(t1, default_dt_settings)


    init_select2_filter(t1,'#filter_truck',2);
    init_select_filter(t1,'#filter_dz',1);

    $('#filter_year').on('change', function () {
       var year = $(this).val();
       date_start = year + '-01-01';
       date_end = year + '-12-31';
       clients_total = zero_clients_total();
       update_data_table('#ClientsTable');
    });

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        if ( e.target.hash == '#clients') {
            var t2 = '#ClientsTable';
            date_start = moment().startOf('year').format('YYYY-MM-DD');
            date_end = moment().startOf('year').add(1, 'year').add(-1, 'day').format('YYYY-MM-DD') + ' 23:59:59';
            if ( ! $.fn.DataTable.isDataTable(t2) ) {
                
                init_clients_table(t2,'/' + app_name + '/get_clients_table/', clients_cols);
                init_select2_filter(t2,'#filter_client',1, function () {
                    clients_total = zero_clients_total();
                });
                
            }
        }
   
    });

});
