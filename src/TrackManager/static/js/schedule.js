$(document).ready(function () {
    var date_format = 'DD.MM.YYYY';
    var date_start = moment().format('YYYY-MM-DD'),
        date_end = date_start;

    var start = moment();
    var end = moment();
    var operation_choices = {
        "1": "погр.", "2": "выгр.", "3": "пор."
    };

    $('#date_range').daterangepicker({
            locale: {
                "format": "DD.MM.YYYY",
                "applyLabel": "Применить",
                "cancelLabel": "Отмена",
                "fromLabel": "С",
                "toLabel": "По",
                "customRangeLabel": "Другой",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            startDate: start,
            endDate: end,
            //showCustomRangeLabel: false,
            ranges: {
                'Сегодня': [moment(), moment()],
                'Завтра': [moment().add(1, 'days'), moment().add(1, 'days')],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')]

            }
        },
        function (start, end) {
            date_start = start.format('YYYY-MM-DD');
            date_end = end.format('YYYY-MM-DD');
            update_data_table();
        }
    );

    var removeNSK = function (s) {
        return s === null || typeof s === "undefined" ? "" : s.replace(' ул ', ' ')
            .replace('Кемеровская обл, ', '')
            .replace('Новосибирская обл, ', '')
            .replace('Томская обл, ', '')
            .replace('Омская обл, ', '')
            .replace('г Новосибирск, ', '');
    };

    $('#ScheduleTable').DataTable({
        ajax: {
            'url': '/' + app_name + '/get_schedule_table/', 'type': 'POST',
            'data': function (d) {
                d.date_start = date_start;
                d.date_end = date_end;
            }
        },
        autoWidth: false,
        serverSide: true,
        bLengthChange: false,
        paging: false,
        iDisplayLength: 50,
        searching: true,
        info: false,
        dom: 'Bfrtip',
        order : [[1, "asc"]],
        buttons: [
            {
                extend: 'print',
                text: 'Печать',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16]
                }
            },
            {
                extend: 'excel',
                text: 'В excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16]
                },
                filename: function () {
                    return 'Работа машин ' + date_start + ' - ' + date_end;
                }
            },
            {
                extend: 'colvis',
                collectionLayout: 'fixed four-column'
            }
        ],
        language: {
            buttons: {
                colvis: 'Выбрать колонки'
            }
        },
        columns: [
            {'data': 'row_num', 'className': 'cell-row_num', 'width': '10px', 'orderable': false},
            {'data': 'TruckNumber', 'className': 'cell-TruckNumber', 'width': '80px', 'orderable': true},
            {'data': 'TrailerNumber', 'className': 'cell-TrailerNumber', 'width': '80px', 'orderable': false},
            {'data': 'FIO', 'className': 'cell-FIO', 'width': '100px', 'orderable': false},
            {'data': 'foot', 'className': 'cell-foot', 'width': '15px'},
            {'data': 'dz', 'className': 'cell-dz', 'width': '50px', 'orderable': true},
            {'data': 'ContainerNum', 'className': 'cell-ContainerNum', 'width': '80px', 'orderable': false},
            {'data': 'LoadName', 'className': 'cell-LoadName', 'width': '60px', 'orderable': false},
            {'data': 'DeliverName', 'className': 'cell-DeliverName', 'width': '60px', 'orderable': false},
            {'data': 'Address', 'className': 'cell-Address', 'width': '140px', 'orderable': false},
            {'data': 'District', 'className': 'cell-District', 'width': '80px', 'orderable': false},
            {'data': 'RaywayCr', 'className': 'cell-RaywayCr', 'width': '90px', 'orderable': false},
            {'data': 'Operation', 'className': 'cell-Operation', 'width': '35px', 'orderable': false},
            {'data': 'ExportTime', 'className': 'cell-ExportTime', 'width': '35px', 'orderable': false},
            {'data': 'ClientName', 'className': 'cell-ClientName', 'width': '80px', 'orderable': false},
            {'data': 'Customer', 'className': 'cell-Customer', 'width': '100px', 'orderable': false},
            {'data': 'trip_num', 'className': 'cell-trip_num', 'width': '50px', 'orderable': false},
            {'data': 'month_trips', 'className': 'cell-month_trips', 'width': '70px', 'orderable': false},
            {'data': 'Comment', 'className': 'cell-Comment', 'width': '100px', 'orderable': false}

        ],
        columnDefs: [
            {
                "targets": 3,
                "data": "FIO",
                "render": function (data, type, full) {
                    var arr = full.FIO.split(' ');
                    var template = arr[0] + " " + arr[1];
                    return template;
                }
            },
            {
                "targets": 5,
                "data": "dz",
                "render": function (data) {
                    return data === 1 ? "Восток": "Клещиха";
                }
            },
            {
                "targets": 9,
                "data": "Address",
                "render": function (data) {
                    return "<span title='" + data + "'>" + removeNSK(data) + "</span>"
                }
            },
            {
                "targets": 11,
                "data": "RaywayCr",
                "render": function (data, type, full) {
                    var template = "<span title='" + data + "'>" + removeNSK(data) + "</span>";
                    if (full.RaywayCr2) template += "<hr class='short'><span title='" + full.RaywayCr2 + "'>" + removeNSK(full.RaywayCr2) + "</span>";
                    return template;
                }
            },
            {
                "targets": 12,
                "data": "Operation",
                "render": function (data) {
                    return data === null || typeof data === "undefined" ? "" : operation_choices[data];
                }
            },
            {
                "targets": 13,
                "data": "ExportTime",
                "render": function (data) {
                    return data === null || typeof data === "undefined" ? "" : data.substr(0, 5);
                }
            },
            {
                "targets": 16,
                "data": "trip_num",
                "render": function (data, type, full) {
                    if (data > 0) return data;
                    var work = (full.add_work == null || full.add_work == "") ? 'Добавить' : full.add_work;
                    var result = ($.type(work) == 'number') ? "" : work;
                    template = "<a href='#' role='button' data-toggle='modal' data-target='#AddWork'>" + result + "</a>";
                    return template
                }
            }

        ]
    });


    var update_data_table = function () {
        var table = $('#ScheduleTable').DataTable();
        table.rows()
            .every(function () {
                var d = this.data();
                d.counter++; // update data source for the row
                this.invalidate(); // invalidate the data DataTables has cached for this row
            });
        table.draw();
    };


    $('#AddWork').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        current_row = button.closest('tr');
    });

    $('#AddWorkSave').on('click', function () {
        var post_data = $('#AddWorkForm').serializeArray();
        post_data.push({'name': 'fio', 'value': $('.cell-FIO', current_row).text()})
        post_data.push({'name': 'date_start', 'value': date_start});


        $.ajax({
            url: "save_job/", // the endpoint
            type: "POST", // http method
            data: post_data, // data sent with the post request
            // handle a successful response
            success: function (json) {
                $('#AddWork').modal('hide');
                update_data_table();
            }
        });
    });

    $('#filter_driver')
        .select2({width: '100%', theme: "bootstrap"})
        .on('change', function () {
            var table = $('#ScheduleTable').DataTable(), val = $(this).val();
            table.column(2).search(val).draw();
        });

    $('#filter_truck')
        .select2({width: '100%', theme: "bootstrap"})
        .on('change', function () {
            var table = $('#ScheduleTable').DataTable(), val = $(this).val();
            table.column(0).search(val).draw();
        });

    $('#filter_foot').on('change', function () {
        var table = $('#ScheduleTable').DataTable(), val = $(this).val();
        table.column(3).search(val).draw();
    });

    $('#filter_dz').on('change', function () {
        var table = $('#ScheduleTable').DataTable(), val = $(this).val();
        table.column(4).search(val).draw();
    });

    $('#filter_status').on('change', function () {
        var table = $('#ScheduleTable').DataTable(), val = $(this).val();
        table.column(15).search(val).draw();
    });

    $('#ScheduleTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            var table = $('#ScheduleTable').DataTable();
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

});
