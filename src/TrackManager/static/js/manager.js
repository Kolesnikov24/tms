$(function () {
    var mode = 'new';
    var id = 0;
    var current_row;
    var status_ids = '1,2,3,4,5,6,7,8,9';
    var date_format = 'DD.MM.YYYY';
    var date_start = moment().format('YYYY-MM-DD'),
        date_end = date_start;
    var row_colors = {'Выполняется': 'warning', 'Завершено': 'info', 'Сдан': 'success'};

    $('#datepicker').datetimepicker({
        locale: 'ru',
        format: 'DD.MM.YYYY'  
    }); 
    
    
    $('#timepicker').datetimepicker({
        locale: 'ru',
        format: 'HH:mm'
    });

    $('.timepicker').datetimepicker({
        locale: 'ru',
        format: 'DD.MM HH:mm',
        defaultDate: moment()
    });

    $('.js-kladr-inline-input').closest('.form-group').kladrInline({
        'token': '3863585234a94ff0df853b17b6d6007e57dda087',
        'input': '.js-kladr-inline-input',
        'flatRequired': false,
        'geoLocation': true
    });

    $('#Request').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget),
            number = button.text(),
            modal = $(this);
        mode = button.data('mode');

        modal.find('.modal-title').text(mode == 'new' || mode == 'copy' ? 'Новая заявка' : ('Заявка № ' + number));
        modal.find('.modal-footer .btn-primary').text(mode == 'new' || mode == 'copy' ? 'Создать заявку' : 'Сохранить заявку');
        $('#RequestForm')[0].reset();
        if (mode == 'edit' || mode == 'copy') {
            var ctx = button.closest('tr');
            id = parseInt(ctx.attr('id').replace('request_', ''), 10);
            if (mode == 'copy') id = 0;
            var
                datetime = $('.cell-date', ctx).text(),
                date = datetime.substr(0, 10),
                time = datetime.substr(11, 17),
                client = $('.cell-client', ctx).text(),
                weight = $('.cell-weight', ctx).text(),
                foot = $('.cell-foot', ctx).text(),
                terminal1 = ctx.data('terminalload-id'),
                terminal2 = ctx.data('terminaldeliver-id'),
                customer_id = ctx.data('customer_id'),
                address = $('.cell-address', ctx).text(),
                district = $('.cell-district', ctx).text(),
                customer = $('.cell-customer', ctx).text(),
                container = $('.cell-container', ctx).text(),
                RaywayCr = ctx.data('RaywayCr'),
                RaywayCr2 = ctx.data('RaywayCr2'),
                RaywayCrDistrict = ctx.data('RaywayCrDistrict'),
                RaywayCr2District = ctx.data('RaywayCr2District'),
                destination_station = ctx.data('destination_station');
                cargo_manager = $('.cell-cargo_manager', ctx).text(), ctx.data('cargo_manager');

            $('#id_Client').val(ctx.data('client-id')).trigger('change');
            $('#id_Responsible').val(ctx.data('responsible'));

//            var customer_val = $('#id_customer_new').find("option:contains('"+customer+"')").val();
            $('#id_customer_new').val(customer_id).trigger('change.select2');

            $('#id_Proxy').val(ctx.data('pa'));
            $('#id_CargoName').val(ctx.data('cargo'));
            $('#id_ContainerNum').val(container);
            $('#id_Address').val(address);
            $('#id_District').val(district);
            $('#id_RaywayCr').val(RaywayCr);
            $('#id_RaywayCr2').val(RaywayCr2);
            $('#id_RaywayCrDistrict').val(RaywayCrDistrict);
            $('#id_RaywayCr2District').val(RaywayCr2District);
            $('#id_ExportDate').val(date);
            $('#id_ExportTime').val(time);
            $('#id_Operation').val(ctx.data('operation-id'));
            $('#id_TerminalLoad').val(terminal1);
            $('#id_TerminalDeliver').val(terminal2);
            $('#id_Weight').val(weight);
            $('#id_Foot').val(foot);
            $('#id_Comment').val(ctx.data('description'));
            $('#id_dz').val(ctx.data('dz-id'));
            $('#id_destination_station').val(destination_station);
            $('#id_cargo_manager').val(cargo_manager);

            if (terminal1 === 'Прочее' || terminal2 === 'Прочее') {
                $('.terminal-other').removeClass('hidden');
            }
        } else {
            id = 0;
            var term_other = $('.terminal-other');
            if (!term_other.hasClass('hidden')) term_other.addClass('hidden');
        }

        // Возможность сохранить как шаблон
        if (mode == 'edit') {
            $('#btnSaveTemplate').show();
        }
        else {
          $('#btnSaveTemplate').hide();
        }
    });

    $('#Request').on('hide.bs.modal', function () {
        $(".form-errors").remove()
    });

    var row_init = function (row) {
        var operation = $('#id_Operation :selected').text();
        $('.cell-date', row).text($('#id_ExportDate').val() + ' ' + $('#id_ExportTime').val());
        $('.cell-client', row).text($('#id_Client option:selected').text());
        $('.cell-customer', row).text($('#id_customer_new').val());
        $('.cell-container', row).text($('#id_ContainerNum').val());
        $('.cell-weight', row).text($('#id_Weight').val());
        $('.cell-foot', row).text($('#id_Foot').val());
        $('.cell-operation', row).text(operation);
        $('.cell-terminalload', row).text($('#id_TerminalLoad :selected').text());
        $('.cell-terminaldeliver', row).text($('#id_TerminalDeliver :selected').text());
        $('.cell-address', row).text($('#id_Address').val());
        $('.cell-district', row).text($('#id_District').val());
        $('.cell-destination_station', row).text($('#id_destination_station').val());

        row.data('responsible', $('#id_Responsible').val());
        row.data('pa', $('#id_Proxy').val());
        row.data('cargo', $('#id_CargoName').val());
        row.data('description', $('#id_Comment').val());
    };

    var save_order_call = function(button_id) {
        var w = $('#id_Weight').val();
        if(!w) {
            alert('Укажите вес!');
            return;
        }

        if (mode == 'copy') mode = 'new';
        var row = mode == 'new' ? $('#RequestList tfoot tr').first().clone(true) : $('#request_' + id);
        var service_url = button_id == 'btnSaveTemplate' ? "save_order_template/" : "save_order/";

        row_init(row);

        $.ajax({
            url: service_url + (id == 0 ? "" : "?id=" + id),
            type: "POST",
            data: $('#RequestForm').serialize(),
            // handle a successful response
            success: function (json) {
                var err = json.err.length;
                console.log(json);
                if (err == 0) {
                    $(".form-errors").remove();
                    if (mode == 'new') {
                        $('.cell-number', row).html("<a href='#' role='button' data-toggle='modal' data-target='#Request' data-mode='edit'>" + json.number + "</a>");
                        $('td', row).eq(2).text('Заготовка');
                        row.appendTo('#RequestList tbody');
                    }
                    $('#Request').modal('hide');
                    update_data_table();
                }
                else {
                    row.attr('id', 'request_' + json.id);
                    $.each(json.messages, function (field) {
                        if ($('[name="' + field + '"]').is("select")) {
                            $('[name="' + field + '"]').closest('div').append("<h6 class='form-errors' style='color:red'>Поле не может быть пустым</h6>")
                        }
                        else {
                            $('[name="' + field + '"]').closest('div').parent().append("<h6 class='form-errors' style='color:red'>Поле не может быть пустым</h6>")
                        }
                    })
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ": " + xhr.responseText);
            }
        });

    };



    $('#btnSave').on('click', function () {
        var button_id = this.id
        save_order_call(button_id);
    });

    $('#btnSaveTemplate').on('click', function () {
        var button_id = this.id
        save_order_call(button_id);
    });


    var send = function (row) {
        var id = row.attr('id').replace('request_', '');
        $.post('change_status/', {'action': 'send', 'pk': id})
            .done(function (data) {
                if (!data || data.status_id != 2) return;
                var status_cell = $('.cell-status', row),
                    btn = $('.btn-request-action', row),
                    menu_items = $('.dropdown-menu li', row),
                    cancel_menu_item = $("[data-target='#RequestCancel']", menu_items).parent();

                menu_items.removeClass('disabled').addClass('disabled');
                cancel_menu_item.removeClass('disabled');
                btn.text($('a', cancel_menu_item).text());
                status_cell.text(data.status)
                update_data_table();
            })
            .fail(function (xhr, errmsg, err) {
                console.log(xhr + " | " + errmsg + " | " + err);
            });
    };

    var cancel = function (row) {
        var id = row.attr('id').replace('request_', '');
        $.post('change_status/', {'action': 'cancel', 'pk': id, 'CancelReason': $('#cancel_reason').val()})
            .done(function (data) {
                if (!data || data.status_id != 7) return;
                var status_cell = $('.cell-status', row),
                    action_cell = $('.cell-action', row),
                    number_cell = $('.cell-number', row);
                status_cell.text(data.status);
                action_cell.text('');
                number_cell.text(row.data('number'));
                $('#RequestCancel').modal('hide');
                update_data_table();
            })
            .fail(function (xhr, errmsg, err) {
                $(".cancel-form-errors").append("<h6 style='color:red'>" + xhr.responseJSON.err + "</h6>" )
                console.log(xhr + " | " + errmsg + " | " + err);
            });
    };
    
    $(document).on('click', '.btn-request-action', function (e) {
        var btn = $(this), action = btn.text();
        if (action == 'Отправить') {
            send(btn.closest('tr'));
        } else {
            var menu_item = $(".dropdown-menu a:contains('" + action + "')", btn.closest('div'));
            menu_item.trigger('click');
        }
        e.preventDefault();
    });

    $('#RequestCancel').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        current_row = button.closest('tr');
        $(".cancel-form-errors").empty();
    });

    $('#RequestCancelBtnSave').on('click', function () {
        cancel(current_row);
    });

    $('.terminal-selector').on('change', function () {
        var $this = $(this), selected = $(':selected', $this).text(), term_id = $this.attr('id');
        if (term_id == 'id_TerminalLoad') $('#id_AddressLoad').val(selected);
        if (term_id == 'id_TerminalDeliver') $('#id_AddressDeliver').val(selected);
        if (selected != 'Прочее') return;
        $($this.data('other')).closest('.row').removeClass('hidden');
    });

    var removeNSK = function (s) {
        return s === null || typeof s === "undefined" ? "" : s.replace(' ул ', ' ')
            .replace('Кемеровская обл, ', '')
            .replace('Новосибирская обл, ', '')
            .replace('Томская обл, ', '')
            .replace('Омская обл, ', '')
            .replace('г Новосибирск, ', '');
    };



    var settings = dataTableSettings;

    settings.scrollX = false;
    settings.columns = [
        {'data': 'row_num', 'className': 'cell-row_num', 'width': '20px', 'orderable': false },
        {'data': 'Number', 'className': 'cell-number', 'width': '30px'},
        {'data': 'action', 'className': 'cell-action', 'width': '140px'},
        {'data': 'Status', 'className': 'cell-status', 'width': '60px'},
        {'data': 'driver_action', 'className': 'cell-driver_action', 'width': '60px', 'orderable': false},
        {'data': 'ExportDate', 'className': 'cell-date', 'width': '80px'},
        {'data': 'Client', 'className': 'cell-client', 'width': '100px'},
        {'data': 'Customer', 'className': 'cell-customer', 'width': '120px', 'orderable': false},
        {'data': 'CargoName', 'className': 'cell-cargoname', 'width': '80px', 'orderable': false},
        {'data': 'ContainerNum', 'className': 'cell-container', 'width': '80px', 'orderable': false},
        {'data': 'recognized_container_num', 'className': 'cell-recognized_container_num', 'width': '80px', 'orderable': false},
        {'data': 'Foot', 'className': 'cell-foot', 'width': '30px'},
        {'data': 'Weight', 'className': 'cell-weight', 'width': '30px'},
        {'data': 'Operation', 'className': 'cell-operation', 'width': '60px'},
        {'data': 'TerminalLoad', 'className': 'cell-terminalload', 'width': '80px'},
        {'data': 'Address', 'className': 'cell-address', 'width': '160px', 'orderable': false},
        {'data': 'District', 'className': 'cell-district', 'width': '60px'},
        {'data': 'RaywayCr', 'className': 'cell-RaywayCr1', 'width': '130px', 'orderable': false},
        {'data': 'TerminalDeliver', 'className': 'cell-terminaldeliver', 'width': '80px', 'orderable': false},
        {'data': 'truck', 'className': 'cell-truck', 'width': '80px' , 'orderable': false},
        {'data': 'driver', 'className': 'cell-driver', 'width': '100px', 'orderable': false},
        {'data': 'auto_visit_take', 'className': 'cell-auto_visit_take', 'width': '160px', 'orderable': false},
        {'data': 'auto_visit_return', 'className': 'cell-auto_visit_return', 'width': '160px', 'orderable': false},
        {'data': 'approve_date', 'className': 'cell-approve', 'width': '80px', 'orderable': false},
        {'data': 'Responsible', 'className': 'cell-responsible', 'width': '95px', 'orderable': false},
        {'data': 'Comment', 'className': 'cell-comment', 'width': '180px', 'orderable': false},
        {'data': 'destination_station', 'className': 'cell-destination_station', 'width': '150px', 'orderable': false},
        {'data': 'cargo_manager', 'className': 'cell-cargo_manager', 'width': '70px', 'orderable': false},
        {'data': 'attachments', 'className': 'cell-attachments', 'width': '150px', 'orderable': false},
        {'data': 'attachments_client', 'className': 'cell-attachments_client', 'width': '150px', 'orderable': false}
    ];
    settings.columnDefs = [{
        "targets": 1,
        "data": "Number",
        "orderable": true,
        "render": function (data, type, full) {
            var s = full.Status,
                template = full.Number;
            if (s == 'Заготовка' || s == 'Согласование') {
                template = "<a href='#' role='button' data-toggle='modal' data-target='#Request' data-mode='edit'>" + full.Number + "</a>";
            }
            return template
        }
    },
        {
            "targets": 2,
            "data": "action",
            "orderable": false,
            "render": function (data, type, full) {
                var status = full.Status;
                if (status !== 'Заготовка' && status !== 'Согласование') {
                    return '';
                }
                var send_disabled = status === 'Согласование' ? 'disabled' : '';
                var action = status === 'Заготовка' ? 'Отправить' : 'Отменить';
                var btn = status === 'Заготовка' ? 'btn-primary' : 'btn-warning';
                var template = '<div class="btn-group btn-group-xs">\
                  <button type="button" class="btn {{btn}} btn-xs btn-request-action">{{action}}</button>\
                  <button type="button" class="btn {{btn}} dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                    <span class="caret"></span>\
                    <span class="sr-only">Toggle Dropdown</span>\
                  </button>\
                  <ul class="dropdown-menu">\
                    <li class="{{send_disabled}}"><a href="#">Отправить</a></li>\
                    <li><a href="#" data-toggle="modal" data-target="#RequestCancel">Отменить</a></li>\
                    <li><a href="#" role="button" data-toggle="modal" data-target="#Request" data-mode="copy">Копировать</a></li>\
                  </ul>\
                </div>';
                var re = new RegExp('{{btn}}', 'g'),
                    html = template
                        .replace('{{action}}', action)
                        .replace(re, btn)
                        .replace('{{send_disabled}}', send_disabled);
                return html;
            }
        },
        {
            "targets": 4,
            "data": "driver_action",
            "orderable": true,
            "render": function (data, type, full) {
            var order_id  = parseInt(full.DT_RowId.replace('request_', ''))
            var temp = data + '<br>'
                  + '<a href="/' + app_name + '/get_driver_actions?order_id=' + order_id + '"'
                  + 'data-toggle="modal" data-remote="false" data-target="#AppStatusHistory">Отчет</a>';

            var t = data == '' ? data : temp
            return t
            }
        },
        {
            "targets": 14,
            "data": "TerminalLoad",
            "orderable": true,
            "render": function (data, type, full) {
                return "<span title='" + full.TerminalLoad + "'>" + removeNSK(full.TerminalLoad) + "</span>"
            }
        },
        {
            "targets": 15,
            "data": "Address",
            "orderable": true,
            "render": function (data, type, full) {
                return "<span title='" + full.Address + "'>" + removeNSK(full.Address) + "</span>"
            }
        },
        {
            "targets": 17,
            "data": "RaywayCr",
            "orderable": true,
            "render": function (data, type, full) {
                var template = "<span title='" + full.RaywayCr + "'>" + removeNSK(full.RaywayCr) + "</span>";
                if (full.RaywayCr2) template += "<hr class='short'><span title='" + full.RaywayCr2 + "'>" + removeNSK(full.RaywayCr2) + "</span>";
                return template;
            }
        },
        {
            "targets": 18,
            "data": "TerminalDeliver",
            "orderable": true,
            "render": function (data, type, full) {
                return "<span title='" + full.TerminalDeliver + "'>" + removeNSK(full.TerminalDeliver) + "</span>"
            }
        },
        {
            "targets": 20,
            "data": "driver",
            "orderable": true,
            "render": function (data, type, full) {
                return data + "<br> " + full.driver_phone;
            }
        },
        {
            "targets": 21,
            "data": "auto_visit_take",            
            "orderable": true,
            "render": function (data, type, full) {
                return AutoVisitFormat(full.auto_visit_take_plan,
                                full.auto_visit_take_fact,
                                full.auto_visit_take_num,
                                'take'
                            )
            }
        },
        {
            "targets": 22,
            "data": "auto_visit_return",
            "orderable": true,
            "render": function (data, type, full) {
                return AutoVisitFormat(full.auto_visit_return_plan,
                                full.auto_visit_return_fact,
                                full.auto_visit_return_num,
                                'return'
                            )
            }
        },
        {
            "targets": 23,
            "data": "approve_date",
            "orderable": true,
            "render": function (data, type, full) {
                return data + " " + full.dispatcher_approve;
            }
        },
         {
            "targets": 28,
            "data": "attachments",
            "orderable": true,
            "render": function (data, type, full) {
                return get_document_link(data)
            }
       },
        {
            "targets": 29,
            "data": "attachments_client",
            "orderable": true,
            "render": function (data, type, full) {
                return get_document_link(data)
            }
       }
    ];

    settings.createdRow = function (row, data) {
        var r = $(row), cls = row_colors[data.Status];
        if(typeof cls !== "undefined" && cls) r.addClass(cls);
        r.data('responsible', data.Responsible);
        r.data('pa', data.Proxy);
        r.data('cargo', data.CargoName);
        r.data('description', data.Comment);
        r.data('client-id', data.ClientId);
        r.data('operation-id', data.OperationId);
        r.data('terminalload-id', data.TerminalLoadId);
        r.data('customer_id', data.CustomerId);
        r.data('terminaldeliver-id', data.TerminalDeliverId);
        r.data('number', data.Number);
        r.data('RaywayCr', data.RaywayCr);
        r.data('RaywayCr2', data.RaywayCr2);
        r.data('RaywayCrDistrict', data.RaywayCrDistrict);
        r.data('RaywayCr2District', data.RaywayCr2District);
        r.data('dz-id', data.dzId);
        r.data('destination_station', data.destination_station);
    };
    settings.fixedColumns = false;
    settings.autoWidth = false;
    settings.processing = true;
    settings.searching = true;
    settings.dom = 'Bfrtip';
    settings.buttons = [
        {
            extend: 'print',
            text: 'Печать',
            exportOptions: {
                columns: [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
            }
        },
        {
            extend: 'excel',
            text: 'В excel',
            exportOptions: {
                columns: [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,17,18,19,21,22,23,24,25,26,27]
            },
            filename: function () {
                return 'Заявки ' + date_start + ' - ' + date_end;
            }
        },
        {
            extend: 'colvis',
            collectionLayout: 'fixed four-column'
        }
    ];
    settings.language = {
        buttons: {
            colvis: 'Выбрать колонки'
        }
    };
    settings.order = [[5, "asc"]];

    settings.ajax = {
        'url': "/" + app_name + '/get_manager_table/', 'type': 'POST', 'data': function (d) {
            d.status_ids = status_ids;
            d.date_start = date_start;
            d.date_end = date_end;
        }
    };
    $('#RequestList').DataTable(settings);

    $('#RequestList tbody').on('click', 'tr', function () {
        select_table_row($(this), '#RequestList')
    });

    $('#dropdownMenu1').on('click', function () {
        $(this).parent().toggleClass("open");
    });

    $('body').on('click', function (e) {
        var filter_status_menu = $('#dropdownMenu1'), open = $('.open');
        if (!filter_status_menu.is(e.target) && filter_status_menu.has(e.target).length === 0 && open.has(e.target).length === 0) {
            filter_status_menu.parent().removeClass('open');
        }
    });

    $('#filter_status_apply').on('click', function () {
        $('#dropdownMenu1').parent().removeClass('open');
        var ids = [];
        $('[name=filter_status]:checked').each(function () {
            ids.push(this.value);
        });
        status_ids = ids.join();
        update_data_table();
    });

    $('#btn_refresh').on('click', function () {
        update_data_table();
    });

    $('#date_range').daterangepicker(
        {
            locale: {
                "format": date_format,
                "applyLabel": "Применить",
                "cancelLabel": "Отмена",
                "fromLabel": "С",
                "toLabel": "По",
                "customRangeLabel": "Другой",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            },
            startDate: moment().format(date_format),
            endDate: moment().format(date_format)
        },
        function (start, end) {
            date_start = start.format('YYYY-MM-DD');
            date_end = end.format('YYYY-MM-DD');
            update_data_table();
        });

    $('#id_Client').select2({width: '100%'});

    $('#id_customer_new').select2({width: '100%', tags: true,  theme: "bootstrap"});

    $('#filter_client').select2({width: '100%', theme: "bootstrap"});
    $('#filter_client').on('change', function () {
        var table = $('#RequestList').DataTable(), val = $(this).val();
        table.column(4).search(val).draw();
    });

    $('#filter_foot').on('change', function () {
        var table = $('#RequestList').DataTable(), val = $(this).val();
        table.column(11).search(val).draw();
    });

    $('.order_template').on('click', function() {
        $.post("load_order_from_template/?template_id="+ $(this).data("template_id"))
                .done(function (d) {
                        $('#id_Responsible').val(d.Responsible);

                        $('#id_Client').val(d.Client);
                        $('#id_Client').trigger('change');
                        $('#id_customer_new').val(d.customer_new);
                        $('#id_customer_new').trigger('change');
                        $('#id_Proxy').val(d.Proxy);
                        $('#id_CargoName').val(d.ContainerNum);
                        $('#id_ContainerNum').val();
                        $('#id_Address').val(d.Address);
                        $('#id_District').val(d.District);
                        $('#id_RaywayCr').val(d.RaywayCr);
                        $('#id_RaywayCr2').val(d.RaywayCr2);
                        $('#id_RaywayCrDistrict').val(d.RaywayCrDistrict);
                        $('#id_RaywayCr2District').val(d.RaywayCr2District);
                        $('#id_ExportDate').val(d.ExportDate);
                        $('#id_ExportTime').val(d.ExportTime);
                        $('#id_Operation').val(d.Operation);
                        $('#id_TerminalLoad').val(d.TerminalLoad);
                        $('#id_TerminalDeliver').val(d.TerminalDeliver);
                        $('#id_Weight').val(d.Weight);
                        $('#id_Foot').val(d.Foot);
                        $('#id_Comment').val(d.Comment);
                        $('#id_dz').val(d.dz);
                        $('#id_destination_station').val(d.destination_station);
                        $('#id_cargo_manager').val(d.cargo_manager);
                })

        });
});
