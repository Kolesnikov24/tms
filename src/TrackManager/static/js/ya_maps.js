ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
            center: [55.01794364, 82.93543372],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        }),

    // Создаем геообъект с типом геометрии "Точка".
        myGeoObject = new ymaps.GeoObject({
            // Описание геометрии.
            geometry: {
                type: "Point",
                coordinates: [54.925743103, 82.9485244751]
            },
            // Свойства.
            properties: {
                // Контент метки.
                iconContent: 'у клиента',
            }
        }, {
            // Опции.
            // Иконка метки будет растягиваться под размер ее содержимого.
            preset: 'islands#blackStretchyIcon',
            // Метку можно перемещать.

        });

    myMap.geoObjects.add(myGeoObject);
}
