$(document).ready(function () {
//    var date_format = 'DD.MM.YYYY';
//    var date_start = moment().format('YYYY-MM-DD'),
//        date_end = date_start;

    var start = moment();
    var end = moment();
    var operation_choices = {
        "1": "погр.", "2": "выгр.", "3": "пор."
    };

    var table_id = '#AppReportTable'

    var date_start = moment().startOf('month').format('YYYY-MM-DD'),
      date_end = moment().startOf('month').add(1, 'month').format('YYYY-MM-DD'),
      dz_id = '';
      driver_id=null;
    var start = moment().startOf('month');
    var end = moment().startOf('month').add(1, 'month');

      $('#date_range').daterangepicker({
        locale: {
          "format":"DD.MM.YYYY",
          "applyLabel": "Применить",
          "cancelLabel": "Отмена",
          "customRangeLabel": "Выбрать дату",
        },
          startDate: start,
          endDate: end,
          showCustomRangeLabel:true,
          ranges: {
            'Январь': [moment().startOf('year'), moment().startOf('year').add(1, 'month')],
            'Февраль': [moment().startOf('year').add(1, 'month'), moment().startOf('year').add(2, 'month')],
            'Март': [moment().startOf('year').add(2, 'month'), moment().startOf('year').add(3, 'month')],
            'Апрель': [moment().startOf('year').add(3, 'month'), moment().startOf('year').add(4, 'month')],
            'Май': [moment().startOf('year').add(4, 'month'), moment().startOf('year').add(5, 'month')],
            'Июнь': [moment().startOf('year').add(5, 'month'), moment().startOf('year').add(6, 'month')],
            'Июль': [moment().startOf('year').add(6, 'month'), moment().startOf('year').add(7, 'month')],
            'Август': [moment().startOf('year').add(7, 'month'), moment().startOf('year').add(8, 'month')],
            'Сентябрь': [moment().startOf('year').add(8, 'month'), moment().startOf('year').add(9, 'month')],
            'Октябрь': [moment().startOf('year').add(9, 'month'), moment().startOf('year').add(10, 'month')],
            'Ноябрь': [moment().startOf('year').add(10, 'month'), moment().startOf('year').add(11, 'month')],
            'Декабрь': [moment().startOf('year').add(11, 'month'), moment().startOf('year').add(12, 'month')]
             /*'Текущий месяц': [moment().startOf('month'), moment().startOf('month').add(1, 'month')],
             'Следующий месяц': [moment().startOf('month').add(1, 'month'), moment().startOf('month').add(2, 'month')],
             'Предыдущий месяц': [moment().startOf('month').subtract(1, 'month'),moment().startOf('month')],*/
          }
      },
      function (start, end) {
             date_start = start.format('YYYY-MM-DD');
             date_end = end.format('YYYY-MM-DD');
             update_data_table();
         }
    );

    var removeNSK = function (s) {
        return s === null || typeof s === "undefined" ? "" : s.replace(' ул ', ' ')
            .replace('Кемеровская обл, ', '')
            .replace('Новосибирская обл, ', '')
            .replace('Томская обл, ', '')
            .replace('Омская обл, ', '')
            .replace('г Новосибирск, ', '');
    };

    $(table_id).DataTable({
        ajax: {
            'url': '/' + app_name + '/get_app_report_table/', 'type': 'POST',
            'data': function (d) {
                d.date_start = date_start;
                d.date_end = date_end;
            }
        },
        autoWidth: false,
        serverSide: true,
        bLengthChange: false,
        paging: false,
        iDisplayLength: 50,
        searching: true,
        info: false,
        dom: 'Bript',
        buttons: [
            {
                extend: 'print',
                text: 'Печать',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                }
            },
            {
                extend: 'excel',
                text: 'В excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                },
                filename: function () {
                    return 'Отчет по приложению ' + date_start + ' - ' + date_end;
                }
            },
        ],
        columns: [
            {'data': 'row_num', 'className': 'cell-row_num', 'width': '10px', 'orderable': false},
            {'data': 'FIO', 'className': 'cell-FIO', 'width': '80px', 'orderable': false},
            {'data': 'dz', 'className': 'cell-dz', 'width': '60px', 'orderable': false},
            {'data': 'app_install', 'className': 'cell-app_install', 'width': '30px', 'orderable': false},
            {'data': 'app_version', 'className': 'cell-app_version', 'width': '10px', 'orderable': false},
            {'data': 'orders_total', 'className': 'cell-orders_total', 'width': '60px', 'orderable': false},
            {'data': 'order_status_count', 'className': 'cell-order_status_count', 'width': '60px', 'orderable': false},
            {'data': 'foto_count', 'className': 'cell-foto_count', 'width': '100px', 'orderable': false},
            {'data': 'recognized_total', 'className': 'cell-recognized_total', 'width': '70px', 'orderable': false},
            {'data': 'manual_input', 'className': 'cell-manual_input', 'width': '90px', 'orderable': false}
        ]
    });

    //TODO: delete duplicate code
    var update_data_table = function () {
        var table = $(table_id).DataTable();
        table.rows()
            .every(function () {
                var d = this.data();
                d.counter++; // update data source for the row
                this.invalidate(); // invalidate the data DataTables has cached for this row
            });
        table.draw();
    };


    $('#filter_driver').select2({width: '100%', theme: "bootstrap"})


    $('.table_filter').on('change', function () {
        var col_num = $(this).data('col_num')
        var table = $(table_id).DataTable(), val = $(this).val();
        table.column(col_num).search(val).draw();
    });


    $(table_id + ' tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            var table = $(table_id).DataTable();
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

});
