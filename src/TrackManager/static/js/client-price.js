$(function () {
    var date_format = 'DD.MM.YYYY';

    $('#datepicker').datetimepicker({
        locale: 'ru',
        format: date_format
    }); 

    $('#btnSave').on('click', function () {
        $('#ClientPriceForm').submit();
    });

    $('#id_client').select2({width: '100%', theme: "bootstrap"});

});
