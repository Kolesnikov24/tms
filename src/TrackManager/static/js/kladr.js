/**
 * Created by ildyakov on 05.11.16.
 */

/**
	Работает на DaData
	https://confluence.hflabs.ru/pages/viewpage.action?pageId=426639416
*/

(function ($) {

	$.fn.kladrInline = function (options) {

		var settings = $.extend({
			'serviceUrl': 'https://suggestions.dadata.ru/suggestions/api/4_1/rs',
			'type': 'ADDRESS',
			'token': '',
			'input': '',
			'flatRequired': false, // flat validation
			'geoLocation': true // true/false/{kladr_id: '63000001'}
		}, options);

		return this.each(function () {

			var $input = $(settings.input, this),
				$inputHidden = $input.next('input'),
				parent = $input.parent(),
				$inputError = parent.find('.js-kladr-inline-error'),
				$region_name = parent.find('.js-kladr-inline-regname'),
				$region_code = parent.find('.js-kladr-inline-regcode'),
				district_selector = parent.data('district'),
				$district_name = $(district_selector);

			var showError = function (msg, color) {
				if (msg === '') {
					$inputError.html('');
					return;
				} else {
					$inputError.html('<span class="_' + color + '">' + msg + '</span>');
				}
			};

			$input.suggestions({
				serviceUrl: settings.serviceUrl,
				token: settings.token,
				type: settings.type,
				autoSelectFirst: true,
				hint: false,
				mobileWidth: 420,
				geoLocation: settings.geoLocation,
				onSelect: function (suggestion) {
					var data = suggestion.data;
					$region_name.val(data.region);
					$region_code.val(data.region_kladr_id);
					if (data.fias_code =='54000001000000013720002') {
					    $district_name.val('Терминал СТТ')
					}
					else {
					    $district_name.val(data.city_district);
					}

					//console.log(suggestion);

					showError('');
					if (!data.city && !data.settlement) {
						showError('Введите название населенного пункта', 'red');
						$inputHidden.val('').change();
					} else if (data.city && !data.street) {
						showError('Необходимо указать улицу', 'red');
						$inputHidden.val('').change();
					} else if (data.city && data.street && !data.house) {
						showError('Нужно указать номер дома', 'red');
						$inputHidden.val('').change();
					} else if (data.city && data.street && data.house && !data.flat) {
						if (settings.flatRequired) {
							showError('Укажите номер квартиры', 'red');
							$inputHidden.val('').change();
						} else {
							/*showError('Укажите номер квартиры, если есть', 'gray');
							$inputHidden.val(suggestion.value).change();
							$inputError.next().html('');*/
						}
					} else {
						showError('');
						$inputHidden.val(suggestion.value).change();
						$inputError.next().html('');
					}

				},
				onSelectNothing: function () {
					showError('');
					$inputHidden.val('').change();
				}
			});

		});

	};

}(jQuery));
