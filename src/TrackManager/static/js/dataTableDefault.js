
var date_start = moment().format('YYYY-MM-DD'),
    date_end = date_start;

var default_dt_settings = {
        ajax: {
            'type': 'POST',
            'data': function (d) {
                d.date_start = date_start;
                d.date_end = date_end;
            }
        },
        autoWidth: true,
        serverSide: true,
        bSort : false,
        bLengthChange: false,
        paging: false,
        iDisplayLength: 50,
        searching: true,
        info: false,
        dom: 'Brtip',
        buttons: [
            {
                extend: 'print',
                text: 'Печать',
                /*exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16]
                }*/
            },
            {
                extend: 'excel',
                text: 'В excel',
                /*exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16]
                },*/
                filename: function () {
                    return 'Работа машин ' + date_start + ' - ' + date_end;
                }
            },
            {
                extend: 'colvis',
                collectionLayout: 'fixed four-column'
            }
        ],
        language: {
            buttons: {
                colvis: 'Выбрать колонки'
            }
        }
    }



var init_data_table = function(table_id, settings) {
    $(table_id).DataTable(settings);
}


var update_data_table = function (table_id) {

    var table = $(table_id).DataTable();
    table.rows()
        .every(function () {
            var d = this.data();
            d.counter++;                // update data source for the row
            this.invalidate();          // invalidate the data DataTables has cached for this row
        });
    table.draw();
};


var init_select2_filter = function(table_id, elem_id, col_num) {
    $(elem_id)
    .select2({width: '100%', theme: "bootstrap"})
    .on('change', function () {
        var table = $(table_id).DataTable(), val = $(this).val();
        table.column(col_num).search(val).draw();        
    });
}


var init_select_filter = function(table_id, elem_id, col_num) {
    $(elem_id).on('change', function () {
        var table = $(table_id).DataTable(), val = $(this).val();
        table.column(col_num).search(val).draw();        
        });
}

// Submit формы в модальном окне
var modal_form_submit = function(url,form_id, modal_id, table_id) {
        $(modal_id).modal('hide')
         $.post(url, $(form_id).serialize())
                .done( function (data) {
                    update_data_table(table_id)
                });
};


