$(document).ready(function() {
  var status_ids = '1,2,3,4,5,6,7';
  var date_start = moment().startOf('month').format('YYYY-MM-DD'),
      date_end = moment().startOf('month').add(1, 'month').format('YYYY-MM-DD'),
      dz_id = '';
      var start = moment().startOf('month');
      var end = moment().startOf('month').add(1, 'month');

      $('#date_range').daterangepicker({
        locale: {
          "format":"DD.MM.YYYY",
          "applyLabel": "Применить",
          "cancelLabel": "Отмена"
        },
          startDate: start,
          endDate: end,
          showCustomRangeLabel:false,
          ranges: {
            'Январь': [moment().startOf('year'), moment().startOf('year').add(1, 'month')],
            'Февраль': [moment().startOf('year').add(1, 'month'), moment().startOf('year').add(2, 'month')],
            'Март': [moment().startOf('year').add(2, 'month'), moment().startOf('year').add(3, 'month')],
            'Апрель': [moment().startOf('year').add(3, 'month'), moment().startOf('year').add(4, 'month')],
            'Май': [moment().startOf('year').add(4, 'month'), moment().startOf('year').add(5, 'month')],
            'Июнь': [moment().startOf('year').add(5, 'month'), moment().startOf('year').add(6, 'month')],
            'Июль': [moment().startOf('year').add(6, 'month'), moment().startOf('year').add(7, 'month')],
            'Август': [moment().startOf('year').add(7, 'month'), moment().startOf('year').add(8, 'month')],
            'Сентябрь': [moment().startOf('year').add(8, 'month'), moment().startOf('year').add(9, 'month')],
            'Октябрь': [moment().startOf('year').add(9, 'month'), moment().startOf('year').add(10, 'month')],
            'Ноябрь': [moment().startOf('year').add(10, 'month'), moment().startOf('year').add(11, 'month')],
            'Декабрь': [moment().startOf('year').add(11, 'month'), moment().startOf('year').add(12, 'month')]
             /*'Текущий месяц': [moment().startOf('month'), moment().startOf('month').add(1, 'month')],
             'Следующий месяц': [moment().startOf('month').add(1, 'month'), moment().startOf('month').add(2, 'month')],
             'Предыдущий месяц': [moment().startOf('month').subtract(1, 'month'),moment().startOf('month')],*/
          }
      },
      function (start, end) {
             date_start = start.format('YYYY-MM-DD');
             date_end = end.format('YYYY-MM-DD');
             update_data_table();
         }
    );


    var weekend_days = function () {
      var arr = [];
      var now = new Date();
      var m = now.getMonth();
      var y = now.getYear();

      for (var d = new Date(y, m, 1); d <= new Date(y, m + 1, 0); d.setDate(d.getDate() + 1)) {

        var day = d.getDay();
        if (day == 6 || day == 0) {
            arr.push(d);
        }
      }

      return arr
    };

    // Draw datatable

    $('#ActTable').DataTable({
        ajax: {
            'url': '/' + app_name + '/get_acts_table/', 'type': 'POST', 'data': function (d) {
                d.date_start = date_start;
                d.date_end = date_end;
                d.status_ids = status_ids;
            }
        },
        serverSide: true,
        bLengthChange: false,
        bSort : false,
        dom:'rtip',
        searching: true,
        paging: false,
        info: false,        
        columns: [
            {'data': 'row_num', 'className': 'cell-row_num', 'width': '10px'},
            {'data': 'client', 'className': 'cell-foot', 'width': '10px'},
            {'data': 'number', 'className': 'cell-TruckNumber', 'width': '50px'},
            {'data': 'create_date', 'className': 'cell-FIO', 'width': '80px'},
            {'data': 'order_count', 'className': 'cell-foot', 'width': '10px'},
            {'data': 'money', 'className': 'cell-foot', 'width': '10px'},
            {'data': 'date_from', 'className': 'cell-foot', 'width': '10px'},
            {'data': 'date_to', 'className': 'cell-foot', 'width': '10px'},
            {'data': 'status', 'className': 'cell-TrailerNumber', 'width': '40px'}, 
            {'data': 'status', 'className': 'cell-TrailerNumber', 'width': '40px'}, 
        ],
        columnDefs: [            
        {
            "targets": 9,
            "data":"status",
            "orderable": true,
            "render": function (data, type, full) {
                var html = '';
                var pk = parseInt(full.DT_RowId.replace('request_', ''), 10);
                var status = full.status;   
//                if (status == 'Оплачен') {
//                    return '';
//                }
                var send_disabled = status === 'Согласование' ? 'disabled' : '';
                var action = status === 'Заготовка' ? 'Отправить' : 'Погасить';
                var btn = status === 'Заготовка' ? 'btn-primary' : 'btn-warning';
                var mode = status === 'Заготовка' ? 'send' : 'paid';
                var del_btn = ''
                if (full.status != 'Выставлен' || full.is_admin == 1 ) {
                   del_btn = '<li><a href="#" class="act_button" data-pk="' + pk + '" data-status="delete">Удалить</a></li></ul>'
                }
                // if not summary row - show buttons
                if (pk > 0) {
                    if (full.is_client_bill == 0) {
                        var template = '<div class="btn-group btn-group-xs">\
                        <a role="button" class="btn btn-primary dropdown-toggle btn-xs" href="/'+ app_name + '/act_to_excel?pk='+pk+ '">В Excel</a>\
                        <button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                          <span class="caret"></span>\
                          <span class="sr-only">Toggle Dropdown</span>\
                        </button>\
                        <ul class="dropdown-menu">\
                          <li><a href="#" class="act_button" data-pk="' + pk + '" data-status="2">Проверить</a></li>\
                          <li><a href="#" class="act_button" data-pk="' + pk + '" data-status="3">Отправить</a></li>\
                          <li><a href="#" class="act_button" data-pk="' + pk + '" data-status="5">Погасить</a></li>\
                          <li><a href="#" class="act_button" data-pk="' + pk + '" data-status="7">Выставлен</a></li>'
                          + del_btn + '</div>';


                        var re = new RegExp('{{btn}}', 'g'),
                        html = template
                            .replace('{{action}}', action)
                            .replace('{{mode}}', mode)
                            .replace(re, btn);


                    }
                    else {
                        var html = '<div class="btn-group btn-group-xs">\
                        <a role="button" class="btn btn-primary dropdown-toggle btn-xs" href="/" + app_name + "/act_to_excel?pk='+pk+ '">В Excel</a>\
                        <button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                          <span class="caret"></span>\
                          <span class="sr-only">Toggle Dropdown</span>\
                        </button>\
                        <ul class="dropdown-menu">\
                          <li><a href="#" class="act_button" data-pk="' + pk + '" data-status="4">Согласовать</a></li>\
                          <li><a href="#" class="act_button" data-pk="' + pk + '" data-status="6">Отказать</a></li></ul>\
                        </div>';
                    }
                }


              
          return html;
                
            }
        }]
    });

    // Update table after filters/button apply
    
    var update_data_table = function () {
        var table = $('#ActTable').DataTable();
        table.rows()
            .every(function () {
                var d = this.data();
                d.counter++; // update data source for the row
                this.invalidate(); // invalidate the data DataTables has cached for this row
            });
        table.draw();
    };

    // Change act status
    $(document).on('click', '.act_button', function (e) {
        var pk = $(this).data('pk');
        var status = $(this).data('status');
        if(status ==='delete' && !confirm('Удалить выбранный акт?')) return;
        $.post(status !=='delete' ? '/' + app_name + '/send_act/' : '/' + app_name + '/del_act/', {'pk': pk, 'status': status})
            .done(function (data) {
                update_data_table()
            });
    });

    // Filter by status
    $('#dropdownMenu1').on('click', function (event) {
        $(this).parent().toggleClass("open");
    });

    $('body').on('click', function (e) {
        var filter_status_menu = $('#dropdownMenu1'), open = $('.open');
        if (!filter_status_menu.is(e.target) && filter_status_menu.has(e.target).length === 0 && open.has(e.target).length === 0) {
            filter_status_menu.parent().removeClass('open');
        }
    });

    $('#filter_status_apply').on('click', function () {
        $('#dropdownMenu1').parent().removeClass('open');
        var ids = [];
        $('[name=filter_status]:checked').each(function () {
            ids.push(this.value);
        });
        status_ids = ids.join();
        update_data_table();
    });

    // Filter by client
    $('#filter_client').select2({width: '100%', theme: "bootstrap"});
    $('#filter_client').on('change', function () {
        var table = $('#ActTable').DataTable(), val = $(this).val();
        table.column(1).search(val).draw();
    });


    // Make row yellow on click    
    $('#ActTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            var table = $('#ActTable').DataTable();
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    // $('#filter_client').on('change', function () {
    //     client_id = $(this).val();
    //     update_data_table();
    // });

} );
