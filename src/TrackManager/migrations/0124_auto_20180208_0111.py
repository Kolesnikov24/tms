# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-02-08 01:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0123_auto_20180108_2243'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='agent',
            field=models.CharField(blank=True, max_length=80, null=True),
        ),
        # migrations.AlterField(
        #     model_name='salary',
        #     name='month',
        #     field=models.IntegerField(default=2),
        # ),
    ]
