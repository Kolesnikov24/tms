# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-27 02:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0032_auto_20170526_1659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='Operation',
            field=models.IntegerField(choices=[(1, 'Погрузка'), (2, 'Разгрузка'), (3, 'Порожний')], default=1),
        ),
    ]
