# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-08 04:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0077_auto_20170808_0440'),
    ]

    operations = [
        migrations.AlterField(
            model_name='price',
            name='from_point',
            field=models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, to='TrackManager.Terminal'),
        ),
    ]
