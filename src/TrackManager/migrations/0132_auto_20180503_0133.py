# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-03 01:33
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0131_auto_20180503_0131'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'permissions': (('manager', 'Ввод заявки'), ('dispatcher', 'Обработка заявки'), ('info', 'Справка'), ('salary', 'Табель'), ('schedule', 'Работа машин'), ('acts', 'Акты'), ('reports_page', 'Отчеты'), ('client-price', 'Прайсы')), 'verbose_name_plural': 'Заявки'},
        ),
    ]
