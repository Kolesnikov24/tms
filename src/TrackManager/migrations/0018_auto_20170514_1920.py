# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-14 12:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0017_client_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='ContractName',
        ),
        migrations.AlterField(
            model_name='client',
            name='Name',
            field=models.CharField(max_length=100),
        ),
    ]
