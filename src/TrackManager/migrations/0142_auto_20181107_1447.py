# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-07 14:47
from __future__ import unicode_literals

import TrackManager.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0141_auto_20181107_1438'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salary',
            name='day',
            field=models.IntegerField(default=TrackManager.models.current_day),
        ),
        migrations.AlterField(
            model_name='salary',
            name='month',
            field=models.IntegerField(default=TrackManager.models.current_month),
        ),
        migrations.AlterField(
            model_name='salary',
            name='year',
            field=models.IntegerField(default=TrackManager.models.current_year),
        ),
    ]
