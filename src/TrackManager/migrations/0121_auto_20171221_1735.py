# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-21 10:35
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0120_auto_20171221_1507'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'permissions': (('manager', 'Ввод заявки'), ('dispatcher', 'Обработка заявки'), ('info', 'Справка'), ('salary', 'Табель'), ('schedule', 'Работа машин'), ('acts', 'Акты'), ('reports_page', 'Отчеты')), 'verbose_name_plural': 'Заявки'},
        ),
    ]
