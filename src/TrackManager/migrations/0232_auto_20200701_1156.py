# Generated by Django 3.0.7 on 2020-07-01 04:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('TrackManager', '0231_feedback_create_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calchistory',
            name='Manager',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='calchistory',
            name='Order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Order'),
        ),
        migrations.AlterField(
            model_name='calclog',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Order'),
        ),
        migrations.AlterField(
            model_name='clientadditionalprice',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Client', verbose_name='Клиент'),
        ),
        migrations.AlterField(
            model_name='clientadditionalprice',
            name='dz',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.DispZone', verbose_name='Зона диспетчеризации'),
        ),
        migrations.AlterField(
            model_name='clientadditionalprice',
            name='price_idle',
            field=models.FloatField(verbose_name='Ставка простой руб/мин'),
        ),
        migrations.AlterField(
            model_name='clientbill',
            name='client',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Client', verbose_name='Клиент'),
        ),
        migrations.AlterField(
            model_name='clientbill',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Менеджер'),
        ),
        migrations.AlterField(
            model_name='driver',
            name='dz',
            field=models.ForeignKey(blank=True, default=1, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.DispZone', verbose_name='Зона диспетчеризации'),
        ),
        migrations.AlterField(
            model_name='driveradditionalprice',
            name='driver',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Driver', verbose_name='Водитель'),
        ),
        migrations.AlterField(
            model_name='driveradditionalprice',
            name='dz',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.DispZone', verbose_name='Зона диспетчеризации'),
        ),
        migrations.AlterField(
            model_name='driveradditionalprice',
            name='price_idle',
            field=models.FloatField(verbose_name='Ставка простой руб/мин'),
        ),
        migrations.AlterField(
            model_name='driverdirectionprice',
            name='driver',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Driver', verbose_name='Водитель'),
        ),
        migrations.AlterField(
            model_name='driverdirectionprice',
            name='dz',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.DispZone', verbose_name='Зона диспетчеризации'),
        ),
        migrations.AlterField(
            model_name='order',
            name='Client',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Client', verbose_name='Клиент'),
        ),
        migrations.AlterField(
            model_name='order',
            name='Manager',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Менеджер'),
        ),
        migrations.AlterField(
            model_name='order',
            name='TerminalDeliver',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='TerminalDeliver', to='TrackManager.Terminal', verbose_name='Сдача'),
        ),
        migrations.AlterField(
            model_name='order',
            name='TerminalLoad',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='TerminalLoad', to='TrackManager.Terminal', verbose_name='Забор'),
        ),
        migrations.AlterField(
            model_name='order',
            name='bill_number',
            field=models.ForeignKey(blank=True, max_length=50, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.ClientBill', verbose_name='Номер счета'),
        ),
        migrations.AlterField(
            model_name='order',
            name='customer_new',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Customers', verbose_name='Грузополучатель'),
        ),
        migrations.AlterField(
            model_name='order',
            name='dispatcher_approve',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='dispatcher_approve', to=settings.AUTH_USER_MODEL, verbose_name='диспетчер'),
        ),
        migrations.AlterField(
            model_name='order',
            name='dispatcher_assign',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='dispatcher_assign', to=settings.AUTH_USER_MODEL, verbose_name='диспетчер назначивший'),
        ),
        migrations.AlterField(
            model_name='order',
            name='dispatcher_decline',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='dispatcher_decline', to=settings.AUTH_USER_MODEL, verbose_name='отклонивший диспетчер'),
        ),
        migrations.AlterField(
            model_name='order',
            name='dispatcher_unapprove',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='dispatcher_unapprove', to=settings.AUTH_USER_MODEL, verbose_name='диспетчер не согл.'),
        ),
        migrations.AlterField(
            model_name='order',
            name='dispatcher_unassign',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='dispatcher_unassign', to=settings.AUTH_USER_MODEL, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='order',
            name='driver',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Driver', verbose_name='Водитель'),
        ),
        migrations.AlterField(
            model_name='order',
            name='dz',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.DispZone', verbose_name='Зона диспетчеризации'),
        ),
        migrations.AlterField(
            model_name='order',
            name='trailer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Trailer', verbose_name='Прицеп'),
        ),
        migrations.AlterField(
            model_name='order',
            name='truck',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Truck', verbose_name='Машина'),
        ),
        migrations.AlterField(
            model_name='orderadditionalservice',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Order', verbose_name='заявка'),
        ),
        migrations.AlterField(
            model_name='orderdocuments',
            name='is_client_doc',
            field=models.BooleanField(blank=True, default=False, verbose_name='Документ клиента'),
        ),
        migrations.AlterField(
            model_name='orderdocuments',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Order', verbose_name='Заявка'),
        ),
        migrations.AlterField(
            model_name='orderdocuments',
            name='upload_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Кем добавлен'),
        ),
        migrations.AlterField(
            model_name='orderdriveraction',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Order', verbose_name='Заявка'),
        ),
        migrations.AlterField(
            model_name='orderhistory',
            name='Manager',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='orderhistory',
            name='Order',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Order'),
        ),
        migrations.AlterField(
            model_name='ordertemplate',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Client'),
        ),
        migrations.AlterField(
            model_name='ordertemplate',
            name='create_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='ordertemplate',
            name='order',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Order'),
        ),
        migrations.AlterField(
            model_name='paymentsreport',
            name='driver',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Driver', verbose_name='Водитель'),
        ),
        migrations.AlterField(
            model_name='price',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Client', verbose_name='Клиент'),
        ),
        migrations.AlterField(
            model_name='price',
            name='dz',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.DispZone', verbose_name='Зона диспетчеризация'),
        ),
        migrations.AlterField(
            model_name='price',
            name='from_point',
            field=models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Terminal'),
        ),
        migrations.AlterField(
            model_name='price',
            name='salary_franchise',
            field=models.FloatField(blank=True, null=True, verbose_name='з/п получастник'),
        ),
        migrations.AlterField(
            model_name='price',
            name='salary_own',
            field=models.FloatField(blank=True, null=True, verbose_name='з/п наш водитель'),
        ),
        migrations.AlterField(
            model_name='price',
            name='salary_person',
            field=models.FloatField(blank=True, null=True, verbose_name='з/п частник'),
        ),
        migrations.AlterField(
            model_name='raywaytariffrules',
            name='terminal_deliver_group',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='TerminalDeliverGroup', to='TrackManager.TerminalGroup', verbose_name='Сдача'),
        ),
        migrations.AlterField(
            model_name='raywaytariffrules',
            name='terminal_load_group',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='TerminalLoadGroup', to='TrackManager.TerminalGroup', verbose_name='Забор'),
        ),
        migrations.AlterField(
            model_name='salary',
            name='employee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Driver'),
        ),
        migrations.AlterField(
            model_name='terminal',
            name='Direction',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Direction', verbose_name='Район'),
        ),
        migrations.AlterField(
            model_name='terminal',
            name='dz',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.DispZone', verbose_name='Зона диспетчеризации'),
        ),
        migrations.AlterField(
            model_name='terminal',
            name='terminal_group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.TerminalGroup', verbose_name='тарифная группа'),
        ),
        migrations.AlterField(
            model_name='trailer',
            name='car',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Truck'),
        ),
        migrations.AlterField(
            model_name='truck',
            name='driver',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.Driver'),
        ),
        migrations.AlterField(
            model_name='truck',
            name='dz',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.DispZone'),
        ),
        migrations.AlterField(
            model_name='userlocation',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь'),
        ),
        migrations.AlterField(
            model_name='userlocation',
            name='zone',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.DispZone', verbose_name='Зона диспетчеризации'),
        ),
        migrations.AlterField(
            model_name='userpermission',
            name='rule',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='TrackManager.UserActions'),
        ),
        migrations.AlterField(
            model_name='userpermission',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='info_profile',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='TrackManager.InfoProfile', verbose_name='Поля в справке'),
        ),
    ]
