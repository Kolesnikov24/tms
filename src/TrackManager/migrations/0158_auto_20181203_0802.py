# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-12-03 08:02
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0157_auto_20181203_0759'),
    ]

    operations = [
        migrations.RenameField(
            model_name='agent',
            old_name='director',
            new_name='sign_by',
        ),
    ]
