# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-02-11 14:24
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0178_auto_20190131_1539'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paymentsreport',
            name='auto',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='TrackManager.Truck', verbose_name='А/М'),
        ),
    ]
