# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-14 07:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0015_auto_20170514_1234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='Client',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='TrackManager.Client'),
        ),
    ]
