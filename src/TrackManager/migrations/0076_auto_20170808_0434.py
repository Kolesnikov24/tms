# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-08 04:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0075_auto_20170731_1559'),
    ]

    operations = [
        migrations.AlterField(
            model_name='price',
            name='wait_tarif',
            field=models.FloatField(blank=True, null=True, verbose_name='простой'),
        ),
        # migrations.AlterField(
        #     model_name='salary',
        #     name='day',
        #     field=models.IntegerField(default=8),
        # ),
        # migrations.AlterField(
        #     model_name='salary',
        #     name='month',
        #     field=models.IntegerField(default=8),
        # ),
    ]
