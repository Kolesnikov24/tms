# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-31 15:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0074_tarif_dz'),
    ]

    operations = [
        migrations.AlterField(
            model_name='additonalprice',
            name='driverType',
            field=models.CharField(blank=True, choices=[('O', 'Наш'), ('F', 'Получастник'), ('P', 'Частник')], max_length=1, null=True),
        ),
        # migrations.AlterField(
        #     model_name='salary',
        #     name='day',
        #     field=models.IntegerField(default=31),
        # ),
    ]
