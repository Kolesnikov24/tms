# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-05-27 16:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0210_auto_20190527_2303'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderdocuments',
            name='file',
            field=models.FileField(blank=True, null=True, upload_to='', verbose_name='Файл'),
        ),
    ]
