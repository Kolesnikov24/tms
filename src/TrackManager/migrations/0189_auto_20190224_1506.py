# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-02-24 15:06
from __future__ import unicode_literals
from django.db import migrations
from django.db.models import F
import itertools


def create_rules(apps, schema_editor):
    """This this test"""
    rules = apps.get_model('TrackManager', 'RaywayTariffRules')

    tgl = {
              "TLG1": "Клещиха, СТТ, Легион",
              "TLG2": "2-я Станционная, Архонский переулок 3, Левый берег",
              "TLG3": "ТРГ, Евросиб, ТК Терминал",
              "TLG4": "Правый берег, Аэропорт 2/3, Фабричный пер. 7б, п. Озерный мкр Армейский"}

    adrs = {
        'AL': "Левый",
        'AP': "Правый"
    }
    rws = {
        'PP': "Левый",
        'PL': "Правый"
    }

    tdd = {
        "TDG1": "Клещиха, СТТ, Легион",
        "TDG2": "Правый берег, ТРГ, Евросиб, ТК Терминал, Аэропорт 2/3, Фабричный пер. 7б, п. Озерный мкр Армейский"
    }

    load_matrix = itertools.product(tgl.keys(), adrs.keys(), rws.keys(), tdd.keys())
    for i in load_matrix:
        key = ".".join(i)
        name = "Забор:{0} Адрес:{1} Переезд:{2} Сдача:{3}".format(tgl[i[0]],adrs[i[1]],rws[i[2]],tdd[i[3]])

        # s = rules(search_code=key, name=name, rule=3, oper=1)
        # s.save()



class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0188_raywaytariffrules_oper'),
    ]

    operations = [
        migrations.RunPython(create_rules),
    ]
