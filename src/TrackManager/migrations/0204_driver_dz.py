# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-05-16 15:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0203_order_dispatcher_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='dz',
            field=models.ForeignKey(blank=True, default=1, null=True, on_delete=django.db.models.deletion.CASCADE, to='TrackManager.DispZone', verbose_name='Зона диспетчеризации'),
        ),
    ]
