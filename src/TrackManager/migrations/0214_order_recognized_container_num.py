# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-11-02 09:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0213_driver_driver_key'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='recognized_container_num',
            field=models.CharField(blank=True, max_length=300, null=True, verbose_name='Распознанный номер контейра'),
        ),
    ]
