# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2020-03-01 14:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0229_order_customer_new'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=200, verbose_name='Email')),
                ('text', models.CharField(max_length=1000, verbose_name='Сообщение')),
            ],
            options={
                'verbose_name_plural': 'Вопросы с сайта',
            },
        ),
        migrations.AlterModelOptions(
            name='ordertemplate',
            options={'verbose_name_plural': 'Шаблоны заявок'},
        ),
    ]
