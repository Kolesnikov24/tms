# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-01-19 13:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TrackManager', '0159_auto_20181203_0818'),
    ]

    operations = [
        migrations.AddField(
            model_name='clientadditionalprice',
            name='weight_28_coef',
            field=models.FloatField(blank=True, null=True, verbose_name='Коэффициент вес контейнера более 28 т брутто'),
        ),
        migrations.AddField(
            model_name='driveradditionalprice',
            name='weight_280_coef',
            field=models.FloatField(blank=True, null=True, verbose_name='Коэффициент вес контейнера более 28 т брутто'),
        ),
        migrations.AlterField(
            model_name='client',
            name='agent',
            field=models.CharField(blank=True, default='ООО "АВТО ВЛ"', max_length=100, null=True),
        ),
    ]
