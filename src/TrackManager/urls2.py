from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.views.generic import TemplateView
from . import ctrack

urlpatterns = [

                  url(r'doc/', TemplateView.as_view(template_name='swagger_ui.html'), name='swagger-ui'),
                  url(r'auth/request/', ctrack.RegApi.as_view(), name='auth_request'),
                  url(r'auth/confirm/', ctrack.RegConfirmApi.as_view(), name='auth_confirm'),
                  url(r'load_data/', ctrack.OrderView.as_view(), name='load_data'),
                  url(r'upload_doc', ctrack.UploadDocView.as_view(), name='upload_doc'),
                  url(r'driver_report', ctrack.OrderHistoryView.as_view(), name='driver_report'),
                  url(r'driver_aggregate_report', ctrack.OrderAggregateHistoryView.as_view(),
                      name='driver_aggregate_report'),
                  url(r'save_app_token', ctrack.FireBaseTokenView.as_view(), name='save_app_token'),
                  url(r'set_driver_action', ctrack.OrderDriverActionView.as_view(), name='set_driver_action'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
