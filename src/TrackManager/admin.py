# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from django.urls import reverse
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import (Order, Direction, Price, Driver, Client, Terminal, Trailer,
                     Truck, AdditonalPrice, Option, UserActions, UserPermission,
                     DispZone, UserLocation, DriverDirectionPrice, RegRequests,
                     DriverAdditionalPrice, ClientAdditionalPrice, ReportData,
                     ClientBill, UserProfile, DistrictCoastMapping,  AddDirectionTaxes,
                     Agent, InfoProfile, UserTablePermission, PaymentsReport, RaywayTariffRules,
                     TerminalGroup, OrderAdditionalService, OrderDocuments, OrderDriverAction,
                     Customers, OrderTemplate, Feedback,
                     remove_order_from_salary)

from .constants import get_zone, DATE_FORMAT, DATETIME_FORMAT


def get_order(obj):
    return obj.Order.Number


class ReqRequestAdmin(admin.ModelAdmin):
    model = RegRequests


class EmployeeInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'Контроль доступа'


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    verbose_name = 'Пользователи'
    inlines = (EmployeeInline,)


def save_salary(modeladmin, request, queryset):
    for obj in queryset:
        obj.save_salary_record()


def remove_from_salary(modeladmin, request, queryset):
    for obj in queryset:
        remove_order_from_salary(obj)


remove_from_salary.short_description = 'Удалить данные из табеля по выбранным заявкам ЗП'
save_salary.short_description = 'Обновить ЗП по заявкам'


class OrderAdmin(admin.ModelAdmin):
    def cr_date(self, obj):
        return obj.CreateDate.strftime(DATETIME_FORMAT)

    def ex_date(self, obj):
        return obj.ExportDate.strftime('%d.%m') + ' ' + obj.ExportTime.strftime('%H:%M')

    cr_date.short_description = 'Дата создания'
    ex_date.short_description = 'Дата вывоза'

    list_display = ('Number', 'cr_date', 'ex_date', 'Client', 'Status')
    list_filter = ('ExportDate', 'Status')
    search_fields = ('Number', 'Comment')
    actions = save_salary, remove_from_salary


class StatusAdmin(admin.ModelAdmin):
    list_display = ('pk', 'Name')


class OrderHistoryAdmin(admin.ModelAdmin):
    list_display = ('get_order', 'Manager', 'OldValue', 'NewValue', 'Actiontime')

    def get_order(self, obj):
        num = obj.Order.Number if obj.Order is not None else ''
        return num

    get_order.short_description = 'Заявка'

    search_fields = ('Order__Number', 'NewValue')


class CalcHistoryAdmin(admin.ModelAdmin):
    list_display = ('get_order', 'Manager', 'OldValue', 'NewValue', 'Actiontime')


class CalcLogAdmin(admin.ModelAdmin):
    list_display = ('get_order', 'params', 'error', 'action_time')


def change_dz(modeladmin, request, queryset):
    for obj in queryset:
        try:
            truck_dz = Truck.objects.filter(driver=obj, status='A')[0].dz
            if truck_dz.pk > 1:
                obj.dz = truck_dz
                obj.save()
        except Exception as e:
            print(e)


change_dz.short_description = 'Обновить зону по машине'


class DriverAdmin(admin.ModelAdmin):
    list_display = ('FIO', 'PassportData', 'Phone', 'DrivenLesson', 'DriverType', 'Status', 'dz')
    ordering = ('FIO',)
    list_filter = ('dz', 'Status', 'app_version')
    actions = change_dz,


def delete_salary(modeladmin, request, queryset):
    for obj in queryset:
        try:
            obj.delete()
        except Exception:
            pass


delete_salary.short_description = 'Удалить ЗП по заявкам'


class SalaryAdmin(admin.ModelAdmin):
    list_display = ('employee', 'fulldate', 'value', 'day', 'month', 'year')

    @staticmethod
    def get_employee(obj):
        return obj.Driver.FIO

    ordering = ('-fulldate', 'employee__FIO',)
    search_fields = ('employee__FIO',)
    list_filter = ('year', 'month', 'day')
    actions = delete_salary,


class TruckAdmin(admin.ModelAdmin):
    def get_dz(self, obj):
        return get_zone(obj.dz.id)

    get_dz.short_description = 'Зона'
    list_display = ('get_dz', 'driver', 'brand', 'number', 'status')
    list_filter = ('driver__dz', 'status', 'driver')
    ordering = ('status', 'driver__FIO',)


class TrailerAdmin(admin.ModelAdmin):

    def get_dr(self, obj):
        return obj.car.driver.FIO

    def get_dz(self, obj):
        return get_zone(obj.car.dz.id)

    get_dr.short_description = 'Водитель'
    get_dz.short_description = 'Зона'

    list_display = ('get_dz', 'get_dr', 'car', 'get_foot_display', 'number', 'status')

    list_filter = ('status', 'car__dz')
    ordering = ('car__driver__FIO',)


class ClientAdmin(admin.ModelAdmin):
    def cd(self, o):
        return o.ContractDate.strftime(DATE_FORMAT)

    cd.short_description = 'Дата договора'

    list_display = ('Name', 'ContractNumber', 'cd', 'email', 'agent', 'status')
    ordering = ('Name',)
    list_filter = ('status',)


class DirectionAdmin(admin.ModelAdmin):
    list_display = ('Name', 'Type', 'coast')


class TerminalAdmin(admin.ModelAdmin):
    list_display = ('Name', 'coast', 'dz', 'terminal_group')
    list_filter = ('terminal_group',)


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('Code', 'Description')


class PriceAdmin(admin.ModelAdmin):

    def display_date_f(self, obj):
        return obj.dateFrom.strftime(DATETIME_FORMAT)

    display_date_f.short_description = 'Начало действия'

    def display_date_c(self, obj):
        return obj.createDate.strftime(DATETIME_FORMAT)

    display_date_c.short_description = 'Загружен'

    def display_date_t(self, obj):
        if obj.dateTo:
            return obj.dateTo.strftime(DATETIME_FORMAT)
        else:
            return ''

    display_date_t.short_description = 'Окончание'

    model = Price
    list_display = ('client', 'foot', 'dz', 'to_point', 'price', 'display_date_c', 'display_date_f', 'display_date_t')

    search_fields = (
        'to_point',
    )

    list_filter = ('foot', 'dz', 'client__Name',)


class AdditonalPriceAdmin(admin.ModelAdmin):
    model = AdditonalPrice


class OptionAdmin(admin.ModelAdmin):
    model = Option
    list_display = ('desc', 'value', 'code')


class UserActionsAdmin(admin.ModelAdmin):
    model = UserActions
    list_display = ('name_plural', 'value', 'name')


class UserPermissionAdmin(admin.ModelAdmin):
    model = UserPermission
    list_display = ('user', 'rule')


class DispZoneAdmin(admin.ModelAdmin):
    model = DispZone
    list_display = ('code', 'name')


class UserLocationAdmin(admin.ModelAdmin):
    model = UserLocation
    list_display = ('user', 'zone')


class DriverDirectionPriceAdmin(admin.ModelAdmin):
    model = DriverDirectionPrice
    list_display = (
        'dt', 'foot', 'dz', 'to_point', 'price', 'display_date_c', 'display_date_f', 'display_date_t', 'driver')

    search_fields = (
        'to_point',
    )

    list_filter = ('foot', 'dz', 'dt', 'driver')

    def display_date_f(self, obj):
        return obj.dateFrom.strftime(DATETIME_FORMAT)

    display_date_f.short_description = 'Начало действия'

    def display_date_c(self, obj):
        return obj.createDate.strftime(DATETIME_FORMAT)

    display_date_c.short_description = 'Загружен'

    def display_date_t(self, obj):
        if obj.dateTo:
            return obj.dateTo.strftime(DATETIME_FORMAT)
        else:
            return ''

    display_date_t.short_description = 'Окончание'


class DriverAdditionalPriceAdmin(admin.ModelAdmin):

    def display_date_f(self, obj):
        return obj.dateFrom.strftime(DATETIME_FORMAT)

    display_date_f.short_description = 'Начало действия'

    def display_date_c(self, obj):
        return obj.createDate.strftime(DATETIME_FORMAT)

    display_date_c.short_description = 'Загружен'

    def display_date_t(self, obj):
        if obj.dateTo:
            return obj.dateTo.strftime(DATETIME_FORMAT)
        else:
            return ''

    display_date_t.short_description = 'Окончание'

    model = DriverAdditionalPrice

    list_display = (
        'dt', 'dz', 'foot', 'price_one_coast', 'price_other_coast', 'display_date_c', 'display_date_f',
        'display_date_t',
        'driver')

    list_filter = ('foot', 'dz', 'dt', 'driver')


class ClientAdditionalPriceAdmin(admin.ModelAdmin):
    model = ClientAdditionalPrice

    def df(self, obj):
        return obj.dateFrom.strftime(DATE_FORMAT)

    @staticmethod
    def get_client(obj):
        return 'Общий тариф' if obj.client is None else obj.client.Name

    df.short_description = 'Начало действия'
    list_display = ('get_client', 'dz', 'foot', 'df')


class ReportDataAdmin(admin.ModelAdmin):
    model = ReportData
    list_display = ('code', 'name', 'value')


class UserProfileAdmin(admin.ModelAdmin):
    model = UserProfile


class ClientBillAdmin(admin.ModelAdmin):
    model = ClientBill

    def cr_date(self, obj):
        return obj.create_date.strftime(DATE_FORMAT)

    cr_date.short_description = 'Дата'
    list_display = ('client', 'number', 'cr_date', 'money', 'order_count')


class DistrictCoastMappingAdmin(admin.ModelAdmin):
    model = DistrictCoastMapping


class OrderAdditionalServiceAdmin(admin.ModelAdmin):
    model = OrderAdditionalService

    def get_order(self, obj):
        return obj.order.Number

    get_order.short_description = 'Заявка'

    list_display = ('get_order', 'service_code', 'quantity', 'client_price', 'client_total', 'salary_total')


class AddDirectionTaxesAdmin(admin.ModelAdmin):
    model = AddDirectionTaxes
    list_display = ('direction', 'value')
    ordering = ('direction',)


class AgentAdmin(admin.ModelAdmin):
    model = Agent


class InfoProfileAdmin(admin.ModelAdmin):
    model = InfoProfile


class UserTablePermissionAdmin(admin.ModelAdmin):
    model = UserTablePermission


class PaymentsReportAdmin(admin.ModelAdmin):
    model = PaymentsReport
    list_filter = ('month', 'driver')


class TerminalGroupAdmin(admin.ModelAdmin):
    model = TerminalGroup


def change_tariff_on_rule_1(modeladmin, request, queryset):
    queryset.update(rule=1)


def change_tariff_on_rule_2(modeladmin, request, queryset):
    queryset.update(rule=2)


def change_tariff_on_rule_3(modeladmin, request, queryset):
    queryset.update(rule=3)


change_tariff_on_rule_1.short_description = 'переезд по одному берегу'
change_tariff_on_rule_2.short_description = 'переезд по одному берегу+переезд на другой берег'
change_tariff_on_rule_3.short_description = 'два переезда по одному берегу'


class RaywayTariffRulesAdmin(admin.ModelAdmin):
    model = RaywayTariffRules
    list_display = ('oper', 'terminal_load_group', 'address_coast', 'rayway_coast', 'terminal_deliver_group', 'rule')
    list_filter = ('oper', 'rule', 'terminal_load_group', 'terminal_deliver_group', 'address_coast', 'rayway_coast')
    actions = [change_tariff_on_rule_1, change_tariff_on_rule_2, change_tariff_on_rule_3]


class OrderDocumentsAdmin(admin.ModelAdmin):
    model = OrderDocuments

    def upload_time_f(self, obj):
        if obj.upload_time:
            return obj.upload_time.strftime(DATETIME_FORMAT)
        else:
            return ''

    upload_time_f.short_description = 'Когда загружен'
    list_display = ('order', 'name', 'file', 'upload_time_f', 'upload_by')
    search_fields = ('order__Number',)


class DriverActionsAdmin(admin.ModelAdmin):
    model = OrderDriverAction


class CustomersAdmin(admin.ModelAdmin):
    model = Customers

    search_fields = ('name',)


class OrderTemplateAdmin(admin.ModelAdmin):
    model = OrderTemplate

    def create_time_f(self, obj):
        if obj.create_time:
            return obj.create_time.strftime(DATETIME_FORMAT)
        else:
            return ''

    def link_to_b(self, obj):
        link = reverse('admin:TrackManager_order_change', args=[obj.order.id])
        return f'<a href="{link}">Изменить заявку</a>'

    link_to_b.allow_tags = True
    link_to_b.short_description = 'Заявка'

    def customer(self, obj):
        if obj.order.customer_new is None:
            res = ''
        else:
            res = obj.order.customer_new.name
        return res

    customer.short_description = 'Грузополучатель'
    create_time_f.short_description = 'Когда загружен'
    list_display = ('order', 'customer', 'link_to_b', 'name', 'create_by', 'create_time_f', 'client')


class FeedBackAdmin(admin.ModelAdmin):

    def create_by(self, obj):
        if obj.create_date:
            return obj.create_date.strftime(DATETIME_FORMAT)
        else:
            return ''

    create_by.short_description = 'Дата'
    list_display = ('create_by', 'email', 'text')
    model = Feedback


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(TerminalGroup, TerminalGroupAdmin)
admin.site.register(Agent, AgentAdmin)
admin.site.register(UserLocation, UserLocationAdmin)
admin.site.register(ReportData, ReportDataAdmin)
admin.site.register(Option, OptionAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Direction, DirectionAdmin)
admin.site.register(Driver, DriverAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Terminal, TerminalAdmin)
admin.site.register(Truck, TruckAdmin)
admin.site.register(Trailer, TrailerAdmin)
admin.site.register(InfoProfile, InfoProfileAdmin)
admin.site.register(UserTablePermission, UserTablePermissionAdmin)
admin.site.register(Customers, CustomersAdmin)
admin.site.register(Feedback, FeedBackAdmin)
admin.site.register(OrderTemplate, OrderTemplateAdmin)
admin.site.register(Price, PriceAdmin)
