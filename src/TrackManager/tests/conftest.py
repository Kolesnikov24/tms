import pytest
import uuid
from TrackManager.models import UserProfile, Client
from django.contrib.auth.models import Group


@pytest.fixture
def test_password():
    return 'strong-test-pass'


@pytest.fixture
def create_user(db, django_user_model, test_password):
    def make_user(**kwargs):
        kwargs['password'] = test_password
        if 'username' not in kwargs:
            kwargs['username'] = str(uuid.uuid4())
        return django_user_model.objects.create_user(**kwargs)

    return make_user


@pytest.fixture
def auto_login_user(db, client, create_user, test_password):
    def make_auto_login(user=None):
        if user is None:
            user = create_user()
            profile = UserProfile(user=user)
            profile.save()
            profile.clients.set(Client.objects.all())
            my_group = Group.objects.get(name='Администратор')
            my_group.user_set.add(user)

        client.login(username=user.username, password=test_password)
        return client, user

    return make_auto_login
