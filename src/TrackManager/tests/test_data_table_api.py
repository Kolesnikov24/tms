import pytest
from django.urls import reverse
from datetime import datetime, timedelta


DATE_FORMAT = '%Y-%m-%d'

TABLE_DATA = ['manager_list_json', 'app_report_table', 'orders_calc', 'dispatcher_list_json',
              'dispatcher_experimental', 'actreport', 'check_salary', 'get_reports_table', 'get_clients_table']

# TODO: fix test errors
ERR_DATA = ['salaryreport', 's_table', 'get_payments_table']


@pytest.mark.parametrize('view_name,', TABLE_DATA)
def test_data_tables(client, auto_login_user, view_name):
    client, user = auto_login_user()
    url = reverse(view_name)
    response = client.post(url, data={
        'date_start': (datetime.today() - timedelta(weeks=8)).strftime(DATE_FORMAT),
        'date_end': (datetime.today() - timedelta(weeks=7)).strftime(DATE_FORMAT),
        'draw': 1,
    })
    data = response.json()
    assert isinstance(data, dict)
