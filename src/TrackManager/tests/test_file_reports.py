import pytest
from django.urls import reverse

FILE_REPORTS = ['download-driver-price', 'download-client-price',
                'get_driver_actions', 'disp_excel', 'report_sal']

# TODO: fix test errors
ERR_DATA = ['hist', 'excel', 'send_acts', 'del_act', 'report_total_drivers',
            'containers_all', 'tn', 'pl', 'get_act_report', 'act_to_excel',
            'payments_to_excel', 'all_payments_to_excel$']


@pytest.mark.parametrize('view_name,', FILE_REPORTS)
def test_order_actions(client, auto_login_user, view_name):
    client, user = auto_login_user()
    url = reverse(view_name)
    response = client.get(url)
    assert response.status_code == 200
