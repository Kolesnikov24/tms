import pytest
from django.urls import reverse
from datetime import datetime, timedelta

DATE_FORMAT = '%Y-%m-%d'

ACTIONS = ['save_work', 'save_order_data', 'save_order_doc', 'check_tariff']

# TODO: fix test errors
ERR_ACTIONS = [
    'change_status',
    'save_order',
    'save_order_template',
    'load_order_from_template'
    'save_job',
    'delete_order_doc',
    'calc_tariff_form',
    'update_payments_report'
]


@pytest.mark.parametrize('view_name,', ACTIONS)
def test_order_actions(client, auto_login_user, view_name):
    client, user = auto_login_user()
    url = reverse(view_name)
    data = client.post(url, data={
        'date_start': (datetime.today() - timedelta(weeks=8)).strftime(DATE_FORMAT),
    }).json()
    assert isinstance(data, dict)
