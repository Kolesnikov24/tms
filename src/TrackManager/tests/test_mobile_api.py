import pytest
from django.urls import reverse
from django.contrib.sessions.models import Session
from django.utils import timezone

AUTH_VIEWS = [
    'auth_request',
    'auth_confirm',
]

GET_METHODS = [
    'load_data',
    'driver_report',
    'driver_aggregate_report',
]

POST_METHODS = [
    'save_app_token',
    'set_driver_action',
    'upload_doc',
]


@pytest.fixture
def session_key(db):
    s = Session.objects.filter(expire_date__gt=timezone.now()).first()
    return s.session_key


@pytest.mark.parametrize('view_name,', GET_METHODS)
def test_api_get_methods(client, session_key, view_name):
    url = reverse(view_name)
    data = client.get(url, data={'session_key': session_key}).json()
    assert isinstance(data, dict)


@pytest.mark.parametrize('view_name,', POST_METHODS)
def test_api_post_methods(client, session_key, view_name):
    url = reverse(view_name)
    data = client.post(url, data={'session_key': session_key}).json()
    assert isinstance(data, dict)
