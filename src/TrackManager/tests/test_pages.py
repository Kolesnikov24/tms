import pytest
from django.urls import reverse


PAGES = ['info', 'maps', 'ymaps', 'manager', 'dispatcher', 'manager_calc', 'salary', 'schedule', 'app_report', 'acts',
         'reports_page', 'payments', 'deny', 'tariff_check', 'client-price', 'driver-price']


@pytest.mark.parametrize('view_name,', PAGES)
def test_pages_is_ok(client, auto_login_user, view_name):
    client, user = auto_login_user()
    url = reverse(view_name)
    response = client.get(url)
    assert response.status_code in (200, 302)
