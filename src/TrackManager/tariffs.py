from copy import deepcopy
from collections import namedtuple
from .models import (Price, DistrictCoastMapping, ClientAdditionalPrice, AddDirectionTaxes, RaywayTariffRules,
                     DriverDirectionPrice, DriverAdditionalPrice)


def define_coast_value(order, price_obj):
    tl = str(order.TerminalLoad)
    td = str(order.TerminalDeliver)

    #  coast in (1, 'Левый'), (2, 'Правый'),
    tlc = order.TerminalLoad.coast
    tdc = order.TerminalDeliver.coast

    # Operation in (1, 'погр.'), (2, 'выгр.'),
    oper = order.Operation

    # для межгорода переезд не считаем
    # # Список районов Новосибирска
    city_districts = [d.district for d in DistrictCoastMapping.objects.all()]
    if order.District not in city_districts:
        coast_value = 0
        return coast_value

    # # Если внутри города + забор и сдача в разных терминалах - добавляем переезды:
    # if order.District in city_districts and order.TerminalLoad != order.TerminalDeliver:

    # Защита от дублирования
    dcm = DistrictCoastMapping.objects.filter(district=order.District).first()
    if dcm:
        coast_deliver = dcm.coast
    else:
        return 0

    # 2.1 Если забор контейнера и сдача по заявке на терминалы Клещиха
    #   CТТ     Легион   – переезда нет (операция и погрузка и выгрузка)

    # 2.2 Если забор контейнера и сдача по заявке на терминалы
    # Евросиб   ТРГ   ТК Терминал   - переезда нет (операция и погрузка и выгрузка)

    # 2.3  Если одна операция забор контейнера или сдача по заявке на терминалы    Клещиха     CТТ     Легион
    #  а вторая   Левый берег или 2-я Станционная то переезд по одному берегу (операция и погрузка и выгрузка)
    list_1_1 = ['Клещиха', 'CТТ', 'Легион']
    list_1_2 = ['Левый берег', '2-я Станционная 21, корп 2']
    list_2_1 = ['Евросиб', 'ТРГ', 'ТК Терминал']
    list_2_2 = ['Аэропорт 2/3', 'Правый берег']

    if (tl in list_1_1 and td in list_1_2) or (tl in list_1_2 and td in list_1_1):
        coast_value = price_obj.price_one_coast

    # 2.4  Если одна операция забор контейнера или сдача по заявке на терминалы    Евросиб   ТРГ   ТК Терминал
    #  а вторая Аэропорт 2\3 или Правый берег то переезд по одному берегу (операция и погрузка и выгрузка)
    elif (tl in list_2_1 and td in list_2_2) or (tl in list_2_2 and td in list_2_1):
        coast_value = price_obj.price_one_coast

    # 2.5 Если забор контейнера на терминалах левого берега и операция погрузка на левом берегу
    # а сдача на правый берег то переезд по одному берегу
    elif tlc == 1 and oper == 1 and coast_deliver == 1 and tdc == 2:
        coast_value = price_obj.price_one_coast

    # 2.6 Если забор контейнера на терминалах левого берега и операция погрузка на правом берегу
    # а сдача на правый берег то переезд на другой берег
    elif tlc == 1 and oper == 1 and coast_deliver == 2 and tdc == 2:
        coast_value = price_obj.price_other_coast

    # 2.7 Если забор контейнера на терминалах левого берега и операция выгрузка на левом берегу
    # а сдача на правый берег то переезд на другой берег
    elif tlc == 1 and oper == 2 and coast_deliver == 1 and tdc == 2:
        coast_value = price_obj.price_other_coast

    # 2.8 Если забор контейнера на терминалах левого берега  операция выгрузка на правом берегу
    # а сдача на правый берег то переезд по одному берегу
    elif tlc == 1 and oper == 2 and coast_deliver == 2 and tdc == 2:
        coast_value = price_obj.price_one_coast

    # 2.9 Если забор контейнера на терминалах правого берега операция погрузка на правом берегу
    # а сдача на левый берег то переезд по одному берегу
    elif tlc == 2 and oper == 1 and coast_deliver == 2 and tdc == 1:
        coast_value = price_obj.price_one_coast

    # 2.10 Если забор контейнера на терминалах правого берега  операция погрузка на левом берегу
    # а сдача на левый берег то переезд на другой берег
    elif tlc == 2 and oper == 1 and coast_deliver == 1 and tdc == 1:
        coast_value = price_obj.price_other_coast

    # 2.11 Если забор контейнера на терминалах правого берега  операция выгрузка на правом берегу
    # а сдача на левый берег то переезд на другой берег
    elif tlc == 2 and oper == 2 and coast_deliver == 2 and tdc == 1:
        coast_value = price_obj.price_other_coast

    # 2.12 Если забор контейнера на терминалах правого берега  операция выгрузка на левом берегу
    # а сдача на левый берег то переезд по одному берегу
    elif tlc == 2 and oper == 2 and coast_deliver == 1 and tdc == 1:
        coast_value = price_obj.price_one_coast

    else:
        coast_value = 0

    return coast_value


# add migration
def define_weight_coef(w, price_obj):
    w_coef = 1

    if w is not None:
        d = {
            22000: price_obj.weight_22_coef,
            24000: price_obj.weight_24_coef,
            26000: price_obj.weight_26_coef,
            28000: price_obj.weight_28_coef,
            30000: price_obj.weight_30_coef,
            35000: price_obj.weight_35_coef
        }
        for k, v in d.items():
            if w >= k and v is not None:
                w_coef = v

    return w_coef


def define_weight_coef_driver(w, price_obj):
    w_coef = 1

    if w is not None:
        d = {
            24000: price_obj.weight_245_coef,
            28000: price_obj.weight_280_coef,
            30000: price_obj.weight_305_coef
        }
        for k, v in d.items():
            if w >= k and v is not None:
                w_coef = v

    return w_coef


def calc_order_rates(order, user, save_to_db=False, check_tariff=False):
    """ Основная функция расчета ставок """
    #   нет района - сразу выходим
    if order.District is None:
        error = "District not found"
        order.log_error(error, params={"d": order.District, "c": order.Client})
        return order, error

    # Дефолтные значения, если не найдем в справочнике
    error = None
    platon = None
    client_weight_rate = 0
    salary_weight_rate = 0
    client_price = 0
    salary_price = 0

    # Если загружено более 1го прайса, то учитываем Дату начала и окончания
    # Например мы найдем тарифы текущего месяца, если уже загружены новые

    # Если погрузка то прайс по терминалу сдачи, eсли выгрузка то прайс по терминалу забора
    # (1, 'погр.'), (2, 'выгр.'),
    dz = order.TerminalDeliver.dz if order.Operation == 1 else order.TerminalLoad.dz

    base_filter = {
        'client': order.Client,
        'foot': order.Foot,
        'dz': dz,
        'dateFrom__lte': order.ExportDate,
        'dateTo__gt': order.ExportDate
    }

    # Если не найден тариф с окончанием срока действия - берем последний заруженный
    base_filter_no_end = {
        'client': order.Client,
        'foot': order.Foot,
        'dz': dz,
        'dateFrom__lte': order.ExportDate,
        'dateTo__isnull': True
    }

    base_filter_d = {
        'dt': order.driver.DriverType,
        'foot': order.Foot,
        'dz': dz,
        'dateFrom__lte': order.ExportDate,
        'dateTo__gt': order.ExportDate
    }

    base_filter_no_end_d = {
        'dt': order.driver.DriverType,
        'foot': order.Foot,
        'dz': dz,
        'dateFrom__lte': order.ExportDate,
        'dateTo__isnull': True
    }

    # Ищем в справочнике тарифы (основной и дополнительный)
    district_filter = deepcopy(base_filter)
    district_filter['to_point'] = order.District

    # персональные прайсы для водителей-партнеров
    if order.driver.DriverType == 'S':
        base_filter_d['driver'] = order.driver
        base_filter_no_end_d['driver'] = order.driver

    # Сначала ищем клиентские прайсы с заполненной датой начала и датой окнчания
    price_set = Price.objects.filter(**district_filter)

    try:
        if price_set.count() == 1:
            add_price = ClientAdditionalPrice.objects.get(**base_filter)
            p = price_set[0]

        # Либо последний загруженный
        else:
            new_filter = deepcopy(base_filter_no_end)
            new_filter['to_point'] = order.District
            p = Price.objects.get(**new_filter)
            add_price = ClientAdditionalPrice.objects.get(**base_filter_no_end)

        # Весовой коэффицент
        w_coef = define_weight_coef(order.Weight, add_price)

        # Порожний груз
        operation_coef = 1 if order.Operation != 3 else add_price.empty_coef

        # Ставку заполняем сразу
        client_rate = p.price * w_coef * operation_coef
        client_price = deepcopy(p.price)
        client_weight_rate = client_rate - (p.price * operation_coef)
        platon = AddDirectionTaxes.objects.filter(direction=order.District).first()

    except Exception as e:
        order.log_error(str(e), params={"d": order.District, "c": order.Client})
        # Make default values
        client_rate = 0
        AddPriceDefault = namedtuple('AddPriceDefault', ['price_one_coast', 'price_other_coast', 'price_idle'])
        add_price = AddPriceDefault(0, 0, 0)

    # Увы - придется повторить ту же процедуру и для водителей
    # Сначала ищем прайсы водителей с заполненной датой начала и датой окнчания
    # Фильтры уже готовы
    # DriverDirectionPrice, DriverAdditionalPrice
    district_filter_d = deepcopy(base_filter_d)
    district_filter_d['to_point'] = order.District
    salary_set = DriverDirectionPrice.objects.filter(**district_filter_d)

    try:
        if salary_set.count() == 1:
            add_salary = DriverAdditionalPrice.objects.get(**base_filter_d)
            s = salary_set[0]
        # Либо последний загруженный
        else:
            new_filter_d = deepcopy(base_filter_no_end_d)
            new_filter_d['to_point'] = order.District
            s = DriverDirectionPrice.objects.filter(**new_filter_d).order_by('-createDate')[0]
            add_salary = DriverAdditionalPrice.objects.filter(**base_filter_no_end_d).order_by('-createDate')[0]

        # Весовой коэффицент
        w_coef = define_weight_coef(order.Weight, add_salary)

        # Порожний груз
        operation_coef = 1 if order.Operation != 3 else add_salary.empty_coef

        # Ставку заполняем сразу
        salary_rate = s.price * w_coef * operation_coef
        salary_weight_rate = salary_rate - (s.price * operation_coef)
        salary_price = deepcopy(s.price)

    except Exception as e:
        order.log_error(str(e), params={"d": order.District, "c": order.Client})
        # Make default values
        salary_rate = 0
        AddSalaryDefault = namedtuple('AddSalaryDefault', ['price_one_coast', 'price_other_coast', 'price_idle'])
        add_salary = AddSalaryDefault(0, 0, 0)

    # Вычисляем - нужно ли добавить расчет переезда в зависимости от терминалов
    client_transfer = define_coast_value(order, add_price)
    salary_transfer = define_coast_value(order, add_salary)

    # Вычисляем - нужно ли добавить расчет переезда в зависимости от адреса переезда
    if order.RaywayCr != '':
        client_transfer = define_railway_value(order, add_price)
        salary_transfer = define_railway_value(order, add_salary)

    # Считаем простой для водителя, если есть
    if order.salary_idle_minutes is not None and order.salary_idle_minutes > 0:
        salary_idle_cost = add_salary.price_idle * order.salary_idle_minutes
    else:
        salary_idle_cost = 0

    # Считаем простой для клиента, если есть
    if order.client_idle_minutes is not None and order.client_idle_minutes > 0:
        client_idle_cost = add_price.price_idle * order.client_idle_minutes
    else:
        client_idle_cost = 0

    client_total = client_rate + client_transfer + client_idle_cost
    salary_total = salary_rate + salary_transfer + salary_idle_cost

    result = {
        'client_rate': client_rate,
        'client_price': client_price,
        'client_total': client_total,
        'client_weight_rate': client_weight_rate,
        'client_transfer': client_transfer,
        'client_idle_price': add_price.price_idle,
        'client_idle_cost': client_idle_cost,
        'add_price': add_price,
        'platon': platon.value if platon else 0,
        'salary_rate': salary_rate,
        'salary_price': salary_price,
        'salary_total': salary_total,
        'salary_weight_rate': salary_weight_rate,
        'salary_transfer': salary_transfer,
        'salary_idle_price': add_salary.price_idle,
        'add_salary': add_salary,
        'salary_idle_cost': salary_idle_cost,
        'save_to_db': save_to_db
    }
    if check_tariff:
        return result

    order.set_client_rates(**result)

    return order, error


def define_railway_value(order, price_obj):
    try:
        """Определение стоимости переезда"""
        search_conditions = dict(
            oper=order.Operation,
            terminal_load_group=order.TerminalLoad.terminal_group,
            address_coast=DistrictCoastMapping.objects.get(district=order.District).coast,
            rayway_coast=DistrictCoastMapping.objects.get(district=order.RaywayCrDistrict).coast,
            terminal_deliver_group=order.TerminalDeliver.terminal_group)

        # Filter rule
        rule = RaywayTariffRules.objects.get(**search_conditions).rule

        if rule == 1:
            coast_value = price_obj.price_one_coast
        elif rule == 2:
            coast_value = price_obj.price_one_coast + price_obj.price_other_coast
        elif rule == 3:
            coast_value = price_obj.price_one_coast + price_obj.price_one_coast
        else:
            coast_value = 0

    except Exception as e:
        coast_value = 0
        order.log_error("RayWay rule not found", params={"d": str(e), "c": order.RaywayCrDistrict})

    return coast_value
