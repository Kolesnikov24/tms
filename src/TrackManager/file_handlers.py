import shutil
import xlsxwriter
from datetime import datetime, date, timedelta
from os import remove, path
from urllib.parse import quote

from django.contrib.auth.decorators import login_required
from django.db.models import Max
from django.http import HttpResponse, QueryDict
from django.shortcuts import redirect

from . import reports
from .prices import PriceDownloader
from .dt import SalaryReport, OrdersCalc
from .sql import get_orders_for_payment, get_orders_for_payment_all
from .tools import logger, has_privs
from .constants import get_status, get_operation, DT_FORMAT, LOGIN_URL
from .models import Order, ClientBill, PaymentsReport, ClientAdditionalPrice


def file_response(filename,
                  verb_name=None,
                  remove_report=False,
                  content_type='application/force-download',
                  content_disposition=None):

    verbose_name = verb_name or filename
    disposition = content_disposition or f'attachment; filename={verbose_name}'

    with open(filename, 'rb') as ex:
        response = HttpResponse(ex.read(), content_type=content_type)
        response['Content-Disposition'] = disposition

    if remove_report:
        remove(filename)

    return response


@login_required(login_url=LOGIN_URL)
def payments_to_excel(request):
    """ Ведомости ЗП  в Excel файле"""
    p_id = request.GET['id']

    payment = PaymentsReport.objects.get(pk=p_id)
    report_date = payment.month.strftime('%Y-%m-%d')
    orders = get_orders_for_payment(report_date, payment.driver.pk)
    filename = reports.get_payment_report(payment, orders)
    # имя файла на русском
    verb_name = "{0}_{1}.xlsx".format(payment.driver.FIO, report_date)
    encode_verb_name = "filename*=utf-8''{}".format(quote(verb_name))

    return file_response(filename, content_disposition='attachment;' + encode_verb_name)


@login_required(login_url=LOGIN_URL)
def all_payments_to_excel(request):
    """ Ведомости ЗП  в Excel файле """
    report_date = request.GET['m']
    month = datetime.strptime(report_date, '%Y-%m-%d').strftime("%m")
    year = datetime.strptime(report_date, '%Y-%m-%d').year

    orders_all = get_orders_for_payment_all(report_date)
    payments_obj = PaymentsReport.objects.filter(month__month=month, month__year=year)
    all_rows = payments_obj.order_by('driver').distinct('driver')
    for row in all_rows:
        try:
            driver = row.driver
            orders = filter(lambda x: x['driver_id'] == driver.id, orders_all)
            payment = payments_obj.get(driver=driver)
            reports.get_payment_report(payment, orders, single_mode=False)
        except Exception as e:
            err_message = 'cant get payment for {0} on {1} date'.format(row.driver, report_date)
            logger.error(err_message)
            raise e

    filename = 'payments.zip'
    shutil.make_archive('payments', 'zip', 'payments')

    return file_response(filename,
                         content_disposition='attachment;' + "filename*=utf-8''{}".format(filename))


@login_required(login_url=LOGIN_URL)
def report_sal(request):
    filename = 'report.xlsx'
    sr = SalaryReport(request=request)
    qs = sr.get_initial_queryset()
    res_data = sr.prepare_results(qs)
    columns_lst = [{'header': '№'}, {'header': 'ФИО'}, {'header': 'Машина'}]
    for i in range(1, 32):
        columns_lst.append({'header': str(i)})
    columns_lst.append({'header': 'Бол.'})
    columns_lst.append({'header': 'Отп.'})
    columns_lst.append({'header': 'Рем.'})
    columns_lst.append({'header': 'Меж.'})
    columns_lst.append({'header': 'Заявок'})
    workbook = xlsxwriter.Workbook(filename, {'remove_timezone': True})
    worksheet = workbook.add_worksheet()
    worksheet.add_table('A1:AM{0}'.format(len(res_data) + 1),
                        {'data': res_data,
                         'style': 'No',
                         'columns': columns_lst})
    workbook.close()
    return file_response(filename, remove_report=True)


@login_required(login_url=LOGIN_URL)
def report_profit(request):
    filename = 'profit_report.xlsx'
    sr = OrdersCalc(request=request)
    qs = sr.get_initial_queryset(req=request)
    raw_data = sr.prepare_results(qs)
    res_data = []
    for i in raw_data:
        item = []
        for k, v in i.items():
            if k != 'DT_RowId':
                item.append(v)
        res_data.append(item)

    reports.profit_report(filename, res_data)
    return file_response(filename, remove_report=True)


@login_required(login_url=LOGIN_URL)
def excel(request):
    filename = 'report_new.xlsx'
    if 'report_disp' in request.path:
        # корректируем введенную дату
        date_min = (date.today().replace(day=1) - timedelta(days=2)).replace(day=1)
        date_max = date.today() + timedelta(days=100)
        date_start = request.GET.get('date_start', date.today())
        date_end = request.GET.get('date_end', date_max)
        if not isinstance(date_start, date):
            date_start = datetime.strptime(date_start, '%Y-%m-%d').date()
        if not isinstance(date_end, date):
            date_end = datetime.strptime(date_end, '%Y-%m-%d').date()
        if date_start < date_min:
            date_start = date_min
        if date_end > date_max:
            date_end = date_max
        if date_start == date_end:
            date_end = date_start + timedelta(days=1)
        else:
            date_end = date_end + timedelta(days=1)

        # определяем фильтр по статусам
        status_ids = request.POST.get('status_ids')
        if status_ids is None:
            status_ids = [1, 2, 3, 4, 5, 6, 7]
        else:
            status_ids = status_ids.split(',')

        raw_data = Order.objects.filter(ExportDate__gte=date_start, ExportDate__lt=date_end,
                                        Status__in=status_ids)
    else:
        raw_data = Order.objects.all()
    res_data = []
    for i in raw_data:
        item_data = [i.Number,
                     i.ExportDate.strftime("%Y.%m.%d") + " " + i.ExportTime.strftime("%H:%m"),
                     i.Client.Name,
                     i.Customer,
                     i.Foot, i.ContainerNum,
                     i.Weight, get_operation(i.Operation),
                     i.Proxy,
                     i.AddressLoad.replace('Новосибирская обл, Новосибирский р-н, ', '') if str(
                         i.TerminalLoad) == "Прочее" else str(i.TerminalLoad),
                     i.Address,
                     i.District,
                     i.RaywayCr,
                     i.RaywayCr2,
                     i.AddressDeliver.replace('Новосибирская обл, Новосибирский р-н, ', '') if str(
                         i.TerminalDeliver) == "Прочее" else str(i.TerminalDeliver),
                     i.Responsible,
                     get_status(i.Status),
                     '' if i.truck is None else str(i.truck),
                     '' if i.trailer is None else str(i.trailer),
                     '' if i.driver is None else str(i.driver),
                     '' if i.dispatch_date is None else i.dispatch_date.strftime(
                         DT_FORMAT) + " " + i.Manager.first_name + " " + i.Manager.last_name,
                     '' if i.approve_date is None else i.approve_date.strftime(
                         DT_FORMAT) + " " + i.dispatcher_approve.first_name + " " + i.dispatcher_approve.last_name,
                     '' if i.cancel_date is None else i.cancel_date.strftime(
                         DT_FORMAT) + " " + i.Manager.first_name + " " + i.Manager.last_name,
                     '' if i.correction_date is None else i.correction_date.strftime(
                         DT_FORMAT) + " " + i.Manager.first_name + " " + i.Manager.last_name,
                     i.Comment
                     ]
        res_data.append(item_data)

    workbook = xlsxwriter.Workbook(filename, {'remove_timezone': True})
    worksheet = workbook.add_worksheet()
    worksheet.add_table('A1:Y{0}'.format(len(res_data) + 1),
                        {'data': res_data,
                         'style': 'No',
                         'columns': [{'header': 'Номер'},
                                     {'header': 'Подача машины'},
                                     {'header': 'Клиент'},
                                     {'header': 'Заказчик'},
                                     {'header': 'Фут'},
                                     {'header': 'Контейнер'},
                                     {'header': 'Вес'},
                                     {'header': 'Операция'},
                                     {'header': 'Доверенность'},
                                     {'header': 'Забор'},
                                     {'header': 'Адрес'},
                                     {'header': 'Район'},
                                     {'header': 'Переезд1'},
                                     {'header': 'Переезд2'},
                                     {'header': 'Сдача'},
                                     {'header': 'Ответсвенный, телефон'},
                                     {'header': 'Статус'},
                                     {'header': '№ машины'},
                                     {'header': '№ прицепа'},
                                     {'header': 'Водитель'},
                                     {'header': 'Отправлено диспетчеру'},
                                     {'header': 'Принято диспетчером'},
                                     {'header': 'Время отказа'},
                                     {'header': 'Время коррект.'},
                                     {'header': 'Примечание'}
                                     ]})
    workbook.close()
    return file_response(filename, remove_report=True)


@login_required(login_url=LOGIN_URL)
def report_total_drivers(request):
    """Годовой отчет в разрезе водителей"""
    if not has_privs(request, 'reports_page'):
        return redirect('deny')

    filename = 'drivers_all.xlsx'
    reports.total_drivers(filename)
    return file_response(filename, remove_report=True)


@login_required(login_url=LOGIN_URL)
def transport_invoice(request):
    """Сохраняем траспортную накладную """
    id_order = request.GET.get('order_id')
    is_full = True if request.GET.get('full') == '1' else False
    filename = 'tn_res.xls'
    reports.get_transport_invoice_xls(id_order, is_full)
    return file_response(filename, remove_report=True)


@login_required(login_url=LOGIN_URL)
def get_act_report(request):
    """Сохраняем акт """

    # Берем параметры из GET т.к. так легче сериализовать таблицу
    # POST так же затруднен, т.к. в jquery сложно работть с приходящей бинарщиной
    get_params = QueryDict(request.get_full_path().split('?')[1])
    date_start = get_params.get('date_start')
    date_end = get_params.get('date_end')

    # Обычный запрос, как будто грузим страницу
    # upd: только для заявок, не включенных в другие акты.
    sr = OrdersCalc(request=request)
    qs = sr.get_initial_queryset(params=get_params)
    filt = sr.filter_queryset(qs, params=get_params).filter(bill_number__isnull=True)
    res = sr.prepare_results(filt)

    # Сохраняем Excel на диск (для отправки клиенту) и возвращаем имя файла
    # и тут же сохраняем акт в БД

    if len(res) == 0 and len(qs) > 0:
        return redirect('acts')
    elif len(res) == 0 and len(qs) == 0:
        return redirect('manager_calc')
    else:
        client = res[0]['Client']

        # Для  клиента ТС-контейнер -  отдельный акт
        if client == 'ТС-Контейнер':
            filename = reports.get_act_custom(request.user, res, date_start, date_end, filt)
        elif client == 'ЦД аттс':
            filename = reports.get_act_transcontainer(request.user, res, date_start, date_end, filt)
        else:
            filename = reports.get_act(request.user, res, date_start, date_end, filt)

    return file_response(filename, verb_name='act.xlsx', content_type='application/octet-stream')


@login_required(login_url=LOGIN_URL)
def download_client_price(request):
    client_id = request.GET.get('client_id')
    dz_id = request.GET.get('dz_id')
    date_from = request.GET.get('dateFrom')

    # Для калькулятора не можем определить дату - поэтому ищем послдедний
    if date_from is None:
        date_from = ClientAdditionalPrice. \
            objects.filter(dz_id=dz_id, client_id=client_id).aggregate(Max('dateFrom'))['dateFrom__max']

    pd = PriceDownloader(dz_id, date_from)
    price_name = pd.generate_client_price(client_id)
    if not path.isfile(price_name):
        raise Exception('price not found')

    return file_response(price_name,
                         path.basename(price_name),
                         remove_report=True,
                         content_type='application/vnd.ms-excel',
                         content_disposition='inline; filename=' + path.basename(price_name))


@login_required(login_url=LOGIN_URL)
def download_driver_price(request):
    driver_type = request.GET.get('driver_type')
    dz_id = request.GET.get('dz_id')
    date_from = request.GET.get('dateFrom')

    pd = PriceDownloader(dz_id, date_from)
    price_name = pd.generate_driver_price(driver_type)
    if not path.isfile(price_name):
        raise Exception("price not found")

    return file_response(price_name,
                         verb_name=path.basename(price_name),
                         remove_report=True,
                         content_type='application/vnd.ms-excel',
                         content_disposition='inline; filename=' + path.basename(price_name))


@login_required(login_url=LOGIN_URL)
def containers_all(request):
    """Годовой отчет в кол-ва контейнеров по клиентам"""
    if not has_privs(request, 'reports_page'):
        return redirect('deny')

    filename = 'containers_all.xlsx'
    reports.get_foot_client_report(filename)
    return file_response(filename, remove_report=True)


@login_required(login_url=LOGIN_URL)
def road_list(request):
    """Сохраняем траспортную накладную """
    id_order = request.GET.get('order_id')
    num = Order.objects.get(pk=int(id_order)).Number

    reports.get_road_list(id_order)

    return file_response('pl.docx',
                         verb_name=f'putevoj_{num}.docx',
                         remove_report=True)


@login_required(login_url=LOGIN_URL)
def act_to_excel(request):
    """ Акты в Excel файле"""
    act_id = request.POST.get('pk', request.GET.get('pk', 1))
    act = ClientBill.objects.get(pk=act_id)
    filename = path.join('acts', 'act_{}.xlsx'.format(act.id))
    d_from = act.date_from.strftime('%Y%m%d')
    d_to = act.date_to.strftime('%Y%m%d')
    verb_name = 'Act_{0}_{1}_{2}.xlsx'.format(d_from, d_to, act.number)

    return file_response(filename, verb_name=verb_name)
