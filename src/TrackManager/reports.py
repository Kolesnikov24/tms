import datetime
import os
import os.path
import string
from itertools import groupby
from django.db import connection

from docxtpl import DocxTemplate
from openpyxl import load_workbook
from xlsxwriter import Workbook
from xlrd import open_workbook
from xlutils.copy import copy

from . import models, num2t4ru
from .tools import str_date, xstr
from .constants import get_add_service, MONTH_LIST_LOWER


def get_transport_invoice(id_order, is_full):
    """ Формирует транспортную накладную """
    order = models.Order.objects.get(pk=int(id_order))
    is_rayway = order.RaywayCr is not None and order.RaywayCr != ''
    xfile = load_workbook('resources/tn-rayway.xlsx' if is_rayway else 'resources/tn.xlsx')

    c_num = order.ContainerNum or ''
    exp_time = order.ExportDate.strftime('%d.%m.%Y')
    exp_time1 = order.ExportDate.strftime('%d.%m.%Y') + ' ' + order.ExportTime.strftime('%H:%M')
    # Page 1
    sheet = xfile.get_sheet_by_name('str1')

    # Дата
    sheet['BI9'] = exp_time

    # Client
    sheet['B12'] = str(order.Client).replace('ООО', '').replace('ТК', '').replace('"', '')
    sheet['BD12'] = str(order.Client).replace('ООО', '').replace('ТК', '').replace('"', '')

    sheet['B14'] = order.Responsible
    sheet['BD14'] = order.Responsible

    # Наименование
    sheet['B17'] = f'Контейнер № {c_num}'

    # Кол-во грузовых мест
    sheet['B23'] = order.Weight or ''

    # 5. Указания грузоотправителя
    sheet['B31'] = f'Контейнер № {c_num}  фут: {order.Foot}'

    # прием или сдача груза
    row_num_address = 'B39' if order.Operation == 1 else 'BD39'
    sheet[row_num_address] = order.Address

    if is_rayway:
        row_num_address = 'B51' if order.Operation == 1 else 'BD51'
        sheet[row_num_address] = order.RaywayCr

    row_num_date = 'B41' if order.Operation == 1 else 'BD41'
    sheet[row_num_date] = exp_time1

    # Водитель
    name_list = order.driver.FIO.split(' ')
    drive_second_name = name_list[0]

    if is_rayway:
        sheet['B57'] = drive_second_name
        sheet['BD57'] = drive_second_name
    else:
        sheet['B51'] = drive_second_name
        sheet['BD51'] = drive_second_name

    # Page 2
    sheet = xfile.get_sheet_by_name('str2')

    # Перевозчик, он же водитель
    sheet['B5'] = drive_second_name

    # 11 транспортное средство
    tr_num = str(order.truck).split(' ')
    truck_num = tr_num[1][1:4]

    sheet['B12'] = tr_num[0]
    sheet['BP12'] = truck_num

    xfile.save('tn_res.xlsx')


def get_out_cell(out_sheet, col_index, row_index):
    """ HACK: Extract the internal xlwt cell representation. """
    row = out_sheet._Worksheet__rows.get(row_index)
    if not row:
        return None

    cell = row._Row__cells.get(col_index)
    return cell


def set_out_cell(out_sheet, col, row, value):
    """ Change cell value without changing formatting. """
    # Hack to retain cell style. part 1
    previous_cell = get_out_cell(out_sheet, col, row)
    out_sheet.write(row, col, value)

    # Hack, part 2
    if previous_cell:
        new_cell = get_out_cell(out_sheet, col, row)
        if new_cell:
            new_cell.xf_idx = previous_cell.xf_idx


def get_transport_invoice_xls(id_order, is_full):
    """ Формирует транспортную накладную """
    order = models.Order.objects.get(pk=int(id_order))
    is_rayway = ((order.RaywayCr is not None and order.RaywayCr != '')
                 or (order.RaywayCr2 is not None and order.RaywayCr2 != ''))
    rb = open_workbook('resources/tn-rayway.xls' if is_rayway else 'resources/tn.xls', formatting_info=True,
                       on_demand=True)

    wb = copy(rb)
    # Page 1
    sheet = wb.get_sheet(0)

    sheet.header_margin = 0
    sheet.footer_margin = 0
    sheet.header_str = b""
    sheet.footer_str = b""
    sheet.top_margin = 0.20
    sheet.bottom_margin = 0.10

    c_num = order.ContainerNum or ''
    exp_time = order.ExportDate.strftime('%d.%m.%Y')
    exp_time1 = order.ExportDate.strftime('%d.%m.%Y') + ' ' + order.ExportTime.strftime('%H:%M')

    # Дата
    set_out_cell(sheet, 60, 8, exp_time)

    # 1. Грузоотправитель  2. Грузополучатель
    customer = str(order.customer_new) if order.customer_new is not None else ''
    customer_clear = customer.replace('ООО', '').replace('ТК', '').replace('"', '')

    client = str(order.Client)
    set_out_cell(sheet, 1, 11, client)
    set_out_cell(sheet, 55, 11, customer_clear if is_full else client)

    set_out_cell(sheet, 55, 13, order.Responsible)

    # Наименование
    cargo_name = order.CargoName or ''
    set_out_cell(sheet, 1, 16, "Контейнер № " + c_num + ",  " + cargo_name)

    # Кол-во грузовых мест
    set_out_cell(sheet, 1, 20, order.Weight or '')

    # 5. Указания грузоотправителя
    set_out_cell(sheet, 1, 30, "Контейнер №" + c_num + " фут: " + str(order.Foot))

    road_info = "Забор контейнера {} - Сдача контейнера - {}" \
        .format(order.TerminalLoad, order.TerminalDeliver)
    set_out_cell(sheet, 1, 34, road_info)

    # 6, 7. прием или сдача груза
    set_out_cell(sheet, 1 if order.Operation == 1 else 55, 38, order.Address)
    set_out_cell(sheet, 1 if order.Operation == 1 else 55, 40, exp_time1)

    desc = "{} за исправными пломбами без пересчета мест и веса".format("принят" if order.Operation == 1 else 'сдан')
    set_out_cell(sheet, 1 if order.Operation == 1 else 55, 44, desc)

    if is_rayway:
        set_out_cell(sheet, 1 if order.Operation == 1 else 55, 50, order.RaywayCr)

    # Водитель
    drive_second_name = ''
    if order.driver is not None:
        name_list = order.driver.FIO.split(' ')
        drive_second_name = name_list[0]

        set_out_cell(sheet, 1, 56 if is_rayway else 50, drive_second_name)
        set_out_cell(sheet, 55, 56 if is_rayway else 50, drive_second_name)

    # 8. Условия перевозки
    cond = 'Груз {} без проверки качества свойств и количества мест за исправными пломбами'
    set_out_cell(sheet, 1, 54 if not is_rayway else 60, cond.format('принят' if order.Operation == 1 else 'сдан'))

    # Page 2
    sheet = wb.get_sheet(1)
    sheet.header_margin = 0
    sheet.footer_margin = 0
    sheet.header_str = b""
    sheet.footer_str = b""
    sheet.top_margin = 0.20
    sheet.bottom_margin = 0.10
    # sheet.left_margin = 0.07

    # Перевозчик, он же водитель
    set_out_cell(sheet, 1, 4, drive_second_name)

    # 11 транспортное средство
    if order.truck is not None:
        tr_num = str(order.truck).split(' ')
        truck_num = tr_num[1][1:4]

        set_out_cell(sheet, 1, 11, tr_num[0])
        set_out_cell(sheet, 67, 11, truck_num)

    wb.save('tn_res.xls')


def get_road_list(id_order):
    """ Формирует путевой лист """
    doc = DocxTemplate('resources/road_list_template.docx')

    order = models.Order.objects.get(pk=int(id_order))

    # Page 1
    month_list = ['января', 'февраля', 'марта', 'апреля',
                  'мая', 'июня', 'июля', 'августа',
                  'сентября', 'октября', 'ноября', 'декабря']
    sheet = {'day': order.ExportDate.strftime('%d'),
             'month': month_list[int(order.ExportDate.strftime('%m')) - 1],
             'year': order.ExportDate.strftime('%Y'),
             'time': order.ExportTime.strftime('%H:%M'),
             'company': 'АВТО-ВЛ, Станиславского 2/3',
             'brand': order.truck.brand,
             'number': order.truck.number,
             'FIO': order.driver.FIO,
             'lessons': order.driver.DrivenLesson,
             'tr_num': order.trailer.number}

    master = models.ReportData.objects.get(code='road_list_master_1')

    sheet['master'] = str(master)
    sheet['doctor'] = 'Tараканова Г.В.'
    # Дата
    sheet['client'] = str(order.Client)
    sheet['con'] = order.ContainerNum
    sheet['load'] = order.Address if order.Operation == 1 else ''
    sheet['deliver'] = order.Address if order.Operation == 2 else ''

    doc.render(sheet)
    doc.save('pl.docx')


def total_drivers(filename):
    sql = """SELECT
        d.name,
        substr(t.number,2,3),
        count(o.id),
        sum(o.client_total),
        sum(o.salary_total),
        sum(o.profit)
        FROM TrackManager_truck t
        JOIN TrackManager_dispzone d on d.id = t.dz_id
        LEFT JOIN TrackManager_order o on t.id = o.truck_id
        group by d.name,substr(t.number,2,3)
        order by 1,2
        """
    with connection.cursor() as cursor:
        cursor.execute(sql)
        db_rows = cursor.fetchall()

    columns_lst = [{'header': 'Зона', 'total_string': 'Итого'},
                   {'header': '№ A/M'},
                   {'header': 'Число рейсов', 'total_function': 'sum'},
                   {'header': 'Выручка', 'total_function': 'sum'},
                   {'header': 'з/п', 'total_function': 'sum'},
                   {'header': 'Прибыль', 'total_function': 'sum'},
                   ]
    workbook = Workbook(filename)
    worksheet = workbook.add_worksheet()
    worksheet.add_table('A1:F{0}'.format(len(db_rows) + 1),
                        {'data': db_rows,
                         'total_row': 1,
                         'style': 'Table Style Light 15',
                         'columns': columns_lst})
    workbook.close()


def get_foot_client_report(filename):
    sql = '''SELECT
        c.name,
        o.Foot,
        strftime('%m', ExportDate),
        count(o.id)
        FROM TrackManager_order o
        JOIN TrackManager_client c on o.client_id = c.id
        group by c.name, o.Foot, strftime('%m', ExportDate)
        order by 1,3,2'''

    with connection.cursor() as cursor:
        cursor.execute(sql)
        db_rows = cursor.fetchall()

    month_list = ['Январь', 'Февраль', 'Март', 'Апрель',
                  'Май', 'Июнь', 'Июль', 'Август',
                  'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
    db_rows1 = []
    for r in db_rows:
        m = month_list[int(r[2]) - 1]
        db_rows1.append((r[0], r[1], m, r[3]))

    columns_lst = [{'header': 'Клиент', 'total_string': 'Итого'},
                   {'header': 'Фут'},
                   {'header': 'Месяц'},
                   {'header': 'Кол-во конт.', 'total_function': 'sum'},
                   ]
    workbook = Workbook(filename)
    worksheet = workbook.add_worksheet()
    worksheet.add_table('A1:D{0}'.format(len(db_rows1) + 1),
                        {'data': db_rows1,
                         'total_row': 1,
                         #  'style': 'No',
                         'style': 'Table Style Light 15',
                         'columns': columns_lst})
    workbook.close()


def get_act(user, queryset, date_start, date_end, filtered_qs):
    """ Формирует приложение к акту оплаты """
    # Создаем запись об акте
    if len(queryset) == 0:
        return 'no orders for acts'

    # remove summary row
    queryset = [q for q in queryset if q['row_num'] != '']

    client = queryset[0]['ClientFullName']
    client_id = models.Client.objects.get(Name=queryset[0]['Client'])
    number = models.ClientBill.objects.filter(client=client_id).count() + 1

    # define agent
    o_id = str(queryset[0]['DT_RowId']).split('_')[1]
    order = models.Order.objects.get(pk=o_id)
    client_name = order.Client.Name
    agent = order.Client.agent
    director_sign = 'Абрашитов В.Ш.'

    description_cell = ''

    if agent == 'ООО "АВТО ВЛ"' or agent == 'ООО "АВТО-ВЛ"':
        description_cell = "Услуги по автоподвозу {cargo1} {foot}ф контейнеров {cont} с {from} " \
                           "на {address}{rayway1}{rayway2}, снятие {cargo2} на {to}"

    if agent == 'ООО "АВТОТРАНСТЕХСЕРВИС"':
        description_cell = "Услуги по доставке {foot}ф контейнера(ов) {cont} с {from} " \
                           "в {address}, снятие {to}"
        director_sign = 'Кудояр М.С.'

    if agent == 'ООО "ГРУЗАВТОТРАНС"':
        description_cell = "Услуги по доставке {foot}ф контейнера(ов) {cont} с {from} " \
                           "в {address}, снятие {to}"
        director_sign = 'Гайсин С.А.'

    if client_name == 'Евросиб':
        description_cell = "Услуги по доставке {foot}ф контейнеров {cont} согласно приложению 1.0 с {from} " \
                           "в {district} р-н, снятие {to}"

    # Берем из БД -
    # TODO: если берем шаблоны агентов из БД - в АКТАх, в  наименовании услуг - лишняя пустая строка
    # client_row = models.Client.objects.select_related('agent').get(pk=client_id.pk)
    # agent = client_row.agent.name
    # cellDescrTemplate = client_row.agent.service_name
    # directorSign = client_row.agent.sign_by

    # Создаем запись об акте
    bill = models.ClientBill.objects.create(created_by=user, client=client_id, number=number, date_from=date_start,
                                            date_to=date_end, order_count=len(queryset))

    # Сохраняем номер акта в заявках только заявки, которые еще не включены в акты
    filtered_qs.update(bill_number=bill)

    # Сохраняем файл
    filename = os.path.join('acts', 'act_{}.xlsx'.format(bill.id))

    workbook = Workbook(filename)
    sheet = workbook.add_worksheet()

    # Дата акта
    n = datetime.datetime.now()
    d = n.strftime('%d')
    m = MONTH_LIST_LOWER[int(n.strftime('%m')) - 1]
    y = n.strftime('%Y')

    default_format = {
        'border': 1,
        'font_size': 10,
        'font_name': 'Times New Roman',
        'align': 'center',
        'valign': 'vcenter',
        'text_wrap': True
    }

    footer_format = {
        'font_size': 12,
        'font_name': 'Times New Roman',
        'valign': 'vcenter',
        'text_wrap': True
    }

    # Формат заголовка документа
    t_format = dict(default_format)
    t_format['font_size'] = 14
    t_format['border'] = 0
    title_format = workbook.add_format(t_format)

    # Формат обычной ячейки
    t_format = dict(default_format)
    t_format['align'] = 'left'
    text_format = workbook.add_format(t_format)

    # Формат целочисленной ячейки
    t_format = dict(default_format)
    t_format['num_format'] = '0'
    int_format = workbook.add_format(t_format)

    # Формат дробной ячейки
    t_format = dict(default_format)
    t_format.update({'num_format': '# ##0.00', 'align': 'right'})
    float_format = workbook.add_format(t_format)

    # Формат шапки
    t_format = dict(default_format)
    t_format.update({'bold': True, 'font_size': 12, 'border': 0})
    header_format = workbook.add_format(t_format)

    # Расширем 2й столбец
    sheet.set_column('A:A', 5)
    sheet.set_column('B:B', 45)
    sheet.set_column('C:C', 5)
    sheet.set_column('D:D', 6)
    sheet.set_column('E:E', 10)
    sheet.set_column('F:F', 13)

    # Объединим и увеличим 1ю строку
    sheet.merge_range('A1:F1', "", title_format)
    sheet.set_row(0, 40)

    # Заголовок документа
    sheet.write_string('A1', 'Приложение к акту №     от {0} {1} {2} г.'.format(d, m, y), header_format)

    # Шапка таблицы
    t_format = dict(default_format)
    t_format.update({'bold': True, 'border': 1})
    header_format = workbook.add_format(t_format)
    sheet.write_string('A2', '№', header_format)
    sheet.write_string('B2', 'Наименование работ, услуг', header_format)
    sheet.write_string('C2', 'Ед.', header_format)
    sheet.write_string('D2', 'Кол-во', header_format)
    sheet.write_string('E2', 'Цена', header_format)
    sheet.write_string('F2', 'Сумма', header_format)

    # Cумма Итого
    total = 0.0

    # объявляем сортировочную и группировочную функцию
    def sorting_func(x):
        return str(x['client_rate']) + str(x['client_transfer']) \
               + '_' + (x['District'] if x['District'] is not None else '') \
               + '_' + str(x['Foot']) \
               + '_' + x['TerminalDeliver'] \
               + '_' + (x['RaywayCr'] if x['RaywayCr'] is not None else '') \
               + '_' + (x['RaywayCr2'] if x['RaywayCr2'] is not None else '') \
               + '_' + x['Operation']

    # получаем отсортированный список заявок
    orders_sorted = sorted(queryset, key=sorting_func)
    # группируем заявки
    orders_grouped = groupby(orders_sorted, sorting_func)

    groups_cnt = 0
    ind = 3
    cell_num = 1
    for key, group in orders_grouped:
        groups_cnt += 1
        orders = list(group)

        # промежуточные данные заявок
        operation = orders[0]['Operation']
        containers = list(filter(None, [c['ContainerNum'] or None for c in orders]))
        rayway1 = None if orders[0]['RaywayCr'] is None or orders[0]['RaywayCr'] == '' else orders[0]['RaywayCr']
        rayway2 = None if orders[0]['RaywayCr2'] is None or orders[0]['RaywayCr2'] == '' else orders[0]['RaywayCr2']

        # параметры для ячейки с наименованием
        descr_data = {
            'cargo1': 'груженых' if operation == 'выгр.' else 'порожних',
            'cargo2': 'груженых' if operation == 'погр.' else 'порожних',
            'foot': orders[0]['Foot'],
            'from': orders[0]['TerminalLoad'],
            'to': orders[0]['TerminalDeliver'],
            'address': orders[0]['Address'],
            'district': orders[0]['District'],
            'cont': '---' if containers is None or len(containers) == 0 else ", ".join(containers),
            'rayway1': '' if rayway1 is None else ', переезд на ' + rayway1,
            'rayway2': '' if rayway2 is None else ', переезд на ' + rayway2
        }

        # ячейка Кол-во
        cell_cnt = len(orders)
        # ячейка Цена
        cl_rate = orders[0]['client_rate']
        cl_transfer = 0 if orders[0]['client_transfer'] is None else orders[0]['client_transfer']
        cell_price = float(cl_rate) + float(cl_transfer)
        # ячейка Стоимость
        cell_total = cell_cnt * cell_price
        # ячейка Наименование
        cell_descr = description_cell.format(**descr_data)

        # сохраняем инфо об акте в заявке
        for n in orders:
            models.Order.objects.filter(pk=n['DT_RowId'].replace('request_', '')).update(bill_number=bill)

        sheet.set_row(ind - 1, 40)
        # Номер строки таблицы
        i = str(ind)
        ind += 1

        # №
        sheet.write_number('A' + i, cell_num, int_format)

        # Наименование
        sheet.write_string('B' + i, cell_descr, text_format)

        # Ед.
        sheet.write_string('C' + i, 'усл.', int_format)

        # Кол-во
        sheet.write_number('D' + i, cell_cnt, int_format)

        # Цена
        sheet.write_number('E' + i, cell_price, float_format)

        # Сумма
        sheet.write_number('F' + i, cell_total, float_format)

        # Count total summ
        total += cell_total

        cell_num += 1

        idleOrders = list(
            filter(lambda x: x['client_idle_minutes'] is not None and x['client_idle_minutes'] > 0, orders))

        for o in idleOrders:
            # Номер строки таблицы
            i = str(ind)
            ind += 1

            cell_descr = "Задержка автомобиля под контейнером {}".format(o['ContainerNum'])
            cell_cnt = o['client_idle_minutes']
            cell_price = o['client_idle_price']
            cell_total = o['client_idle_cost']

            # №
            sheet.write_number('A' + i, cell_num, int_format)

            # Наименование
            sheet.write_string('B' + i, cell_descr, text_format)

            # Ед.
            sheet.write_string('C' + i, 'мин.', int_format)

            # Кол-во
            sheet.write_number('D' + i, cell_cnt, int_format)

            # Цена
            sheet.write_number('E' + i, cell_price, float_format)

            # Сумма
            sheet.write_number('F' + i, cell_total, float_format)

            # Count total sum
            total += cell_total

            cell_num += 1

        # Ищем доп услуги
        for order in orders:
            add_services = models.OrderAdditionalService.objects.filter(order__pk=int(order['DT_RowId'].split('_')[1]))
            if add_services.count() > 0:
                for ad_serv in add_services:
                    i = str(ind)
                    ind += 1

                    service_name = get_add_service(ad_serv.service_code)
                    service_description = '{0} под контейнером {1}'.format(service_name, order['ContainerNum'])
                    service_cnt = ad_serv.quantity or 1

                    # Пишем строку
                    sheet.write_number('A' + i, cell_num, int_format)
                    sheet.write_string('B' + i, service_description, text_format)
                    sheet.write_string('C' + i, 'усл.', int_format)
                    sheet.write_number('D' + i, service_cnt, int_format)
                    sheet.write_number('E' + i, ad_serv.client_total, float_format)
                    sheet.write_number('F' + i, ad_serv.client_total, float_format)

                    # Count total sum
                    total += ad_serv.client_total
                    cell_num += 1

    bill.money = total
    bill.save()
    # Document footer
    # Описываем форматы
    t_format = dict(footer_format)
    t_format.update({'num_format': '# ##0.00', 'align': 'right', 'bold': True})
    sum_format = workbook.add_format(t_format)

    sum_text_format = workbook.add_format(footer_format)

    t_format = dict(footer_format)
    t_format.update({'bold': True})
    sign_title_format = workbook.add_format(t_format)

    t_format = dict(footer_format)
    t_format.update({'font_size': 10})
    title_format = workbook.add_format(t_format)

    # Пропуск
    sheet.set_row(ind - 1, 5)

    # Итого
    ind += 1
    row = str(ind)
    sheet.merge_range('A' + row + ':' + 'E' + row, "", sum_format)
    sheet.write('A' + row, 'Итого', sum_format)
    sheet.write('F' + row, total, sum_format)

    # НДС
    ind += 1
    row = str(ind)
    sheet.merge_range('A' + row + ':' + 'E' + row, "", sum_format)
    sheet.write('A' + row, 'В том числе НДС', sum_format)
    sheet.write('F' + row, total * 20 / 120, sum_format)

    # Пропуск
    sheet.set_row(ind, 5)
    ind += 1

    # Сумма прописью
    ind += 1
    row = str(ind)
    int_units = ((u'рубль', u'рубля', u'рублей'), 'm')
    exp_units = ((u'копейка', u'копейку', u'копеек'), 'f')
    sheet.merge_range('A' + row + ':' + 'F' + row, "", sum_text_format)
    total_str_num = num2t4ru.decimal2text(total, int_units=int_units, exp_units=exp_units)
    total_text = "Сумма прописью: {}, в т. ч. НДС 20% - {} руб. {} коп."
    nds = str(total * 20 / 120)
    nds1 = nds.split('.')[0]
    nds2 = nds.split('.')[1][:2]
    total_final = total_text.format(total_str_num, nds1, nds2)
    sheet.write_string('A' + row, total_final, sum_text_format)
    sheet.set_row(ind - 1, 30)

    ind += 1
    sheet.set_row(ind, 30)

    # Пропуск
    ind += 1
    sheet.set_row(ind, 40)

    # Строка Исполнитель - Заказчик
    row = str(ind)
    ind += 1
    sheet.set_row(ind, 20)
    sheet.merge_range('A' + row + ':' + 'B' + row, "", sign_title_format)
    sheet.merge_range('D' + row + ':' + 'F' + row, "", sign_title_format)
    sheet.write('A' + row, 'ИСПОЛНИТЕЛЬ', sign_title_format)
    sheet.write('D' + row, 'ЗАКАЗЧИК', sign_title_format)

    # Строка ФИО
    row = str(ind)
    ind += 1
    # agent = 'ООО "АВТО-ВЛ"'
    sheet.set_row(ind, 20)
    sheet.merge_range('A' + row + ':' + 'B' + row, "", title_format)
    sheet.write('A' + row, 'Директор, {}'.format(agent), title_format)

    sheet.merge_range('D' + row + ':' + 'F' + row, "", title_format)
    sheet.write('D' + row, client, title_format)

    # Подписи
    t_format = dict(footer_format)
    t_format.update({'align': 'center', 'top': 1})
    title_format = workbook.add_format(t_format)

    ind += 1
    row = str(ind)
    sheet.merge_range('A' + row + ':' + 'B' + row, "", title_format)
    sheet.write('A' + row, director_sign, title_format)

    sheet.merge_range('D' + row + ':' + 'F' + row, "", title_format)
    sheet.write('D' + row, '', title_format)

    # Save file
    workbook.close()

    return filename


def get_act_custom(user, queryset, date_start, date_end, filtered_qs):
    """ Формирует приложение к акту оплаты """
    # Создаем запись об акте
    if len(queryset) == 0:
        return 'no orders for acts'
    client = queryset[0]['ClientFullName']
    client_id = models.Client.objects.get(Name=queryset[0]['Client'])
    number = models.ClientBill.objects.filter(client=client_id).count() + 1

    # define agent
    o_id = str(queryset[0]['DT_RowId']).split('_')[1]
    order = models.Order.objects.get(pk=o_id)
    agent = order.Client.agent
    director_sign = 'Абрашитов В.Ш.'

    # Создаем запись об акте
    bill = models.ClientBill.objects.create(created_by=user, client=client_id, number=number, date_from=date_start,
                                            date_to=date_end, order_count=len(queryset))

    # Сохраняем номер акта в заявках только заявки, которые еще не включены в акты
    filtered_qs.update(bill_number=bill)

    # Сохраняем файл
    filename = os.path.join('acts', 'act_{}.xlsx'.format(bill.id))
    workbook = Workbook(filename)
    sheet = workbook.add_worksheet()

    # Дата акта
    n = datetime.datetime.now()
    d = n.strftime('%d')
    m = MONTH_LIST_LOWER[int(n.strftime('%m')) - 1]
    y = n.strftime('%Y')

    default_format = {
        'border': 1,
        'font_size': 10,
        'font_name': 'Times New Roman',
        'align': 'center',
        'valign': 'vcenter',
        'text_wrap': True
    }

    footer_format = {
        'font_size': 10,
        'font_name': 'Times New Roman',
        'valign': 'vcenter',
        'text_wrap': True
    }

    # Формат заголовка документа
    t_format = dict(default_format)
    t_format['font_size'] = 14
    t_format['border'] = 0
    title_format = workbook.add_format(t_format)

    # Формат обычной ячейки
    t_format = dict(default_format)
    t_format['align'] = 'left'
    text_format = workbook.add_format(t_format)

    # Формат целочисленной ячейки
    t_format = dict(default_format)
    t_format['num_format'] = '0'

    # Формат дробной ячейки
    t_format = dict(default_format)
    t_format.update({'num_format': '# ##0.00', 'align': 'right'})
    float_format = workbook.add_format(t_format)

    # Формат шапки
    t_format = dict(default_format)
    t_format.update({'bold': True, 'font_size': 12, 'border': 0})
    header_format = workbook.add_format(t_format)

    # Расширем 2й столбец
    sheet.set_column('A:A', 13)
    sheet.set_column('B:B', 11)
    sheet.set_column('C:C', 4)
    sheet.set_column('F:F', 16)

    # Объединим и увеличим 1ю строку
    sheet.merge_range('A1:P1', "", title_format)
    sheet.set_row(0, 40)

    # Заголовок документа
    sheet.write_string('A1', 'Приложение к акту №     от {0} {1} {2} г.'.format(d, m, y), header_format)

    # Шапка таблицы
    t_format = dict(default_format)
    header_format = workbook.add_format(t_format)
    sheet.write_string('A2', 'Дата', header_format)
    sheet.write_string('B2', 'Контейнер', header_format)
    sheet.write_string('C2', 'Фут', header_format)
    sheet.write_string('D2', 'Вес', header_format)
    sheet.write_string('E2', 'Забор', header_format)
    sheet.write_string('F2', 'Адрес', header_format)
    sheet.write_string('G2', 'Район', header_format)
    sheet.write_string('H2', 'Сдача', header_format)
    sheet.write_string('I2', 'Начало операции', header_format)
    sheet.write_string('J2', 'Конец операции', header_format)
    sheet.write_string('K2', 'Ставка, руб', header_format)
    sheet.write_string('L2', 'Простой, мин', header_format)
    sheet.write_string('M2', 'Цена мин', header_format)
    sheet.write_string('N2', 'Простой, руб', header_format)
    sheet.write_string('O2', 'Переезд', header_format)
    sheet.write_string('P2', 'Сумма', header_format)

    # Итого
    total = 0.0

    # получаем отсортированный список заявок
    orders_sorted = sorted(queryset, key=lambda x: x['ExportDate'])
    ind = 3
    cell_num = 1

    # Пишем строки акта с данными по каждой заявке
    for order in orders_sorted:
        # Skip summary row
        if order['DT_RowId'] == '0':
            continue

        i = str(ind)
        ind += 1

        sheet.write_string('A' + i, order['ExportDate'], text_format)
        sheet.write_string('B' + i, order['ContainerNum'], text_format)
        sheet.write_string('C' + i, str(order['Foot']), text_format)
        sheet.write_string('D' + i, str(order['Weight']), text_format)
        sheet.write_string('E' + i, str(order['TerminalLoad']), text_format)
        sheet.write_string('F' + i, order['Address'], text_format)
        sheet.write_string('G' + i, order['District'], text_format)
        sheet.write_string('H' + i, str(order['TerminalDeliver']), text_format)
        sheet.write_string('I' + i, order['date_begin'], text_format)
        sheet.write_string('J' + i, order['date_end'], text_format)
        sheet.write_string('K' + i, order['client_rate'], text_format)
        sheet.write_number('L' + i, order['client_idle_minutes'], float_format)
        sheet.write_number('M' + i, order['client_idle_price'], float_format)
        sheet.write_number('N' + i, order['client_idle_cost'], float_format)
        sheet.write_number('O' + i, order['client_transfer'], float_format)
        sheet.write_string('P' + i, order['client_total'], text_format)

        # Count total summ
        total += float(order['client_total'])

        cell_num += 1

    bill.money = total
    bill.save()

    # Document footer
    # Описываем форматы
    sum_text_format = workbook.add_format(footer_format)

    t_format = dict(footer_format)
    t_format.update({'bold': True})
    sign_title_format = workbook.add_format(t_format)

    t_format = dict(footer_format)
    t_format.update({'font_size': 10})
    title_format = workbook.add_format(t_format)

    # Итого
    row = str(ind)
    sheet.write('A' + row, 'Итого', text_format)
    sheet.write('B' + row, '{} шт.'.format(len(orders_sorted) - 1), text_format)
    # ПРосто границы
    for cell in ['C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O']:
        sheet.write(cell + row, None, text_format)

    sheet.write('P' + row, total, text_format)

    # НДС
    ind += 1
    row = str(ind)
    sheet.write('A' + row, 'В том числе НДС', text_format)
    # ПРосто границы
    for cell in ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O']:
        sheet.write(cell + row, None, text_format)
    sheet.write('P' + row, str(round(total * 20 / 120, 3))[0:-1], text_format)

    # Сумма прописью
    ind += 1
    row = str(ind)
    int_units = ((u'рубль', u'рубля', u'рублей'), 'm')
    exp_units = ((u'копейка', u'копейку', u'копеек'), 'f')
    sheet.merge_range('A' + row + ':' + 'P' + row, "", sum_text_format)
    total_str_num = num2t4ru.decimal2text(total, int_units=int_units, exp_units=exp_units)
    total_text = "Сумма прописью: {}, в т. ч. НДС 20% - {} руб. {} коп."
    nds = str(total * 20 / 120)
    nds1 = nds.split('.')[0]
    nds2 = nds.split('.')[1][:2]
    total_final = total_text.format(total_str_num, nds1, nds2)
    sheet.write_string('A' + row, total_final, sum_text_format)
    sheet.set_row(ind - 1, 30)

    ind += 1
    sheet.set_row(ind, 30)

    # Пропуск
    ind += 1
    sheet.set_row(ind, 40)

    # Строка Исполнитель - Заказчик
    row = str(ind)
    ind += 1
    sheet.set_row(ind, 20)
    sheet.merge_range('A' + row + ':' + 'B' + row, "", sign_title_format)
    sheet.merge_range('D' + row + ':' + 'F' + row, "", sign_title_format)
    sheet.write('A' + row, 'ИСПОЛНИТЕЛЬ', sign_title_format)
    sheet.write('D' + row, 'ЗАКАЗЧИК', sign_title_format)

    # Строка ФИО
    row = str(ind)
    ind += 1
    sheet.set_row(ind, 20)
    sheet.merge_range('A' + row + ':' + 'B' + row, '', title_format)
    sheet.write('A' + row, 'Директор, {}'.format(agent), title_format)

    sheet.merge_range('D' + row + ':' + 'F' + row, '', title_format)
    sheet.write('D' + row, client, title_format)

    # Подписи
    t_format = dict(footer_format)
    t_format.update({'align': 'center', 'top': 1})
    title_format = workbook.add_format(t_format)

    ind += 1
    row = str(ind)
    sheet.merge_range('A' + row + ':' + 'B' + row, '', title_format)
    sheet.write('A' + row, director_sign, title_format)

    sheet.merge_range('D' + row + ':' + 'F' + row, '', title_format)
    sheet.write('D' + row, '', title_format)

    # Save file
    workbook.close()

    return filename


def get_act_transcontainer(user, queryset, date_start, date_end, filtered_qs):
    """ Формирует приложение к акту оплаты для ЦД (Трансконтейнер). """
    # Создаем запись об акте
    if len(queryset) == 0:
        return 'no orders for acts'

    client_id = models.Client.objects.get(Name=queryset[0]['Client'])
    number = models.ClientBill.objects.filter(client=client_id).count() + 1

    # объявляем сортировочную и группировочную функцию
    def sorting_func(x):
        return x['Foot'], x['ExportDate']

    # получаем отсортированный список заявок - это важно для итогов
    orders_sorted = sorted(queryset, key=sorting_func)

    # Создаем запись об акте
    bill = models.ClientBill.objects.create(created_by=user,
                                            client=client_id,
                                            number=number,
                                            date_from=date_start,
                                            date_to=date_end,
                                            order_count=len(queryset))

    # Сохраняем номер акта в заявках только заявки, которые еще не включены в акты
    filtered_qs.update(bill_number=bill)

    # Сохраняем файл
    filename = os.path.join('acts', 'act_{}.xlsx'.format(bill.id))

    workbook = Workbook(filename)
    sheet = workbook.add_worksheet()

    # Дата акта
    n = datetime.datetime.now()
    d = n.strftime('%d')
    m = MONTH_LIST_LOWER[int(n.strftime('%m')) - 1]
    y = n.strftime('%Y')

    default_format = {
        'border': 1,
        'font_size': 12,
        'font_name': 'Times New Roman',
        'align': 'center',
        'valign': 'vcenter',
        'text_wrap': True
    }

    footer_format = {
        'font_size': 10,
        'font_name': 'Times New Roman',
        'valign': 'vcenter',
        'text_wrap': True
    }

    # Формат заголовка документа
    t_format = dict(default_format)
    t_format.update({'text_wrap': False, 'align': 'right', 'border': 0})
    title_format = workbook.add_format(t_format)

    # Формат заголовка документа
    t_format = dict(default_format)
    t_format.update({'text_wrap': False, 'align': 'left', 'border': 0})
    footer_format2 = workbook.add_format(t_format)

    # Формат заголовка документа
    t_format = dict(default_format)
    t_format.update({'align': 'center', 'border': 0})
    th2_format = workbook.add_format(t_format)

    # Формат обычной ячейки
    t_format = dict(default_format)
    t_format['align'] = 'left'
    text_format = workbook.add_format(t_format)

    # Формат целочисленной ячейки
    t_format = dict(default_format)
    t_format.update({'text_wrap': False, 'align': 'left'})
    iterim_summary_format = workbook.add_format(t_format)

    # Формат дробной ячейки
    t_format = dict(default_format)
    t_format.update({'num_format': '# ##0.00', 'align': 'right'})
    float_format = workbook.add_format(t_format)

    # Формат наименования отчета
    t_format = dict(default_format)
    t_format.update({'bold': True, 'font_size': 12, 'border': 0})
    header_format = workbook.add_format(t_format)

    # Верхний колонтитул
    sheet.write_string('T1', 'Приложение № 3', title_format)
    sheet.write_string('T2', 'к договору аренды транспортного средства с экипажем', title_format)
    sheet.write_string('T3', '№ 165/18-ТК  от «15» ноября 2018', title_format)

    # Заголовок документа
    doc_date = 'от «{0}» {1} {2} г. № 165/18-ТК'.format(d, m, y)
    start_period = orders_sorted[0]['ExportDate'].split(' ')[0] + '.' + y
    end_period = orders_sorted[-1]['ExportDate'].split(' ')[0] + '.' + y

    sheet.merge_range('A5:T5', 'Сводная ведомость', header_format)
    sheet.merge_range('A6:T6', 'по договору аренды транспортного средства с экипажем', header_format)
    sheet.merge_range('A7:T7', doc_date, header_format)
    sheet.merge_range('A9:T8', 'за период с {0}г. по {1}г.'.format(start_period, end_period), header_format)

    # колонтитул перед таблицей
    sheet.merge_range('A9:C9', 'г.Новосибирск', th2_format)
    sheet.merge_range('R9:T9', 'к акту №  от {}г.'.format(end_period), th2_format)

    # Шапка таблицы
    # стиль
    t_format = dict(default_format)
    t_format.update({'font_size': 8})
    th_format = workbook.add_format(t_format)

    # Увеличить строки
    sheet.set_row(10, 40)
    sheet.set_row(11, 50)

    # Увеличить стролбцы
    sheet.set_column('B:B', 15)
    sheet.set_column('E:E', 12)
    sheet.set_column('G:G', 11)
    sheet.set_column('J:J', 12)
    sheet.set_column('K:K', 12)
    sheet.set_column('H:H', 30)
    sheet.set_column('I:I', 30)

    #  данные
    sheet.merge_range('A11:A12', '№ п/п', th_format)
    sheet.merge_range('B11:B12', '№ контейнера', th_format)
    sheet.merge_range('C11:C12', 'футовость', th_format)
    sheet.merge_range('D11:D12', '№ заявки Арендатора', th_format)
    sheet.merge_range('E11:E12', '№ транспортного средства', th_format)

    sheet.merge_range('F11:G11', 'Транспортная накладная', th_format)
    sheet.write_string('F12', '№ транспортной накладной', th_format)
    sheet.write_string('G12', 'Дата транспортной накладной', th_format)

    sheet.merge_range('H11:I11', 'Маршрут перевозки', th_format)
    sheet.write_string('H12', 'место приема/передачи ТС с экипажем в/из аренды', th_format)
    sheet.write_string('I12', 'Адрес склада грузоотправителя/грузополучателя', th_format)

    sheet.merge_range('J11:K11', 'Срок аренды ТС с экипажем', th_format)
    sheet.write_string('J12', 'Дата и время передачи ТС в аренду', th_format)
    sheet.write_string('K12', 'Дата и время передачи ТС из аренды', th_format)

    sheet.merge_range('L11:L12', 'Общее время аренды ТС с экипажем', th_format)
    sheet.merge_range('M11:M12',
                      'Ставка арендной платы ТС с экипажем при завозе/вывозе с тарификацией: '
                      '(зона, расстояние, время), руб. без НДС',
                      th_format)
    sheet.merge_range('N11:N12', 'Превышение нормы времени на погрузку/выгрузку (час)', th_format)
    sheet.merge_range('O11:O12', 'Стоимость превышения времени под погрузкой/выгрузкой, руб. без НДС', th_format)
    sheet.merge_range('P11:P12', 'Стоимость загрузки –выгрузки по дополнительному адресу, руб. без НДС', th_format)
    sheet.merge_range('Q11:Q12', 'Стоимость прочих услуг, руб. без НДС', th_format)
    sheet.merge_range('R11:R12', 'Итого стоимость арендной платы в руб. без НДС', th_format)
    sheet.merge_range('S11:S12', 'НДС, руб.', th_format)
    sheet.merge_range('T11:T12', 'Итого стоимость арендной платы в руб. с НДС', th_format)

    # Номера столбцов
    for ind, apl in enumerate(list(string.ascii_uppercase)[0:20]):
        sheet.write_number(apl + '13', ind + 1, th_format)

    # пустая строка
    sheet.merge_range('A14:T14', '', workbook.add_format({'bg_color': '#5F7EA9'}))

    # Итоговые суммы
    # промежуточные итоги
    total_no_tax_iterim = 0.0
    nds_iterim = 0.0
    total_iterim = 0.0

    # Итог
    total_no_tax = 0.0
    total_nds = 0.0
    total = 0.0

    ind = 15
    row_number = 1
    prev_order_foot = 20

    # Пишем строки акта с данными по каждой заявке
    for nn, order in enumerate(orders_sorted):

        i = str(ind)
        ind += 1

        #  Если 20 футовые заявки закончились - пишем итог и только потом очередную строку
        if prev_order_foot < int(order['Foot']):
            sheet.merge_range('A{i}:Q{i}'.format(i=i), 'Итого по перевозкам 20 фут. контейнеров', iterim_summary_format)
            sheet.write_number('R' + i, total_no_tax_iterim, float_format)
            sheet.write_number('S' + i, nds_iterim, float_format)
            sheet.write_number('T' + i, total_iterim, float_format)
            #  Обновляем итог
            total_no_tax += total_no_tax_iterim
            total_nds += nds_iterim
            total += total_iterim
            #  обнуляем промежуточный итог
            total_no_tax_iterim = 0.0
            nds_iterim = 0.0
            total_iterim = 0.0

            i = str(ind)
            ind += 1

        # расчитываемые поля
        row_number += nn
        c_num = order['ContainerNum'] or ''

        if len(order['date_begin']) > 0 and len(order['date_begin']) > 0:
            date_begin = datetime.datetime.strptime(order['date_begin'], '%d.%m %H:%M')
            date_end = datetime.datetime.strptime(order['date_end'], '%d.%m %H:%M')
            duration = str(date_end - date_begin)[:4]
        else:
            duration = 0

        adr_load = models.Terminal.objects.get(Name=order['TerminalLoad']).Address
        adr_deliver = order['Address']

        # суммы с учетом ндс
        client_total_foot_nds = float(order['client_total'])
        nds_foot = round(client_total_foot_nds * 20 / 120, 0)
        client_total_foot = client_total_foot_nds - nds_foot
        client_idle_cost = round(float(order['client_idle_cost']) - float(order['client_idle_cost']) * 20 / 120, 0)

        # Запись в строки
        sheet.write_string('A' + i, str(nn + 1), float_format)
        sheet.write_string('B' + i, c_num, text_format)
        sheet.write_string('C' + i, str(order['Foot']), text_format)
        sheet.write_string('D' + i, "", text_format)
        sheet.write_string('E' + i, str(order['truck']), text_format)
        sheet.write_string('F' + i, "", text_format)
        sheet.write_string('G' + i, order['ExportDate'].split(' ')[0] + '.' + y, text_format)
        sheet.write_string('H' + i, 'г. Новосибирск, ' + xstr(adr_load), text_format)
        sheet.write_string('I' + i, adr_deliver, text_format)
        sheet.write_string('J' + i, order['date_begin'], text_format)
        sheet.write_string('K' + i, order['date_end'], text_format)
        sheet.write_string('L' + i, duration, text_format)
        sheet.write_string('M' + i, "", text_format)
        sheet.write_number('N' + i, order['client_idle_minutes'], text_format)
        sheet.write_number('O' + i, client_idle_cost, text_format)
        sheet.write_string('P' + i, "", text_format)
        sheet.write_string('Q' + i, "", text_format)
        sheet.write_number('R' + i, client_total_foot, float_format)
        sheet.write_number('S' + i, nds_foot, float_format)
        sheet.write_number('T' + i, client_total_foot_nds, float_format)

        # Инкремент по итогам
        total_no_tax_iterim += client_total_foot
        nds_iterim += nds_foot
        total_iterim += client_total_foot_nds

        # смещаем указатель разделов по футам
        prev_order_foot = int(order['Foot'])

    #  Обновляем итог
    total_no_tax += total_no_tax_iterim
    total_nds += nds_iterim
    total += total_iterim

    # Сохраняем итог в БД
    bill.money = total
    bill.save()

    ind += 1
    i = str(ind)

    sheet.merge_range('A{i}:Q{i}'.format(i=i), 'Итого по перевозкам 40 фут. контейнеров', iterim_summary_format)
    sheet.write_number('R' + i, total_no_tax_iterim, float_format)
    sheet.write_number('S' + i, nds_iterim, float_format)
    sheet.write_number('T' + i, total_iterim, float_format)

    ind += 1
    i = str(ind)
    sheet.merge_range('A{i}:Q{i}'.format(i=i), 'Итого размер арендной платы в рублях', iterim_summary_format)
    sheet.write_number('R' + i, total_no_tax, float_format)
    sheet.write_number('S' + i, total_nds, float_format)
    sheet.write_number('T' + i, total, float_format)

    # # Document footer
    # # Описываем форматы
    sum_text_format = workbook.add_format(footer_format)
    #
    t_format = dict(footer_format)
    t_format.update({'bold': True})
    sign_title_format = workbook.add_format(t_format)

    # Сумма прописью
    ind += 1
    row = str(ind)
    int_units = ((u'рубль', u'рубля', u'рублей'), 'm')
    exp_units = ((u'копейка', u'копейку', u'копеек'), 'f')
    sheet.merge_range('B' + row + ':' + 'S' + row, "", sum_text_format)
    total_str_num = num2t4ru.decimal2text(total, int_units=int_units, exp_units=exp_units)
    total_text = "Сумма прописью: {}, в т. ч. НДС 20% - {} руб. {} коп."
    nds = str(total * 20 / 120)
    nds1 = nds.split('.')[0]
    nds2 = nds.split('.')[1][:2]
    total_final = total_text.format(total_str_num, nds1, nds2)
    sheet.write_string('B' + row, total_final, sum_text_format)

    # Строка подписи клиента
    ind += 2
    sign_title_format.set_text_wrap(False)
    client_sign = 'Объем перевезенных контейнеров и предоставленных автомобилей в/из аренды подтверждаю' \
                  ' ________________________ Заместитель начальника контейнерного терминала по продажам и' \
                  ' коммерции  Терещенко А.В.'
    sheet.write('B' + str(ind), client_sign, sign_title_format)

    # Строка Арендодатель - Арендатор
    ind += 2
    row = str(ind)
    sheet.merge_range('A' + row + ':' + 'D' + row, 'Арендодатель', footer_format2)
    sheet.merge_range('K' + row + ':' + 'P' + row, 'Арендатор', footer_format2)

    # Строка должность
    ind += 1
    row = str(ind)
    # sheet.set_row(ind, 20)
    sheet.merge_range('A' + row + ':' + 'D' + row, "", footer_format2)
    sheet.write('A' + row, 'Директор', footer_format2)
    sheet.merge_range('K' + row + ':' + 'P' + row, "", footer_format2)
    sheet.write('K' + row, 'Директор филиала', footer_format2)

    # Строка Подпись
    ind += 1
    row = str(ind)
    # sheet.set_row(ind, 20)
    sheet.merge_range('A' + row + ':' + 'D' + row, "", footer_format2)
    sheet.write('A' + row, 'Подпись__________________/ Кудояр М.С. /', footer_format2)
    sheet.merge_range('K' + row + ':' + 'P' + row, "", footer_format2)
    sheet.write('K' + row, 'Подпись________________________________/ Лебедев С.А. /', footer_format2)

    # Строка М.П.
    ind += 1
    row = str(ind)
    sheet.set_row(ind, 20)
    sheet.merge_range('A' + row + ':' + 'D' + row, "", footer_format2)
    sheet.write('A' + row, 'М.П.', footer_format2)
    sheet.merge_range('K' + row + ':' + 'P' + row, "", footer_format2)
    sheet.write('K' + row, 'М.П.		Доверенность № Ц/2019/Н11-61г от 21.02.2019 г.', footer_format2)

    #  Пустая строка в конце
    ind += 1
    sheet.write('A{i}:T{i}'.format(i=ind), "")

    # Save file
    workbook.close()

    return filename


def get_transport_invoice_word(id_order, is_full):
    """ Формирует транспортную накладную в word шаблоне"""

    order = models.Order.objects.get(pk=int(id_order))
    is_rayway = order.RaywayCr is not None and order.RaywayCr != ''
    doc = DocxTemplate('resources/tn-rayway.docx' if is_rayway else 'resources/tn.docx')

    c_num = order.ContainerNum or ''
    exp_time = order.ExportDate.strftime('%d.%m.%Y')
    exp_time1 = order.ExportDate.strftime('%d.%m.%Y') + " " + order.ExportTime.strftime('%H:%M')

    # Массив переменных
    sheet = {'exp_time': exp_time}

    # Страница 1
    # Дата

    # 1. Грузоотправитель  2. Грузополучатель
    customer = str(order.customer_new) if order.customer_new is not None else ''
    customer_clear = customer.replace('ООО', '').replace('ТК', '').replace('"', '')

    if is_full:
        sheet['sender'] = order.Client  # customer_clear if order.Operation == 1 else "Сибирь Контейнер"
        sheet['receiver'] = customer_clear  # "Сибирь Контейнер" if order.Operation == 1 else customer_clear
    else:
        sheet['sender'] = order.Client
        sheet['receiver'] = order.Client

    # Уполномоченное лицо
    sheet['responsible'] = order.Responsible

    # 3. Наименование
    sheet['c_num'] = "Контейнер № " + c_num

    # Кол-во грузовых мест
    sheet['weight'] = order.Weight or ''

    # 5. Указания грузоотправителя
    sheet['c_num_detail'] = "Контейнер №" + c_num + " фут: " + str(order.Foot)
    sheet['fz'] = "Груз соответствует требованиям безопасности и правилам перевозок регламентируемых ФЗ №83"

    decs = "{} за исправными пломбами без пересчета мест и веса"
    # 6. Прием груза
    sheet['load_address'] = order.Address if order.Operation == 1 else ''
    sheet['load_date'] = exp_time1 if order.Operation == 1 else ''
    sheet['load_desc'] = decs.format("принят", ) if order.Operation == 1 else ''
    if is_rayway:
        sheet['load_address2'] = order.RaywayCr if order.Operation == 1 else ''

    # 7. Сдача груза
    sheet['deliver_address'] = '' if order.Operation == 1 else order.Address
    sheet['deliver_date'] = '' if order.Operation == 1 else exp_time1
    sheet['deliver_desc'] = '' if order.Operation == 1 else decs.format("сдан", )
    if is_rayway:
        sheet['deliver_address2'] = '' if order.Operation == 1 else order.RaywayCr

    drive_second_name = ''
    # Водитель
    if order.driver is not None:
        name_list = order.driver.FIO.split(' ')
        drive_second_name = name_list[0]
        sheet['driver'] = drive_second_name
    else:
        sheet['driver'] = drive_second_name

    # 8. Условия перевозки
    cond = "Груз {} без проверки качества свойств и количества мест за исправными пломбами"
    sheet['condition'] = cond.format("принят", ) if order.Operation == 1 else cond.format("сдан", )

    rayway_info = ''
    if order.RaywayCr is not None:
        rayway_info = order.RaywayCr
    if order.RaywayCr2 is not None:
        rayway_info = order.RaywayCr2

    sheet['road_info'] = 'Забор контейнера {} - {} Сдача контейнера - {}'.format(order.TerminalLoad, rayway_info,
                                                                                 order.TerminalDeliver)

    # Страница 2
    # 10. Перевозчик
    sheet['company'] = drive_second_name

    # 11. транспортное средство
    tr_num = str(order.truck).split(' ')
    truck_num = tr_num[1][1:4]

    sheet['tr_num'] = tr_num[0]
    sheet['truck_num'] = truck_num

    # save document
    doc.render(sheet)
    doc.save('invoice.docx')


def get_payment_report(payment, orders, single_mode=True):
    # Платежная ведомость
    workbook = load_workbook('resources/payment-template.xlsx')
    s1 = workbook['report']
    s1['A1'].value = payment.driver.FIO + ' ' + ",".join([str(i) for i in payment.truck.all()])

    row_num = 4
    # Заявки
    for order in orders:
        str_ind = str(row_num)
        s1['A' + str_ind].value = order['ExportDate'].strftime('%d.%m.')
        s1['B' + str_ind].value = order['ContainerNum']
        s1['C' + str_ind].value = order['Foot']
        s1['D' + str_ind].value = order['Weight']
        s1['E' + str_ind].value = order['Operation']
        s1['F' + str_ind].value = order['TerminalLoad']
        s1['G' + str_ind].value = order['Address']
        s1['H' + str_ind].value = order['District']
        s1['I' + str_ind].value = order['TerminalDeliver']
        s1['J' + str_ind].value = str_date(order['operation_date_begin'])
        s1['K' + str_ind].value = str_date(order['operation_date_end'])
        s1['L' + str_ind].value = order['salary_rate']
        s1['M' + str_ind].value = order['salary_idle_minutes']
        s1['N' + str_ind].value = order['salary_idle_cost']
        s1['O' + str_ind].value = order['salary_transfer']
        s1['P' + str_ind].value = order['add_services']
        s1['Q' + str_ind].value = order['salary_total']

        row_num += 1

    # Итог
    name_col = "G"
    value_col = "H"
    summary_list = [
        (1, payment.salary, 'Основной заработок'),
        (2, payment.bonus, 'Доплаты'),
        (3, payment.repair, 'Ремонт'),
        (4, payment.fuel, 'ДТ'),
        (5, payment.gazprom, 'Газпром'),
        (6, payment.simbios, 'Симбиоз'),
        (7, payment.prepayment, 'Аванс'),
        (8, payment.card, 'Карта'),
        (9, payment.tax, 'Налог'),
        (10, payment.credit, 'Кредит'),
        (11, payment.garage, 'Гараж'),
        (12, payment.debt, 'Долг'),
        (13, payment.summary, 'К выдаче')
    ]
    for el in summary_list:
        s1[value_col + str(row_num + 1 + el[0])].value = el[1]
        s1[name_col + str(row_num + 1 + el[0])].value = el[2]

    info = '''По вопросам начисления обращаться по телефону 8-913-721-70-21  Евгения (бухгалтерия)'''
    s1['B' + str(row_num + 15)].value = info

    # Сохраняем во временный файл
    fio = payment.driver.FIO
    filename = os.path.join('payments', 'payment_{}.xlsx'.format(fio))

    workbook.save(filename)

    if single_mode:
        return filename


def profit_report(filename, res_data):
    columns_lst = [
        {'header': '№'},
        {'header': 'Дата'},
        {'header': 'Водитель'},
        {'header': 'Машина'},
        {'header': 'Клиент'},
        {'header': 'Контейнер'},
        {'header': 'Фут'},
        {'header': 'Вес'},
        {'header': 'Операция'},
        {'header': 'Забор'},
        {'header': 'Адрес'},
        {'header': 'Район'},
        {'header': 'Переезд1'},
        {'header': 'Переезд2'},
        {'header': 'Сдача'},
        {'header': 'Примечание'},
        {'header': 'Доп. затраты'},
        {'header': 'Платон'},
        {'header': 'Ставка, руб'},
        {'header': 'Простой, мин'},
        {'header': 'Цена, мин'},
        {'header': 'Простой, руб'},
        {'header': 'Переезд'},
        {'header': 'Итог з/п'},
        {'header': 'Ставка2, руб'},
        {'header': 'Простой2, мин'},
        {'header': 'Цена2, мин'},
        {'header': 'Простой2, руб'},
        {'header': 'Переезд2, руб'},
        {'header': 'Сумма'},
        {'header': '№ счета'},
        {'header': 'Дата оплаты'},
        {'header': 'Прибыль'}]

    workbook = Workbook(filename, {'remove_timezone': True})
    worksheet = workbook.add_worksheet()
    worksheet.add_table('A1:AG{0}'.format(len(res_data) + 1),
                        {'data': res_data,
                         'style': 'No',
                         'columns': columns_lst})
    workbook.close()
