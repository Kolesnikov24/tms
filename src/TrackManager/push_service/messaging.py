"""
based on snipped
https://github.com/firebase/quickstart-python/blob/688fcfa8068dcac67978a171df828c9e77cd320e/messaging/messaging.py
"""

import json
import requests
import logging
from pathlib import Path
from oauth2client.service_account import ServiceAccountCredentials

logger = logging.getLogger('tms')

PROJECT_ID = 'ctrack-18b0a'
BASE_URL = 'https://fcm.googleapis.com'
FCM_ENDPOINT = 'v1/projects/' + PROJECT_ID + '/messages:send'
FCM_URL = BASE_URL + '/' + FCM_ENDPOINT
SCOPES = ['https://www.googleapis.com/auth/firebase.messaging']

# find key file in configs
KEY_FILE_PATH = Path(Path.cwd().parent, 'conf', 'service-account.json')


def _get_access_token():
    """Retrieve a valid access token that can be used to authorize requests.

    :return: Access token.
    """
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        KEY_FILE_PATH, SCOPES)
    access_token_info = credentials.get_access_token()
    return access_token_info.access_token


def _send_fcm_message(fcm_message):
    """Send HTTP request to FCM with given message.

    Args:
      fcm_message: JSON object that will make up the body of the request.
    """
    # [START use_access_token]
    headers = {
        'Authorization': 'Bearer ' + _get_access_token(),
        'Content-Type': 'application/json; UTF-8',
    }
    # [END use_access_token]
    resp = requests.post(FCM_URL, data=json.dumps(fcm_message), headers=headers)

    if resp.status_code != 200:
        logger.error('Unable to send message to Firebase')
        logger.error(resp.text)
    # else:
    #     logger.info('Push send')


def send(token, text):
    message = {
        'message': {
            'token': token,
            'notification': {
                'title': 'Ctrack:',
                'body': text
            }
        }
    }
    _send_fcm_message(message)
