# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.db.models import F
from django.db.models import PROTECT
from django.utils import timezone
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.core.exceptions import ValidationError

import datetime
import logging
import os
from . import constants as choices
from .push_service.messaging import send as send_push

log = logging.getLogger('tms')

TIME_ZONE = timezone.get_current_timezone()


def tz_now():
    return timezone.localtime(timezone.now())


class Client(models.Model):
    """Справочник контрагентов
        ContractName- Наименование
        ContractNumber - Номер договора
        ContractDate - Дата договора
        INN - ИНН
        KPP - КПП
        LegalAdress - Юр. адрес
        BankAccount - Банковские реквизиты
        ShortName - Короткое имя для удобства ввода
    """
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        result = self.shortname if self.shortname else self.Name
        return result

    def __unicode__(self):
        return self.Name

    Name = models.CharField(max_length=100)
    ContractNumber = models.CharField(max_length=100)
    ContractDate = models.DateField()
    INN = models.BigIntegerField()
    KPP = models.IntegerField()
    LegalAdress = models.CharField(max_length=300)
    BankAccount = models.CharField(max_length=300)
    shortname = models.CharField(max_length=80, blank=True, null=True)
    email = models.CharField(max_length=80, blank=True, null=True)
    agent = models.CharField(max_length=100, blank=True, null=True, default='ООО "АВТО ВЛ"')
    # agent = models.ForeignKey('Agent', null=True, blank=True)
    status = models.CharField(
        max_length=1,
        choices=choices.CLIENT_STATUS,
        default='A')


class Driver(models.Model):
    """Справочник водителей
        FIO - ФИО водителя
        PassportData - Паспортные данные
        Phone - телефон
        DrivenLesson - Вод. Удостоверение
        DriverType - Признак [O - Наш, F - Получастник, P - Частник ]
    """
    objects = models.Manager()

    FIO = models.CharField(max_length=100)
    PassportData = models.CharField(max_length=100)
    Phone = models.CharField(max_length=100)
    DrivenLesson = models.CharField(max_length=100)
    DriverType = models.CharField(
        max_length=10,
        choices=choices.DRIVER_TYPE_CHOICES,
        default='O')

    Status = models.CharField(
        max_length=1,
        choices=choices.DRIVER_STATUS,
        default='A')

    dz = models.ForeignKey('DispZone', blank=True, null=True,
                           verbose_name='Зона диспетчеризации', default=1, on_delete=PROTECT)
    app_token = models.CharField(max_length=1000, blank=True, null=True, verbose_name='ID телефона')
    app_version = models.CharField(max_length=100, blank=True, null=True, verbose_name='Версия приложения')

    class Meta:
        verbose_name_plural = 'Водители'

    def __str__(self):
        return " ".join(self.FIO.split(' ')[:-1])


class Customers(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Грузополучатели'

    name = models.CharField(max_length=300, verbose_name='Наименование')

    def __str__(self):
        return self.name


class Order(models.Model):
    """ Основная таблица фактов"""
    objects = models.Manager()

    class Meta:
        """Define custom permission to show alailable menu item
        format of name custom_view_"view_name"
        """
        permissions = (
            ("manager", "Ввод заявки"),
            ("dispatcher", "Обработка заявки"),
            ("tariff_check", "Калькулятор"),
            ("manager_calc", "Справка"),
            ("payments", "Ведомость"),
            ("maps", "Отслеживание"),
            ("salary", "Табель"),
            ("schedule", "Работа машин"),
            ("acts", "Акты"),
            ("reports_page", "Отчеты"),
            ("client-price", "Прайсы"),
        )
        verbose_name_plural = 'Заявки'

    # system fields
    Number = models.CharField(max_length=6, blank=False, null=False, verbose_name='Номер')
    CreateDate = models.DateTimeField(auto_now_add=True, blank=False, null=False, verbose_name='Дата создания')
    Manager = models.ForeignKey(User, blank=False, null=False, verbose_name='Менеджер', on_delete=PROTECT)
    Status = models.IntegerField(default=1, choices=choices.STATUS_CHOICES, verbose_name='Статус')

    # order fields
    ExportDate = models.DateTimeField(default=datetime.date.today, verbose_name='Дата подачи')
    ExportTime = models.TimeField(default=datetime.datetime(2017, 1, 1, 0, 0), verbose_name='Время подачи')
    Client = models.ForeignKey(Client, blank=False, null=False, verbose_name='Клиент', on_delete=PROTECT)
    Customer = models.CharField(max_length=300, blank=True, null=True, verbose_name='Заказчик')
    customer_new = models.ForeignKey(Customers, blank=True, null=True, verbose_name='Грузополучатель',
                                     on_delete=PROTECT)
    Proxy = models.CharField(max_length=300, blank=True, null=True, verbose_name='Доверенность')
    ContainerNum = models.CharField(max_length=300, blank=True, null=True, verbose_name='№ контейнера')
    Foot = models.IntegerField(blank=True, null=True, choices=choices.FOOT_CHOICES, verbose_name='Фут')
    Weight = models.FloatField(default=1, blank=True, null=True, verbose_name='Вес')
    CargoName = models.CharField(max_length=300, blank=True, null=True, verbose_name='Наим.груза')
    Operation = models.IntegerField(default=1, choices=choices.OPERATION_CHOICES, verbose_name='Операция')
    Address = models.CharField(max_length=300, blank=True, null=True, verbose_name='Адрес')
    District = models.CharField(max_length=300, blank=True, null=True, verbose_name='Район')
    TerminalLoad = models.ForeignKey('Terminal', blank=True, null=True, related_name="TerminalLoad",
                                     verbose_name='Забор', on_delete=PROTECT)
    TerminalDeliver = models.ForeignKey('Terminal', blank=True, null=True, related_name="TerminalDeliver",
                                        verbose_name='Сдача', on_delete=PROTECT)
    AddressLoad = models.CharField(max_length=300, blank=True, verbose_name='Адрес забора')
    AddressDeliver = models.CharField(max_length=300, blank=True, verbose_name='Адрес сдачи')
    RaywayCr = models.CharField(max_length=300, blank=True, verbose_name='Переезд1')
    RaywayCrDistrict = models.CharField(max_length=100, blank=True, verbose_name='Район Переезда1')
    RaywayCr2 = models.CharField(max_length=300, blank=True, verbose_name='Переезд2')
    RaywayCr2District = models.CharField(max_length=100, blank=True, verbose_name='Район Переезда2')
    Responsible = models.CharField(max_length=300, blank=True, verbose_name='Отвественный')
    Comment = models.CharField(max_length=300, blank=True, null=True, verbose_name='Примечание')
    truck = models.ForeignKey('Truck', blank=True, null=True, verbose_name='Машина', on_delete=PROTECT)
    trailer = models.ForeignKey('Trailer', blank=True, null=True, verbose_name='Прицеп', on_delete=PROTECT)
    driver = models.ForeignKey(Driver, blank=True, null=True, verbose_name='Водитель', on_delete=PROTECT)

    # Зона диспетчеризации
    dz = models.ForeignKey('DispZone', blank=True, null=True, verbose_name='Зона диспетчеризации', on_delete=PROTECT)
    # workflow fields
    # Дата/время передачи заявки диспетчеру
    dispatch_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата передачи диспетчеру')
    # Дата/время согласования заявки диспетчером
    approve_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата согласования')
    # диспетчер согласовавший
    dispatcher_approve = models.ForeignKey(User, blank=True, null=True, related_name="dispatcher_approve",
                                           verbose_name='диспетчер', on_delete=PROTECT)
    # Дата/время отмены согласования заявки диспетчером
    unapprove_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата отмены согласования')
    # Причина отмены согласования заявки диспетчером
    unapprove_reason = models.CharField(max_length=300, blank=True, verbose_name='Причина отмены согл-я')
    # диспетчер отменивший согласование
    dispatcher_unapprove = models.ForeignKey(User, blank=True, null=True, related_name="dispatcher_unapprove",
                                             verbose_name='диспетчер не согл.', on_delete=PROTECT)
    # Дата/время корректировки заявки
    correction_date = models.DateTimeField(blank=True, null=True, verbose_name='Время корректировки')
    # Дата/время отмены заявки менеджером
    cancel_date = models.DateTimeField(blank=True, null=True, verbose_name='Время отмены менеджером')
    # Причина отмены заявки менеджером
    cancel_reason = models.CharField(max_length=300, blank=True, verbose_name='Причина отмены')
    # Дата/время отклонения заявки диспетчером
    decline_date = models.DateTimeField(blank=True, null=True, verbose_name='время отклонения диспетчером')
    # Причина отклонения заявки диспетчером
    decline_reason = models.CharField(max_length=300, blank=True, verbose_name='Причина отклонения')
    # диспетчер отклонивший заявку
    dispatcher_decline = models.ForeignKey(User, blank=True, null=True, related_name="dispatcher_decline",
                                           verbose_name='отклонивший диспетчер', on_delete=PROTECT)
    # Дата/время назначения авто
    assign_date = models.DateTimeField(blank=True, null=True, verbose_name='время назначения')
    # диспетчер назначивший авто
    dispatcher_assign = models.ForeignKey(User, blank=True, null=True, related_name="dispatcher_assign",
                                          verbose_name='диспетчер назначивший', on_delete=PROTECT)
    # Дата/время снятия авто
    unassign_date = models.DateTimeField(blank=True, null=True, verbose_name='')
    # Причина отклонения заявки диспетчером
    unassign_reason = models.CharField(max_length=300, blank=True, verbose_name='')
    # диспетчер отменивший авто
    dispatcher_unassign = models.ForeignKey(User, blank=True, null=True, related_name="dispatcher_unassign",
                                            verbose_name='', on_delete=PROTECT)

    # дата/время начала операции
    operation_date_begin = models.DateTimeField(blank=True, null=True, verbose_name='время начала операции')
    # дата/время завершения операции
    operation_date_end = models.DateTimeField(blank=True, null=True, verbose_name='время завершения операции')

    # Расчеты
    # дополнительные затраты
    additional_wastes = models.FloatField(blank=True, null=True, default=0, verbose_name='дополнительные затраты')

    # Платон
    platon = models.FloatField(blank=True, null=True, default=0, verbose_name='Платон')

    # Расчет з/п
    # Ставка
    salary_rate = models.FloatField(blank=True, null=True, default=0, verbose_name='Ставка з/п')

    # Простой, мин
    salary_idle_minutes = models.FloatField(blank=True, null=True, default=0, verbose_name='Простой мин')

    # Простой, цена за минуту, руб
    salary_idle_price = models.FloatField(blank=True, null=True, default=0, verbose_name='Простой, цена за мин. з.п.')

    # Простой, стоимость, руб
    salary_idle_cost = models.FloatField(blank=True, null=True, default=0, verbose_name='Простой, стоимость з/п')

    # Переезд
    salary_transfer = models.FloatField(blank=True, null=True, default=0, verbose_name='Переезд з/п')

    # Итог з/п
    salary_total = models.FloatField(blank=True, null=True, default=0, verbose_name='Итог з/п')

    # Расчет для клиента
    # Ставка
    client_rate = models.FloatField(blank=True, null=True, default=0, verbose_name='Ставка клиента')

    # Простой, мин
    client_idle_minutes = models.FloatField(blank=True, null=True, default=0, verbose_name='Простой, мин')

    # Простой, цена за минуту, руб
    client_idle_price = models.FloatField(blank=True, null=True, default=0, verbose_name='Простой, цена за минуту')

    # Простой, стоимость, руб
    client_idle_cost = models.FloatField(blank=True, null=True, default=0, verbose_name='Простой, стоимость')

    # Переезд
    client_transfer = models.FloatField(blank=True, null=True, default=0, verbose_name='Переезд сумма')

    # Экспедирование
    client_forwarding = models.FloatField(blank=True, null=True, default=0, verbose_name='Экспедирование.клиент')

    # Зачистка
    client_cleaning = models.FloatField(blank=True, null=True, default=0, verbose_name='Зачистка.клиент')

    # Экспедирование
    salary_forwarding = models.FloatField(blank=True, null=True, default=0, verbose_name='Экспедирование.водитель')

    # Зачистка
    salary_cleaning = models.FloatField(blank=True, null=True, default=0, verbose_name='Зачистка.водитель')

    # Итог з/п
    client_total = models.FloatField(blank=True, null=True, default=0, verbose_name='Итог c клиента')

    # Номер счета
    bill_number = models.ForeignKey('ClientBill', blank=True, null=True, max_length=50, verbose_name='Номер счета',
                                    on_delete=PROTECT)

    # Дата оплаты
    payment_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата оплаты')

    # Прибыль
    profit = models.FloatField(blank=True, null=True, default=0, verbose_name='Прибыль')

    # Автовизит забор номер
    auto_visit_take_num = models.CharField(max_length=20, blank=True, null=True, verbose_name='Автовизит забор номер')

    # Автовизит забор время план
    auto_visit_take_plan = models.DateTimeField(blank=True, null=True, verbose_name='Автовизит забор время план')

    # Автовизит забор время факт
    auto_visit_take_fact = models.DateTimeField(blank=True, null=True, verbose_name='Автовизит забор время факт')

    # Автовизит сдача номер
    auto_visit_return_num = models.CharField(max_length=20, blank=True, null=True, verbose_name='Автовизит сдача номер')

    # Автовизит сдача время план
    auto_visit_return_plan = models.DateTimeField(blank=True, null=True, verbose_name='Автовизит время сдачи план')

    # Автовизит сдача время факт
    auto_visit_return_fact = models.DateTimeField(blank=True, null=True, verbose_name='Автовизит время сдачи факт')

    # Станция назначения
    destination_station = models.CharField(max_length=50, blank=True, null=True, verbose_name='Станция назначения')

    # произвольный менеджер заполняемый с формы
    cargo_manager = models.CharField(max_length=50, blank=True, null=True, verbose_name='Менеджер')

    dispatcher_comment = models.CharField(max_length=300, blank=True, null=True, verbose_name='Комментарий диспетчера')

    recognized_container_num = models.CharField(max_length=300, blank=True, null=True,
                                                verbose_name='Распознанный номер контейра')

    manual_input = models.BooleanField(default=False, verbose_name='Номер введен вручную')

    last_driver_action = models.IntegerField(blank=True, null=True, choices=choices.ORDER_DRIVER_STATUS,
                                             verbose_name='Действие водителя')

    def app_notify(self, txt):
        """
         Push в приложение
        """
        if self.driver is not None:
            try:
                token = self.driver.app_token or ''
                if len(token) > 0:
                    send_push(token, txt)
                else:
                    log.error('can not send push - invalid app token')
            except Exception as e:
                log.error("push sending failed")
                log.error(e, exc_info=True)

    def get_driver_action(self, raw=False):
        #  статус водителя
        try:
            last_act = self.orderdriveraction_set.last()
            driver_action = last_act.action if last_act else 0
        except Exception:
            driver_action = 0

        if raw:
            res = driver_action
        elif driver_action == 0:
            res = ''
        else:
            res = choices.get_app_action(driver_action)

        return res

    # Сохранить любые данные заявки
    def save_data(self, user, **kwargs):
        old_val = []
        new_val = []
        for key, value in kwargs.items():
            # Try to convert data into valid Datetime
            try:
                year = datetime.datetime.now().strftime('%Y')
                value_to_set = datetime.datetime.strptime(year + '.' + value[0], '%Y.%d.%m %H:%M').strftime(
                    '%Y-%m-%d %H:%M:%S')
            except Exception:
                value_to_set = value[0]

            # Set Field
            if value_to_set != '':
                setattr(self, key, value_to_set)

        self.save()

        # Аудит
        s = OrderHistory(Order=self, Manager=user, OldValue=";".join(old_val),
                         NewValue=";".join(new_val))
        s.save()

    # Отправка на согласование диспетчеру
    def send(self, user):
        old_status = choices.get_status(self.Status)
        self.Status = 2
        self.dispatch_date = datetime.datetime.now()
        s = OrderHistory(Order=self, Manager=user, OldValue=old_status,
                         NewValue=choices.get_status(self.Status))
        s.save()
        self.save()

    # Согласование диспетчером
    def approve(self, user):
        old_status = choices.get_status(self.Status)
        self.Status = 3
        # self.approve_date = datetime.datetime.now()
        self.approve_date = timezone.now()
        s = OrderHistory(Order=self, Manager=user, OldValue=old_status,
                         NewValue=choices.get_status(self.Status))
        self.dispatcher_approve = user
        s.save()
        self.save()

    # Отмена согласования диспетчером
    def unapprove(self, user, reason):
        old_status = choices.get_status(self.Status)
        self.Status = 2
        self.unapprove_date = datetime.datetime.now()
        self.unapprove_reason = reason
        self.dispatcher_unapprove = user
        s = OrderHistory(Order=self, Manager=user, OldValue=old_status,
                         NewValue=choices.get_status(self.Status))
        s.save()
        self.save()

    # Отмена менеджером
    def cancel(self, user, reason):
        old_status = choices.get_status(self.Status)
        self.Status = 7
        self.cancel_date = datetime.datetime.now()
        self.cancel_reason = reason
        s = OrderHistory(Order=self, Manager=user, OldValue=old_status,
                         NewValue=choices.get_status(self.Status) + ": " + reason)
        s.save()
        self.save()
        self.remove_salary_record()

    # Отказ диспетчером
    def decline(self, user, reason):
        old_status = choices.get_status(self.Status)
        self.Status = 6
        self.decline_date = datetime.datetime.now()
        self.decline_reason = reason
        self.dispatcher_decline = user
        s = OrderHistory(Order=self, Manager=user, OldValue=old_status,
                         NewValue=choices.get_status(self.Status) + ": " + reason)
        s.save()
        self.save()
        self.remove_salary_record()

    # Назначение авто
    def assign_car(self, user, truck_id, trailer_id, driver_id, auto_visit_take_plan, auto_visit_return_plan):
        old_truck = self.truck.number if self.truck is not None else ''
        old_trailer = self.trailer.number if self.trailer is not None else ''
        old_driver = self.driver.FIO if self.driver is not None else ''
        self.Status = 4
        self.last_driver_action = 1
        year = datetime.datetime.now().strftime('%Y')
        if auto_visit_take_plan != '':
            self.auto_visit_take_plan = datetime.datetime.strptime(year + '.' + auto_visit_take_plan,
                                                                   '%Y.%d.%m %H:%M').strftime('%Y-%m-%d %H:%M:%S')
        if auto_visit_return_plan != '':
            self.auto_visit_return_plan = datetime.datetime.strptime(year + '.' + auto_visit_return_plan,
                                                                     '%Y.%d.%m %H:%M').strftime('%Y-%m-%d %H:%M:%S')
        self.truck = Truck.objects.get(pk=truck_id)
        self.trailer = Trailer.objects.get(pk=trailer_id)
        self.driver = Driver.objects.get(pk=driver_id)
        self.dispatcher_assign = user
        s = OrderHistory(Order=self, Manager=user,
                         OldValue=old_truck + ', ' + old_trailer + ', ' + old_driver,
                         NewValue=self.truck.number + ', ' + self.trailer.number + ', ' + self.driver.FIO)
        s.save()
        self.save()

        # если водитель снчала был на ремонте, а затем вышел на работу  - удаляем запись о простое
        Salary.objects.filter(employee=self.driver, fulldate=self.ExportDate, value__in=['Б', 'О', 'Р', 'М']).delete()

        # проставляем статус для приложения
        try:
            OrderDriverAction.objects.create(order=self, action=1)
        except Exception as e:
            log.error(e, exc_info=True)

        # отправим push
        self.app_notify('Назначена новая заявка, войдите в приложение')

    # Снятие авто
    def un_assign_car(self, user, reason):
        old_truck = self.truck.number if self.truck is not None else ''
        old_trailer = self.trailer.number if self.trailer is not None else ''
        old_driver = self.driver.FIO if self.driver is not None else ''
        self.Status = 3
        self.truck = None
        self.trailer = None
        self.driver = None
        self.dispatcher_unassign = user
        s = OrderHistory(Order=self, Manager=user,
                         OldValue=old_truck + ', ' + old_trailer + ', ' + old_driver,
                         NewValue='')
        s.save()
        self.save()

        # Удаляем статусы приложения
        try:
            OrderDriverAction.objects.filter(order=self).delete()
        except Exception as e:
            log.error(e, exc_info=True)

        # отправим push
        self.app_notify('Заявка отозвана, просмотрите свои заявки в приложении')

    # Завершена
    def close(self, user):
        old_status = choices.get_status(self.Status)
        self.Status = 5
        s = OrderHistory(Order=self, Manager=user, OldValue=old_status,
                         NewValue=choices.get_status(self.Status))
        s.save()
        self.save()
        self.save_salary_record()

    def save_salary_record(self):

        # пропускаем тестовые заявки
        if 'no_salary' in self.Comment:
            return

        # postgres возвращает в UTC, надо преобразовывать в Asia/Novosibirsk
        export_date = self.ExportDate.astimezone(TIME_ZONE)

        d = export_date.day
        m = export_date.month
        y = export_date.year

        # если такая заявка уже посчитана для табеля, выходим
        # это необходимо для исключения дублей
        is_exists = Salary.objects.filter(year=y, month=m, comment__contains='{}'.format(self.pk))

        if is_exists.count() > 0:
            return

        salary_rec = Salary.objects.filter(employee=self.driver, day=d, month=m, year=y)
        if salary_rec.count() > 0:
            try:
                c = salary_rec[0].comment or ''
                new_comment = c + str(self.pk) + ','
                new_value = int(salary_rec[0].value) + 1
            except Exception:
                self.log_error("error cast salary as int")
                new_value = 1
                new_comment = ''

            salary_rec.update(comment=new_comment, value=new_value)

        else:
            salary = Salary(employee=self.driver, value='1', fulldate=self.ExportDate, day=d, month=m, year=y,
                            comment=',' + str(self.pk) + ',')
            salary.save()

    def remove_salary_record(self):
        """
        Удаление записи из табеля
        Необходимо при отмене, или отзыве заявки
        :return: nothing
        """

        m = self.ExportDate.month
        y = self.ExportDate.year
        salary_rec = Salary.objects.filter(year=y, month=m, comment__contains=',{},'.format(self.pk))

        # если в табеле нет записей, выходим
        if salary_rec.count() == 0:
            return

        # если для водителя это единственная запись в табеле, можно ее удалить совсем
        if salary_rec.first().value == '1':
            salary_rec.delete()
            return

        # если рейсов для водителя в день было больше, удаляем запись только по текущей заявке
        c = salary_rec.first().comment or ''
        new_comment = c.replace(',{},'.format(self.pk), ',').replace(',,', ',')
        salary_rec.update(comment=new_comment)
        salary_rec.update(value=F('value') - 1)

    def log_exp(self, mess, params, user):
        error_param = ";".join([str(q) for q in params.values()])
        log.info(mess + "{0}".format(error_param))
        s = CalcHistory(Order=self, Manager=user, OldValue=mess, NewValue=error_param)
        s.save()

    def log_error(self, error, params=None):
        if self.pk is None:
            order = Order.objects.get(pk=1000)
            params['mode'] = "calculator"
        else:
            order = self

        params_list = ";".join([str(q) for q in params.values()]) if params else None
        s = CalcLog(order=order, error=error, params=params_list)
        s.save()

    @staticmethod
    def log_exp_no_save(mess, params, user):
        error_param = ";".join([str(q) for q in params.values()])
        log.info(mess + "{0}".format(error_param))

    def set_client_rates(self, **x):
        # Устанвка клиенстких ставок из модуля тарифов
        self.client_transfer = x['client_transfer']
        self.client_idle_price = x['client_idle_price']
        self.client_idle_cost = x['client_idle_cost']
        self.client_rate = x['client_rate']
        self.platon = x['platon']

        self.salary_idle_price = x['salary_idle_price']
        self.salary_idle_cost = x['salary_idle_cost']
        self.salary_transfer = x['salary_transfer']
        self.salary_rate = x['salary_rate']

        self.client_total = x['client_total']
        self.salary_total = x['salary_total']
        self.platon = x['platon']

        if x['save_to_db']:
            self.save()

    def calc_profit(self, user, fix):

        # Ищем "тяжелое плечо"
        # Operation: 1, 'погр.' - по терминалу сдачи
        # Operation: 2, 'выгр.' - по терминалу забора

        dz = self.TerminalDeliver.dz if self.Operation == 1 else self.TerminalLoad.dz

        params = dict(dz=dz,
                      to_point=self.District,
                      foot=self.Foot)

        w = 20 if self.Weight is None else self.Weight

        # на всякий случай сохраняем
        self.client_rate = 0
        self.client_transfer = 0
        self.client_idle_cost = 0
        self.salary_total = 0
        self.client_total = 0
        self.profit = 0
        self.save()

        ####################################################################
        # Set client prices
        ####################################################################
        if Price.objects.filter(**params).exists():
            params['client'] = self.Client
            #  Если есть инд. тариф берем его, если нет - ищем тариф без указания клиента
            if Price.objects.filter(**params).count() < 0:
                params['client'] = None

            # find base client_rate
            try:
                finded_tariff = Price.objects.get(**params).price
            except Exception:
                self.log_exp("Не найден основной тариф по параметрам:", params, user)
                return

            base_price = 0 if finded_tariff is None else finded_tariff
        else:
            self.log_exp("{0}: Не найден основной тариф по параметрам:".format(self.Number), params, user)
            return

        # find additional client_rate
        del params['to_point']

        try:
            if ClientAdditionalPrice.objects.filter(**params).count() == 0:
                params['client'] = None

            client_add_price_row = ClientAdditionalPrice.objects.get(**params)
        except Exception:
            self.log_exp("{0}: Не найдены коэффициены клиента по параметрам:".format(self.Number), params, user)
            return

        # Try to find Empty coef
        if self.Operation == choices.OPERATION_CHOICES[2][0]:
            w_coef = client_add_price_row.empty_coef

            # if container is not empty, define weight coef
        else:
            if w > 35000:
                w_coef = client_add_price_row.weight_35_coef
            elif w > 30000:
                w_coef = client_add_price_row.weight_30_coef
            elif w > 26000:
                w_coef = client_add_price_row.weight_26_coef
            elif w > 24000:
                w_coef = client_add_price_row.weight_24_coef
            elif w > 22000:
                w_coef = client_add_price_row.weight_22_coef
            else:
                w_coef = 1

            # if can't to find any coefficents set it to 1

        norm_w_coef = w_coef or 1
        # Set client_rate
        self.client_rate = base_price * norm_w_coef

        ####################################################################
        # Set client transfer price
        ####################################################################
        if self.RaywayCr and client_add_price_row:
            self.client_transfer = client_add_price_row.price_one_coast

        if self.RaywayCr2 and client_add_price_row:
            self.client_transfer = client_add_price_row.price_other_coast

        ####################################################################
        # Set client idle
        # (repeatition, but more readable)
        ####################################################################
        self.client_idle_price = client_add_price_row.price_idle if client_add_price_row else 0
        self.client_idle_cost = self.client_idle_price * self.client_idle_minutes

        ####################################################################
        # Set salary
        ####################################################################
        driver_filter = dict(dz=self.dz,
                             foot=self.Foot,
                             to_point=self.District,
                             dt='O')
        # c 1/10/17 - все зарплаты как у наших)
        # dt=Driver.objects.get(pk=self.driver_id).DriverType)
        try:
            # Select price values
            base_salary_row = DriverDirectionPrice.objects.get(**driver_filter)
        except Exception:
            self.log_exp("{0}: Не найдено ЗП по параметрам:".format(self.Number), driver_filter, user)
            return
        try:
            del driver_filter['to_point']
            add_salary_row = DriverAdditionalPrice.objects.get(**driver_filter)
        except Exception:
            self.log_exp("{0}: Не найдены коэффициены водителя по параметрам:".format(self.Number), params, user)
            return

        # Empty coef
        if self.Operation == choices.OPERATION_CHOICES[2][0]:
            w_coef = add_salary_row.empty_coef
        else:
            if w > 30500:
                w_coef = add_salary_row.weight_305_coef
            elif w > 24500:
                w_coef = add_salary_row.weight_245_coef
            else:
                w_coef = 1

        # Set base salary
        self.salary_rate = base_salary_row.price * w_coef if base_salary_row.price else 0

        ####################################################################
        # Set salary transfer price
        # (repeatition, but more readable)
        ####################################################################
        # set default
        st = 0
        if self.RaywayCr:
            st = add_salary_row.price_one_coast

        if self.RaywayCr2:
            st = add_salary_row.price_other_coast
        # Set salary transfer price
        self.salary_transfer = st

        ####################################################################
        # Set salary idle
        # (repeatition, but more readable)
        ####################################################################
        self.salary_idle_price = add_salary_row.price_idle
        self.salary_idle_cost = self.salary_idle_price * self.salary_idle_minutes

        ####################################################################
        # Set total values
        # (repeatition, but more readable)
        ####################################################################
        # Salary
        self.salary_total = self.salary_rate + self.salary_transfer + self.salary_idle_cost
        # Client
        self.client_total = self.client_rate + self.client_transfer + self.client_idle_cost
        # Profit
        self.profit = self.client_total - self.salary_total - self.additional_wastes - self.platon

        if fix:
            # Move to fix method
            pass
            # old_status = choices.get_status(self.Status)
            # self.Status = 8
            # s = OrderHistory(Order=self, Manager=user, OldValue=old_status,
            # NewValue=choices.get_status(self.Status))
            # s.save()
        self.save()

    def calc_profit_old(self, user, fix):
        params = dict(dz=self.dz,
                      to_point=self.District,
                      foot=self.Foot)
        if Price.objects.filter(**params).exists():
            params['client'] = self.Client
            #  Если есть инд. тариф берем его, если нет - ищем тариф без указания клиента
            if Price.objects.filter(**params).count() > 0:
                price_row = Price.objects.get(**params)
            else:
                params['client'] = None
                price_row = Price.objects.get(**params)

            base_price = price_row.price
            coef = 1
            qs = AdditonalPrice.objects.filter(service='wc').order_by('weight')
            # Defaut weigth if none
            w = 20 if self.Weight is None else self.Weight
            # w = self.Weight
            for c in qs.values():
                if w > int(c['weight']):
                    coef = c['value']

            self.client_rate = base_price * coef

            # salary_rate - ставка з\п
            if self.driver.DriverType == 'P':
                self.salary_rate = price_row.salary_person if price_row.salary_person else 0
            elif self.driver.DriverType == 'F':
                self.salary_rate = price_row.salary_franchise if price_row.salary_franchise else 0
            else:
                self.salary_rate = price_row.salary_own

        if self.RaywayCr and AdditonalPrice.objects.filter(service__contains='cr1').count() == 2:
            cl_row = AdditonalPrice.objects.get(service='cr1_client')
            sal_row = AdditonalPrice.objects.get(service='cr1_sal')
            self.client_transfer = cl_row.value
            self.salary_transfer = sal_row.value

        if self.RaywayCr2 and AdditonalPrice.objects.filter(service__contains='cr2').count() == 2:
            cl_row = AdditonalPrice.objects.get(service='cr2_client')
            sal_row = AdditonalPrice.objects.get(service='cr2_sal')
            self.client_transfer = cl_row.value
            self.salary_transfer = sal_row.value

        sic = self.salary_idle_cost if self.salary_idle_cost else 0
        st = self.salary_transfer if self.salary_transfer else 0
        so = self.salary_rate if self.salary_rate else 0

        self.salary_total = so + st + sic
        self.client_total = self.client_rate + self.client_transfer + self.client_idle_cost
        self.profit = self.client_total - self.salary_total - self.additional_wastes - self.platon
        # self.salary_total = self.salary_rate + self.salary_transfer + self.salary_idle_cost

        if fix:
            # move to fix method
            pass
        self.save()

    @staticmethod
    def get_status(obj):
        return obj.get_status_display()

    def fix(self, user, Status, add_services):

        # Calc add service
        add_salary = 0
        add_client = 0
        if len(add_services) > 0:
            for i in add_services:
                add_salary += float(i.get('salary_total', 0))
                add_client += float(i.get('client_total', 0))

        # Check for None
        salary_idle_price = self.salary_idle_price or 0
        salary_idle_minutes = self.salary_idle_minutes or 0

        client_idle_price = self.client_idle_price or 0
        client_idle_minutes = self.client_idle_minutes or 0

        salary_transfer = self.salary_transfer or 0
        salary_rate = self.salary_rate or 0

        client_rate = self.client_rate or 0
        client_transfer = self.client_transfer or 0

        additional_wastes = self.additional_wastes or 0
        platon = self.platon or 0

        # idle
        self.salary_idle_cost = salary_idle_price * salary_idle_minutes
        self.client_idle_cost = client_idle_price * client_idle_minutes

        # Salary
        self.salary_total = salary_rate + salary_transfer + self.salary_idle_cost + add_salary
        # Client
        self.client_total = client_rate + client_transfer + self.client_idle_cost + add_client
        # Profit
        self.profit = self.client_total - self.salary_total - additional_wastes - platon

        old_status = choices.get_status(self.Status)
        self.Status = Status
        s = OrderHistory(Order=self, Manager=user, OldValue=old_status,
                         NewValue=choices.get_status(self.Status))
        s.save()
        self.save()

    def __str__(self):
        return str(self.Number) + '_' + str(self.pk)


class OrderTemplate(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Шаблоны заявок'

    order = models.ForeignKey(Order, blank=True, null=True, on_delete=PROTECT)
    name = models.CharField(max_length=300, blank=True, null=True)
    create_by = models.ForeignKey(User, blank=True, null=True, on_delete=PROTECT)
    create_time = models.DateTimeField(auto_now_add=True)
    client = models.ForeignKey(Client, blank=True, null=True, on_delete=PROTECT)


class OrderHistory(models.Model):
    objects = models.Manager()

    Order = models.ForeignKey('Order', blank=True, null=True, on_delete=PROTECT)
    Manager = models.ForeignKey(User, blank=True, null=True, on_delete=PROTECT)
    OldValue = models.CharField(max_length=300, blank=True)
    NewValue = models.CharField(max_length=300)
    Actiontime = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'История заявок'


class CalcHistory(models.Model):
    objects = models.Manager()

    Order = models.ForeignKey('Order', on_delete=PROTECT)
    Manager = models.ForeignKey(User, on_delete=PROTECT)
    OldValue = models.CharField(max_length=300, blank=True)
    NewValue = models.CharField(max_length=300)
    Actiontime = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Журнал расчета стоимости'


class CalcLog(models.Model):
    objects = models.Manager()

    order = models.ForeignKey('Order', on_delete=PROTECT)
    params = models.CharField(max_length=300, blank=True, null=True)
    error = models.CharField(max_length=300, blank=True, null=True)
    action_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Ошибки расчета тарифа'


class Option(models.Model):
    """docstring for Options."""
    objects = models.Manager()

    desc = models.CharField(max_length=100, blank=True, null=True)
    value = models.CharField(max_length=100, blank=True, null=True)
    code = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Настройки'


class Direction(models.Model):
    """Районы назначения (город, пригород, межгород)"""
    objects = models.Manager()

    Name = models.CharField(max_length=100, verbose_name='Название')
    Type = models.CharField(choices=choices.DIRECTION_CHOICES, max_length=100, verbose_name='Тип')
    coast = models.IntegerField(choices=choices.COAST_CHOICES, verbose_name='Берег', blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Справочник направлений'

    def __str__(self):
        return self.Name


class TerminalGroup(models.Model):
    """"""
    objects = models.Manager()

    name = models.CharField(max_length=100, verbose_name='Наименование')

    class Meta:
        verbose_name_plural = 'Группы терминалов'

    def __str__(self):
        terminal_list = [str(i) for i in Terminal.objects.filter(terminal_group=self)]
        return ",".join(terminal_list)


class Terminal(models.Model):
    """Районы назначения (город, пригород, межгород)"""
    objects = models.Manager()

    Name = models.CharField(max_length=100, verbose_name='Наименование')
    Direction = models.ForeignKey('Direction', verbose_name='Район', on_delete=PROTECT)
    Address = models.CharField(blank=True, null=True, max_length=200, verbose_name='Адрес')
    dz = models.ForeignKey('DispZone', blank=True, null=True, verbose_name='Зона диспетчеризации', on_delete=PROTECT)
    coast = models.IntegerField(choices=choices.COAST_CHOICES, null=True, verbose_name='Берег')
    terminal_group = models.ForeignKey(TerminalGroup, verbose_name="тарифная группа",
                                       blank=True, null=True, on_delete=PROTECT)

    class Meta:
        verbose_name_plural = 'Справочник терминалов'

    def __str__(self):
        return self.Name


class RaywayTariffRules(models.Model):
    objects = models.Manager()

    """Районы назначения (город, пригород, межгород)"""
    oper = models.IntegerField(default=1, choices=choices.OPERATION_CHOICES, verbose_name='Операция')
    terminal_load_group = models.ForeignKey(TerminalGroup, verbose_name='Забор',
                                            related_name="TerminalLoadGroup", null=True, on_delete=PROTECT)
    address_coast = models.IntegerField(default=1, choices=choices.COAST_CHOICES, verbose_name='Адрес')
    rayway_coast = models.IntegerField(default=1, choices=choices.COAST_CHOICES, verbose_name='Переезд')
    terminal_deliver_group = models.ForeignKey(TerminalGroup, verbose_name='Сдача',
                                               related_name="TerminalDeliverGroup", null=True, on_delete=PROTECT)
    rule = models.IntegerField(choices=choices.RAYWAY_TARIFF, null=True, verbose_name='правило')

    class Meta:
        verbose_name_plural = 'Тарифы переезда'

    def __str__(self):
        description = [self.terminal_deliver_group, self.address_coast, self.rayway_coast, self.terminal_deliver_group]
        return "Забор:{0} Адрес:{1} Переезд:{2} Сдача:{3}".format(*description)


class Price(models.Model):
    """ Справочник тарифов для расчет цены исполнения заказа в
        зависимости от пункта назначения и дополнительных услуг
         -- Поля для хранения истории --
        CreateDate - дата создания записи
        DateFrom - срок действия тарифа с даты
        DateTo - срок действия тарифа до, по умолчанию - бессрочно

    """
    objects = models.Manager()

    createDate = models.DateTimeField(auto_now=True)
    dateFrom = models.DateField(blank=False, null=False)
    dateTo = models.DateField(blank=True, null=True, verbose_name='Срок действия')
    ####################################
    # Терминал отправки
    # Foot
    client = models.ForeignKey(Client, blank=True, null=True, verbose_name='Клиент', on_delete=PROTECT)
    foot = models.IntegerField(choices=choices.FOOT_CHOICES, null=True, verbose_name='Фут')
    from_point = models.ForeignKey('Terminal', blank=True, null=True, editable=False, on_delete=PROTECT)
    dz = models.ForeignKey('DispZone', blank=True, null=True, verbose_name='Зона диспетчеризация', on_delete=PROTECT)
    # Пункт назначения
    to_point = models.CharField(blank=True, null=True, max_length=200, verbose_name='Район')
    # Стоимость за 20т контейнер
    price = models.FloatField(blank=True, null=True, verbose_name='Ставка')
    # З\П собственному водителю
    salary_own = models.FloatField(blank=True, null=True, verbose_name='з/п наш водитель')
    # З\П полу - частнику
    salary_franchise = models.FloatField(blank=True, null=True, verbose_name='з/п получастник')
    # З\П частнику
    salary_person = models.FloatField(blank=True, null=True, verbose_name='з/п частник')
    # Переезд через мост 1
    # Стоимость межгорода
    region_coef = models.FloatField(blank=True, null=True, verbose_name='межгород')
    # Стоимость минуты простоя
    wait_tarif = models.IntegerField(blank=True, null=True, verbose_name='простой')

    # сборы по системе Платон
    platon = models.FloatField(blank=True, null=True, verbose_name='Платон')

    class Meta:
        verbose_name_plural = 'Тарифы'


class DispZone(models.Model):
    objects = models.Manager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Зоны диспетчеризации'

    code = models.CharField(blank=True, null=True, max_length=1)
    name = models.CharField(blank=True, null=True, max_length=20)


class AdditonalPrice(models.Model):
    """
        Справочник дополнителнительных статей расчета для заявки
    """
    objects = models.Manager()
    # Код услуги для поиска в прайсах
    service_code = models.CharField(blank=True, null=True, max_length=100)
    # Название услуги
    service_name = models.CharField(blank=True, null=True, max_length=300)
    # Название услуги
    service_type = models.CharField(max_length=2, choices=choices.ADD_SERVICE_TYPES, null=True)
    # foot
    foot = models.IntegerField(choices=choices.FOOT_CHOICES, null=True)
    # DriverType
    driverType = models.CharField(choices=choices.DRIVER_TYPE_CHOICES, blank=True, null=True, max_length=1)
    # Вес
    weight = models.IntegerField(blank=True, null=True)

    # Значание
    default_value = models.FloatField(blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Расчет доп.услуг'


class Service(models.Model):
    """Справочник услуг"""

    class Meta:
        verbose_name_plural = 'Справочник услуг'

    def __str__(self):
        return self.Description

    Code = models.CharField(max_length=100)
    Description = models.CharField(max_length=300)


class Truck(models.Model):
    """Новый справочник машин
        driver - водитель
        brand - Марка автомобиля
        number - № автомобиля
    """
    objects = models.Manager()

    driver = models.ForeignKey(Driver, on_delete=PROTECT)
    brand = models.CharField(max_length=100)
    number = models.CharField(max_length=100)
    # Зона диспетчеризации
    dz = models.ForeignKey('DispZone', blank=True, null=True, on_delete=PROTECT)
    status = models.CharField(
        max_length=1,
        choices=choices.DRIVER_STATUS,
        default='A')

    class Meta:
        verbose_name_plural = 'Машины'

    def __str__(self):
        if self.status != 'A':
            name = self.brand + " " + self.number + '(Уволена)'
        else:
            name = self.brand + " " + self.number
        return name


class Trailer(models.Model):
    """
    Справочник прицепов
    """
    objects = models.Manager()

    car = models.ForeignKey('Truck', on_delete=PROTECT)
    foot = models.IntegerField(choices=choices.FOOT_CHOICES)
    number = models.CharField(max_length=100)
    status = models.CharField(
        max_length=1,
        choices=choices.DRIVER_STATUS,
        default='A')

    class Meta:
        verbose_name_plural = 'Прицепы'

    def __str__(self):
        if self.status != 'A':
            name = str(self.foot) + ": " + str(self.number) + '(Уволен)'
        else:
            name = str(self.foot) + ": " + str(self.number)
        return name


def current_year():
    return datetime.datetime.now().year


def current_month():
    return datetime.datetime.now().month


def current_day():
    return datetime.datetime.now().day


class Salary(models.Model):
    """ Основная модель для раздела Табель
    emloyee - Сотрудник
    day - число
    month - месяц
    year - год
    value - УРВ
    """
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Табель'

    employee = models.ForeignKey(Driver, on_delete=PROTECT)
    day = models.IntegerField(default=current_day)
    month = models.IntegerField(default=current_month)
    year = models.IntegerField(default=current_year)
    value = models.CharField(max_length=2)
    fulldate = models.DateField(default=datetime.date.today)
    comment = models.CharField(max_length=200, blank=True, null=True)


class UserLocation(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Привязка диспетчеров'

    user = models.ForeignKey(User, blank=False, null=False, verbose_name='Пользователь', on_delete=PROTECT)
    zone = models.ForeignKey('DispZone', verbose_name='Зона диспетчеризации', default=1, on_delete=PROTECT)


class UserActions(models.Model):
    """Справочник пользовательских действий"""
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Справочник действий пользователя'

    name = models.CharField(max_length=50)
    name_plural = models.CharField(max_length=50)
    value = models.CharField(max_length=50)

    def __str__(self):
        return self.value


class UserPermission(models.Model):
    """Справочник для прав пользователя"""
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Права пользователей'

    user = models.ForeignKey(User, blank=False, null=False, on_delete=PROTECT)
    rule = models.ForeignKey('UserActions', on_delete=PROTECT)

    def __str__(self):
        return str(self.user) + ": " + str(self.rule)


class DriverDirectionPrice(models.Model):
    """Справочник тарифов по направлениям для водителей"""
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Справочник тарифов по направлениям для водителей'

    createDate = models.DateTimeField(auto_now=True, verbose_name='Дата создания')
    dateFrom = models.DateField(blank=False, null=False, verbose_name='Дата начала действия')
    dateTo = models.DateField(blank=True, null=True, verbose_name='Срок действия')

    # DriverType
    dt = models.CharField(choices=choices.DRIVER_TYPE_CHOICES, blank=False, null=False, max_length=1,
                          verbose_name='Тип водителя')

    driver = models.ForeignKey(Driver, blank=True, null=True, verbose_name='Водитель', on_delete=PROTECT)
    # foot
    foot = models.IntegerField(choices=choices.FOOT_CHOICES, blank=False, null=False, verbose_name='Фут')
    # Зона диспетчеризации
    dz = models.ForeignKey('DispZone', blank=False, null=False, verbose_name='Зона диспетчеризации', on_delete=PROTECT)
    # Пункт назначения
    to_point = models.CharField(blank=False, null=False, max_length=200, verbose_name='Пункт назначения')
    # Ставка
    price = models.FloatField(blank=False, null=False, verbose_name='Ставка')


class DriverAdditionalPrice(models.Model):
    """Справочник дополнительных коэффициентов для водителей"""
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Справочник дополнительных коэффициентов для водителей'

    createDate = models.DateTimeField(auto_now=True, verbose_name='Дата создания')
    dateFrom = models.DateField(blank=False, null=False, verbose_name='Дата начала действия')
    dateTo = models.DateField(blank=True, null=True, verbose_name='Срок действия')

    # DriverType
    dt = models.CharField(choices=choices.DRIVER_TYPE_CHOICES, blank=False, null=False, max_length=1,
                          verbose_name='Тип водителя')

    driver = models.ForeignKey(Driver, blank=True, null=True, verbose_name='Водитель', on_delete=PROTECT)
    # foot
    foot = models.IntegerField(choices=choices.FOOT_CHOICES, blank=False, null=False, verbose_name='Фут')
    # Зона диспетчеризации
    dz = models.ForeignKey('DispZone', blank=False, null=False, verbose_name='Зона диспетчеризации', on_delete=PROTECT)

    # Ставка переезд по одному берегу
    price_one_coast = models.FloatField(blank=False, null=False, verbose_name='Ставка переезд по одному берегу')
    # Ставка переезд с другого берега
    price_other_coast = models.FloatField(blank=False, null=False, verbose_name='Ставка переезд с другого берега')
    # Время на погр-выгр
    time_to_load = models.FloatField(blank=False, null=False, verbose_name='Время на погр-выгр')
    # Ставка простой руб\мин
    price_idle = models.FloatField(blank=False, null=False, verbose_name='Ставка простой руб/мин')
    # Коэффициент перевозка порожнего
    empty_coef = models.FloatField(blank=False, null=False, verbose_name='Коэффициент перевозка порожнего')
    # Коэффициент вес контейнера более 24,5 т брутто
    weight_245_coef = models.FloatField(blank=True, null=True,
                                        verbose_name='Коэффициент вес контейнера более 24 т брутто')
    weight_280_coef = models.FloatField(blank=True, null=True,
                                        verbose_name='Коэффициент вес контейнера более 28 т брутто')
    # Коэффициент вес контейнера более 30,5 т брутто
    weight_305_coef = models.FloatField(blank=True, null=True,
                                        verbose_name='Коэффициент вес контейнера более 30 т брутто')
    weight_22_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 22 т брутто')
    # Коэффициент вес контейнера более 24 т брутто
    weight_24_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 24 т брутто')
    # Коэффициент вес контейнера более 26 т брутто
    weight_26_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 26 т брутто')
    weight_28_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 28 т брутто')
    # Коэффициент вес контейнера более 30 т брутто
    weight_30_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 30 т брутто')
    # Коэффициент вес контейнера более 35 т брутто
    weight_35_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 35 т брутто')

    # Экспедирование
    forwarding = models.FloatField(blank=True, null=True, verbose_name='Экспедирование')

    # Зачистка
    cleaning = models.FloatField(blank=True, null=True, verbose_name='Зачистка')


class ClientAdditionalPrice(models.Model):
    """Справочник дополнительных коэффициентов для клиентов"""
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Справочник дополнительных коэффициентов для клиентов'

    createDate = models.DateTimeField(auto_now=True, verbose_name='Дата создания')
    dateFrom = models.DateField(blank=False, null=False, verbose_name='Дата начала действия')
    dateTo = models.DateField(blank=True, null=True, verbose_name='Срок действия')

    # Клиент
    client = models.ForeignKey(Client, blank=True, null=True, verbose_name='Клиент', on_delete=PROTECT)
    # foot
    foot = models.IntegerField(choices=choices.FOOT_CHOICES, blank=False, null=False, verbose_name='Фут')
    # Зона диспетчеризации
    dz = models.ForeignKey('DispZone', blank=False, null=False, verbose_name='Зона диспетчеризации', on_delete=PROTECT)

    # Ставка переезд по одному берегу
    price_one_coast = models.FloatField(blank=False, null=False, verbose_name='Ставка переезд по одному берегу')
    # Ставка переезд с другого берега
    price_other_coast = models.FloatField(blank=False, null=False, verbose_name='Ставка переезд с другого берега')
    # Время на погр-выгр
    time_to_load = models.FloatField(blank=False, null=False, verbose_name='Время на погр-выгр')
    # Ставка простой руб\мин
    price_idle = models.FloatField(blank=False, null=False, verbose_name='Ставка простой руб/мин')
    # Коэффициент перевозка порожнего
    empty_coef = models.FloatField(blank=False, null=False, verbose_name='Коэффициент перевозка порожнего')
    # Коэффициент вес контейнера более 22 т брутто
    weight_22_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 22 т брутто')
    # Коэффициент вес контейнера более 24 т брутто
    weight_24_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 24 т брутто')
    # Коэффициент вес контейнера более 26 т брутто
    weight_26_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 26 т брутто')
    weight_28_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 28 т брутто')
    # Коэффициент вес контейнера более 30 т брутто
    weight_30_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 30 т брутто')
    # Коэффициент вес контейнера более 35 т брутто
    weight_35_coef = models.FloatField(blank=True, null=True,
                                       verbose_name='Коэффициент вес контейнера более 35 т брутто')
    # Экспедирование
    forwarding = models.FloatField(blank=True, null=True, verbose_name='Экспедирование')

    # Зачистка
    cleaning = models.FloatField(blank=True, null=True, verbose_name='Зачистка')


class ReportData(models.Model):
    """Справочник дополнительных коэффициентов для клиентов"""
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Данные для отчетов'

    code = models.CharField(max_length=30, verbose_name='Код', null=True)
    name = models.CharField(max_length=100, verbose_name='Параметр')
    value = models.CharField(max_length=100, verbose_name='Значение')

    def __str__(self):
        return self.value


class ClientBill(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Акты сверки'

    created_by = models.ForeignKey(User, blank=True, null=True, verbose_name='Менеджер', on_delete=PROTECT)
    create_date = models.DateTimeField(auto_now_add=True, blank=False, null=False, verbose_name='Дата создания')
    client = models.ForeignKey(Client, blank=True, null=False, verbose_name='Клиент', on_delete=PROTECT)
    number = models.CharField(max_length=100, verbose_name='Номер')
    date_from = models.DateTimeField(blank=True, null=True, verbose_name='Начало периода')
    date_to = models.DateTimeField(blank=True, null=True, verbose_name='Конец периода')
    order_count = models.IntegerField(blank=True, null=True, verbose_name='кол-во заявок в отчете')
    money = models.FloatField(blank=True, null=True, default=0, verbose_name='Сумма')
    status = models.IntegerField(default=1, choices=choices.BILL_STATUS_CHOICES, verbose_name='Статус')
    send_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата отправки')

    def __str__(self):
        return self.client.Name + ' № ' + str(self.number) + ' от ' + self.create_date.strftime('%d.%m.%Y')


class UserTablePermission(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Перечень полей в разделах'

    table_name = models.CharField(max_length=100, verbose_name='Раздел')
    field_name = models.CharField(max_length=100, verbose_name='Код поля')
    verbose_name = models.CharField(max_length=100, null=True, blank=True, verbose_name='Наименование поля')

    def __str__(self):
        name = self.verbose_name or ''
        return self.table_name + ': ' + name


class InfoProfile(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Доступ к полям раздела Справка'

    name = models.CharField(max_length=30, verbose_name='Название правила')
    table_fields = models.ManyToManyField(UserTablePermission, verbose_name='Список полей')

    def __str__(self):
        return self.name


class UserProfile(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Профиль пользователя'

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
        verbose_name='Пользователь')
    clients = models.ManyToManyField(Client, verbose_name='Клиент')
    show_personal_bill = models.IntegerField(default=0, choices=choices.ACTS_CHOICES,
                                             verbose_name='Пользователь видит акты')
    info_profile = models.ForeignKey(InfoProfile, null=True, blank=True, verbose_name='Поля в справке',
                                     on_delete=PROTECT)

    def __str__(self):
        return str(self.user)


class DistrictCoastMapping(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Привязка района к берегу'

    district = models.CharField(max_length=100, verbose_name='Район', null=True)
    coast = models.IntegerField(choices=choices.COAST_CHOICES, null=True, verbose_name='Берег')

    def __str__(self):
        return self.district + choices.get_coast(self.coast)


class OrderAdditionalService(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Доп. услуги по заявке'

    order = models.ForeignKey("Order", verbose_name='заявка', on_delete=PROTECT)
    service_code = models.CharField(max_length=100, choices=choices.ADD_SERVICE_CODES, verbose_name='код услуги',
                                    null=True)
    quantity = models.IntegerField(verbose_name='кол-во')
    salary_price = models.FloatField(verbose_name='зп ставка', null=True, blank=True)
    salary_total = models.FloatField(verbose_name='зп', null=True, blank=True)
    client_price = models.FloatField(verbose_name='ставка клиента', null=True, blank=True)
    client_total = models.FloatField(verbose_name='с клиента итого', null=True, blank=True)
    company_price = models.FloatField(verbose_name='затраты ставка', null=True, blank=True)
    company_total = models.FloatField(verbose_name='затраты итого', null=True, blank=True)

    def __str__(self):
        return str(self.pk) + '_order_' + str(self.order)


class AddDirectionTaxes(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Платон'

    direction = models.CharField(max_length=100, verbose_name='Направление', null=True)
    tax_code = models.CharField(max_length=5, choices=choices.DirectionTaxes, null=False, default='P',
                                verbose_name='наименование платежа')
    value = models.FloatField(verbose_name='ставка', null=True, blank=True)

    def __str__(self):
        return str(self.direction) + ', ставка "Платон": ' + str(self.value)


class Agent(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Агенты'

    name = models.CharField(max_length=100, verbose_name='Название', null=True)
    service_name = models.TextField(verbose_name='Наименование услуги в акте')
    sign_by = models.CharField(max_length=100, verbose_name='Подпись', null=True, blank=True)

    def __str__(self):
        return self.name


class PaymentsReport(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Платежная ведомость'

    month = models.DateField(verbose_name='Расчетный период')
    driver = models.ForeignKey(Driver, verbose_name='Водитель', on_delete=PROTECT)
    truck = models.ManyToManyField(Truck, verbose_name='Машины', null=True, blank=True)
    salary = models.FloatField(null=True, blank=True, verbose_name='Начислено')
    bonus = models.FloatField(null=True, blank=True, verbose_name='Доплаты')
    repair = models.FloatField(null=True, blank=True, verbose_name='Ремонт')
    fuel = models.FloatField(null=True, blank=True, verbose_name='ДТ')
    gazprom = models.FloatField(null=True, blank=True, verbose_name='Газпром')
    simbios = models.FloatField(null=True, blank=True, verbose_name='Симбиоз')
    prepayment = models.FloatField(null=True, blank=True, verbose_name='Аванс')
    card = models.FloatField(null=True, blank=True, verbose_name='Карта')
    tax = models.FloatField(null=True, blank=True, verbose_name='Налог')
    credit = models.FloatField(null=True, blank=True, verbose_name='Кредит')
    garage = models.FloatField(null=True, blank=True, verbose_name='Гараж')
    debt = models.FloatField(null=True, blank=True, verbose_name='Долг')
    summary = models.FloatField(null=True, blank=True, verbose_name='ИТОГО')

    def __str__(self):
        return self.month.strftime('%Y%m') + ':' + str(self.driver) + ': ' + str(self.summary)


def file_size(value):
    limit = 15 * 1024 * 1024
    if value.size > limit:
        raise ValidationError('File too large. Size should not exceed 2 MiB.')


class OrderDocuments(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Документы к заявкам'

    order = models.ForeignKey(Order, verbose_name='Заявка', on_delete=PROTECT)
    name = models.CharField(max_length=200, verbose_name='Наименование документа', null=True, blank=True)
    file = models.FileField(blank=True, null=True, verbose_name='Файл')
    upload_time = models.DateTimeField(auto_now_add=True, verbose_name='Время добавления')
    upload_by = models.ForeignKey(User, verbose_name='Кем добавлен', null=True, blank=True, on_delete=PROTECT)
    is_client_doc = models.BooleanField(verbose_name='Документ клиента', default=False, blank=True)

    def __str__(self):
        return self.name


class OrderDriverAction(models.Model):
    objects = models.Manager()
    order = models.ForeignKey(Order, verbose_name='Заявка', on_delete=PROTECT)
    action = models.IntegerField(blank=True, null=True, choices=choices.ORDER_DRIVER_STATUS,
                                 verbose_name='Действие водителя')
    action_time = models.DateTimeField(auto_now_add=True, verbose_name='Время добавления')
    lat = models.FloatField(blank=True, null=True,
                            verbose_name='широта')
    lng = models.FloatField(blank=True, null=True,
                            verbose_name='долгота')


class RegRequests(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Запросы на регистрацию'

    create_date = models.DateTimeField(auto_now=True, verbose_name='Дата создания')
    phone_number = models.BigIntegerField(verbose_name='Номер телефона')
    confirm_code = models.IntegerField(verbose_name='Код подтверждения')
    valid_to = models.DateTimeField(verbose_name='Дата окончания срока кода')
    confirm_status = models.IntegerField(choices=choices.CONFIRM_CODE_STATUSES, verbose_name='Статус')
    attempts_count = models.IntegerField(verbose_name='Кол-во попыток ввода')

    def __str__(self):
        return self.create_date.strftime('%Y-%m-%d %H:%M:%S') + ' ' + str(self.phone_number)


class Feedback(models.Model):
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Вопросы с сайта'

    create_date = models.DateTimeField(auto_now=True, verbose_name='Дата создания')
    email = models.CharField(max_length=200, verbose_name='Email')
    text = models.CharField(max_length=1000, verbose_name='Сообщение')


def remove_order_from_salary(order):
    # postgres возвращает в UTC, надо преобразовывать в Asia/Novosibirsk
    salary_rec = Salary.objects.filter(comment__contains='{}'.format(order.pk))
    if salary_rec.count() > 0:
        row = salary_rec[0]
        try:
            # Сколько осталось заявок
            orders_count = int(row.value) - 1

            # Если удаляется единственная заявка - удаляем табель
            if orders_count == 0:
                salary_rec.delete()
            else:
                salary_rec.update(comment=row.comment.replace(str(order.pk), ''), value=str(orders_count))

        except Exception as e:
            log.error("can not set new salary when order deleted")
            log.error(e)


def log_order_deleting(order):
    OrderHistory.objects.create(Order=None, Manager=None, OldValue=str(order), NewValue="заявка удалена")


@receiver(post_delete)
def process_order_deleting(sender, **kwargs):
    """ Обрабатываем факт удаления заявки """
    obj = kwargs['instance']

    if isinstance(obj, Order):
        remove_order_from_salary(obj)
        log_order_deleting(obj)


@receiver(models.signals.post_delete, sender=OrderDocuments)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)
